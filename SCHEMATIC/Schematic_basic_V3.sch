<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="10" fill="10" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="11" fill="10" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="12" fill="10" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="13" fill="10" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="14" fill="10" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="15" fill="10" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="16" fill="10" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="LC3">
<packages>
<package name="1_BATTERY">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-7.773" y1="2.383" x2="7.873" y2="2.383" width="0.0508" layer="39"/>
<wire x1="7.873" y1="-2.483" x2="-7.773" y2="-2.483" width="0.0508" layer="39"/>
<wire x1="-7.773" y1="-2.483" x2="-7.773" y2="2.383" width="0.0508" layer="39"/>
<wire x1="7.873" y1="2.383" x2="7.873" y2="-2.483" width="0.0508" layer="39"/>
<wire x1="6.5" y1="2.213" x2="-6.5" y2="2.213" width="0.1016" layer="51"/>
<wire x1="-6.5" y1="-2.213" x2="6.5" y2="-2.213" width="0.1016" layer="51"/>
<smd name="1" x="-4" y="0" dx="4" dy="4" layer="1"/>
<smd name="2" x="4" y="0" dx="4" dy="4" layer="1"/>
<text x="-2.79" y="3.11" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.39" y="-4.4" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-6" y1="-2" x2="-2" y2="2" layer="51"/>
<rectangle x1="2" y1="-2" x2="6" y2="2" layer="51"/>
<text x="-7.62" y="-2.54" size="1.27" layer="21" rot="R90">BAT_P</text>
</package>
</packages>
<symbols>
<symbol name="1_BATTERY">
<text x="-16.51" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="19.05" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-27.94" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="38.1" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="-15.24" y1="35.56" x2="15.24" y2="35.56" width="0.254" layer="94"/>
<wire x1="15.24" y1="35.56" x2="15.24" y2="-25.4" width="0.254" layer="94"/>
<wire x1="15.24" y1="-25.4" x2="-15.24" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-25.4" x2="-15.24" y2="35.56" width="0.254" layer="94"/>
<text x="-2.54" y="30.48" size="1.27" layer="94">POS</text>
<text x="-2.54" y="-22.86" size="1.27" layer="94">NEG</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BATTERY">
<gates>
<gate name="G$1" symbol="1_BATTERY" x="0" y="-7.62"/>
</gates>
<devices>
<device name="" package="1_BATTERY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;map name="nav_main"&gt;
&lt;area shape="rect" coords="0,1,140,23" href="../military_specs.asp" title=""&gt;
&lt;area shape="rect" coords="0,24,140,51" href="../about.asp" title=""&gt;
&lt;area shape="rect" coords="1,52,140,77" href="../rfq.asp" title=""&gt;
&lt;area shape="rect" coords="0,78,139,103" href="../products.asp" title=""&gt;
&lt;area shape="rect" coords="1,102,138,128" href="../excess_inventory.asp" title=""&gt;
&lt;area shape="rect" coords="1,129,138,150" href="../edge.asp" title=""&gt;
&lt;area shape="rect" coords="1,151,139,178" href="../industry_links.asp" title=""&gt;
&lt;area shape="rect" coords="0,179,139,201" href="../comments.asp" title=""&gt;
&lt;area shape="rect" coords="1,203,138,231" href="../directory.asp" title=""&gt;
&lt;area shape="default" nohref&gt;
&lt;/map&gt;

&lt;html&gt;

&lt;title&gt;&lt;/title&gt;

 &lt;LINK REL="StyleSheet" TYPE="text/css" HREF="style-sheet.css"&gt;

&lt;body bgcolor="#ffffff" text="#000000" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0"&gt;
&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0 height="55%"&gt;
&lt;tr valign="top"&gt;

&lt;/td&gt;
&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/BODY&gt;&lt;/HTML&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="R01005">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="C0201">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
<package name="C01005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="HPC0201">
<description>&lt;b&gt; &lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<smd name="1" x="-0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<smd name="2" x="0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<text x="-0.75" y="0.74" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.785" y="-1.865" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.305" y1="-0.15" x2="0.305" y2="0.15" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-EU-1">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-US_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU-1" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HPC0201" package="HPC0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="C01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ICs">
<packages>
<package name="VQFN_RGE_24">
<smd name="PAD_GND" x="0" y="0" dx="2.6924" dy="2.6924" layer="1"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="3.25" width="0.1524" layer="25"/>
<wire x1="3.25" y1="-3.25" x2="3.25" y2="3.25" width="0.1524" layer="25"/>
<wire x1="-3.25" y1="3.25" x2="3.25" y2="3.25" width="0.1524" layer="25"/>
<wire x1="-3.25" y1="-3.25" x2="3.25" y2="-3.25" width="0.1524" layer="25"/>
<wire x1="-2.5954" y1="1.9192" x2="-3.0018" y2="1.9192" width="0.1524" layer="25" curve="-180"/>
<wire x1="-3.0018" y1="1.9192" x2="-2.5954" y2="1.9192" width="0.1524" layer="25" curve="-180"/>
<text x="-2.9542" y="-6.143" size="1.27" layer="25" ratio="6" rot="SR0">.Designator</text>
<text x="-2.8702" y="-4.735" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<wire x1="-3.2352" y1="-3.2352" x2="-2.9066" y2="-3.2352" width="0.2032" layer="51"/>
<wire x1="-3.2352" y1="-3.2352" x2="-3.2352" y2="-2.9066" width="0.2032" layer="51"/>
<wire x1="3.2352" y1="-3.2352" x2="3.2352" y2="-2.9066" width="0.2032" layer="51"/>
<wire x1="2.9066" y1="-3.2352" x2="3.2352" y2="-3.2352" width="0.2032" layer="51"/>
<wire x1="2.9066" y1="3.2352" x2="3.2352" y2="3.2352" width="0.2032" layer="51"/>
<wire x1="3.2352" y1="2.9066" x2="3.2352" y2="3.2352" width="0.2032" layer="51"/>
<text x="-3.1544" y="3.665" size="1.27" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
<smd name="6" x="-1.25" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="5" x="-0.75" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<wire x1="-3.2352" y1="3.2352" x2="-3.2352" y2="2.9066" width="0.2032" layer="51"/>
<wire x1="-2.8066" y1="3.2352" x2="-3.2352" y2="3.2352" width="0.2032" layer="51"/>
<smd name="1" x="1.25" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="2" x="0.75" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="3" x="0.25" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="4" x="-0.25" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="18" x="1.25" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="15" x="-0.25" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="17" x="0.75" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="16" x="0.25" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="14" x="-0.75" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="13" x="-1.25" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="19" x="2.65" y="-1.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="20" x="2.65" y="-0.75" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="21" x="2.65" y="-0.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="22" x="2.65" y="0.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="23" x="2.65" y="0.75" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="24" x="2.65" y="1.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="10" x="-2.65" y="-0.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="9" x="-2.65" y="0.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="8" x="-2.65" y="0.75" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="11" x="-2.65" y="-0.75" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="7" x="-2.65" y="1.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="12" x="-2.65" y="-1.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<pad name="PAD_VIA3" x="-0.6731" y="-0.6731" drill="0.254"/>
<pad name="PAD_VIA1" x="-0.6731" y="0.6731" drill="0.254"/>
<pad name="PAD_VIA2" x="0.6731" y="0.6731" drill="0.254"/>
<pad name="PAD_VIA4" x="0.6731" y="-0.6731" drill="0.254"/>
</package>
<package name="PW20">
<smd name="1" x="-2.8194" y="2.9250125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="2" x="-2.8194" y="2.275003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="3" x="-2.8194" y="1.624990625" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="4" x="-2.8194" y="0.975003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="5" x="-2.8194" y="0.32499375" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="6" x="-2.8194" y="-0.32499375" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="7" x="-2.8194" y="-0.975003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="8" x="-2.8194" y="-1.624990625" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="9" x="-2.8194" y="-2.275003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="10" x="-2.8194" y="-2.9249875" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="11" x="2.8194" y="-2.9250125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="12" x="2.8194" y="-2.275003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="13" x="2.8194" y="-1.624990625" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="14" x="2.8194" y="-0.975003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="15" x="2.8194" y="-0.32499375" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="16" x="2.8194" y="0.32499375" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="17" x="2.8194" y="0.975003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="18" x="2.8194" y="1.624990625" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="19" x="2.8194" y="2.275003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="20" x="2.8194" y="2.9249875" dx="1.6764" dy="0.3556" layer="1"/>
<wire x1="-2.2352" y1="-3.302" x2="2.2352" y2="-3.302" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="3.302" x2="0.3048" y2="3.302" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="3.302" x2="-2.2352" y2="3.302" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="3.302" x2="-0.3048" y2="3.302" width="0.127" layer="21" curve="-180"/>
<text x="-3.6576" y="3.1496" size="0.508" layer="21" ratio="6" rot="SR90">*</text>
<text x="-1.778" y="4.1402" size="0.508" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-1.8542" y="3.5052" size="0.508" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
</package>
<package name="QFN_MOG_35">
<wire x1="-3.6" y1="-3.6" x2="-3.6" y2="3.6" width="0.1524" layer="25"/>
<wire x1="10.7" y1="-3.6" x2="10.7" y2="3.6" width="0.1524" layer="25"/>
<wire x1="-3.6" y1="3.6" x2="10.7" y2="3.6" width="0.1524" layer="25"/>
<wire x1="-3.6" y1="-3.6" x2="10.7" y2="-3.6" width="0.1524" layer="25"/>
<text x="-3.1345375" y="5.901678125" size="0.8" layer="25" ratio="6" rot="SR0">.Designator</text>
<text x="-3.1369" y="4.858578125" size="0.8" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-3.131540625" y="3.946940625" size="0.8" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
<smd name="1" x="-3.05" y="2.25" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<pad name="PAD_VIA_25" x="-1.6" y="0" drill="0.254" diameter="0.254"/>
<text x="-3.533140625" y="1.996440625" size="0.7" layer="21" rot="R90">*</text>
<smd name="25" x="-1.6" y="0" dx="1" dy="1" layer="1" rot="R180"/>
<smd name="26" x="0" y="-1.6" dx="1" dy="1" layer="1" rot="R180"/>
<smd name="27" x="0" y="0" dx="1" dy="1" layer="1" rot="R180"/>
<smd name="28" x="0" y="1.6" dx="1" dy="1" layer="1" rot="R180"/>
<smd name="29" x="1.6" y="0" dx="1" dy="1" layer="1" rot="R180"/>
<smd name="2" x="-3.05" y="1.35" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="3" x="-3.05" y="0.45" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="4" x="-3.05" y="-0.45" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="5" x="-3.05" y="-1.35" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="6" x="-3.05" y="-2.25" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="31" x="-3.05" y="-3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="7" x="-2.25" y="-3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="8" x="-1.35" y="-3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="9" x="-0.45" y="-3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="10" x="0.45" y="-3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="11" x="1.35" y="-3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="12" x="2.25" y="-3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="32" x="3.05" y="-3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="13" x="3.05" y="-2.25" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="14" x="3.05" y="-1.35" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="15" x="3.05" y="-0.45" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="16" x="3.05" y="0.45" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="17" x="3.05" y="1.35" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="18" x="3.05" y="2.25" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="33" x="3.05" y="3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="19" x="2.25" y="3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="20" x="1.35" y="3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="21" x="0.45" y="3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="22" x="-0.45" y="3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="23" x="-1.35" y="3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="24" x="-2.25" y="3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="30" x="-3.05" y="3.05" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="34" x="7.925" y="-3.135" dx="4.85" dy="0.4" layer="1" rot="R180"/>
<smd name="35" x="7.925" y="3.15" dx="4.85" dy="0.4" layer="1" rot="R180"/>
<pad name="PAD_VIA_26" x="0" y="-1.6" drill="0.254" diameter="0.254"/>
<pad name="PAD_VIA_27" x="0" y="0" drill="0.254" diameter="0.254"/>
<pad name="PAD_VIA_28" x="0" y="1.6" drill="0.254" diameter="0.254"/>
<pad name="PAD_VIA_29" x="1.6" y="0" drill="0.254" diameter="0.254"/>
</package>
<package name="QFN.24.4X4__MPU-6050">
<wire x1="-2.55" y1="2.55" x2="2.55" y2="2.55" width="0.1" layer="21"/>
<wire x1="2.55" y1="2.55" x2="2.55" y2="-2.55" width="0.1" layer="21"/>
<wire x1="2.55" y1="-2.55" x2="-2.55" y2="-2.55" width="0.1" layer="21"/>
<wire x1="-2.55" y1="-2.55" x2="-2.55" y2="2.55" width="0.1" layer="21"/>
<smd name="1" x="-1.975" y="1.25" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="-1.44" x2="-1.55" y2="-1.06" layer="29"/>
<rectangle x1="-2.4" y1="-1.44" x2="-1.55" y2="-1.06" layer="31"/>
<smd name="2" x="-1.975" y="0.75" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="-0.94" x2="-1.55" y2="-0.56" layer="29"/>
<rectangle x1="-2.4" y1="-0.94" x2="-1.55" y2="-0.56" layer="31"/>
<smd name="3" x="-1.975" y="0.25" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="-0.44" x2="-1.55" y2="-0.06" layer="29"/>
<rectangle x1="-2.4" y1="-0.44" x2="-1.55" y2="-0.06" layer="31"/>
<smd name="4" x="-1.975" y="-0.25" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="0.06" x2="-1.55" y2="0.44" layer="29"/>
<rectangle x1="-2.4" y1="0.06" x2="-1.55" y2="0.44" layer="31"/>
<smd name="5" x="-1.975" y="-0.75" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="0.56" x2="-1.55" y2="0.94" layer="29"/>
<rectangle x1="-2.4" y1="0.56" x2="-1.55" y2="0.94" layer="31"/>
<smd name="6" x="-1.975" y="-1.25" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="1.06" x2="-1.55" y2="1.44" layer="29"/>
<rectangle x1="-2.4" y1="1.06" x2="-1.55" y2="1.44" layer="31"/>
<smd name="7" x="-1.25" y="-1.975" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-1.44" y1="1.55" x2="-1.06" y2="2.4" layer="29"/>
<rectangle x1="-1.44" y1="1.55" x2="-1.06" y2="2.4" layer="31"/>
<smd name="8" x="-0.75" y="-1.975" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-0.94" y1="1.55" x2="-0.56" y2="2.4" layer="29"/>
<rectangle x1="-0.94" y1="1.55" x2="-0.56" y2="2.4" layer="31"/>
<smd name="9" x="-0.25" y="-1.975" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-0.44" y1="1.55" x2="-0.06" y2="2.4" layer="29"/>
<rectangle x1="-0.44" y1="1.55" x2="-0.06" y2="2.4" layer="31"/>
<smd name="10" x="0.25" y="-1.975" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="0.06" y1="1.55" x2="0.44" y2="2.4" layer="29"/>
<rectangle x1="0.06" y1="1.55" x2="0.44" y2="2.4" layer="31"/>
<smd name="11" x="0.75" y="-1.975" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="0.56" y1="1.55" x2="0.94" y2="2.4" layer="29"/>
<rectangle x1="0.56" y1="1.55" x2="0.94" y2="2.4" layer="31"/>
<smd name="12" x="1.25" y="-1.975" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="1.06" y1="1.55" x2="1.44" y2="2.4" layer="29"/>
<rectangle x1="1.06" y1="1.55" x2="1.44" y2="2.4" layer="31"/>
<smd name="13" x="1.975" y="-1.25" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="1.55" y1="-1.44" x2="2.4" y2="-1.06" layer="29"/>
<rectangle x1="1.55" y1="-1.44" x2="2.4" y2="-1.06" layer="31"/>
<smd name="14" x="1.975" y="-0.75" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="1.55" y1="-0.94" x2="2.4" y2="-0.56" layer="29"/>
<rectangle x1="1.55" y1="-0.94" x2="2.4" y2="-0.56" layer="31"/>
<smd name="15" x="1.975" y="-0.25" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="1.55" y1="-0.44" x2="2.4" y2="-0.06" layer="29"/>
<rectangle x1="1.55" y1="-0.44" x2="2.4" y2="-0.06" layer="31"/>
<smd name="16" x="1.975" y="0.25" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="1.55" y1="0.06" x2="2.4" y2="0.44" layer="29"/>
<rectangle x1="1.55" y1="0.06" x2="2.4" y2="0.44" layer="31"/>
<smd name="17" x="1.975" y="0.75" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="1.55" y1="0.56" x2="2.4" y2="0.94" layer="29"/>
<rectangle x1="1.55" y1="0.56" x2="2.4" y2="0.94" layer="31"/>
<smd name="18" x="1.975" y="1.25" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="1.55" y1="1.06" x2="2.4" y2="1.44" layer="29"/>
<rectangle x1="1.55" y1="1.06" x2="2.4" y2="1.44" layer="31"/>
<smd name="19" x="1.25" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="-1.44" y1="-2.4" x2="-1.06" y2="-1.55" layer="29"/>
<rectangle x1="-1.44" y1="-2.4" x2="-1.06" y2="-1.55" layer="31"/>
<smd name="20" x="0.75" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="-0.94" y1="-2.4" x2="-0.56" y2="-1.55" layer="29"/>
<rectangle x1="-0.94" y1="-2.4" x2="-0.56" y2="-1.55" layer="31"/>
<smd name="21" x="0.25" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="-0.44" y1="-2.4" x2="-0.06" y2="-1.55" layer="29"/>
<rectangle x1="-0.44" y1="-2.4" x2="-0.06" y2="-1.55" layer="31"/>
<smd name="22" x="-0.25" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="0.06" y1="-2.4" x2="0.44" y2="-1.55" layer="29"/>
<rectangle x1="0.06" y1="-2.4" x2="0.44" y2="-1.55" layer="31"/>
<smd name="23" x="-0.75" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="0.56" y1="-2.4" x2="0.94" y2="-1.55" layer="29"/>
<rectangle x1="0.56" y1="-2.4" x2="0.94" y2="-1.55" layer="31"/>
<smd name="24" x="-1.25" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="1.06" y1="-2.4" x2="1.44" y2="-1.55" layer="29"/>
<rectangle x1="1.06" y1="-2.4" x2="1.44" y2="-1.55" layer="31"/>
<text x="-2.348" y="1.265" size="1" layer="21" rot="R90">*</text>
<text x="-2.4" y="2.8" size="0.85" layer="27">&gt;VALUE</text>
<text x="-2.38" y="3.82" size="0.85" layer="25">&gt;NAME</text>
<text x="-2.38" y="4.91" size="0.85" layer="25">.Designator</text>
</package>
<package name="FT2232D">
<wire x1="-2.55" y1="2.55" x2="8.9" y2="2.55" width="0.1" layer="21"/>
<wire x1="8.9" y1="2.55" x2="8.9" y2="-7.63" width="0.1" layer="21"/>
<wire x1="8.9" y1="-7.63" x2="-2.55" y2="-7.63" width="0.1" layer="21"/>
<wire x1="-2.55" y1="-7.63" x2="-2.55" y2="2.55" width="0.1" layer="21"/>
<smd name="1" x="-1.975" y="1.25" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="-1.44" x2="-1.55" y2="-1.06" layer="29"/>
<rectangle x1="-2.4" y1="-1.44" x2="-1.55" y2="-1.06" layer="31"/>
<smd name="2" x="-1.975" y="0.75" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="-0.94" x2="-1.55" y2="-0.56" layer="29"/>
<rectangle x1="-2.4" y1="-0.94" x2="-1.55" y2="-0.56" layer="31"/>
<smd name="3" x="-1.975" y="0.25" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="-0.44" x2="-1.55" y2="-0.06" layer="29"/>
<rectangle x1="-2.4" y1="-0.44" x2="-1.55" y2="-0.06" layer="31"/>
<smd name="4" x="-1.975" y="-0.25" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="0.06" x2="-1.55" y2="0.44" layer="29"/>
<rectangle x1="-2.4" y1="0.06" x2="-1.55" y2="0.44" layer="31"/>
<smd name="5" x="-1.975" y="-0.75" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="0.56" x2="-1.55" y2="0.94" layer="29"/>
<rectangle x1="-2.4" y1="0.56" x2="-1.55" y2="0.94" layer="31"/>
<smd name="6" x="-1.975" y="-1.25" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="-2.4" y1="1.06" x2="-1.55" y2="1.44" layer="29"/>
<rectangle x1="-2.4" y1="1.06" x2="-1.55" y2="1.44" layer="31"/>
<smd name="7" x="-1.975" y="-2.56" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="4.87" y1="-6.21" x2="5.25" y2="-5.36" layer="29" rot="R180"/>
<rectangle x1="4.87" y1="-6.21" x2="5.25" y2="-5.36" layer="31" rot="R180"/>
<smd name="8" x="-1.975" y="-3.06" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="4.37" y1="-6.21" x2="4.75" y2="-5.36" layer="29" rot="R180"/>
<rectangle x1="4.37" y1="-6.21" x2="4.75" y2="-5.36" layer="31" rot="R180"/>
<smd name="9" x="-1.975" y="-3.56" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="3.87" y1="-6.21" x2="4.25" y2="-5.36" layer="29" rot="R180"/>
<rectangle x1="3.87" y1="-6.21" x2="4.25" y2="-5.36" layer="31" rot="R180"/>
<smd name="10" x="-1.975" y="-4.06" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="3.37" y1="-6.21" x2="3.75" y2="-5.36" layer="29" rot="R180"/>
<rectangle x1="3.37" y1="-6.21" x2="3.75" y2="-5.36" layer="31" rot="R180"/>
<smd name="11" x="-1.975" y="-4.56" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="2.87" y1="-6.21" x2="3.25" y2="-5.36" layer="29" rot="R180"/>
<rectangle x1="2.87" y1="-6.21" x2="3.25" y2="-5.36" layer="31" rot="R180"/>
<smd name="12" x="-1.975" y="-5.06" dx="0.85" dy="0.38" layer="1" stop="no" cream="no"/>
<rectangle x1="2.37" y1="-6.21" x2="2.75" y2="-5.36" layer="29" rot="R180"/>
<rectangle x1="2.37" y1="-6.21" x2="2.75" y2="-5.36" layer="31" rot="R180"/>
<smd name="13" x="-1.25" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-1.675" y1="-5.975" x2="-0.825" y2="-5.595" layer="29" rot="R270"/>
<rectangle x1="-1.675" y1="-5.975" x2="-0.825" y2="-5.595" layer="31" rot="R270"/>
<smd name="14" x="-0.75" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-1.175" y1="-5.975" x2="-0.325" y2="-5.595" layer="29" rot="R270"/>
<rectangle x1="-1.175" y1="-5.975" x2="-0.325" y2="-5.595" layer="31" rot="R270"/>
<smd name="15" x="-0.25" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-0.675" y1="-5.975" x2="0.175" y2="-5.595" layer="29" rot="R270"/>
<rectangle x1="-0.675" y1="-5.975" x2="0.175" y2="-5.595" layer="31" rot="R270"/>
<smd name="16" x="0.25" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-0.175" y1="-5.975" x2="0.675" y2="-5.595" layer="29" rot="R270"/>
<rectangle x1="-0.175" y1="-5.975" x2="0.675" y2="-5.595" layer="31" rot="R270"/>
<smd name="17" x="0.75" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="0.325" y1="-5.975" x2="1.175" y2="-5.595" layer="29" rot="R270"/>
<rectangle x1="0.325" y1="-5.975" x2="1.175" y2="-5.595" layer="31" rot="R270"/>
<smd name="18" x="1.25" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="0.825" y1="-5.975" x2="1.675" y2="-5.595" layer="29" rot="R270"/>
<rectangle x1="0.825" y1="-5.975" x2="1.675" y2="-5.595" layer="31" rot="R270"/>
<smd name="19" x="2.56" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-2.165" y1="-2.985" x2="-1.785" y2="-2.135" layer="29" rot="R270"/>
<rectangle x1="-2.165" y1="-2.985" x2="-1.785" y2="-2.135" layer="31" rot="R270"/>
<smd name="20" x="3.06" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-2.165" y1="-3.485" x2="-1.785" y2="-2.635" layer="29" rot="R270"/>
<rectangle x1="-2.165" y1="-3.485" x2="-1.785" y2="-2.635" layer="31" rot="R270"/>
<smd name="21" x="3.56" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-2.165" y1="-3.985" x2="-1.785" y2="-3.135" layer="29" rot="R270"/>
<rectangle x1="-2.165" y1="-3.985" x2="-1.785" y2="-3.135" layer="31" rot="R270"/>
<smd name="22" x="4.06" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-2.165" y1="-4.485" x2="-1.785" y2="-3.635" layer="29" rot="R270"/>
<rectangle x1="-2.165" y1="-4.485" x2="-1.785" y2="-3.635" layer="31" rot="R270"/>
<smd name="23" x="4.56" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-2.165" y1="-4.985" x2="-1.785" y2="-4.135" layer="29" rot="R270"/>
<rectangle x1="-2.165" y1="-4.985" x2="-1.785" y2="-4.135" layer="31" rot="R270"/>
<smd name="24" x="5.06" y="-5.785" dx="0.85" dy="0.38" layer="1" rot="R90" stop="no" cream="no"/>
<rectangle x1="-2.165" y1="-5.485" x2="-1.785" y2="-4.635" layer="29" rot="R270"/>
<rectangle x1="-2.165" y1="-5.485" x2="-1.785" y2="-4.635" layer="31" rot="R270"/>
<text x="-2.348" y="1.265" size="1" layer="21" rot="R90">*</text>
<text x="-2.4" y="2.8" size="0.85" layer="27">&gt;VALUE</text>
<text x="-2.38" y="3.82" size="0.85" layer="25">&gt;NAME</text>
<text x="-2.38" y="4.91" size="0.85" layer="25">.Designator</text>
<rectangle x1="5.595" y1="0.825" x2="5.975" y2="1.675" layer="29" rot="R270"/>
<rectangle x1="5.595" y1="0.825" x2="5.975" y2="1.675" layer="31" rot="R270"/>
<rectangle x1="5.595" y1="0.325" x2="5.975" y2="1.175" layer="29" rot="R270"/>
<rectangle x1="5.595" y1="0.325" x2="5.975" y2="1.175" layer="31" rot="R270"/>
<rectangle x1="5.595" y1="-0.175" x2="5.975" y2="0.675" layer="29" rot="R270"/>
<rectangle x1="5.595" y1="-0.175" x2="5.975" y2="0.675" layer="31" rot="R270"/>
<rectangle x1="5.595" y1="-0.675" x2="5.975" y2="0.175" layer="29" rot="R270"/>
<rectangle x1="5.595" y1="-0.675" x2="5.975" y2="0.175" layer="31" rot="R270"/>
<rectangle x1="5.595" y1="-1.175" x2="5.975" y2="-0.325" layer="29" rot="R270"/>
<rectangle x1="5.595" y1="-1.175" x2="5.975" y2="-0.325" layer="31" rot="R270"/>
<rectangle x1="5.595" y1="-1.675" x2="5.975" y2="-0.825" layer="29" rot="R270"/>
<rectangle x1="5.595" y1="-1.675" x2="5.975" y2="-0.825" layer="31" rot="R270"/>
<smd name="25" x="5.785" y="-5.06" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="5.36" y1="-5.25" x2="6.21" y2="-4.87" layer="29"/>
<rectangle x1="5.36" y1="-5.25" x2="6.21" y2="-4.87" layer="31"/>
<smd name="26" x="5.785" y="-4.56" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="5.36" y1="-4.75" x2="6.21" y2="-4.37" layer="29"/>
<rectangle x1="5.36" y1="-4.75" x2="6.21" y2="-4.37" layer="31"/>
<smd name="27" x="5.785" y="-4.06" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="5.36" y1="-4.25" x2="6.21" y2="-3.87" layer="29"/>
<rectangle x1="5.36" y1="-4.25" x2="6.21" y2="-3.87" layer="31"/>
<smd name="28" x="5.785" y="-3.56" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="5.36" y1="-3.75" x2="6.21" y2="-3.37" layer="29"/>
<rectangle x1="5.36" y1="-3.75" x2="6.21" y2="-3.37" layer="31"/>
<smd name="29" x="5.785" y="-3.06" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="5.36" y1="-3.25" x2="6.21" y2="-2.87" layer="29"/>
<rectangle x1="5.36" y1="-3.25" x2="6.21" y2="-2.87" layer="31"/>
<smd name="30" x="5.785" y="-2.56" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="5.36" y1="-2.75" x2="6.21" y2="-2.37" layer="29"/>
<rectangle x1="5.36" y1="-2.75" x2="6.21" y2="-2.37" layer="31"/>
<smd name="31" x="5.785" y="-1.25" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="32" x="5.785" y="-0.75" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="33" x="5.785" y="-0.25" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="34" x="5.785" y="0.25" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="35" x="5.785" y="0.75" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="36" x="5.785" y="1.25" dx="0.85" dy="0.38" layer="1" rot="R180" stop="no" cream="no"/>
<rectangle x1="-1.44" y1="1.55" x2="-1.06" y2="2.4" layer="29"/>
<rectangle x1="-1.44" y1="1.55" x2="-1.06" y2="2.4" layer="31"/>
<rectangle x1="-0.94" y1="1.55" x2="-0.56" y2="2.4" layer="29"/>
<rectangle x1="-0.94" y1="1.55" x2="-0.56" y2="2.4" layer="31"/>
<rectangle x1="-0.44" y1="1.55" x2="-0.06" y2="2.4" layer="29"/>
<rectangle x1="-0.44" y1="1.55" x2="-0.06" y2="2.4" layer="31"/>
<rectangle x1="0.06" y1="1.55" x2="0.44" y2="2.4" layer="29"/>
<rectangle x1="0.06" y1="1.55" x2="0.44" y2="2.4" layer="31"/>
<rectangle x1="0.56" y1="1.55" x2="0.94" y2="2.4" layer="29"/>
<rectangle x1="0.56" y1="1.55" x2="0.94" y2="2.4" layer="31"/>
<rectangle x1="1.06" y1="1.55" x2="1.44" y2="2.4" layer="29"/>
<rectangle x1="1.06" y1="1.55" x2="1.44" y2="2.4" layer="31"/>
<smd name="37" x="5.06" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="4.635" y1="1.785" x2="5.485" y2="2.165" layer="29" rot="R90"/>
<rectangle x1="4.635" y1="1.785" x2="5.485" y2="2.165" layer="31" rot="R90"/>
<smd name="38" x="4.56" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="4.135" y1="1.785" x2="4.985" y2="2.165" layer="29" rot="R90"/>
<rectangle x1="4.135" y1="1.785" x2="4.985" y2="2.165" layer="31" rot="R90"/>
<smd name="39" x="4.06" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="3.635" y1="1.785" x2="4.485" y2="2.165" layer="29" rot="R90"/>
<rectangle x1="3.635" y1="1.785" x2="4.485" y2="2.165" layer="31" rot="R90"/>
<smd name="40" x="3.56" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="3.135" y1="1.785" x2="3.985" y2="2.165" layer="29" rot="R90"/>
<rectangle x1="3.135" y1="1.785" x2="3.985" y2="2.165" layer="31" rot="R90"/>
<smd name="41" x="3.06" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="2.635" y1="1.785" x2="3.485" y2="2.165" layer="29" rot="R90"/>
<rectangle x1="2.635" y1="1.785" x2="3.485" y2="2.165" layer="31" rot="R90"/>
<smd name="42" x="2.56" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<rectangle x1="2.135" y1="1.785" x2="2.985" y2="2.165" layer="29" rot="R90"/>
<rectangle x1="2.135" y1="1.785" x2="2.985" y2="2.165" layer="31" rot="R90"/>
<smd name="43" x="1.25" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="44" x="0.75" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="45" x="0.25" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="46" x="-0.25" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="47" x="-0.75" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="48" x="-1.25" y="1.975" dx="0.85" dy="0.38" layer="1" rot="R270" stop="no" cream="no"/>
</package>
<package name="PWP20_2P45X3P2">
<smd name="1" x="-2.9718" y="2.9250625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="2" x="-2.9718" y="2.275028125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="3" x="-2.9718" y="1.625040625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="4" x="-2.9718" y="0.975003125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="5" x="-2.9718" y="0.32501875" dx="1.27" dy="0.3048" layer="1"/>
<smd name="6" x="-2.9718" y="-0.32501875" dx="1.27" dy="0.3048" layer="1"/>
<smd name="7" x="-2.9718" y="-0.975003125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="8" x="-2.9718" y="-1.625040625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="9" x="-2.9718" y="-2.275028125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="10" x="-2.9718" y="-2.9250625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="11" x="2.9718" y="-2.9250625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="12" x="2.9718" y="-2.275028125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="13" x="2.9718" y="-1.625040625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="14" x="2.9718" y="-0.975003125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="15" x="2.9718" y="-0.32501875" dx="1.27" dy="0.3048" layer="1"/>
<smd name="16" x="2.9718" y="0.32501875" dx="1.27" dy="0.3048" layer="1"/>
<smd name="17" x="2.9718" y="0.975003125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="18" x="2.9718" y="1.625040625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="19" x="2.9718" y="2.275028125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="20" x="2.9718" y="2.9250625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="PAD" x="0" y="0" dx="2.4384" dy="3.2004" layer="1"/>
<wire x1="-2.0828" y1="-3.302" x2="2.0828" y2="-3.302" width="0.1524" layer="51"/>
<wire x1="2.0828" y1="3.302" x2="0.3048" y2="3.302" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="3.302" x2="-0.3048" y2="3.302" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="3.302" x2="-2.0828" y2="3.302" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="3.302" x2="-0.3048" y2="3.302" width="0" layer="51" curve="-180"/>
<polygon width="0" layer="51">
<vertex x="-4.1148" y="-2.7432"/>
<vertex x="-4.1148" y="-3.1242"/>
<vertex x="-3.8608" y="-3.1242"/>
<vertex x="-3.8608" y="-2.7432"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="4.1148" y="3.1242"/>
<vertex x="4.1148" y="2.7432"/>
<vertex x="3.8608" y="2.7432"/>
<vertex x="3.8608" y="3.1242"/>
</polygon>
<text x="-3.81" y="3.0988" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<wire x1="-2.2352" y1="2.7686" x2="-2.2352" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="3.0734" x2="-3.302" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="3.0734" x2="-3.302" y2="2.7686" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="2.7686" x2="-2.2352" y2="2.7686" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="2.1336" x2="-2.2352" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="2.4384" x2="-3.302" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="2.4384" x2="-3.302" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="2.1336" x2="-2.2352" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="1.4732" x2="-2.2352" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="1.778" x2="-3.302" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.778" x2="-3.302" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.4732" x2="-2.2352" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="0.8128" x2="-2.2352" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="1.1176" x2="-3.302" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.1176" x2="-3.302" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.8128" x2="-2.2352" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="0.1778" x2="-2.2352" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="0.4826" x2="-3.302" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.4826" x2="-3.302" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.1778" x2="-2.2352" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-0.4826" x2="-2.2352" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-0.1778" x2="-3.302" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-0.1778" x2="-3.302" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-0.4826" x2="-2.2352" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-1.1176" x2="-2.2352" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-0.8128" x2="-3.302" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-0.8128" x2="-3.302" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.1176" x2="-2.2352" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-1.778" x2="-2.2352" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-1.4732" x2="-3.302" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.4732" x2="-3.302" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.778" x2="-2.2352" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-2.4384" x2="-2.2352" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-2.1336" x2="-3.302" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-2.1336" x2="-3.302" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-2.4384" x2="-2.2352" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-3.0734" x2="-2.2352" y2="-2.7686" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-2.7686" x2="-3.302" y2="-2.7686" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-2.7686" x2="-3.302" y2="-3.0734" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-3.0734" x2="-2.2352" y2="-3.0734" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.7686" x2="2.2352" y2="-3.0734" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-3.0734" x2="3.302" y2="-3.0734" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-3.0734" x2="3.302" y2="-2.7686" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-2.7686" x2="2.2352" y2="-2.7686" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.1336" x2="2.2352" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.4384" x2="3.302" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-2.4384" x2="3.302" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-2.1336" x2="2.2352" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-1.4732" x2="2.2352" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-1.778" x2="3.302" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.778" x2="3.302" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.4732" x2="2.2352" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-0.8128" x2="2.2352" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-1.1176" x2="3.302" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.1176" x2="3.302" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.8128" x2="2.2352" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-0.1778" x2="2.2352" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-0.4826" x2="3.302" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.4826" x2="3.302" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.1778" x2="2.2352" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="0.4826" x2="2.2352" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="0.1778" x2="3.302" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="3.302" y1="0.1778" x2="3.302" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="3.302" y1="0.4826" x2="2.2352" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="1.1176" x2="2.2352" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="0.8128" x2="3.302" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="3.302" y1="0.8128" x2="3.302" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.1176" x2="2.2352" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="1.778" x2="2.2352" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="1.4732" x2="3.302" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.4732" x2="3.302" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.778" x2="2.2352" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="2.4384" x2="2.2352" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="2.1336" x2="3.302" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="3.302" y1="2.1336" x2="3.302" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="3.302" y1="2.4384" x2="2.2352" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="3.0734" x2="2.2352" y2="2.7686" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="2.7686" x2="3.302" y2="2.7686" width="0.1524" layer="21"/>
<wire x1="3.302" y1="2.7686" x2="3.302" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="3.302" y1="3.0734" x2="2.2352" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-3.302" x2="2.2352" y2="-3.302" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-3.302" x2="2.2352" y2="3.302" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="3.302" x2="0.3048" y2="3.302" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="3.302" x2="-0.3048" y2="3.302" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="3.302" x2="-2.2352" y2="3.302" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="3.302" x2="-2.2352" y2="-3.302" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="3.302" x2="-0.3048" y2="3.302" width="0" layer="21" curve="-180"/>
<text x="-3.81" y="3.0988" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<polygon width="0.1524" layer="1">
<vertex x="-0.635" y="1.6002"/>
<vertex x="-0.635" y="1.905"/>
<vertex x="0.635" y="1.905"/>
<vertex x="0.635" y="1.6002"/>
</polygon>
<polygon width="0.1524" layer="1">
<vertex x="-0.635" y="-1.6002"/>
<vertex x="-0.635" y="-1.905"/>
<vertex x="0.635" y="-1.905"/>
<vertex x="0.635" y="-1.6002"/>
</polygon>
<polygon width="0.1524" layer="29">
<vertex x="-0.635" y="1.6002"/>
<vertex x="-0.635" y="1.905"/>
<vertex x="0.635" y="1.905"/>
<vertex x="0.635" y="1.6002"/>
</polygon>
<polygon width="0.1524" layer="29">
<vertex x="-0.635" y="-1.6002"/>
<vertex x="-0.635" y="-1.905"/>
<vertex x="0.635" y="-1.905"/>
<vertex x="0.635" y="-1.6002"/>
</polygon>
<text x="-2.8702" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
</package>
<package name="TSSOP_PW_14">
<smd name="1" x="-2.8" y="1.95" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="2" x="-2.8" y="1.3" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="3" x="-2.8" y="0.65" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="4" x="-2.8" y="0" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="5" x="-2.8" y="-0.65" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="6" x="-2.8" y="-1.3" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="7" x="-2.8" y="-1.95" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="8" x="2.8" y="-1.95" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="9" x="2.8" y="-1.3" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="10" x="2.8" y="-0.65" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="11" x="2.8" y="0" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="12" x="2.8" y="0.65" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="13" x="2.8" y="1.3" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="14" x="2.8" y="1.95" dx="1.6764" dy="0.3556" layer="1"/>
<wire x1="-2.286" y1="-2.413" x2="2.1844" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="2.225040625" y1="2.3622" x2="0.294640625" y2="2.3622" width="0.1524" layer="21"/>
<wire x1="-0.314959375" y1="2.3622" x2="-2.245359375" y2="2.3622" width="0.1524" layer="21"/>
<wire x1="0.294640625" y1="2.3622" x2="-0.314959375" y2="2.3622" width="0.127" layer="21" curve="-180"/>
<text x="-2.778759375" y="2.413" size="0.508" layer="21" ratio="6" rot="SR90">*</text>
<text x="-2.550159375" y="3.175" size="0.508" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-2.423159375" y="2.54" size="0.508" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
</package>
<package name="QFN32">
<description>&lt;b&gt;QFN 32&lt;/b&gt; 5 x 5 mm&lt;p&gt;
Source: www.qprox.com ... QT1106_-8IR0.07-16262.pdf</description>
<wire x1="-2.45" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="51"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="2.45" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="2.45" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="2.05" x2="-2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="2.45" x2="-2.05" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.05" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="2.05" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.05" x2="2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.45" x2="2.05" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.05" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="-2.05" width="0.1016" layer="21"/>
<circle x="-2.175" y="2.175" radius="0.15" width="0" layer="21"/>
<smd name="EXP_GNDD" x="0" y="0" dx="2.95" dy="2.95" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="IREF_DAC" x="-2.45" y="1.75" dx="0.7" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="GNDA_DAC" x="-2.45" y="1.25" dx="0.7" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="VDDA_25_DAC" x="-2.45" y="0.75" dx="0.7" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="VDDA_HSC_4" x="-2.45" y="0.25" dx="0.7" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="VDDA_HSC_PIN5" x="-2.45" y="-0.25" dx="0.7" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="NC" x="-2.45" y="-0.75" dx="0.7" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="GNDA_HSC_PIN7" x="-2.45" y="-1.25" dx="0.7" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="GNDA_HSC_PIN8" x="-2.45" y="-1.75" dx="0.7" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="VDDA_RX_PIN9" x="-1.75" y="-2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="GNDA_RX_PIN10" x="-1.25" y="-2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="RFIN" x="-0.75" y="-2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="GNDA_RX_PIN12" x="-0.25" y="-2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="VDDA_RX_PIN13" x="0.25" y="-2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="NARST" x="0.75" y="-2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="VDDD_PIN15" x="1.25" y="-2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="VDDD_PIN16" x="1.75" y="-2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="RESERVED" x="2.45" y="-1.75" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="MISO" x="2.45" y="-1.25" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="MOSI" x="2.45" y="-0.75" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="SCLK" x="2.45" y="-0.25" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="NSS" x="2.45" y="0.25" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="EXTCLK" x="2.45" y="0.75" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="GNDDIO" x="2.45" y="1.25" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="VDDD25_IO" x="2.45" y="1.75" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="VDDD_TCTRL" x="1.75" y="2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="GNDD_TCTRL" x="1.25" y="2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="CLK_OUT" x="0.75" y="2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="VDDD" x="0.25" y="2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="GNDD_TX_PIN29" x="-0.25" y="2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="RFOUT" x="-0.75" y="2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="GNDD_TX_PIN31" x="-1.25" y="2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="VDDD_TX" x="-1.75" y="2.45" dx="0.7" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<text x="-4.05" y="-4.35" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.3" y="3.25" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.3" y1="1.1" x2="0.3" y2="1.4" layer="31"/>
<rectangle x1="-0.3" y1="0.6" x2="0.3" y2="0.9" layer="31"/>
<rectangle x1="-0.3" y1="0.1" x2="0.3" y2="0.4" layer="31"/>
<rectangle x1="-0.3" y1="-0.4" x2="0.3" y2="-0.1" layer="31"/>
<rectangle x1="-0.3" y1="-0.9" x2="0.3" y2="-0.6" layer="31"/>
<rectangle x1="-0.3" y1="-1.4" x2="0.3" y2="-1.1" layer="31"/>
<rectangle x1="-1.3" y1="1.1" x2="-0.7" y2="1.4" layer="31"/>
<rectangle x1="-1.3" y1="0.6" x2="-0.7" y2="0.9" layer="31"/>
<rectangle x1="-1.3" y1="0.1" x2="-0.7" y2="0.4" layer="31"/>
<rectangle x1="-1.3" y1="-0.4" x2="-0.7" y2="-0.1" layer="31"/>
<rectangle x1="-1.3" y1="-0.9" x2="-0.7" y2="-0.6" layer="31"/>
<rectangle x1="-1.3" y1="-1.4" x2="-0.7" y2="-1.1" layer="31"/>
<rectangle x1="0.7" y1="1.1" x2="1.3" y2="1.4" layer="31"/>
<rectangle x1="0.7" y1="0.6" x2="1.3" y2="0.9" layer="31"/>
<rectangle x1="0.7" y1="0.1" x2="1.3" y2="0.4" layer="31"/>
<rectangle x1="0.7" y1="-0.4" x2="1.3" y2="-0.1" layer="31"/>
<rectangle x1="0.7" y1="-0.9" x2="1.3" y2="-0.6" layer="31"/>
<rectangle x1="0.7" y1="-1.4" x2="1.3" y2="-1.1" layer="31"/>
<rectangle x1="-2.5" y1="0.25" x2="-0.25" y2="2.5" layer="51"/>
<polygon width="0.5" layer="29">
<vertex x="-1.325" y="1.175"/>
<vertex x="-1.175" y="1.325"/>
<vertex x="1.325" y="1.325"/>
<vertex x="1.325" y="-1.325"/>
<vertex x="-1.325" y="-1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="1.85"/>
<vertex x="-2.1" y="1.85"/>
<vertex x="-2.05" y="1.8"/>
<vertex x="-2.05" y="1.65"/>
<vertex x="-2.55" y="1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="1.825"/>
<vertex x="-2.125" y="1.825"/>
<vertex x="-2.075" y="1.775"/>
<vertex x="-2.075" y="1.675"/>
<vertex x="-2.525" y="1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="1.35"/>
<vertex x="-2.05" y="1.35"/>
<vertex x="-2.05" y="1.15"/>
<vertex x="-2.55" y="1.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="1.325"/>
<vertex x="-2.075" y="1.325"/>
<vertex x="-2.075" y="1.175"/>
<vertex x="-2.525" y="1.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="0.85"/>
<vertex x="-2.05" y="0.85"/>
<vertex x="-2.05" y="0.65"/>
<vertex x="-2.55" y="0.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="0.825"/>
<vertex x="-2.075" y="0.825"/>
<vertex x="-2.075" y="0.675"/>
<vertex x="-2.525" y="0.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="0.35"/>
<vertex x="-2.05" y="0.35"/>
<vertex x="-2.05" y="0.15"/>
<vertex x="-2.55" y="0.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="0.325"/>
<vertex x="-2.075" y="0.325"/>
<vertex x="-2.075" y="0.175"/>
<vertex x="-2.525" y="0.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-0.15"/>
<vertex x="-2.05" y="-0.15"/>
<vertex x="-2.05" y="-0.35"/>
<vertex x="-2.55" y="-0.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-0.175"/>
<vertex x="-2.075" y="-0.175"/>
<vertex x="-2.075" y="-0.325"/>
<vertex x="-2.525" y="-0.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-0.65"/>
<vertex x="-2.05" y="-0.65"/>
<vertex x="-2.05" y="-0.85"/>
<vertex x="-2.55" y="-0.85"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-0.675"/>
<vertex x="-2.075" y="-0.675"/>
<vertex x="-2.075" y="-0.825"/>
<vertex x="-2.525" y="-0.825"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-1.15"/>
<vertex x="-2.05" y="-1.15"/>
<vertex x="-2.05" y="-1.35"/>
<vertex x="-2.55" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-1.175"/>
<vertex x="-2.075" y="-1.175"/>
<vertex x="-2.075" y="-1.325"/>
<vertex x="-2.525" y="-1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-1.85"/>
<vertex x="-2.1" y="-1.85"/>
<vertex x="-2.05" y="-1.8"/>
<vertex x="-2.05" y="-1.65"/>
<vertex x="-2.55" y="-1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-1.825"/>
<vertex x="-2.125" y="-1.825"/>
<vertex x="-2.075" y="-1.775"/>
<vertex x="-2.075" y="-1.675"/>
<vertex x="-2.525" y="-1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.85" y="-2.55"/>
<vertex x="-1.85" y="-2.1"/>
<vertex x="-1.8" y="-2.05"/>
<vertex x="-1.65" y="-2.05"/>
<vertex x="-1.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.825" y="-2.525"/>
<vertex x="-1.825" y="-2.125"/>
<vertex x="-1.775" y="-2.075"/>
<vertex x="-1.675" y="-2.075"/>
<vertex x="-1.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.35" y="-2.55"/>
<vertex x="-1.35" y="-2.05"/>
<vertex x="-1.15" y="-2.05"/>
<vertex x="-1.15" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.325" y="-2.525"/>
<vertex x="-1.325" y="-2.075"/>
<vertex x="-1.175" y="-2.075"/>
<vertex x="-1.175" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.85" y="-2.55"/>
<vertex x="-0.85" y="-2.05"/>
<vertex x="-0.65" y="-2.05"/>
<vertex x="-0.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.825" y="-2.525"/>
<vertex x="-0.825" y="-2.075"/>
<vertex x="-0.675" y="-2.075"/>
<vertex x="-0.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.35" y="-2.55"/>
<vertex x="-0.35" y="-2.05"/>
<vertex x="-0.15" y="-2.05"/>
<vertex x="-0.15" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.325" y="-2.525"/>
<vertex x="-0.325" y="-2.075"/>
<vertex x="-0.175" y="-2.075"/>
<vertex x="-0.175" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.15" y="-2.55"/>
<vertex x="0.15" y="-2.05"/>
<vertex x="0.35" y="-2.05"/>
<vertex x="0.35" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.175" y="-2.525"/>
<vertex x="0.175" y="-2.075"/>
<vertex x="0.325" y="-2.075"/>
<vertex x="0.325" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.65" y="-2.55"/>
<vertex x="0.65" y="-2.05"/>
<vertex x="0.85" y="-2.05"/>
<vertex x="0.85" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.675" y="-2.525"/>
<vertex x="0.675" y="-2.075"/>
<vertex x="0.825" y="-2.075"/>
<vertex x="0.825" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.15" y="-2.55"/>
<vertex x="1.15" y="-2.05"/>
<vertex x="1.35" y="-2.05"/>
<vertex x="1.35" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.175" y="-2.525"/>
<vertex x="1.175" y="-2.075"/>
<vertex x="1.325" y="-2.075"/>
<vertex x="1.325" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.85" y="-2.55"/>
<vertex x="1.85" y="-2.1"/>
<vertex x="1.8" y="-2.05"/>
<vertex x="1.65" y="-2.05"/>
<vertex x="1.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.825" y="-2.525"/>
<vertex x="1.825" y="-2.125"/>
<vertex x="1.775" y="-2.075"/>
<vertex x="1.675" y="-2.075"/>
<vertex x="1.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-1.85"/>
<vertex x="2.1" y="-1.85"/>
<vertex x="2.05" y="-1.8"/>
<vertex x="2.05" y="-1.65"/>
<vertex x="2.55" y="-1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-1.825"/>
<vertex x="2.125" y="-1.825"/>
<vertex x="2.075" y="-1.775"/>
<vertex x="2.075" y="-1.675"/>
<vertex x="2.525" y="-1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-1.35"/>
<vertex x="2.05" y="-1.35"/>
<vertex x="2.05" y="-1.15"/>
<vertex x="2.55" y="-1.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-1.325"/>
<vertex x="2.075" y="-1.325"/>
<vertex x="2.075" y="-1.175"/>
<vertex x="2.525" y="-1.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-0.85"/>
<vertex x="2.05" y="-0.85"/>
<vertex x="2.05" y="-0.65"/>
<vertex x="2.55" y="-0.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-0.825"/>
<vertex x="2.075" y="-0.825"/>
<vertex x="2.075" y="-0.675"/>
<vertex x="2.525" y="-0.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-0.35"/>
<vertex x="2.05" y="-0.35"/>
<vertex x="2.05" y="-0.15"/>
<vertex x="2.55" y="-0.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-0.325"/>
<vertex x="2.075" y="-0.325"/>
<vertex x="2.075" y="-0.175"/>
<vertex x="2.525" y="-0.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="0.15"/>
<vertex x="2.05" y="0.15"/>
<vertex x="2.05" y="0.35"/>
<vertex x="2.55" y="0.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="0.175"/>
<vertex x="2.075" y="0.175"/>
<vertex x="2.075" y="0.325"/>
<vertex x="2.525" y="0.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="0.65"/>
<vertex x="2.05" y="0.65"/>
<vertex x="2.05" y="0.85"/>
<vertex x="2.55" y="0.85"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="0.675"/>
<vertex x="2.075" y="0.675"/>
<vertex x="2.075" y="0.825"/>
<vertex x="2.525" y="0.825"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="1.15"/>
<vertex x="2.05" y="1.15"/>
<vertex x="2.05" y="1.35"/>
<vertex x="2.55" y="1.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="1.175"/>
<vertex x="2.075" y="1.175"/>
<vertex x="2.075" y="1.325"/>
<vertex x="2.525" y="1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="1.85"/>
<vertex x="2.1" y="1.85"/>
<vertex x="2.05" y="1.8"/>
<vertex x="2.05" y="1.65"/>
<vertex x="2.55" y="1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="1.825"/>
<vertex x="2.125" y="1.825"/>
<vertex x="2.075" y="1.775"/>
<vertex x="2.075" y="1.675"/>
<vertex x="2.525" y="1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.85" y="2.55"/>
<vertex x="1.85" y="2.1"/>
<vertex x="1.8" y="2.05"/>
<vertex x="1.65" y="2.05"/>
<vertex x="1.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.825" y="2.525"/>
<vertex x="1.825" y="2.125"/>
<vertex x="1.775" y="2.075"/>
<vertex x="1.675" y="2.075"/>
<vertex x="1.675" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.35" y="2.55"/>
<vertex x="1.35" y="2.05"/>
<vertex x="1.15" y="2.05"/>
<vertex x="1.15" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.325" y="2.525"/>
<vertex x="1.325" y="2.075"/>
<vertex x="1.175" y="2.075"/>
<vertex x="1.175" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.85" y="2.55"/>
<vertex x="0.85" y="2.05"/>
<vertex x="0.65" y="2.05"/>
<vertex x="0.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.825" y="2.525"/>
<vertex x="0.825" y="2.075"/>
<vertex x="0.675" y="2.075"/>
<vertex x="0.675" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.35" y="2.55"/>
<vertex x="0.35" y="2.05"/>
<vertex x="0.15" y="2.05"/>
<vertex x="0.15" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.325" y="2.525"/>
<vertex x="0.325" y="2.075"/>
<vertex x="0.175" y="2.075"/>
<vertex x="0.175" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.15" y="2.55"/>
<vertex x="-0.15" y="2.05"/>
<vertex x="-0.35" y="2.05"/>
<vertex x="-0.35" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.175" y="2.525"/>
<vertex x="-0.175" y="2.075"/>
<vertex x="-0.325" y="2.075"/>
<vertex x="-0.325" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.65" y="2.55"/>
<vertex x="-0.65" y="2.05"/>
<vertex x="-0.85" y="2.05"/>
<vertex x="-0.85" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.675" y="2.525"/>
<vertex x="-0.675" y="2.075"/>
<vertex x="-0.825" y="2.075"/>
<vertex x="-0.825" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.15" y="2.55"/>
<vertex x="-1.15" y="2.05"/>
<vertex x="-1.35" y="2.05"/>
<vertex x="-1.35" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.175" y="2.525"/>
<vertex x="-1.175" y="2.075"/>
<vertex x="-1.325" y="2.075"/>
<vertex x="-1.325" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.85" y="2.55"/>
<vertex x="-1.85" y="2.1"/>
<vertex x="-1.8" y="2.05"/>
<vertex x="-1.65" y="2.05"/>
<vertex x="-1.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.825" y="2.525"/>
<vertex x="-1.825" y="2.125"/>
<vertex x="-1.775" y="2.075"/>
<vertex x="-1.675" y="2.075"/>
<vertex x="-1.675" y="2.525"/>
</polygon>
</package>
<package name="SOT23">
<description>&lt;b&gt;TPD2EUSB30DRTR&lt;/b&gt; &lt;p&gt;
http://www.digikey.com/product-detail/en/TPD2EUSB30DRTR/296-25509-1-ND/2193502</description>
<wire x1="1.0824" y1="1.0404" x2="1.0824" y2="0.0596" width="0.1524" layer="51"/>
<wire x1="1.0824" y1="0.0596" x2="0.0676" y2="0.0596" width="0.1524" layer="51"/>
<wire x1="0.0676" y1="0.0596" x2="0.0676" y2="1.0404" width="0.1524" layer="51"/>
<wire x1="0.0676" y1="1.0404" x2="1.0824" y2="1.0404" width="0.1524" layer="51"/>
<smd name="2" x="0.9" y="-0.02" dx="0.2" dy="0.2" layer="1"/>
<text x="-0.415" y="2.625" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.415" y="-2.455" size="1.27" layer="27">&gt;VALUE</text>
<smd name="1" x="0.24" y="-0.01" dx="0.2" dy="0.2" layer="1"/>
<smd name="3" x="0.56" y="1.11" dx="0.2" dy="0.2" layer="1"/>
</package>
<package name="MSOP08">
<description>&lt;b&gt;8-Lead micro SO&lt;/b&gt; (RM-8)&lt;p&gt;
Source: http://www.analog.com/UploadedFiles/Data_Sheets/703465986AD8611_2_0.pdf</description>
<wire x1="1.624" y1="1.299" x2="1.624" y2="-1.301" width="0.1524" layer="21"/>
<wire x1="-1.626" y1="-1.301" x2="-1.626" y2="1.299" width="0.1524" layer="21"/>
<wire x1="1.299" y1="1.624" x2="1.624" y2="1.299" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.626" y1="1.299" x2="-1.301" y2="1.624" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.626" y1="-1.301" x2="-1.301" y2="-1.626" width="0.1524" layer="21" curve="90"/>
<wire x1="1.299" y1="-1.626" x2="1.624" y2="-1.301" width="0.1524" layer="21" curve="90"/>
<wire x1="-1.341" y1="-1.626" x2="1.299" y2="-1.626" width="0.1524" layer="21"/>
<wire x1="-1.301" y1="1.624" x2="1.299" y2="1.624" width="0.1524" layer="21"/>
<circle x="-1.0456" y="-1.0406" radius="0.2448" width="0.0508" layer="21"/>
<smd name="8" x="-0.976" y="2.202" dx="0.4" dy="1" layer="1"/>
<smd name="7" x="-0.326" y="2.202" dx="0.4" dy="1" layer="1"/>
<smd name="6" x="0.324" y="2.202" dx="0.4" dy="1" layer="1"/>
<smd name="5" x="0.974" y="2.202" dx="0.4" dy="1" layer="1"/>
<smd name="4" x="0.984" y="-2.203" dx="0.4" dy="1" layer="1"/>
<smd name="3" x="0.324" y="-2.203" dx="0.4" dy="1" layer="1"/>
<smd name="2" x="-0.336" y="-2.213" dx="0.4" dy="1" layer="1"/>
<smd name="1" x="-0.976" y="-2.213" dx="0.4" dy="1" layer="1"/>
<text x="-2.032" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.302" y="-2.54" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.0975" y1="1.65" x2="-0.8537" y2="2.5057" layer="51"/>
<rectangle x1="-0.4475" y1="1.65" x2="-0.2037" y2="2.5057" layer="51"/>
<rectangle x1="0.2025" y1="1.65" x2="0.4463" y2="2.5057" layer="51"/>
<rectangle x1="0.8525" y1="1.65" x2="1.0963" y2="2.5057" layer="51"/>
<rectangle x1="-1.0975" y1="-2.5069" x2="-0.8537" y2="-1.65" layer="51"/>
<rectangle x1="-0.4575" y1="-2.5069" x2="-0.2137" y2="-1.65" layer="51"/>
<rectangle x1="0.2025" y1="-2.5369" x2="0.4463" y2="-1.68" layer="51"/>
<rectangle x1="0.8525" y1="-2.5069" x2="1.0963" y2="-1.65" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="BQ24616">
<pin name="ACDRV" x="-20.32" y="58.42" length="short" direction="out" rot="R270"/>
<wire x1="-35.56" y1="55.88" x2="-35.56" y2="-76.2" width="0.1524" layer="94"/>
<wire x1="-35.56" y1="-76.2" x2="35.56" y2="-76.2" width="0.1524" layer="94"/>
<wire x1="35.56" y1="-76.2" x2="35.56" y2="55.88" width="0.1524" layer="94"/>
<wire x1="35.56" y1="55.88" x2="-35.56" y2="55.88" width="0.1524" layer="94"/>
<text x="0.3556" y="16.7386" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-0.5842" y="11.6586" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="CE" x="-38.1" y="33.02" length="short" direction="in"/>
<pin name="STAT1" x="-38.1" y="-48.26" length="short" direction="out"/>
<pin name="PG" x="-38.1" y="-58.42" length="short" direction="out"/>
<pin name="GND" x="-15.24" y="-78.74" length="short" direction="pwr" rot="R90"/>
<pin name="PAD_GND1" x="2.54" y="-78.74" length="short" direction="pwr" rot="R90"/>
<text x="-12.7" y="-43.18" size="3.81" layer="94">GNDs</text>
<pin name="TS" x="-38.1" y="-71.12" length="short" direction="in"/>
<pin name="ACP" x="-5.08" y="58.42" length="short" direction="in" rot="R270"/>
<pin name="ACN" x="10.16" y="58.42" length="short" direction="in" rot="R270"/>
<pin name="STAT2" x="-38.1" y="-53.34" length="short" direction="out"/>
<pin name="VREF" x="-38.1" y="22.86" length="short" direction="out"/>
<pin name="ACSET" x="-38.1" y="-10.16" length="short" direction="in"/>
<pin name="ISET1" x="-38.1" y="5.08" length="short" direction="in"/>
<pin name="ISET2" x="-38.1" y="-2.54" length="short" direction="in"/>
<pin name="TTC" x="38.1" y="-53.34" length="short" direction="in" rot="R180"/>
<pin name="VFB" x="38.1" y="-45.72" length="short" direction="in" rot="R180"/>
<pin name="SRP" x="38.1" y="-7.62" length="short" rot="R180"/>
<pin name="SRN" x="38.1" y="-25.4" length="short" rot="R180"/>
<pin name="LODRV" x="38.1" y="7.62" length="short" direction="out" rot="R180"/>
<pin name="PH" x="38.1" y="20.32" length="short" direction="in" rot="R180"/>
<pin name="HIDRV" x="38.1" y="33.02" length="short" direction="out" rot="R180"/>
<pin name="BATDRV" x="38.1" y="45.72" length="short" direction="out" rot="R180"/>
<text x="20.32" y="-17.78" size="1.778" layer="94">CHRG
 CS</text>
<text x="-2.54" y="40.64" size="1.778" layer="94">ADAPTER
    CS</text>
<text x="22.86" y="12.7" size="1.778" layer="94" rot="R90">DRIVE BATTERY CHRGING</text>
<pin name="VCC" x="-38.1" y="48.26" length="short" direction="pwr"/>
<pin name="REGN" x="30.48" y="58.42" length="short" direction="out" rot="R270"/>
<pin name="BTST" x="20.32" y="58.42" length="short" direction="in" rot="R270"/>
</symbol>
<symbol name="BQ76920">
<pin name="VC0" x="-30.48" y="-38.1" length="short" direction="in"/>
<wire x1="-27.94" y1="60.96" x2="-27.94" y2="-73.66" width="0.1524" layer="94"/>
<wire x1="-27.94" y1="-73.66" x2="22.86" y2="-73.66" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-73.66" x2="22.86" y2="60.96" width="0.1524" layer="94"/>
<wire x1="22.86" y1="60.96" x2="-27.94" y2="60.96" width="0.1524" layer="94"/>
<text x="-25.0444" y="62.4586" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-5.6642" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="SDA" x="25.4" y="-17.78" length="short" rot="R180"/>
<pin name="SCL" x="25.4" y="-10.16" length="short" direction="in" rot="R180"/>
<pin name="BAT" x="25.4" y="55.88" length="short" direction="pwr" rot="R180"/>
<pin name="CAP1" x="25.4" y="12.7" length="short" direction="out" rot="R180"/>
<pin name="REGOUT" x="25.4" y="35.56" length="short" direction="pwr" rot="R180"/>
<pin name="ALERT" x="-30.48" y="-63.5" length="short"/>
<pin name="VSS" x="7.62" y="-76.2" length="short" direction="pwr" rot="R90"/>
<text x="7.62" y="-66.04" size="3.81" layer="94" rot="R90">GND</text>
<text x="12.7" y="-17.78" size="3.81" layer="94" rot="R90">I2C</text>
<pin name="TS1" x="25.4" y="-38.1" length="short" direction="in" rot="R180"/>
<pin name="REGSRC" x="25.4" y="48.26" length="short" direction="in" rot="R180"/>
<pin name="NC_PIN11" x="7.62" y="63.5" length="short" direction="nc" rot="R270"/>
<pin name="VC1" x="-30.48" y="-22.86" length="short" direction="in"/>
<pin name="VC2" x="-30.48" y="-2.54" length="short" direction="in"/>
<pin name="VC3" x="-30.48" y="17.78" length="short" direction="in"/>
<pin name="VC4" x="-30.48" y="38.1" length="short" direction="in"/>
<pin name="VC5" x="-30.48" y="58.42" length="short" direction="in"/>
<pin name="SRN" x="0" y="-76.2" length="short" direction="in" rot="R90"/>
<pin name="SRP" x="-20.32" y="-76.2" length="short" direction="in" rot="R90"/>
<text x="-12.7" y="-30.48" size="4.826" layer="94" rot="R90">Battery Terminal Inputs</text>
<pin name="DSG" x="25.4" y="-68.58" length="short" direction="out" rot="R180"/>
<pin name="CHG" x="25.4" y="-60.96" length="short" direction="out" rot="R180"/>
<text x="-17.78" y="-66.04" size="3.302" layer="94">Current
Sense</text>
</symbol>
<symbol name="CC2564MODA">
<wire x1="-27.94" y1="35.56" x2="-27.94" y2="-38.1" width="0.1524" layer="94"/>
<wire x1="-27.94" y1="-38.1" x2="27.94" y2="-38.1" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-38.1" x2="27.94" y2="35.56" width="0.1524" layer="94"/>
<wire x1="27.94" y1="35.56" x2="-27.94" y2="35.56" width="0.1524" layer="94"/>
<text x="-25.0444" y="39.5986" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-25.9842" y="37.0586" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<text x="-12.7" y="-25.4" size="3.81" layer="94">GND PADs</text>
<pin name="TX_DBG" x="30.48" y="30.48" length="short" direction="out" rot="R180"/>
<text x="2.54" y="-15.24" size="5.588" layer="94" rot="R90">C2564MODA</text>
<pin name="HCI_CTS" x="-30.48" y="10.16" length="short" direction="in"/>
<pin name="PGND_25" x="-25.4" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="PGND_26" x="-20.32" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="PGND_27" x="-15.24" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="PGND_28" x="-10.16" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="PGND_29" x="-5.08" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="HCI_TX" x="-30.48" y="0" length="short" direction="out"/>
<pin name="HCI_RX" x="-30.48" y="-7.62" length="short" direction="in"/>
<pin name="HCI_RTS" x="-30.48" y="-15.24" length="short" direction="out"/>
<pin name="NC_PIN_6" x="5.08" y="38.1" length="short" direction="nc" rot="R270"/>
<pin name="NC_PIN_10" x="7.62" y="38.1" length="short" direction="nc" rot="R270"/>
<pin name="NC_PIN_11" x="10.16" y="38.1" length="short" direction="nc" rot="R270"/>
<pin name="NC_PIN_14" x="12.7" y="38.1" length="short" direction="nc" rot="R270"/>
<pin name="NC_PIN_23" x="15.24" y="38.1" length="short" direction="nc" rot="R270"/>
<pin name="PGND_30" x="0" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="PGND_31" x="5.08" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="PGND_32" x="10.16" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="PGND_33" x="15.24" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="PGND_34" x="20.32" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="PGND_35" x="25.4" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="VDD_IN" x="-30.48" y="33.02" length="short" direction="pwr"/>
<pin name="SLOW_CLK_IN" x="-30.48" y="20.32" length="short" direction="in"/>
<pin name="GND_5" x="30.48" y="-10.16" length="short" direction="pwr" rot="R180"/>
<pin name="GND_7" x="30.48" y="-12.7" length="short" direction="pwr" rot="R180"/>
<pin name="GND_9" x="30.48" y="-15.24" length="short" direction="pwr" rot="R180"/>
<pin name="GND_13" x="30.48" y="-17.78" length="short" direction="pwr" rot="R180"/>
<pin name="GND_15" x="30.48" y="-20.32" length="short" direction="pwr" rot="R180"/>
<pin name="GND_17" x="30.48" y="-22.86" length="short" direction="pwr" rot="R180"/>
<text x="15.24" y="-22.86" size="3.81" layer="94" rot="R90">GNDs</text>
<pin name="AUD_IN" x="30.48" y="22.86" length="short" direction="in" rot="R180"/>
<pin name="AUD_OUT" x="30.48" y="17.78" length="short" direction="out" rot="R180"/>
<pin name="AUD_CLK" x="30.48" y="12.7" length="short" rot="R180"/>
<pin name="AUD_FSYNC" x="30.48" y="7.62" length="short" rot="R180"/>
<pin name="VDD_IO" x="-30.48" y="27.94" length="short" direction="pwr"/>
<pin name="NSHUTD" x="-30.48" y="-22.86" length="short" direction="in"/>
<text x="-10.16" y="-12.7" size="3.81" layer="94" rot="R90">HCI Comm.</text>
<text x="12.7" y="2.54" size="2.54" layer="94" rot="R90">AUD Comm.</text>
</symbol>
<symbol name="MPU-6050_CUSTOM_SYMBOL">
<wire x1="-33.02" y1="22.86" x2="30.48" y2="22.86" width="0.254" layer="94"/>
<wire x1="30.48" y1="22.86" x2="30.48" y2="-25.4" width="0.254" layer="94"/>
<wire x1="30.48" y1="-25.4" x2="-33.02" y2="-25.4" width="0.254" layer="94"/>
<text x="-33.02" y="25.781" size="1.27" layer="95">&gt;NAME</text>
<text x="-33.02" y="23.495" size="1.27" layer="96">&gt;VALUE</text>
<pin name="13_VDD" x="-15.24" y="25.4" visible="pin" length="short" direction="pwr" rot="R270"/>
<pin name="1_CLKIN" x="-15.24" y="-27.94" visible="pin" length="short" direction="in" function="clk" rot="R90"/>
<pin name="24_SDA" x="-35.56" y="5.08" visible="pin" length="short"/>
<pin name="23_SCL" x="-35.56" y="15.24" visible="pin" length="short"/>
<pin name="6_AUX_DA" x="33.02" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="7_AUX_CL" x="33.02" y="-12.7" visible="pin" length="short" function="clk" rot="R180"/>
<pin name="8_VLOGIC" x="-2.54" y="25.4" visible="pin" length="short" direction="pwr" rot="R270"/>
<pin name="9_AD0" x="-35.56" y="-5.08" visible="pin" length="short"/>
<pin name="10_REGOUT" x="5.08" y="-27.94" visible="pin" length="short" rot="R90"/>
<pin name="11_FSYNC" x="-35.56" y="-15.24" visible="pin" length="short" direction="in"/>
<pin name="12_INT" x="-35.56" y="-22.86" visible="pin" length="short" direction="out"/>
<pin name="18_GND" x="15.24" y="-27.94" visible="pin" length="short" rot="R90"/>
<pin name="20_CPOUT" x="-5.08" y="-27.94" visible="pin" length="short" direction="out" rot="R90"/>
<wire x1="-33.02" y1="-25.4" x2="-33.02" y2="22.86" width="0.254" layer="94"/>
<pin name="19_RESV" x="27.94" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<pin name="21_RESV" x="25.4" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<pin name="22_RESV" x="22.86" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<pin name="2_NC" x="20.32" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<pin name="3_NC" x="17.78" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<pin name="4_NC" x="15.24" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<pin name="5_NC" x="12.7" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<pin name="14_NC" x="10.16" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<pin name="15_NC" x="7.62" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<pin name="16_NC" x="5.08" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<pin name="17_NC" x="2.54" y="25.4" visible="pin" length="short" direction="nc" rot="R270"/>
<text x="-20.32" y="-5.08" size="5.08" layer="94">MPU-6050</text>
</symbol>
<symbol name="FT2232D_UNFINISHED">
<wire x1="-25.4" y1="88.9" x2="-25.4" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-66.04" x2="25.4" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-66.04" x2="25.4" y2="88.9" width="0.1524" layer="94"/>
<wire x1="25.4" y1="88.9" x2="-25.4" y2="88.9" width="0.1524" layer="94"/>
<text x="-22.5044" y="92.9386" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-23.4442" y="90.3986" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="GND_34" x="10.16" y="-68.58" length="short" direction="pwr" rot="R90"/>
<text x="-5.08" y="-53.34" size="3.81" layer="94">GND</text>
<pin name="USBDM" x="-27.94" y="55.88" length="short" direction="in"/>
<pin name="VCCIOA" x="7.62" y="91.44" length="short" direction="pwr" rot="R270"/>
<pin name="GND_25" x="5.08" y="-68.58" length="short" direction="pwr" rot="R90"/>
<pin name="GND_18" x="0" y="-68.58" length="short" direction="pwr" rot="R90"/>
<pin name="GND_9" x="-5.08" y="-68.58" length="short" direction="pwr" rot="R90"/>
<pin name="AGND_45" x="-10.16" y="-68.58" length="short" direction="pwr" rot="R90"/>
<pin name="VCCIOB" x="12.7" y="91.44" length="short" direction="pwr" rot="R270"/>
<pin name="VCC_42" x="2.54" y="91.44" length="short" direction="pwr" rot="R270"/>
<pin name="VCC_3" x="-2.54" y="91.44" length="short" direction="pwr" rot="R270"/>
<pin name="AVCC" x="-7.62" y="91.44" length="short" direction="pwr" rot="R270"/>
<pin name="3V3OUT" x="20.32" y="91.44" length="short" direction="out" rot="R270"/>
<pin name="USBDP" x="-27.94" y="45.72" length="short" direction="in"/>
<pin name="RSTOUT#" x="-27.94" y="35.56" length="short" direction="out"/>
<pin name="RESET#" x="-27.94" y="25.4" length="short" direction="in"/>
<pin name="XTIN" x="-27.94" y="15.24" length="short" direction="in" function="clk"/>
<pin name="XTOUT" x="-27.94" y="5.08" length="short" direction="out"/>
<pin name="EECS" x="-27.94" y="-5.08" length="short"/>
<pin name="EESK" x="-27.94" y="-15.24" length="short" direction="out"/>
<pin name="EEDATA" x="-27.94" y="-25.4" length="short"/>
<pin name="TEST" x="-27.94" y="-35.56" length="short" direction="in"/>
<pin name="PWREN#" x="-27.94" y="-45.72" length="short" direction="out"/>
<pin name="BCBUS3" x="27.94" y="-58.42" length="short" rot="R180"/>
<pin name="BCBUS1" x="27.94" y="-48.26" length="short" rot="R180"/>
<pin name="BCBUS2" x="27.94" y="-53.34" length="short" rot="R180"/>
<pin name="BCBUS0" x="27.94" y="-43.18" length="short" rot="R180"/>
<pin name="BDBUS0" x="27.94" y="0" length="short" rot="R180"/>
<pin name="BDBUS1" x="27.94" y="-5.08" length="short" rot="R180"/>
<pin name="BDBUS2" x="27.94" y="-10.16" length="short" rot="R180"/>
<pin name="BDBUS3" x="27.94" y="-15.24" length="short" rot="R180"/>
<pin name="BDBUS4" x="27.94" y="-20.32" length="short" rot="R180"/>
<pin name="BDBUS5" x="27.94" y="-25.4" length="short" rot="R180"/>
<pin name="BDBUS6" x="27.94" y="-30.48" length="short" rot="R180"/>
<pin name="BDBUS7" x="27.94" y="-35.56" length="short" rot="R180"/>
<pin name="SI/WUA" x="-27.94" y="-53.34" length="short"/>
<pin name="ACBUS0" x="27.94" y="27.94" length="short" rot="R180"/>
<pin name="ACBUS1" x="27.94" y="22.86" length="short" rot="R180"/>
<pin name="ACBUS2" x="27.94" y="17.78" length="short" rot="R180"/>
<pin name="ACBUS3" x="27.94" y="12.7" length="short" rot="R180"/>
<pin name="ADBUS0" x="27.94" y="73.66" length="short" rot="R180"/>
<pin name="ADBUS1" x="27.94" y="68.58" length="short" rot="R180"/>
<pin name="ADBUS2" x="27.94" y="63.5" length="short" rot="R180"/>
<pin name="ADBUS3" x="27.94" y="58.42" length="short" rot="R180"/>
<pin name="ADBUS4" x="27.94" y="53.34" length="short" rot="R180"/>
<pin name="ADBUS5" x="27.94" y="48.26" length="short" rot="R180"/>
<pin name="ADBUS6" x="27.94" y="43.18" length="short" rot="R180"/>
<pin name="ADBUS7" x="27.94" y="38.1" length="short" rot="R180"/>
<pin name="SI/WUB" x="-27.94" y="-63.5" length="short"/>
</symbol>
<symbol name="TPS55332_Q1">
<pin name="RST" x="-27.94" y="-7.62" length="short" direction="out"/>
<wire x1="-25.4" y1="63.5" x2="-25.4" y2="-45.72" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-45.72" x2="25.4" y2="-45.72" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-45.72" x2="25.4" y2="63.5" width="0.1524" layer="94"/>
<wire x1="25.4" y1="63.5" x2="-25.4" y2="63.5" width="0.1524" layer="94"/>
<text x="-22.5044" y="64.9986" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-3.1242" y="16.7386" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="RSLEW" x="27.94" y="-38.1" length="short" direction="out" rot="R180"/>
<pin name="BOOT" x="27.94" y="-20.32" length="short" direction="out" rot="R180"/>
<pin name="VREG" x="27.94" y="50.8" length="short" direction="in" rot="R180"/>
<pin name="GND1" x="-20.32" y="-48.26" length="short" direction="pwr" rot="R90"/>
<pin name="GND3" x="-5.08" y="-48.26" length="short" direction="pwr" rot="R90"/>
<text x="-7.62" y="-35.56" size="3.81" layer="94">GND</text>
<pin name="RST_TH" x="27.94" y="-5.08" length="short" direction="in" rot="R180"/>
<pin name="SW" x="27.94" y="60.96" length="short" rot="R180"/>
<pin name="SYNC" x="-27.94" y="2.54" length="short" direction="in"/>
<pin name="CDLY" x="-27.94" y="22.86" length="short" direction="out"/>
<pin name="RT" x="-27.94" y="43.18" length="short" direction="out"/>
<pin name="EN" x="-27.94" y="53.34" length="short" direction="in"/>
<pin name="VIN" x="-27.94" y="60.96" length="short" direction="pwr"/>
<pin name="GND2" x="-12.7" y="-48.26" length="short" direction="pwr" rot="R90"/>
<pin name="SS" x="-27.94" y="-17.78" length="short" direction="out"/>
<pin name="VSENSE" x="27.94" y="30.48" length="short" direction="in" rot="R180"/>
<pin name="COMP" x="27.94" y="10.16" length="short" direction="out" rot="R180"/>
<pin name="PGND" x="5.08" y="-48.26" length="short" direction="pwr" rot="R90"/>
<text x="5.08" y="-22.86" size="8.382" layer="94" ratio="6" rot="R90">TPS55332-Q1</text>
<pin name="NC1" x="15.24" y="66.04" length="short" direction="nc" rot="R270"/>
<pin name="NC2" x="7.62" y="66.04" length="short" direction="nc" rot="R270"/>
<pin name="PAD" x="12.7" y="-48.26" length="short" direction="pwr" rot="R90"/>
</symbol>
<symbol name="SN74LVC125">
<wire x1="5.08" y1="0" x2="0" y2="2.54" width="0.4064" layer="94"/>
<wire x1="0" y1="2.54" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<text x="-15.24" y="36.195" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.24" y="33.02" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2A_IN" x="-10.16" y="0" visible="pad" length="middle" direction="in"/>
<pin name="2Y_OUT" x="10.16" y="0" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="0" y1="2.54" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="2OE" x="-10.16" y="7.62" visible="pad" length="middle" direction="in"/>
<wire x1="5.08" y1="20.32" x2="0" y2="22.86" width="0.4064" layer="94"/>
<wire x1="0" y1="22.86" x2="-5.08" y2="25.4" width="0.4064" layer="94"/>
<wire x1="5.08" y1="20.32" x2="-5.08" y2="15.24" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="15.24" x2="-5.08" y2="25.4" width="0.4064" layer="94"/>
<pin name="1A_IN" x="-10.16" y="20.32" visible="pad" length="middle" direction="in"/>
<pin name="1Y_OUT" x="10.16" y="20.32" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="0" y1="22.86" x2="0" y2="27.94" width="0.254" layer="94"/>
<wire x1="0" y1="27.94" x2="-10.16" y2="27.94" width="0.254" layer="94"/>
<pin name="1OE" x="-10.16" y="27.94" visible="pad" length="middle" direction="in"/>
<wire x1="5.08" y1="-20.32" x2="0" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="0" y1="-17.78" x2="-5.08" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="-5.08" y2="-25.4" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-25.4" x2="-5.08" y2="-15.24" width="0.4064" layer="94"/>
<pin name="3A_IN" x="-10.16" y="-20.32" visible="pad" length="middle" direction="in"/>
<pin name="3Y_OUT" x="10.16" y="-20.32" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="0" y1="-17.78" x2="0" y2="-12.7" width="0.254" layer="94"/>
<wire x1="0" y1="-12.7" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<pin name="3OE" x="-10.16" y="-12.7" visible="pad" length="middle" direction="in"/>
<wire x1="5.08" y1="-40.64" x2="0" y2="-38.1" width="0.4064" layer="94"/>
<wire x1="0" y1="-38.1" x2="-5.08" y2="-35.56" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-40.64" x2="-5.08" y2="-45.72" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-45.72" x2="-5.08" y2="-35.56" width="0.4064" layer="94"/>
<pin name="4A_IN" x="-10.16" y="-40.64" visible="pad" length="middle" direction="in"/>
<pin name="4Y_OUT" x="10.16" y="-40.64" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="0" y1="-38.1" x2="0" y2="-33.02" width="0.254" layer="94"/>
<wire x1="0" y1="-33.02" x2="-10.16" y2="-33.02" width="0.254" layer="94"/>
<pin name="4OE" x="-10.16" y="-33.02" visible="pad" length="middle" direction="in"/>
<pin name="VCC" x="0" y="35.56" visible="pad" length="short" direction="pwr" rot="R270"/>
<wire x1="10.16" y1="33.02" x2="-10.16" y2="33.02" width="0.254" layer="94"/>
<text x="-2.54" y="30.48" size="1.27" layer="94">PWR</text>
<wire x1="10.16" y1="-50.8" x2="-10.16" y2="-50.8" width="0.254" layer="94"/>
<pin name="GND" x="0" y="-53.34" visible="pad" length="short" direction="pwr" rot="R90"/>
<text x="-2.54" y="-48.26" size="1.27" layer="94">GND</text>
<circle x="0" y="22.86" radius="2.54" width="0.254" layer="94"/>
<circle x="0" y="2.54" radius="2.54" width="0.254" layer="94"/>
<circle x="0" y="-17.78" radius="2.54" width="0.254" layer="94"/>
<circle x="0" y="-38.1" radius="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="SN74LVC126">
<wire x1="5.08" y1="0" x2="0" y2="2.54" width="0.4064" layer="94"/>
<wire x1="0" y1="2.54" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<text x="-15.24" y="36.195" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.24" y="33.02" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2A_IN" x="-10.16" y="0" visible="pad" length="middle" direction="in"/>
<pin name="2Y_OUT" x="10.16" y="0" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="0" y1="2.54" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="2OE" x="-10.16" y="7.62" visible="pad" length="middle" direction="in"/>
<wire x1="5.08" y1="20.32" x2="0" y2="22.86" width="0.4064" layer="94"/>
<wire x1="0" y1="22.86" x2="-5.08" y2="25.4" width="0.4064" layer="94"/>
<wire x1="5.08" y1="20.32" x2="-5.08" y2="15.24" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="15.24" x2="-5.08" y2="25.4" width="0.4064" layer="94"/>
<pin name="1A_IN" x="-10.16" y="20.32" visible="pad" length="middle" direction="in"/>
<pin name="1Y_OUT" x="10.16" y="20.32" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="0" y1="22.86" x2="0" y2="27.94" width="0.254" layer="94"/>
<wire x1="0" y1="27.94" x2="-10.16" y2="27.94" width="0.254" layer="94"/>
<pin name="1OE" x="-10.16" y="27.94" visible="pad" length="middle" direction="in"/>
<wire x1="5.08" y1="-20.32" x2="0" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="0" y1="-17.78" x2="-5.08" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="-5.08" y2="-25.4" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-25.4" x2="-5.08" y2="-15.24" width="0.4064" layer="94"/>
<pin name="3A_IN" x="-10.16" y="-20.32" visible="pad" length="middle" direction="in"/>
<pin name="3Y_OUT" x="10.16" y="-20.32" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="0" y1="-17.78" x2="0" y2="-12.7" width="0.254" layer="94"/>
<wire x1="0" y1="-12.7" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<pin name="3OE" x="-10.16" y="-12.7" visible="pad" length="middle" direction="in"/>
<wire x1="5.08" y1="-40.64" x2="0" y2="-38.1" width="0.4064" layer="94"/>
<wire x1="0" y1="-38.1" x2="-5.08" y2="-35.56" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-40.64" x2="-5.08" y2="-45.72" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-45.72" x2="-5.08" y2="-35.56" width="0.4064" layer="94"/>
<pin name="4A_IN" x="-10.16" y="-40.64" visible="pad" length="middle" direction="in"/>
<pin name="4Y_OUT" x="10.16" y="-40.64" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="0" y1="-38.1" x2="0" y2="-33.02" width="0.254" layer="94"/>
<wire x1="0" y1="-33.02" x2="-10.16" y2="-33.02" width="0.254" layer="94"/>
<pin name="4OE" x="-10.16" y="-33.02" visible="pad" length="middle" direction="in"/>
<pin name="VCC" x="0" y="35.56" visible="pad" length="short" direction="pwr" rot="R270"/>
<wire x1="10.16" y1="33.02" x2="-10.16" y2="33.02" width="0.254" layer="94"/>
<text x="-2.54" y="30.48" size="1.27" layer="94">PWR</text>
<wire x1="10.16" y1="-50.8" x2="-10.16" y2="-50.8" width="0.254" layer="94"/>
<pin name="GND" x="0" y="-53.34" visible="pad" length="short" direction="pwr" rot="R90"/>
<text x="-2.54" y="-48.26" size="1.27" layer="94">GND</text>
</symbol>
<symbol name="XETHRU_X2">
<pin name="IREF_DAC" x="43.18" y="12.7" length="short" rot="R180"/>
<pin name="GNDA_DAC" x="-25.4" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="VDDA25_DAC" x="-25.4" y="33.02" length="short" direction="pwr" rot="R270"/>
<pin name="VDDA_HSC_PIN4" x="12.7" y="33.02" length="short" direction="pwr" rot="R270"/>
<pin name="VDDA_HSC_PIN5" x="15.24" y="33.02" length="short" direction="pwr" rot="R270"/>
<pin name="NC" x="43.18" y="-33.02" length="short" direction="nc" rot="R180"/>
<pin name="GNDA_HSC_PIN7" x="12.7" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="GNDA_HSC_PIN8" x="15.24" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="VDDA_RX_PIN9" x="2.54" y="33.02" length="short" direction="pwr" rot="R270"/>
<pin name="GNDA_RX_PIN10" x="2.54" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="RFIN" x="-43.18" y="5.08" length="short" direction="in"/>
<pin name="GNDA_RX_PIN12" x="5.08" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="VDDA_RX_PIN13" x="5.08" y="33.02" length="short" direction="pwr" rot="R270"/>
<pin name="NARST" x="-43.18" y="-30.48" length="short"/>
<pin name="VDDD_PIN15" x="22.86" y="33.02" length="short" direction="pwr" rot="R270"/>
<pin name="VDDD_PIN16" x="25.4" y="33.02" length="short" direction="pwr" rot="R270"/>
<pin name="RESERVED" x="43.18" y="-35.56" length="short" direction="nc" rot="R180"/>
<pin name="MISO" x="-43.18" y="0" length="short"/>
<pin name="MOSI" x="-43.18" y="-7.62" length="short"/>
<pin name="SCLK" x="-43.18" y="-15.24" length="short" direction="in" function="clk"/>
<pin name="NSS" x="-43.18" y="-22.86" length="short" direction="in"/>
<pin name="EXTCLK" x="43.18" y="-25.4" length="short" function="clk" rot="R180"/>
<pin name="GNDDIO" x="-20.32" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="VDDD25_IO" x="-20.32" y="33.02" length="short" direction="pwr" rot="R270"/>
<pin name="VDDD_TCTRL" x="-15.24" y="33.02" length="short" direction="pwr" rot="R270"/>
<pin name="GNDD_TCTRL" x="-15.24" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="CLK_OUT" x="43.18" y="-15.24" length="short" direction="out" function="clk" rot="R180"/>
<pin name="VDDD_PIN28" x="27.94" y="33.02" length="short" direction="pwr" rot="R270"/>
<pin name="GNDD_TX_PIN29" x="-7.62" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="RFOUT" x="43.18" y="-7.62" length="short" direction="out" rot="R180"/>
<pin name="GNDD_TX_PIN31" x="-5.08" y="-40.64" length="short" direction="pwr" rot="R90"/>
<pin name="VDDD_TX" x="-5.08" y="33.02" length="short" direction="pwr" rot="R270"/>
<text x="-38.1" y="33.02" size="1.27" layer="94">&lt;NAME</text>
<text x="-22.86" y="-7.62" size="6.35" layer="94">XETHRU_X2</text>
<wire x1="-40.64" y1="30.48" x2="-40.64" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-40.64" y1="-38.1" x2="40.64" y2="-38.1" width="0.254" layer="94"/>
<wire x1="40.64" y1="-38.1" x2="40.64" y2="30.48" width="0.254" layer="94"/>
<wire x1="40.64" y1="30.48" x2="-40.64" y2="30.48" width="0.254" layer="94"/>
<text x="-25.4" y="-15.24" size="5.08" layer="94" rot="R90">SPI</text>
<text x="-15.24" y="-17.78" size="5.08" layer="94">GNDs</text>
<text x="-15.24" y="7.62" size="5.08" layer="94">PWR</text>
</symbol>
<symbol name="TPD2EUSB30DRTR">
<pin name="D+" x="-10.16" y="2.54" length="middle"/>
<pin name="D-" x="-10.16" y="-2.54" length="middle"/>
<pin name="GND" x="10.16" y="0" length="middle" rot="R180"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="93LC46B-I_MS-ND">
<pin name="CS" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="CLK" x="12.7" y="10.16" length="middle" rot="R180"/>
<pin name="DI" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="DO" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="VSS" x="0" y="-12.7" length="middle" rot="R90"/>
<pin name="ORG*" x="-10.16" y="0" length="middle"/>
<pin name="NC" x="-10.16" y="5.08" length="middle"/>
<pin name="VCC" x="-10.16" y="10.16" length="middle"/>
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="-5.08" y2="12.7" width="0.254" layer="94"/>
<text x="-7.62" y="15.24" size="1.778" layer="94">93LC46B-I_MS-ND</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BQ24616">
<gates>
<gate name="G$1" symbol="BQ24616" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="VQFN_RGE_24">
<connects>
<connect gate="G$1" pin="ACDRV" pad="3"/>
<connect gate="G$1" pin="ACN" pad="1"/>
<connect gate="G$1" pin="ACP" pad="2"/>
<connect gate="G$1" pin="ACSET" pad="16"/>
<connect gate="G$1" pin="BATDRV" pad="23"/>
<connect gate="G$1" pin="BTST" pad="22"/>
<connect gate="G$1" pin="CE" pad="4"/>
<connect gate="G$1" pin="GND" pad="17"/>
<connect gate="G$1" pin="HIDRV" pad="21"/>
<connect gate="G$1" pin="ISET1" pad="11"/>
<connect gate="G$1" pin="ISET2" pad="15"/>
<connect gate="G$1" pin="LODRV" pad="19"/>
<connect gate="G$1" pin="PAD_GND1" pad="PAD_GND PAD_VIA1 PAD_VIA2 PAD_VIA3 PAD_VIA4"/>
<connect gate="G$1" pin="PG" pad="8"/>
<connect gate="G$1" pin="PH" pad="20"/>
<connect gate="G$1" pin="REGN" pad="18"/>
<connect gate="G$1" pin="SRN" pad="13"/>
<connect gate="G$1" pin="SRP" pad="14"/>
<connect gate="G$1" pin="STAT1" pad="5"/>
<connect gate="G$1" pin="STAT2" pad="9"/>
<connect gate="G$1" pin="TS" pad="6"/>
<connect gate="G$1" pin="TTC" pad="7"/>
<connect gate="G$1" pin="VCC" pad="24"/>
<connect gate="G$1" pin="VFB" pad="12"/>
<connect gate="G$1" pin="VREF" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BQ76920">
<gates>
<gate name="G$1" symbol="BQ76920" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="PW20">
<connects>
<connect gate="G$1" pin="ALERT" pad="20"/>
<connect gate="G$1" pin="BAT" pad="10"/>
<connect gate="G$1" pin="CAP1" pad="7"/>
<connect gate="G$1" pin="CHG" pad="2"/>
<connect gate="G$1" pin="DSG" pad="1"/>
<connect gate="G$1" pin="NC_PIN11" pad="11"/>
<connect gate="G$1" pin="REGOUT" pad="8"/>
<connect gate="G$1" pin="REGSRC" pad="9"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="SRN" pad="19"/>
<connect gate="G$1" pin="SRP" pad="18"/>
<connect gate="G$1" pin="TS1" pad="6"/>
<connect gate="G$1" pin="VC0" pad="17"/>
<connect gate="G$1" pin="VC1" pad="16"/>
<connect gate="G$1" pin="VC2" pad="15"/>
<connect gate="G$1" pin="VC3" pad="14"/>
<connect gate="G$1" pin="VC4" pad="13"/>
<connect gate="G$1" pin="VC5" pad="12"/>
<connect gate="G$1" pin="VSS" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CC2564MODA">
<gates>
<gate name="G$1" symbol="CC2564MODA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN_MOG_35">
<connects>
<connect gate="G$1" pin="AUD_CLK" pad="21"/>
<connect gate="G$1" pin="AUD_FSYNC" pad="22"/>
<connect gate="G$1" pin="AUD_IN" pad="19"/>
<connect gate="G$1" pin="AUD_OUT" pad="20"/>
<connect gate="G$1" pin="GND_13" pad="13"/>
<connect gate="G$1" pin="GND_15" pad="15"/>
<connect gate="G$1" pin="GND_17" pad="17"/>
<connect gate="G$1" pin="GND_5" pad="5"/>
<connect gate="G$1" pin="GND_7" pad="7"/>
<connect gate="G$1" pin="GND_9" pad="9"/>
<connect gate="G$1" pin="HCI_CTS" pad="1"/>
<connect gate="G$1" pin="HCI_RTS" pad="4"/>
<connect gate="G$1" pin="HCI_RX" pad="3"/>
<connect gate="G$1" pin="HCI_TX" pad="2"/>
<connect gate="G$1" pin="NC_PIN_10" pad="10"/>
<connect gate="G$1" pin="NC_PIN_11" pad="11"/>
<connect gate="G$1" pin="NC_PIN_14" pad="14"/>
<connect gate="G$1" pin="NC_PIN_23" pad="23"/>
<connect gate="G$1" pin="NC_PIN_6" pad="6"/>
<connect gate="G$1" pin="NSHUTD" pad="16"/>
<connect gate="G$1" pin="PGND_25" pad="25 PAD_VIA_25"/>
<connect gate="G$1" pin="PGND_26" pad="26 PAD_VIA_26"/>
<connect gate="G$1" pin="PGND_27" pad="27 PAD_VIA_27"/>
<connect gate="G$1" pin="PGND_28" pad="28 PAD_VIA_28"/>
<connect gate="G$1" pin="PGND_29" pad="29 PAD_VIA_29"/>
<connect gate="G$1" pin="PGND_30" pad="30"/>
<connect gate="G$1" pin="PGND_31" pad="31"/>
<connect gate="G$1" pin="PGND_32" pad="32"/>
<connect gate="G$1" pin="PGND_33" pad="33"/>
<connect gate="G$1" pin="PGND_34" pad="34"/>
<connect gate="G$1" pin="PGND_35" pad="35"/>
<connect gate="G$1" pin="SLOW_CLK_IN" pad="8"/>
<connect gate="G$1" pin="TX_DBG" pad="24"/>
<connect gate="G$1" pin="VDD_IN" pad="12"/>
<connect gate="G$1" pin="VDD_IO" pad="18"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MPU-6050_CUSTOM_SYMBOL">
<gates>
<gate name="G$1" symbol="MPU-6050_CUSTOM_SYMBOL" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="QFN.24.4X4__MPU-6050">
<connects>
<connect gate="G$1" pin="10_REGOUT" pad="10"/>
<connect gate="G$1" pin="11_FSYNC" pad="11"/>
<connect gate="G$1" pin="12_INT" pad="12"/>
<connect gate="G$1" pin="13_VDD" pad="13"/>
<connect gate="G$1" pin="14_NC" pad="14"/>
<connect gate="G$1" pin="15_NC" pad="15"/>
<connect gate="G$1" pin="16_NC" pad="16"/>
<connect gate="G$1" pin="17_NC" pad="17"/>
<connect gate="G$1" pin="18_GND" pad="18"/>
<connect gate="G$1" pin="19_RESV" pad="19"/>
<connect gate="G$1" pin="1_CLKIN" pad="1"/>
<connect gate="G$1" pin="20_CPOUT" pad="20"/>
<connect gate="G$1" pin="21_RESV" pad="21"/>
<connect gate="G$1" pin="22_RESV" pad="22"/>
<connect gate="G$1" pin="23_SCL" pad="23"/>
<connect gate="G$1" pin="24_SDA" pad="24"/>
<connect gate="G$1" pin="2_NC" pad="2"/>
<connect gate="G$1" pin="3_NC" pad="3"/>
<connect gate="G$1" pin="4_NC" pad="4"/>
<connect gate="G$1" pin="5_NC" pad="5"/>
<connect gate="G$1" pin="6_AUX_DA" pad="6"/>
<connect gate="G$1" pin="7_AUX_CL" pad="7"/>
<connect gate="G$1" pin="8_VLOGIC" pad="8"/>
<connect gate="G$1" pin="9_AD0" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FT2232D">
<gates>
<gate name="G$1" symbol="FT2232D_UNFINISHED" x="0" y="-15.24"/>
</gates>
<devices>
<device name="" package="FT2232D">
<connects>
<connect gate="G$1" pin="3V3OUT" pad="6"/>
<connect gate="G$1" pin="ACBUS0" pad="15"/>
<connect gate="G$1" pin="ACBUS1" pad="13"/>
<connect gate="G$1" pin="ACBUS2" pad="12"/>
<connect gate="G$1" pin="ACBUS3" pad="11"/>
<connect gate="G$1" pin="ADBUS0" pad="24"/>
<connect gate="G$1" pin="ADBUS1" pad="23"/>
<connect gate="G$1" pin="ADBUS2" pad="22"/>
<connect gate="G$1" pin="ADBUS3" pad="21"/>
<connect gate="G$1" pin="ADBUS4" pad="20"/>
<connect gate="G$1" pin="ADBUS5" pad="19"/>
<connect gate="G$1" pin="ADBUS6" pad="17"/>
<connect gate="G$1" pin="ADBUS7" pad="16"/>
<connect gate="G$1" pin="AGND_45" pad="45"/>
<connect gate="G$1" pin="AVCC" pad="46"/>
<connect gate="G$1" pin="BCBUS0" pad="30"/>
<connect gate="G$1" pin="BCBUS1" pad="29"/>
<connect gate="G$1" pin="BCBUS2" pad="28"/>
<connect gate="G$1" pin="BCBUS3" pad="27"/>
<connect gate="G$1" pin="BDBUS0" pad="40"/>
<connect gate="G$1" pin="BDBUS1" pad="39"/>
<connect gate="G$1" pin="BDBUS2" pad="38"/>
<connect gate="G$1" pin="BDBUS3" pad="37"/>
<connect gate="G$1" pin="BDBUS4" pad="36"/>
<connect gate="G$1" pin="BDBUS5" pad="35"/>
<connect gate="G$1" pin="BDBUS6" pad="33"/>
<connect gate="G$1" pin="BDBUS7" pad="32"/>
<connect gate="G$1" pin="EECS" pad="48"/>
<connect gate="G$1" pin="EEDATA" pad="2"/>
<connect gate="G$1" pin="EESK" pad="1"/>
<connect gate="G$1" pin="GND_18" pad="18"/>
<connect gate="G$1" pin="GND_25" pad="25"/>
<connect gate="G$1" pin="GND_34" pad="34"/>
<connect gate="G$1" pin="GND_9" pad="9"/>
<connect gate="G$1" pin="PWREN#" pad="41"/>
<connect gate="G$1" pin="RESET#" pad="4"/>
<connect gate="G$1" pin="RSTOUT#" pad="5"/>
<connect gate="G$1" pin="SI/WUA" pad="10"/>
<connect gate="G$1" pin="SI/WUB" pad="26"/>
<connect gate="G$1" pin="TEST" pad="47"/>
<connect gate="G$1" pin="USBDM" pad="8"/>
<connect gate="G$1" pin="USBDP" pad="7"/>
<connect gate="G$1" pin="VCCIOA" pad="14"/>
<connect gate="G$1" pin="VCCIOB" pad="31"/>
<connect gate="G$1" pin="VCC_3" pad="3"/>
<connect gate="G$1" pin="VCC_42" pad="42"/>
<connect gate="G$1" pin="XTIN" pad="43"/>
<connect gate="G$1" pin="XTOUT" pad="44"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPS55332-Q1">
<gates>
<gate name="A" symbol="TPS55332_Q1" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="PWP20_2P45X3P2">
<connects>
<connect gate="A" pin="BOOT" pad="20"/>
<connect gate="A" pin="CDLY" pad="9"/>
<connect gate="A" pin="COMP" pad="15"/>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="GND1" pad="4"/>
<connect gate="A" pin="GND2" pad="10"/>
<connect gate="A" pin="GND3" pad="12"/>
<connect gate="A" pin="NC1" pad="1"/>
<connect gate="A" pin="NC2" pad="2"/>
<connect gate="A" pin="PAD" pad="PAD"/>
<connect gate="A" pin="PGND" pad="17"/>
<connect gate="A" pin="RSLEW" pad="7"/>
<connect gate="A" pin="RST" pad="8"/>
<connect gate="A" pin="RST_TH" pad="13"/>
<connect gate="A" pin="RT" pad="6"/>
<connect gate="A" pin="SS" pad="11"/>
<connect gate="A" pin="SW" pad="18"/>
<connect gate="A" pin="SYNC" pad="3"/>
<connect gate="A" pin="VIN" pad="19"/>
<connect gate="A" pin="VREG" pad="16"/>
<connect gate="A" pin="VSENSE" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN74LVC125">
<description>inverters</description>
<gates>
<gate name="G$1" symbol="SN74LVC125" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP_PW_14">
<connects>
<connect gate="G$1" pin="1A_IN" pad="2"/>
<connect gate="G$1" pin="1OE" pad="1"/>
<connect gate="G$1" pin="1Y_OUT" pad="3"/>
<connect gate="G$1" pin="2A_IN" pad="5"/>
<connect gate="G$1" pin="2OE" pad="4"/>
<connect gate="G$1" pin="2Y_OUT" pad="6"/>
<connect gate="G$1" pin="3A_IN" pad="9"/>
<connect gate="G$1" pin="3OE" pad="10"/>
<connect gate="G$1" pin="3Y_OUT" pad="8"/>
<connect gate="G$1" pin="4A_IN" pad="12"/>
<connect gate="G$1" pin="4OE" pad="13"/>
<connect gate="G$1" pin="4Y_OUT" pad="11"/>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="VCC" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN74LVC126">
<description>buffers</description>
<gates>
<gate name="G$1" symbol="SN74LVC126" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP_PW_14">
<connects>
<connect gate="G$1" pin="1A_IN" pad="2"/>
<connect gate="G$1" pin="1OE" pad="1"/>
<connect gate="G$1" pin="1Y_OUT" pad="3"/>
<connect gate="G$1" pin="2A_IN" pad="5"/>
<connect gate="G$1" pin="2OE" pad="4"/>
<connect gate="G$1" pin="2Y_OUT" pad="6"/>
<connect gate="G$1" pin="3A_IN" pad="9"/>
<connect gate="G$1" pin="3OE" pad="10"/>
<connect gate="G$1" pin="3Y_OUT" pad="8"/>
<connect gate="G$1" pin="4A_IN" pad="12"/>
<connect gate="G$1" pin="4OE" pad="13"/>
<connect gate="G$1" pin="4Y_OUT" pad="11"/>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="VCC" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XETHRU_X2">
<description>THE EXPOSED DIE PAD ON THE BOTTOM OUGHT TO BE CONNECTED 
GNDD- WHICH IS A DIGITAL GROUND. READ THE DATA SHEET FOR FURTHER 
INFO.</description>
<gates>
<gate name="G$1" symbol="XETHRU_X2" x="17.78" y="2.54"/>
</gates>
<devices>
<device name="" package="QFN32">
<connects>
<connect gate="G$1" pin="CLK_OUT" pad="CLK_OUT"/>
<connect gate="G$1" pin="EXTCLK" pad="EXTCLK"/>
<connect gate="G$1" pin="GNDA_DAC" pad="GNDA_DAC"/>
<connect gate="G$1" pin="GNDA_HSC_PIN7" pad="GNDA_HSC_PIN7"/>
<connect gate="G$1" pin="GNDA_HSC_PIN8" pad="GNDA_HSC_PIN8"/>
<connect gate="G$1" pin="GNDA_RX_PIN10" pad="GNDA_RX_PIN10"/>
<connect gate="G$1" pin="GNDA_RX_PIN12" pad="GNDA_RX_PIN12"/>
<connect gate="G$1" pin="GNDDIO" pad="GNDDIO"/>
<connect gate="G$1" pin="GNDD_TCTRL" pad="GNDD_TCTRL"/>
<connect gate="G$1" pin="GNDD_TX_PIN29" pad="GNDD_TX_PIN29"/>
<connect gate="G$1" pin="GNDD_TX_PIN31" pad="GNDD_TX_PIN31"/>
<connect gate="G$1" pin="IREF_DAC" pad="IREF_DAC"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="NARST" pad="NARST"/>
<connect gate="G$1" pin="NC" pad="NC"/>
<connect gate="G$1" pin="NSS" pad="NSS"/>
<connect gate="G$1" pin="RESERVED" pad="RESERVED"/>
<connect gate="G$1" pin="RFIN" pad="RFIN"/>
<connect gate="G$1" pin="RFOUT" pad="RFOUT"/>
<connect gate="G$1" pin="SCLK" pad="SCLK"/>
<connect gate="G$1" pin="VDDA25_DAC" pad="VDDA_25_DAC"/>
<connect gate="G$1" pin="VDDA_HSC_PIN4" pad="VDDA_HSC_4"/>
<connect gate="G$1" pin="VDDA_HSC_PIN5" pad="VDDA_HSC_PIN5"/>
<connect gate="G$1" pin="VDDA_RX_PIN13" pad="VDDA_RX_PIN13"/>
<connect gate="G$1" pin="VDDA_RX_PIN9" pad="VDDA_RX_PIN9"/>
<connect gate="G$1" pin="VDDD25_IO" pad="VDDD25_IO"/>
<connect gate="G$1" pin="VDDD_PIN15" pad="VDDD_PIN15"/>
<connect gate="G$1" pin="VDDD_PIN16" pad="VDDD_PIN16"/>
<connect gate="G$1" pin="VDDD_PIN28" pad="VDDD"/>
<connect gate="G$1" pin="VDDD_TCTRL" pad="VDDD_TCTRL"/>
<connect gate="G$1" pin="VDDD_TX" pad="VDDD_TX"/>
</connects>
<technologies>
<technology name="">
<attribute name="XETHRUCHIP" value="EV1.0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPD2EUSB30DRTR">
<gates>
<gate name="G$1" symbol="TPD2EUSB30DRTR" x="17.78" y="0"/>
</gates>
<devices>
<device name="TPD2EUSB30DRTR" package="SOT23">
<connects>
<connect gate="G$1" pin="D+" pad="1"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="93LC46B-I_MS-ND__EEPROM">
<gates>
<gate name="G$1" symbol="93LC46B-I_MS-ND" x="33.02" y="0"/>
</gates>
<devices>
<device name="" package="MSOP08">
<connects>
<connect gate="G$1" pin="CLK" pad="2"/>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="DI" pad="3"/>
<connect gate="G$1" pin="DO" pad="4"/>
<connect gate="G$1" pin="NC" pad="7"/>
<connect gate="G$1" pin="ORG*" pad="6"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Resistors_Caps_Inductors_Misc">
<packages>
<package name="SRP7030-2R2M__2P2UH">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-4.613" y1="2.02" x2="4.719" y2="2.018" width="0.0508" layer="39"/>
<wire x1="4.72" y1="-2.025" x2="-4.614" y2="-2.024" width="0.0508" layer="39"/>
<wire x1="-4.614" y1="-2.024" x2="-4.613" y2="2.02" width="0.0508" layer="39"/>
<wire x1="4.719" y1="2.018" x2="4.72" y2="-2.025" width="0.0508" layer="39"/>
<wire x1="-4.875" y1="2.17" x2="4.875" y2="2.17" width="0.1524" layer="21"/>
<wire x1="4.875" y1="2.17" x2="4.875" y2="-2.17" width="0.1524" layer="51"/>
<wire x1="4.875" y1="-2.17" x2="-4.875" y2="-2.17" width="0.1524" layer="21"/>
<wire x1="-4.875" y1="-2.17" x2="-4.875" y2="2.17" width="0.1524" layer="51"/>
<smd name="1" x="-3.1" y="0" dx="2.5" dy="3.5" layer="1"/>
<smd name="2" x="3.1" y="0" dx="2.5" dy="3.5" layer="1"/>
<text x="-0.721" y="0.604" size="0.254" layer="25" distance="5">&gt;NAME</text>
<text x="-0.733" y="-0.778" size="0.254" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-1.5" x2="1" y2="1.5" layer="35"/>
</package>
<package name="MGV06051R0M-10__1UH">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-4.5732" y1="2.1002" x2="4.6732" y2="2.1002" width="0.1016" layer="21"/>
<wire x1="-4.6478" y1="-2.1002" x2="4.5732" y2="-2.1002" width="0.1016" layer="21"/>
<smd name="1" x="-3.1" y="0" dx="2.5" dy="3.5" layer="1"/>
<smd name="2" x="3.1" y="0" dx="2.5" dy="3.5" layer="1"/>
<text x="-3.305" y="2.305" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.405" y="-3.575" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<wire x1="-4.5998" y1="-2.1478" x2="-4.5998" y2="2.1732" width="0.1016" layer="51"/>
<wire x1="4.6002" y1="-2.1478" x2="4.6002" y2="2.1732" width="0.1016" layer="51"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="MSS1278T-223__22UH">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="1" x="-4.25" y="0" dx="4" dy="5.5" layer="1"/>
<smd name="2" x="4.25" y="0" dx="4" dy="5.5" layer="1"/>
<text x="-5.87" y="3.137" size="0.4" layer="25">&gt;NAME</text>
<text x="-3.917" y="3.182" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-6.67" y1="3.075" x2="6.67" y2="3.075" width="0.05" layer="21"/>
<wire x1="6.67" y1="3.075" x2="6.67" y2="-3.075" width="0.05" layer="21"/>
<wire x1="6.67" y1="-3.075" x2="-6.67" y2="-3.075" width="0.05" layer="21"/>
<wire x1="-6.67" y1="-3.075" x2="-6.67" y2="3.075" width="0.05" layer="21"/>
<wire x1="-6.67" y1="3.075" x2="6.67" y2="3.075" width="0.05" layer="39"/>
<wire x1="6.67" y1="3.075" x2="6.67" y2="-3.075" width="0.05" layer="39"/>
<wire x1="6.67" y1="-3.075" x2="-6.67" y2="-3.075" width="0.05" layer="39"/>
<wire x1="-6.67" y1="-3.075" x2="-6.67" y2="3.075" width="0.05" layer="39"/>
<rectangle x1="-1.9" y1="-2.7" x2="1.9" y2="2.7" layer="35"/>
</package>
<package name="MSS1278T-103__10UH">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="1" x="-4.25" y="0" dx="4" dy="5.5" layer="1"/>
<smd name="2" x="4.25" y="0" dx="4" dy="5.5" layer="1"/>
<text x="-5.87" y="3.137" size="0.4" layer="25">&gt;NAME</text>
<text x="-3.917" y="3.182" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-6.67" y1="3.075" x2="6.67" y2="3.075" width="0.05" layer="21"/>
<wire x1="6.67" y1="3.075" x2="6.67" y2="-3.075" width="0.05" layer="21"/>
<wire x1="6.67" y1="-3.075" x2="-6.67" y2="-3.075" width="0.05" layer="21"/>
<wire x1="-6.67" y1="-3.075" x2="-6.67" y2="3.075" width="0.05" layer="21"/>
<wire x1="-6.67" y1="3.075" x2="6.67" y2="3.075" width="0.05" layer="39"/>
<wire x1="6.67" y1="3.075" x2="6.67" y2="-3.075" width="0.05" layer="39"/>
<wire x1="6.67" y1="-3.075" x2="-6.67" y2="-3.075" width="0.05" layer="39"/>
<wire x1="-6.67" y1="-3.075" x2="-6.67" y2="3.075" width="0.05" layer="39"/>
<rectangle x1="-1.9" y1="-2.7" x2="1.9" y2="2.7" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="VLS6045EX-6R8M__6P8UH">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="1" x="-3.05" y="0" dx="1.9" dy="5.1" layer="1"/>
<smd name="2" x="3.05" y="0" dx="1.9" dy="5.1" layer="1"/>
<text x="-4.05" y="2.987" size="0.4" layer="25">&gt;NAME</text>
<text x="-1.837" y="2.992" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-4.35" y1="2.8" x2="4.35" y2="2.8" width="0.05" layer="21"/>
<wire x1="4.35" y1="2.8" x2="4.35" y2="-2.8" width="0.05" layer="21"/>
<wire x1="4.35" y1="-2.8" x2="-4.35" y2="-2.8" width="0.05" layer="21"/>
<wire x1="-4.35" y1="-2.8" x2="-4.35" y2="2.8" width="0.05" layer="21"/>
<wire x1="-4.35" y1="2.8" x2="4.35" y2="2.8" width="0.05" layer="39"/>
<wire x1="4.35" y1="2.8" x2="4.35" y2="-2.8" width="0.05" layer="39"/>
<wire x1="4.35" y1="-2.8" x2="-4.35" y2="-2.8" width="0.05" layer="39"/>
<wire x1="-4.35" y1="-2.8" x2="-4.35" y2="2.8" width="0.05" layer="39"/>
<rectangle x1="-1.6" y1="-2.4" x2="1.6" y2="2.4" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="L-US">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
</symbol>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R-US_CURRENT_SENSE">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="0" y="5.08" size="1.27" layer="94" ratio="6">CS</text>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SRP7030-2R2M__2P2UH">
<gates>
<gate name="A" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SRP7030-2R2M__2P2UH">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MGV06051R0M-10__1P0UH">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MGV06051R0M-10__1UH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GRM21BR61E106KA73L__10UF">
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CL21B106KQQNNNE__10UF">
<gates>
<gate name="A" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0805">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CL31A226KPHNNNE__22UF">
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GRM31CR60J476ME19L__47UF">
<gates>
<gate name="A" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WSL2010R0100FEA__10MOHM_CS">
<gates>
<gate name="G$1" symbol="R-US_CURRENT_SENSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MSS1278T-223__22UH">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MSS1278T-223__22UH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MSS1278T-103__10UH">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MSS1278T-103__10UH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESR03EZPF10R0__10OHM">
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VLS6045EX-6R8M__6P8UH">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VLS6045EX-6R8M__6P8UH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LC_LIB5">
<packages>
<package name="1_175-103LAE-301_NTC_THERM">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="NTC_THERMISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-6.8" y1="-3.6" x2="-5" y2="-3.6" width="0.254" layer="94"/>
<wire x1="-5" y1="-3.6" x2="3.2" y2="2.1" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="175-103LAE-301_NTC_THERM_10K">
<gates>
<gate name="G$1" symbol="NTC_THERMISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1_175-103LAE-301_NTC_THERM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diodes_Transistors">
<packages>
<package name="SK310A-LTP__SCHOTTKY">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="ANODE" x="-2.032" y="0" dx="1.778" dy="2.159" layer="1"/>
<smd name="CATHODE" x="2.032" y="0" dx="1.778" dy="2.159" layer="1"/>
<text x="-3.056" y="1.369" size="0.4" layer="25">&gt;NAME</text>
<text x="-1.031" y="1.365" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-3.175" y1="1.27" x2="3.175" y2="1.27" width="0.05" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.175" y2="-1.27" width="0.05" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="-3.175" y2="-1.27" width="0.05" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.175" y2="1.27" width="0.05" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="3.175" y2="1.27" width="0.05" layer="39"/>
<wire x1="3.175" y1="1.27" x2="3.175" y2="-1.27" width="0.05" layer="39"/>
<wire x1="3.175" y1="-1.27" x2="-3.175" y2="-1.27" width="0.05" layer="39"/>
<wire x1="-3.175" y1="-1.27" x2="-3.175" y2="1.27" width="0.05" layer="39"/>
<rectangle x1="-0.889" y1="-1.016" x2="0.889" y2="1.016" layer="35"/>
<text x="-3.297" y="-1.192" size="0.5334" layer="21" rot="R90">ANODE</text>
</package>
<package name="POWERPAK1212-8">
<description>Vishay PowerPAK 1212-8 single device package&lt;br&gt;
High-power, low thermal resistance package.</description>
<smd name="S3" x="-1.4224" y="-0.3302" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="S2" x="-1.4224" y="0.3302" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="S1" x="-1.4224" y="0.9906" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="G" x="-1.4224" y="-0.9906" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="D2" x="1.5494" y="0.3302" dx="0.762" dy="0.4064" layer="1"/>
<smd name="D1" x="1.5494" y="0.9906" dx="0.762" dy="0.4064" layer="1"/>
<smd name="D3" x="1.5494" y="-0.3302" dx="0.762" dy="0.4064" layer="1"/>
<smd name="D4" x="1.5494" y="-0.9906" dx="0.762" dy="0.4064" layer="1"/>
<smd name="DPAD" x="0.5842" y="0" dx="1.7272" dy="2.2352" layer="1"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.5" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.35" width="0.127" layer="21"/>
<text x="-1.143" y="0.508" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-1.143" y="-0.381" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="CSD17483F4_N-CHAN_FET">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<smd name="GATE" x="-0.175" y="-0.37" dx="0.15" dy="0.35" layer="1"/>
<smd name="SOURCE" x="0.175" y="-0.37" dx="0.15" dy="0.35" layer="1"/>
<text x="-0.399" y="0.978" size="0.2" layer="25">&gt;NAME</text>
<text x="-0.408" y="0.748" size="0.2" layer="27">&gt;VALUE</text>
<smd name="DRAIN" x="0" y="0.325" dx="0.5" dy="0.25" layer="1"/>
<wire x1="-0.401" y1="0.646" x2="0.393" y2="0.644" width="0.127" layer="21"/>
<wire x1="0.393" y1="-0.722" x2="-0.338" y2="-0.722" width="0.127" layer="21"/>
</package>
<package name="32P768KHZ_XTAL">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="1" x="-1.25" y="0" dx="1.1" dy="1.9" layer="1"/>
<smd name="2" x="1.25" y="0" dx="1.1" dy="1.9" layer="1"/>
<text x="-1.8066" y="1.5324" size="0.25" layer="25">&gt;NAME</text>
<text x="-1.8264" y="1.1982" size="0.25" layer="27">&gt;VALUE</text>
<wire x1="-1.95" y1="1.1" x2="1.95" y2="1.1" width="0.05" layer="21"/>
<wire x1="1.95" y1="1.1" x2="1.95" y2="-1.1" width="0.05" layer="21"/>
<wire x1="1.95" y1="-1.1" x2="-1.95" y2="-1.1" width="0.05" layer="21"/>
<wire x1="-1.95" y1="-1.1" x2="-1.95" y2="1.1" width="0.05" layer="21"/>
<wire x1="-1.95" y1="1.1" x2="1.95" y2="1.1" width="0.05" layer="39"/>
<wire x1="1.95" y1="1.1" x2="1.95" y2="-1.1" width="0.05" layer="39"/>
<wire x1="1.95" y1="-1.1" x2="-1.95" y2="-1.1" width="0.05" layer="39"/>
<wire x1="-1.95" y1="-1.1" x2="-1.95" y2="1.1" width="0.05" layer="39"/>
<rectangle x1="-0.45" y1="-0.9" x2="0.45" y2="0.9" layer="35"/>
<text x="-2.065" y="0.0256" size="0.35" layer="21" rot="R90">1</text>
</package>
<package name="FA-20H__MHZ_XTAL">
<description>&lt;b&gt;3.3Vdc CMOS SMD CRYSTAL CLOCK OSCILLATOR&lt;/b&gt; ASE Series&lt;p&gt;
Source: www.abracon.com</description>
<wire x1="-1.75" y1="1.5" x2="1.75" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.75" y1="1.5" x2="1.75" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="1.75" y1="-1.5" x2="-1.75" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.5" x2="-1.75" y2="1.5" width="0.2032" layer="51"/>
<circle x="-2.143" y="-0.8016" radius="0.1802" width="0" layer="51"/>
<smd name="2" x="0.85" y="-0.7" dx="1.2" dy="1.1" layer="1"/>
<text x="-2.1738" y="2.7192" size="0.8" layer="25">&gt;NAME</text>
<text x="-2.0976" y="1.7458" size="0.8" layer="27">&gt;VALUE</text>
<smd name="1" x="-0.85" y="-0.7" dx="1.2" dy="1.1" layer="1"/>
<smd name="3" x="0.85" y="0.7" dx="1.2" dy="1.1" layer="1"/>
<smd name="4" x="-0.85" y="0.7" dx="1.2" dy="1.1" layer="1"/>
<text x="-2.1484" y="3.6336" size="0.8" layer="25">.Designator</text>
</package>
<package name="2P4GHZ_BAND_PASS">
<description>&lt;b&gt;3.3Vdc CMOS SMD CRYSTAL CLOCK OSCILLATOR&lt;/b&gt; ASE Series&lt;p&gt;
Source: www.abracon.com</description>
<wire x1="-1.1912" y1="0.7634" x2="1.1658" y2="0.7634" width="0.2032" layer="51"/>
<wire x1="1.1658" y1="0.7634" x2="1.1658" y2="-0.7634" width="0.2032" layer="51"/>
<wire x1="1.1658" y1="-0.7634" x2="-1.1912" y2="-0.7634" width="0.2032" layer="51"/>
<wire x1="-1.1912" y1="-0.7634" x2="-1.1912" y2="0.7634" width="0.2032" layer="51"/>
<circle x="0.016" y="-1.1572" radius="0.1802" width="0" layer="51"/>
<smd name="2" x="0.65" y="0" dx="0.45" dy="1" layer="1"/>
<text x="-1.5642" y="1.9064" size="0.8" layer="25">&gt;NAME</text>
<text x="-1.5388" y="0.9838" size="0.8" layer="27">&gt;VALUE</text>
<smd name="1" x="0" y="-0.325" dx="0.35" dy="0.35" layer="1"/>
<smd name="3" x="0" y="0.325" dx="0.35" dy="0.35" layer="1"/>
<smd name="4" x="-0.65" y="0" dx="0.45" dy="1" layer="1"/>
<text x="-1.615" y="2.9732" size="0.8" layer="25">.Designator</text>
</package>
<package name="CHIP_ANTENNA">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="IN" x="-1.6" y="0" dx="1" dy="1.2" layer="1"/>
<smd name="GND" x="1.6" y="0" dx="1" dy="1.2" layer="1"/>
<text x="-2.1876" y="1.126" size="0.25" layer="25">&gt;NAME</text>
<text x="-2.182" y="0.8426" size="0.25" layer="27">&gt;VALUE</text>
<wire x1="-2.3" y1="0.75" x2="2.3" y2="0.75" width="0.05" layer="21"/>
<wire x1="2.3" y1="0.75" x2="2.3" y2="-0.75" width="0.05" layer="21"/>
<wire x1="2.3" y1="-0.75" x2="-2.3" y2="-0.75" width="0.05" layer="21"/>
<wire x1="-2.3" y1="-0.75" x2="-2.3" y2="0.75" width="0.05" layer="21"/>
<wire x1="-2.3" y1="0.75" x2="2.3" y2="0.75" width="0.05" layer="39"/>
<wire x1="2.3" y1="0.75" x2="2.3" y2="-0.75" width="0.05" layer="39"/>
<wire x1="2.3" y1="-0.75" x2="-2.3" y2="-0.75" width="0.05" layer="39"/>
<wire x1="-2.3" y1="-0.75" x2="-2.3" y2="0.75" width="0.05" layer="39"/>
<text x="-2.446" y="-0.33" size="0.5334" layer="21" rot="R90">IN</text>
</package>
<package name="SER3409-ND">
<pad name="P$1" x="0" y="0" drill="0.3"/>
<circle x="0" y="0.6" radius="3.1" width="0.127" layer="21"/>
<pad name="P$2" x="0" y="1.1" drill="0.3"/>
</package>
</packages>
<symbols>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="ANODE" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="CATHODE" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
</symbol>
<symbol name="MOSFET_P">
<wire x1="-3.556" y1="-2.54" x2="-3.556" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.556" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0.508" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="-2.0066" y2="2.159" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="2.159" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.159" x2="1.27" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.159" width="0.1524" layer="94"/>
<circle x="0" y="2.159" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="-2.159" radius="0.127" width="0.4064" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.143" y="-4.318" size="0.8128" layer="93">D</text>
<text x="-1.143" y="3.556" size="0.8128" layer="93">S</text>
<text x="-4.826" y="1.143" size="0.8128" layer="93">G</text>
<rectangle x1="-2.794" y1="-2.794" x2="-2.032" y2="-1.27" layer="94"/>
<rectangle x1="-2.794" y1="1.27" x2="-2.032" y2="2.794" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="point" direction="pas"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="0" y="0"/>
<vertex x="-1.27" y="0.508"/>
<vertex x="-1.27" y="-0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.27" y="0.508"/>
<vertex x="0.762" y="-0.254"/>
<vertex x="1.778" y="-0.254"/>
</polygon>
</symbol>
<symbol name="MOSFET_N">
<wire x1="-3.556" y1="2.54" x2="-3.556" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.556" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0.508" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="-2.0066" y2="2.159" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="2.159" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.159" x2="1.27" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<circle x="0" y="2.159" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="-2.159" radius="0.127" width="0.4064" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.143" y="3.556" size="0.8128" layer="93">D</text>
<text x="-1.143" y="-4.318" size="0.8128" layer="93">S</text>
<text x="-4.826" y="-1.778" size="0.8128" layer="93">G</text>
<rectangle x1="-2.794" y1="-2.794" x2="-2.032" y2="-1.27" layer="94"/>
<rectangle x1="-2.794" y1="1.27" x2="-2.032" y2="2.794" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="point" direction="pas"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="1.27" y="0.508"/>
<vertex x="0.762" y="-0.254"/>
<vertex x="1.778" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-1.905" y="0"/>
<vertex x="-0.635" y="-0.508"/>
<vertex x="-0.635" y="0.508"/>
</polygon>
</symbol>
<symbol name="NPN-DRIVER">
<wire x1="5.08" y1="2.54" x2="3.048" y2="1.524" width="0.1524" layer="94"/>
<wire x1="4.318" y1="-1.524" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="4.318" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="4.08" y1="-2.04" x2="2.848" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="4.064" y1="-2.413" x2="4.826" y2="-2.413" width="0.254" layer="94"/>
<wire x1="4.826" y1="-2.413" x2="4.318" y2="-1.778" width="0.254" layer="94"/>
<wire x1="4.318" y1="-1.778" x2="4.064" y2="-2.286" width="0.254" layer="94"/>
<wire x1="4.064" y1="-2.286" x2="4.445" y2="-2.286" width="0.254" layer="94"/>
<wire x1="4.445" y1="-2.286" x2="4.318" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.286" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="0.762" x2="-2.032" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.508" x2="-1.778" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="0.762" x2="-1.524" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-0.508" x2="-1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.762" x2="-1.016" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-0.508" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="0.254" y1="0" x2="2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0.254" y1="-0.762" x2="0.762" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.016" x2="-0.254" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.27" x2="0.762" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.524" x2="-0.254" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.778" x2="0.762" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-2.032" x2="-0.254" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-2.286" x2="0.254" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.254" y1="-2.54" x2="0.254" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="0.254" y1="-3.556" x2="5.08" y2="-3.556" width="0.1524" layer="94"/>
<circle x="0.254" y="0" radius="0.254" width="0.3048" layer="94"/>
<circle x="5.08" y="-3.556" radius="0.254" width="0.3048" layer="94"/>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="2.286" y1="-2.54" x2="3.048" y2="2.54" layer="94"/>
<pin name="B" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="5.08" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="5.08" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="CRYSTAL">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CRYSTAL_MHZ_4_PINS">
<wire x1="1.016" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.016" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="4.064" x2="-0.381" y2="1.016" width="0.254" layer="94"/>
<wire x1="-0.381" y1="1.016" x2="0.381" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="4.064" width="0.254" layer="94"/>
<wire x1="0.381" y1="4.064" x2="-0.381" y2="4.064" width="0.254" layer="94"/>
<wire x1="1.016" y1="4.318" x2="1.016" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.016" y1="4.318" x2="-1.016" y2="0.762" width="0.254" layer="94"/>
<text x="-5.08" y="11.176" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="1.397" size="0.8636" layer="93">1</text>
<text x="1.524" y="1.397" size="0.8636" layer="93">3</text>
<pin name="3" x="7.62" y="2.54" visible="off" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-7.62" y="2.54" visible="off" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-2.54" y="-2.54" visible="off" length="short" direction="pwr" swaplevel="1" rot="R90"/>
<pin name="4" x="2.54" y="-2.54" visible="off" length="short" direction="pwr" swaplevel="1" rot="R90"/>
<wire x1="-4.318" y1="6.604" x2="4.318" y2="6.604" width="0.254" layer="94"/>
<wire x1="-4.318" y1="-1.016" x2="4.318" y2="-1.016" width="0.254" layer="94"/>
<wire x1="3.556" y1="-1.778" x2="3.556" y2="6.858" width="0.254" layer="94"/>
<wire x1="-4.064" y1="-1.778" x2="-4.064" y2="6.858" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="0.635" layer="94">GNDs</text>
</symbol>
<symbol name="2P4GHZ_BAND_PASS">
<text x="-5.08" y="8.636" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">3</text>
<pin name="3" x="7.62" y="0" visible="off" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-7.62" y="0" visible="off" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-2.54" y="-5.08" visible="off" length="short" direction="pwr" swaplevel="1" rot="R90"/>
<pin name="4" x="2.54" y="-5.08" visible="off" length="short" direction="pwr" swaplevel="1" rot="R90"/>
<wire x1="-4.318" y1="4.064" x2="4.318" y2="4.064" width="0.254" layer="94"/>
<wire x1="-4.318" y1="-3.556" x2="4.318" y2="-3.556" width="0.254" layer="94"/>
<wire x1="3.556" y1="-4.318" x2="3.556" y2="4.318" width="0.254" layer="94"/>
<wire x1="-4.064" y1="-4.318" x2="-4.064" y2="4.318" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="0.635" layer="94">GNDs</text>
<text x="-2.54" y="2.54" size="0.635" layer="94">IN</text>
<text x="0" y="2.54" size="0.635" layer="94">OUT</text>
</symbol>
<symbol name="ANTENNA">
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-12.446" y="5.334" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.446" y="3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="-5.08" visible="off" length="short" direction="pwr" swaplevel="1" rot="R90"/>
<pin name="IN" x="-5.08" y="0" visible="off" length="short" direction="in" swaplevel="1"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.778" x2="0" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="-1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="0" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="1.016" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="1.778" x2="0" y2="3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="3.302" x2="1.27" y2="4.572" width="0.254" layer="94"/>
<wire x1="1.27" y1="4.572" x2="-1.27" y2="4.572" width="0.254" layer="94"/>
<wire x1="-1.27" y1="4.572" x2="0" y2="3.302" width="0.254" layer="94"/>
</symbol>
<symbol name="SER3409-ND">
<circle x="0" y="0" radius="3.1" width="0.254" layer="94"/>
<pin name="P$1" x="5.08" y="-2.54" length="middle" rot="R180"/>
<pin name="P$2" x="5.08" y="2.54" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SK310A-LTP__SCHOTTKY">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SK310A-LTP__SCHOTTKY">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SI7617DN-T1-GE3__TRENCH-FET">
<gates>
<gate name="G$1" symbol="MOSFET_P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="POWERPAK1212-8">
<connects>
<connect gate="G$1" pin="D" pad="D1 D2 D3 D4 DPAD"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S1 S2 S3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SIS412DN-T1-GE3__TRENCH-FET">
<gates>
<gate name="G$1" symbol="MOSFET_N" x="0" y="0"/>
</gates>
<devices>
<device name="" package="POWERPAK1212-8">
<connects>
<connect gate="G$1" pin="D" pad="D1 D2 D3 D4 DPAD"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S1 S2 S3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CSD17483F4__N-CHAN_FET">
<gates>
<gate name="G$1" symbol="NPN-DRIVER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CSD17483F4_N-CHAN_FET">
<connects>
<connect gate="G$1" pin="B" pad="GATE"/>
<connect gate="G$1" pin="C" pad="DRAIN"/>
<connect gate="G$1" pin="E" pad="SOURCE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ABS07-32.768KHZ-T__XTAL">
<gates>
<gate name="G$1" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="32P768KHZ_XTAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FA-20H__MHZ_XTAL">
<gates>
<gate name="G$1" symbol="CRYSTAL_MHZ_4_PINS" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="FA-20H__MHZ_XTAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DEA202450BT-1294C1-H__2P4GHZ_BAND_PASS">
<description>2.4 GHz band pass filter, 4 pins</description>
<gates>
<gate name="G$1" symbol="2P4GHZ_BAND_PASS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2P4GHZ_BAND_PASS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AH316M245001-T__2P4GHZ_ANTENNA">
<gates>
<gate name="G$1" symbol="ANTENNA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CHIP_ANTENNA">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="IN" pad="IN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SER3409-ND__6MHZ_XTAL">
<gates>
<gate name="G$1" symbol="SER3409-ND" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SER3409-ND">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Board_Features_MISC">
<packages>
<package name="TEST_POINT_SQUARE">
<smd name="1" x="0" y="0.19" dx="1" dy="1" layer="1"/>
<text x="-0.73" y="1.185" size="0.3" layer="25">&gt;NAME</text>
<text x="-0.74" y="0.808" size="0.3" layer="27">&gt;VALUE</text>
</package>
<package name="TSM_2PINS_SAMTEC">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<smd name="1" x="-1.27" y="1.46" dx="1.27" dy="3.43" layer="1"/>
<smd name="2" x="1.27" y="-1.46" dx="1.27" dy="3.43" layer="1"/>
<text x="0.013" y="1.163" size="0.4" layer="25">&gt;NAME</text>
<text x="-0.041" y="0.637" size="0.4" layer="27">&gt;VALUE</text>
</package>
<package name="USB-MICRO-SMD">
<wire x1="-2.15" y1="3.9" x2="-2.15" y2="-3.9" width="0.127" layer="49"/>
<wire x1="2.85" y1="3.9" x2="2.85" y2="-3.9" width="0.127" layer="49"/>
<wire x1="2.85" y1="3.9" x2="-2.15" y2="3.9" width="0.127" layer="49"/>
<wire x1="2.85" y1="-3.9" x2="-2.15" y2="-3.9" width="0.127" layer="49"/>
<wire x1="2.981959375" y1="3.99288125" x2="2" y2="3.99288125" width="0.3048" layer="21"/>
<wire x1="2" y1="3.99288125" x2="2" y2="4" width="0.3048" layer="21"/>
<wire x1="3" y1="4" x2="3" y2="3" width="0.3048" layer="21"/>
<wire x1="2" y1="-4" x2="3" y2="-4" width="0.3048" layer="21"/>
<wire x1="3" y1="-4" x2="3" y2="-3" width="0.3048" layer="21"/>
<wire x1="-1" y1="4" x2="-2" y2="4" width="0.3048" layer="21"/>
<wire x1="-1" y1="-4" x2="-2" y2="-4" width="0.3048" layer="21"/>
<wire x1="1.89991875" y1="1.6002" x2="2.499359375" y2="1.6002" width="0" layer="21"/>
<rectangle x1="-0.75" y1="2.784" x2="0.75" y2="4.584" layer="1"/>
<rectangle x1="-0.75" y1="-4.584" x2="0.75" y2="-2.784" layer="1"/>
<rectangle x1="-0.75" y1="2.784" x2="0.75" y2="4.584" layer="31"/>
<rectangle x1="-0.75" y1="-4.584" x2="0.75" y2="-2.784" layer="31"/>
<rectangle x1="-0.85" y1="2.684" x2="0.85" y2="4.684" layer="29"/>
<rectangle x1="-0.85" y1="-4.684" x2="0.85" y2="-2.684" layer="29"/>
<smd name="D+1" x="2.7" y="0" dx="0.4" dy="1.4" layer="1" rot="R90"/>
<smd name="D-1" x="2.7" y="0.65" dx="0.4" dy="1.4" layer="1" rot="R90"/>
<smd name="GND1" x="2.7" y="-1.3" dx="0.4" dy="1.4" layer="1" rot="R90"/>
<smd name="ID1" x="2.7" y="-0.65" dx="0.4" dy="1.4" layer="1" rot="R90"/>
<smd name="VBUS1" x="2.7" y="1.3" dx="0.4" dy="1.4" layer="1" rot="R90"/>
<text x="-1.27" y="5.969" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="5.461" size="0.4064" layer="27">&gt;VALUE</text>
<hole x="2.2" y="1.9" drill="0.85"/>
<hole x="2.2" y="-1.9" drill="0.85"/>
</package>
</packages>
<symbols>
<symbol name="TEST_POINT">
<text x="-4.064" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.064" y="2.794" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-1.905" y="-1.27" size="1.016" layer="94">Test
Point</text>
</symbol>
<symbol name="TSM_SERIES_2PINS_SAMTEC">
<text x="-4.064" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.064" y="2.794" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-1.905" y="-1.27" size="0.762" layer="94">TSM
SAMTEC
2 pins</text>
</symbol>
<symbol name="USB_AB">
<description>USB AB, 5-pins: USBVCC, GND, D+, D-, USBID.</description>
<wire x1="2.54" y1="6.35" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="2.54" y2="-6.35" width="0.254" layer="94"/>
<pin name="D+" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="D-" x="-5.08" y="-2.54" visible="pad" length="short"/>
<pin name="GND" x="-5.08" y="5.08" visible="pad" length="short"/>
<pin name="USBID" x="-5.08" y="2.54" visible="pad" length="short"/>
<pin name="VBUS" x="-5.08" y="-5.08" visible="pad" length="short"/>
<text x="1.27" y="-6.096" size="2.54" layer="94" rot="R90">USBAB</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEST_POINT__SQUARE">
<description>1mm x 1mm square pad on board</description>
<gates>
<gate name="G$1" symbol="TEST_POINT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TEST_POINT_SQUARE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TSM_SERIES_2PINS_SAMTEC">
<description>TSM 0.1" pitch header, SAMTEC</description>
<gates>
<gate name="G$1" symbol="TSM_SERIES_2PINS_SAMTEC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSM_2PINS_SAMTEC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICRO_USB_CAGE" prefix="U" uservalue="yes">
<description>This is a USB AB connector that is compatible with USB OTG. i.e. this connector can serve as a host or device</description>
<gates>
<gate name="G$1" symbol="USB_AB" x="0" y="0"/>
</gates>
<devices>
<device name="&quot;&quot;" package="USB-MICRO-SMD">
<connects>
<connect gate="G$1" pin="D+" pad="D+1"/>
<connect gate="G$1" pin="D-" pad="D-1"/>
<connect gate="G$1" pin="GND" pad="GND1"/>
<connect gate="G$1" pin="USBID" pad="ID1"/>
<connect gate="G$1" pin="VBUS" pad="VBUS1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ICs_MCUs_FPGAs_Modules">
<packages>
<package name="VQFN_RGC_64__CC3200">
<smd name="PAD_GND" x="0" y="0" dx="7.3" dy="7.3" layer="1"/>
<wire x1="-4.85" y1="-4.85" x2="-4.85" y2="4.85" width="0.1524" layer="25"/>
<wire x1="4.85" y1="-4.85" x2="4.85" y2="4.85" width="0.1524" layer="25"/>
<wire x1="-4.85" y1="4.85" x2="4.85" y2="4.85" width="0.1524" layer="25"/>
<wire x1="-4.85" y1="-4.85" x2="4.85" y2="-4.85" width="0.1524" layer="25"/>
<text x="-4.4274" y="7.319" size="0.85" layer="25" ratio="6" rot="SR0">.Designator</text>
<text x="-4.445" y="6.2124" size="0.85" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-4.4752" y="5.189" size="0.85" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
<smd name="1" x="-4.35" y="3.75" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<pad name="PAD_VIA3" x="-2.5273" y="-2.1717" drill="0.254"/>
<pad name="PAD_VIA1" x="-2.3749" y="2.4765" drill="0.254"/>
<pad name="PAD_VIA2" x="-2.5019" y="0.1905" drill="0.254" rot="R90"/>
<pad name="PAD_VIA4" x="-0.2413" y="-2.1971" drill="0.254"/>
<smd name="2" x="-4.35" y="3.25" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="3" x="-4.35" y="2.75" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="4" x="-4.35" y="2.25" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="5" x="-4.35" y="1.75" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="6" x="-4.35" y="1.25" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="7" x="-4.35" y="0.75" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="8" x="-4.35" y="0.25" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="9" x="-4.35" y="-0.25" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="10" x="-4.35" y="-0.75" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="11" x="-4.35" y="-1.25" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="12" x="-4.35" y="-1.75" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="13" x="-4.35" y="-2.25" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="14" x="-4.35" y="-2.75" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="15" x="-4.35" y="-3.25" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="16" x="-4.35" y="-3.75" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R180"/>
<smd name="17" x="-3.75" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="18" x="-3.25" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="19" x="-2.75" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="20" x="-2.25" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="21" x="-1.75" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="22" x="-1.25" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="23" x="-0.75" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="24" x="-0.25" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="25" x="0.25" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="26" x="0.75" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="27" x="1.25" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="28" x="1.75" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="29" x="2.25" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="30" x="2.75" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="31" x="3.25" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="32" x="3.75" y="-4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R270"/>
<smd name="33" x="4.35" y="-3.75" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="34" x="4.35" y="-3.25" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="35" x="4.35" y="-2.75" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="36" x="4.35" y="-2.25" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="37" x="4.35" y="-1.75" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="38" x="4.35" y="-1.25" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="39" x="4.35" y="-0.75" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="40" x="4.35" y="-0.25" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="41" x="4.35" y="0.25" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="42" x="4.35" y="0.75" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="43" x="4.35" y="1.25" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="44" x="4.35" y="1.75" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="45" x="4.35" y="2.25" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="46" x="4.35" y="2.75" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="47" x="4.35" y="3.25" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="48" x="4.35" y="3.75" dx="0.5" dy="0.24" layer="1" roundness="99"/>
<smd name="49" x="3.75" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="50" x="3.25" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="51" x="2.75" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="52" x="2.25" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="53" x="1.75" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="54" x="1.25" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="55" x="0.75" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="56" x="0.25" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="57" x="-0.25" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="58" x="-0.75" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="59" x="-1.25" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="60" x="-1.75" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="61" x="-2.25" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="62" x="-2.75" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="63" x="-3.25" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<smd name="64" x="-3.75" y="4.35" dx="0.5" dy="0.24" layer="1" roundness="99" rot="R90"/>
<pad name="PAD_VIA5" x="2.1971" y="-2.2479" drill="0.254"/>
<pad name="PAD_VIA6" x="2.3241" y="-0.0635" drill="0.254"/>
<pad name="PAD_VIA7" x="2.2987" y="2.3241" drill="0.254"/>
<pad name="PAD_VIA8" x="-0.0127" y="2.4511" drill="0.254"/>
<pad name="PAD_VIA9" x="-0.1651" y="0.0381" drill="0.254"/>
<text x="-5.207" y="0.0762" size="0.3" layer="21">8</text>
<text x="-0.0762" y="-5.4356" size="0.3" layer="21" rot="R90">24</text>
<text x="5.0038" y="0.5842" size="0.3" layer="21">42</text>
<text x="0.381" y="5.0038" size="0.3" layer="21" rot="R90">56</text>
<text x="-4.7752" y="3.7592" size="1" layer="21" rot="R90">*</text>
</package>
</packages>
<symbols>
<symbol name="CC3200_CUSTOM_SYMBOL">
<pin name="27_NC" x="-15.24" y="-78.74" length="short" direction="nc"/>
<pin name="26_NC" x="-15.24" y="-81.28" length="short" direction="nc"/>
<pin name="25_LDO_IN2" x="-73.66" y="55.88" length="short" direction="pwr"/>
<pin name="24_VDD_PLL" x="-73.66" y="-99.06" length="short" direction="pwr"/>
<pin name="23_WLAN_XTAL_P" x="68.58" y="-93.98" length="short" function="clk" rot="R180"/>
<pin name="22_WLAN_XTAL_N" x="68.58" y="-99.06" length="short" function="clk" rot="R180"/>
<pin name="21_SOP2" x="-25.4" y="-134.62" length="short" direction="out" rot="R90"/>
<pin name="20_TMS" x="22.86" y="-134.62" length="short" rot="R90"/>
<pin name="19_TCK" x="15.24" y="-134.62" length="short" rot="R90"/>
<pin name="18_GPIO28" x="68.58" y="-68.58" length="short" rot="R180"/>
<pin name="17_TDO" x="38.1" y="-134.62" length="short" rot="R90"/>
<pin name="28_NC" x="-15.24" y="-76.2" length="short" direction="nc"/>
<pin name="29_ANTSEL1" x="68.58" y="-124.46" length="short" direction="out" rot="R180"/>
<pin name="30_ANTSEL2" x="68.58" y="-129.54" length="short" direction="out" rot="R180"/>
<pin name="31_RF_BG" x="68.58" y="114.3" length="short" rot="R180"/>
<pin name="32_NRESET" x="35.56" y="119.38" length="short" rot="R270"/>
<pin name="33_VDD_PA_IN" x="-73.66" y="-10.16" length="short" direction="pwr"/>
<pin name="34_SOP1" x="-17.78" y="-134.62" length="short" rot="R90"/>
<pin name="35_SOP0" x="-10.16" y="-134.62" length="short" rot="R90"/>
<pin name="36_LDO_IN1" x="-73.66" y="76.2" length="short" direction="pwr"/>
<pin name="37_VIN_DCDC_ANA" x="-40.64" y="119.38" length="short" direction="pwr" rot="R270"/>
<pin name="38_DCDC_ANA_SW" x="-73.66" y="111.76" length="short" direction="pwr"/>
<pin name="39_VIN_DCDC_PA" x="-27.94" y="119.38" length="short" direction="pwr" rot="R270"/>
<pin name="40_DCDC_PA_SW_P" x="-73.66" y="38.1" length="short" direction="pwr"/>
<pin name="41_DCDC_PA_SW_N" x="-73.66" y="15.24" length="short" direction="pwr"/>
<pin name="42_DCDC_PA_OUT" x="-73.66" y="0" length="short" direction="pwr"/>
<pin name="43_DCDC_DIG_SW" x="-73.66" y="-27.94" length="short" direction="pwr"/>
<pin name="44_VIN_DCDC_DIG" x="-15.24" y="119.38" length="short" direction="pwr" rot="R270"/>
<pin name="1_GPIO10" x="68.58" y="2.54" length="short" rot="R180"/>
<pin name="2_GPIO11" x="68.58" y="-5.08" length="short" rot="R180"/>
<pin name="3_GPIO12" x="68.58" y="-12.7" length="short" rot="R180"/>
<pin name="4_GPIO13" x="68.58" y="-20.32" length="short" rot="R180"/>
<pin name="5_GPIO14" x="68.58" y="-27.94" length="short" rot="R180"/>
<pin name="6_GPIO15" x="68.58" y="-35.56" length="short" rot="R180"/>
<pin name="7_GPIO16" x="68.58" y="-45.72" length="short" rot="R180"/>
<pin name="8_GPIO17" x="68.58" y="-53.34" length="short" rot="R180"/>
<pin name="9_VDD_DIG1" x="-73.66" y="-38.1" length="short" direction="pwr"/>
<pin name="10_VIN_IO1" x="15.24" y="119.38" length="short" rot="R270"/>
<pin name="11_FLASH_SPI_CLK" x="68.58" y="86.36" length="short" direction="out" function="clk" rot="R180"/>
<pin name="12_FLASH_SPI_DOUT" x="68.58" y="91.44" length="short" direction="out" rot="R180"/>
<pin name="13_FLASH_SPI_DIN" x="68.58" y="96.52" length="short" direction="in" rot="R180"/>
<pin name="14_FLASH_SPI_CS" x="68.58" y="101.6" length="short" direction="out" rot="R180"/>
<pin name="15_GPIO22" x="68.58" y="-60.96" length="short" rot="R180"/>
<pin name="16_TDI" x="30.48" y="-134.62" length="short" rot="R90"/>
<pin name="45_DCDC_ANA2_SW_P" x="-73.66" y="-55.88" length="short"/>
<pin name="46_DCDC_ANA2_SW_N" x="-73.66" y="-66.04" length="short" direction="pwr"/>
<pin name="47_VDD_ANA2" x="-73.66" y="-78.74" length="short" direction="pwr"/>
<pin name="48_VDD_ANA1" x="-73.66" y="93.98" length="short" direction="pwr"/>
<pin name="49_VDD_RAM" x="-73.66" y="-91.44" length="short" direction="pwr"/>
<pin name="50_GPIO0" x="68.58" y="78.74" length="short" rot="R180"/>
<pin name="51_RTC_XTAL_P" x="-73.66" y="-129.54" length="short" function="clk"/>
<pin name="52_RTC_XTAL_N" x="-73.66" y="-121.92" length="short" direction="out"/>
<pin name="53_GPIO30" x="68.58" y="-76.2" length="short" rot="R180"/>
<pin name="54_VIN_IO2" x="2.54" y="119.38" length="short" direction="pwr" rot="R270"/>
<pin name="55_GPIO1" x="68.58" y="71.12" length="short" rot="R180"/>
<pin name="56_VDD_DIG2" x="-73.66" y="-48.26" length="short" direction="pwr"/>
<pin name="57_GPIO2" x="68.58" y="63.5" length="short" rot="R180"/>
<pin name="58_GPIO3" x="68.58" y="55.88" length="short" rot="R180"/>
<pin name="59_GPIO4" x="68.58" y="48.26" length="short" rot="R180"/>
<pin name="60_GPIO5" x="68.58" y="40.64" length="short" rot="R180"/>
<pin name="61_GPIO6" x="68.58" y="33.02" length="short" rot="R180"/>
<pin name="62_GPIO7" x="68.58" y="25.4" length="short" rot="R180"/>
<pin name="63_GPIO8" x="68.58" y="17.78" length="short" rot="R180"/>
<wire x1="-71.12" y1="116.84" x2="-71.12" y2="-132.08" width="0.254" layer="94"/>
<wire x1="-71.12" y1="-132.08" x2="66.04" y2="-132.08" width="0.254" layer="94"/>
<wire x1="66.04" y1="-132.08" x2="66.04" y2="116.84" width="0.254" layer="94"/>
<wire x1="66.04" y1="116.84" x2="-71.12" y2="116.84" width="0.254" layer="94"/>
<text x="12.7" y="-58.42" size="25.4" layer="94" rot="R90">CC3200</text>
<text x="-71.12" y="119.38" size="5.08" layer="95">&gt;NAME</text>
<pin name="64_GPIO9" x="68.58" y="10.16" length="short" rot="R180"/>
<pin name="65_PAD_GND" x="7.62" y="-86.36" length="short" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CC3200_CUSTOM_SYMBOL">
<gates>
<gate name="G$1" symbol="CC3200_CUSTOM_SYMBOL" x="5.08" y="0"/>
</gates>
<devices>
<device name="" package="VQFN_RGC_64__CC3200">
<connects>
<connect gate="G$1" pin="10_VIN_IO1" pad="10"/>
<connect gate="G$1" pin="11_FLASH_SPI_CLK" pad="11"/>
<connect gate="G$1" pin="12_FLASH_SPI_DOUT" pad="12"/>
<connect gate="G$1" pin="13_FLASH_SPI_DIN" pad="13"/>
<connect gate="G$1" pin="14_FLASH_SPI_CS" pad="14"/>
<connect gate="G$1" pin="15_GPIO22" pad="15"/>
<connect gate="G$1" pin="16_TDI" pad="16"/>
<connect gate="G$1" pin="17_TDO" pad="17"/>
<connect gate="G$1" pin="18_GPIO28" pad="18"/>
<connect gate="G$1" pin="19_TCK" pad="19"/>
<connect gate="G$1" pin="1_GPIO10" pad="1"/>
<connect gate="G$1" pin="20_TMS" pad="20"/>
<connect gate="G$1" pin="21_SOP2" pad="21"/>
<connect gate="G$1" pin="22_WLAN_XTAL_N" pad="22"/>
<connect gate="G$1" pin="23_WLAN_XTAL_P" pad="23"/>
<connect gate="G$1" pin="24_VDD_PLL" pad="24"/>
<connect gate="G$1" pin="25_LDO_IN2" pad="25"/>
<connect gate="G$1" pin="26_NC" pad="26"/>
<connect gate="G$1" pin="27_NC" pad="27"/>
<connect gate="G$1" pin="28_NC" pad="28"/>
<connect gate="G$1" pin="29_ANTSEL1" pad="29"/>
<connect gate="G$1" pin="2_GPIO11" pad="2"/>
<connect gate="G$1" pin="30_ANTSEL2" pad="30"/>
<connect gate="G$1" pin="31_RF_BG" pad="31"/>
<connect gate="G$1" pin="32_NRESET" pad="32"/>
<connect gate="G$1" pin="33_VDD_PA_IN" pad="33"/>
<connect gate="G$1" pin="34_SOP1" pad="34"/>
<connect gate="G$1" pin="35_SOP0" pad="35"/>
<connect gate="G$1" pin="36_LDO_IN1" pad="36"/>
<connect gate="G$1" pin="37_VIN_DCDC_ANA" pad="37"/>
<connect gate="G$1" pin="38_DCDC_ANA_SW" pad="38"/>
<connect gate="G$1" pin="39_VIN_DCDC_PA" pad="39"/>
<connect gate="G$1" pin="3_GPIO12" pad="3"/>
<connect gate="G$1" pin="40_DCDC_PA_SW_P" pad="40"/>
<connect gate="G$1" pin="41_DCDC_PA_SW_N" pad="41"/>
<connect gate="G$1" pin="42_DCDC_PA_OUT" pad="42"/>
<connect gate="G$1" pin="43_DCDC_DIG_SW" pad="43"/>
<connect gate="G$1" pin="44_VIN_DCDC_DIG" pad="44"/>
<connect gate="G$1" pin="45_DCDC_ANA2_SW_P" pad="45"/>
<connect gate="G$1" pin="46_DCDC_ANA2_SW_N" pad="46"/>
<connect gate="G$1" pin="47_VDD_ANA2" pad="47"/>
<connect gate="G$1" pin="48_VDD_ANA1" pad="48"/>
<connect gate="G$1" pin="49_VDD_RAM" pad="49"/>
<connect gate="G$1" pin="4_GPIO13" pad="4"/>
<connect gate="G$1" pin="50_GPIO0" pad="50"/>
<connect gate="G$1" pin="51_RTC_XTAL_P" pad="51"/>
<connect gate="G$1" pin="52_RTC_XTAL_N" pad="52"/>
<connect gate="G$1" pin="53_GPIO30" pad="53"/>
<connect gate="G$1" pin="54_VIN_IO2" pad="54"/>
<connect gate="G$1" pin="55_GPIO1" pad="55"/>
<connect gate="G$1" pin="56_VDD_DIG2" pad="56"/>
<connect gate="G$1" pin="57_GPIO2" pad="57"/>
<connect gate="G$1" pin="58_GPIO3" pad="58"/>
<connect gate="G$1" pin="59_GPIO4" pad="59"/>
<connect gate="G$1" pin="5_GPIO14" pad="5"/>
<connect gate="G$1" pin="60_GPIO5" pad="60"/>
<connect gate="G$1" pin="61_GPIO6" pad="61"/>
<connect gate="G$1" pin="62_GPIO7" pad="62"/>
<connect gate="G$1" pin="63_GPIO8" pad="63"/>
<connect gate="G$1" pin="64_GPIO9" pad="64"/>
<connect gate="G$1" pin="65_PAD_GND" pad="PAD_GND PAD_VIA1 PAD_VIA2 PAD_VIA3 PAD_VIA4 PAD_VIA5 PAD_VIA6 PAD_VIA7 PAD_VIA8 PAD_VIA9"/>
<connect gate="G$1" pin="6_GPIO15" pad="6"/>
<connect gate="G$1" pin="7_GPIO16" pad="7"/>
<connect gate="G$1" pin="8_GPIO17" pad="8"/>
<connect gate="G$1" pin="9_VDD_DIG1" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ICs_Power">
<packages>
<package name="TSSOP_DBT_30">
<smd name="1" x="-3.6703" y="4.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="2" x="-3.6703" y="4" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="3" x="-3.6703" y="3.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="4" x="-3.6703" y="3" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="5" x="-3.6703" y="2.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="6" x="-3.6703" y="2" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="7" x="-3.6703" y="1.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="8" x="-3.6703" y="1" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="9" x="-3.6703" y="0.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="10" x="-3.6703" y="0" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="11" x="-3.6703" y="-0.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="12" x="-3.6703" y="-1" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="13" x="-3.6703" y="-1.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="14" x="-3.6703" y="-2" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="15" x="-3.6703" y="-2.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="16" x="3.6703" y="-2.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="17" x="3.6703" y="-2" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="18" x="3.6703" y="-1.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="19" x="3.6703" y="-1" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="20" x="3.6703" y="-0.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="21" x="3.6703" y="0" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="22" x="3.6703" y="0.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="23" x="3.6703" y="1" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="24" x="3.6703" y="1.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="25" x="3.6703" y="2" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="26" x="3.6703" y="2.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="27" x="3.6703" y="3" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="28" x="3.6703" y="3.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="29" x="3.6703" y="4" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="30" x="3.6703" y="4.5" dx="1.6764" dy="0.3556" layer="1"/>
<wire x1="-3.0988" y1="-3.1492" x2="3.0988" y2="-3.1492" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="-3.1492" x2="3.0988" y2="4.9492" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="4.9492" x2="0.3148" y2="4.9492" width="0.1524" layer="21"/>
<wire x1="0.3148" y1="4.9492" x2="-0.2948" y2="4.9492" width="0.1524" layer="21"/>
<wire x1="-0.2948" y1="4.9492" x2="-3.0788" y2="4.9492" width="0.1524" layer="21"/>
<wire x1="-3.0788" y1="4.9492" x2="-3.0988" y2="-3.1492" width="0.1524" layer="21"/>
<wire x1="0.3148" y1="4.9492" x2="-0.2948" y2="4.9492" width="0" layer="21" curve="-180"/>
<text x="-4.6458" y="4.8406" size="0.4" layer="22" ratio="6" rot="SMR0">*</text>
<text x="-2.5302" y="5.565" size="0.4" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-2.5344" y="5.115" size="0.4" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="BQ78350">
<wire x1="-43.18" y1="68.58" x2="-43.18" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-66.04" x2="45.72" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="45.72" y1="-66.04" x2="45.72" y2="68.58" width="0.1524" layer="94"/>
<wire x1="45.72" y1="68.58" x2="-43.18" y2="68.58" width="0.1524" layer="94"/>
<text x="-4.7244" y="14.1986" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-5.6642" y="9.1186" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="SMBD" x="48.26" y="-50.8" length="short" rot="R180"/>
<pin name="SMBC" x="48.26" y="-40.64" length="short" rot="R180"/>
<pin name="BAT" x="-45.72" y="30.48" length="short" direction="pwr"/>
<pin name="VSS3" x="2.54" y="-68.58" length="short" direction="pwr" rot="R90"/>
<text x="-10.16" y="-55.88" size="3.81" layer="94">GND</text>
<text x="35.56" y="-53.34" size="3.81" layer="94" rot="R90">SMBus</text>
<pin name="KEYIN" x="-45.72" y="50.8" length="short" direction="in"/>
<pin name="MRST" x="-45.72" y="58.42" length="short" direction="in"/>
<pin name="VCC" x="-45.72" y="66.04" length="short" direction="pwr"/>
<pin name="COM" x="48.26" y="20.32" length="short" direction="out" rot="R180"/>
<pin name="PRES" x="-45.72" y="43.18" length="short" direction="in"/>
<pin name="RBI" x="-45.72" y="5.08" length="short" direction="pwr"/>
<pin name="VAUX" x="48.26" y="50.8" length="short" direction="pwr" rot="R180"/>
<pin name="SAFE" x="-45.72" y="-7.62" length="short" direction="out"/>
<pin name="SMBA" x="48.26" y="-63.5" length="short" direction="in" rot="R180"/>
<pin name="VSS1" x="-12.7" y="-68.58" length="short" direction="pwr" rot="R90"/>
<pin name="VSS2" x="-5.08" y="-68.58" length="short" direction="pwr" rot="R90"/>
<pin name="ALERT" x="12.7" y="-68.58" length="short" rot="R90"/>
<pin name="BQ769X0_DATA" x="20.32" y="-68.58" length="short" rot="R90"/>
<pin name="BQ769X0_CLK" x="27.94" y="-68.58" length="short" rot="R90"/>
<text x="10.16" y="-50.8" size="2.54" layer="94">BQ769x0 
CLK/DATA
AFE</text>
<pin name="PRECHG" x="48.26" y="-12.7" length="short" direction="out" rot="R180"/>
<pin name="GPIO_B" x="48.26" y="-30.48" length="short" rot="R180"/>
<pin name="GPIO_A" x="48.26" y="-22.86" length="short" rot="R180"/>
<pin name="ADREN" x="-30.48" y="-68.58" length="short" direction="out" rot="R90"/>
<pin name="VEN" x="48.26" y="27.94" length="short" direction="out" rot="R180"/>
<pin name="DISP" x="-45.72" y="-50.8" length="short" direction="in"/>
<pin name="PWRM" x="48.26" y="-2.54" length="short" direction="out" rot="R180"/>
<pin name="LED1" x="-45.72" y="-15.24" length="short" direction="out"/>
<pin name="LED2" x="-45.72" y="-22.86" length="short" direction="out"/>
<pin name="LED3" x="-45.72" y="-30.48" length="short" direction="out"/>
<pin name="LED4" x="-45.72" y="-38.1" length="short" direction="out"/>
<pin name="LED5" x="-45.72" y="-45.72" length="short" direction="out"/>
<text x="-27.94" y="-43.18" size="4.826" layer="94" rot="R90">LEDs</text>
<text x="27.94" y="-27.94" size="1.27" layer="94" ratio="6">Optional 
Gen I/O</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BQ78350-R1">
<gates>
<gate name="G$1" symbol="BQ78350" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="TSSOP_DBT_30">
<connects>
<connect gate="G$1" pin="ADREN" pad="29"/>
<connect gate="G$1" pin="ALERT" pad="2"/>
<connect gate="G$1" pin="BAT" pad="7"/>
<connect gate="G$1" pin="BQ769X0_CLK" pad="4"/>
<connect gate="G$1" pin="BQ769X0_DATA" pad="3"/>
<connect gate="G$1" pin="COM" pad="1"/>
<connect gate="G$1" pin="DISP" pad="14"/>
<connect gate="G$1" pin="GPIO_A" pad="21"/>
<connect gate="G$1" pin="GPIO_B" pad="28"/>
<connect gate="G$1" pin="KEYIN" pad="9"/>
<connect gate="G$1" pin="LED1" pad="16"/>
<connect gate="G$1" pin="LED2" pad="17"/>
<connect gate="G$1" pin="LED3" pad="18"/>
<connect gate="G$1" pin="LED4" pad="19"/>
<connect gate="G$1" pin="LED5" pad="20"/>
<connect gate="G$1" pin="MRST" pad="24"/>
<connect gate="G$1" pin="PRECHG" pad="5"/>
<connect gate="G$1" pin="PRES" pad="8"/>
<connect gate="G$1" pin="PWRM" pad="15"/>
<connect gate="G$1" pin="RBI" pad="27"/>
<connect gate="G$1" pin="SAFE" pad="10"/>
<connect gate="G$1" pin="SMBA" pad="30"/>
<connect gate="G$1" pin="SMBC" pad="13"/>
<connect gate="G$1" pin="SMBD" pad="11"/>
<connect gate="G$1" pin="VAUX" pad="6"/>
<connect gate="G$1" pin="VCC" pad="26"/>
<connect gate="G$1" pin="VEN" pad="12"/>
<connect gate="G$1" pin="VSS1" pad="22"/>
<connect gate="G$1" pin="VSS2" pad="23"/>
<connect gate="G$1" pin="VSS3" pad="25"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="BAT1" library="LC3" deviceset="BATTERY" device=""/>
<part name="U$8" library="Resistors_Caps_Inductors_Misc" deviceset="SRP7030-2R2M__2P2UH" device=""/>
<part name="U$9" library="Resistors_Caps_Inductors_Misc" deviceset="MGV06051R0M-10__1P0UH" device=""/>
<part name="U$1" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="U$10" library="Resistors_Caps_Inductors_Misc" deviceset="CL21B106KQQNNNE__10UF" device=""/>
<part name="U$5" library="Resistors_Caps_Inductors_Misc" deviceset="CL31A226KPHNNNE__22UF" device=""/>
<part name="U$6" library="Resistors_Caps_Inductors_Misc" deviceset="GRM31CR60J476ME19L__47UF" device=""/>
<part name="SUPPLY6" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY11" library="supply2" deviceset="GND" device=""/>
<part name="C3" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C4" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY21" library="supply2" deviceset="GND" device=""/>
<part name="R38" library="Resistors_Caps_Inductors_Misc" deviceset="WSL2010R0100FEA__10MOHM_CS" device=""/>
<part name="R14" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R15" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R16" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R18" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R19" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R20" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="SUPPLY26" library="supply2" deviceset="GND" device=""/>
<part name="C25" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY31" library="supply2" deviceset="GND" device=""/>
<part name="C26" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C28" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY32" library="supply2" deviceset="GND" device=""/>
<part name="C29" library="resistor" deviceset="C-EU" device="C0402" value="56n"/>
<part name="SUPPLY33" library="supply2" deviceset="GND" device=""/>
<part name="U3" library="ICs" deviceset="BQ24616" device=""/>
<part name="C31" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY35" library="supply2" deviceset="GND" device=""/>
<part name="R17" library="resistor" deviceset="R-US_" device="R0402" value="1K"/>
<part name="R22" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="C32" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C30" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY37" library="supply2" deviceset="GND" device=""/>
<part name="C35" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="U$27" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="U$29" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="C36" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY38" library="supply2" deviceset="GND" device=""/>
<part name="R23" library="resistor" deviceset="R-US_" device="R0402" value="500K"/>
<part name="C37" library="resistor" deviceset="C-EU" device="C0402" value="25p"/>
<part name="R24" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="SUPPLY39" library="supply2" deviceset="GND" device=""/>
<part name="R45" library="Resistors_Caps_Inductors_Misc" deviceset="WSL2010R0100FEA__10MOHM_CS" device=""/>
<part name="Q2" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="Q1" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="R27" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R28" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="C38" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C40" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C41" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY41" library="supply2" deviceset="GND" device=""/>
<part name="R30" library="Resistors_Caps_Inductors_Misc" deviceset="ESR03EZPF10R0__10OHM" device=""/>
<part name="Q3" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="Q4" library="Diodes_Transistors" deviceset="SIS412DN-T1-GE3__TRENCH-FET" device=""/>
<part name="Q5" library="Diodes_Transistors" deviceset="SIS412DN-T1-GE3__TRENCH-FET" device=""/>
<part name="SUPPLY42" library="supply2" deviceset="GND" device=""/>
<part name="U$3" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="U$7" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="C39" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY40" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY43" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY46" library="supply2" deviceset="GND" device=""/>
<part name="L5" library="Resistors_Caps_Inductors_Misc" deviceset="VLS6045EX-6R8M__6P8UH" device=""/>
<part name="R25" library="Resistors_Caps_Inductors_Misc" deviceset="WSL2010R0100FEA__10MOHM_CS" device=""/>
<part name="C7" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R26" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="SUPPLY7" library="supply2" deviceset="GND" device=""/>
<part name="C42" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R37" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="C43" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R35" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="R29" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="U$11" library="Diodes_Transistors" deviceset="CSD17483F4__N-CHAN_FET" device=""/>
<part name="SUPPLY27" library="supply2" deviceset="GND" device=""/>
<part name="Q8" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="C44" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R46" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="C45" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R50" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="C46" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R53" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="U$24" library="Diodes_Transistors" deviceset="CSD17483F4__N-CHAN_FET" device=""/>
<part name="U$25" library="Diodes_Transistors" deviceset="CSD17483F4__N-CHAN_FET" device=""/>
<part name="U$26" library="Diodes_Transistors" deviceset="CSD17483F4__N-CHAN_FET" device=""/>
<part name="U$28" library="Diodes_Transistors" deviceset="CSD17483F4__N-CHAN_FET" device=""/>
<part name="R56" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="R57" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="R58" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="R59" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="R60" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="SUPPLY28" library="supply2" deviceset="GND" device=""/>
<part name="Q13" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="SUPPLY47" library="supply2" deviceset="GND" device=""/>
<part name="R62" library="resistor" deviceset="R-US_" device="R0402" value="1K"/>
<part name="R63" library="resistor" deviceset="R-US_" device="R0402" value="1M"/>
<part name="Q14" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="Q15" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="R64" library="resistor" deviceset="R-US_" device="R0402" value="1M"/>
<part name="C47" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY48" library="supply2" deviceset="GND" device=""/>
<part name="C48" library="resistor" deviceset="C-EU" device="C0402" value="4.7u"/>
<part name="SUPPLY49" library="supply2" deviceset="GND" device=""/>
<part name="U$2" library="ICs" deviceset="BQ76920" device=""/>
<part name="C49" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY50" library="supply2" deviceset="GND" device=""/>
<part name="C50" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY51" library="supply2" deviceset="GND" device=""/>
<part name="C51" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY52" library="supply2" deviceset="GND" device=""/>
<part name="R36" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="R39" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="R40" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="R47" library="LC_LIB5" deviceset="175-103LAE-301_NTC_THERM_10K" device=""/>
<part name="C52" library="resistor" deviceset="C-EU" device="C0402" value="2.2u"/>
<part name="SUPPLY53" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY56" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY57" library="supply2" deviceset="GND" device=""/>
<part name="TP1" library="Board_Features_MISC" deviceset="TEST_POINT__SQUARE" device=""/>
<part name="C62" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY59" library="supply2" deviceset="GND" device=""/>
<part name="C63" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C64" library="resistor" deviceset="C-EU" device="C0402" value="10u"/>
<part name="SUPPLY61" library="supply2" deviceset="GND" device=""/>
<part name="U7" library="ICs" deviceset="CC2564MODA" device=""/>
<part name="U8" library="ICs" deviceset="MPU-6050_CUSTOM_SYMBOL" device=""/>
<part name="C1" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="C2" library="resistor" deviceset="C-EU" device="C0402" value="2.2n"/>
<part name="C5" library="resistor" deviceset="C-EU" device="C0402" value="10n"/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="C6" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY3" library="supply2" deviceset="GND" device=""/>
<part name="R1" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R2" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R3" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="U9" library="ICs_MCUs_FPGAs_Modules" deviceset="CC3200_CUSTOM_SYMBOL" device=""/>
<part name="SUPPLY4" library="supply2" deviceset="GND" device=""/>
<part name="C8" library="resistor" deviceset="C-EU" device="C0402" value="10p"/>
<part name="C9" library="resistor" deviceset="C-EU" device="C0402" value="10p"/>
<part name="SUPPLY5" library="supply2" deviceset="GND" device=""/>
<part name="R4" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="R5" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="R6" library="resistor" deviceset="R-US_" device="R0402" value="2.7K"/>
<part name="SUPPLY8" library="supply2" deviceset="GND" device=""/>
<part name="R7" library="resistor" deviceset="R-US_" device="R0402" value="2.7K"/>
<part name="C10" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY9" library="supply2" deviceset="GND" device=""/>
<part name="C11" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY10" library="supply2" deviceset="GND" device=""/>
<part name="TP2" library="Board_Features_MISC" deviceset="TEST_POINT__SQUARE" device=""/>
<part name="C12" library="resistor" deviceset="C-EU" device="C0402" value="6.2p"/>
<part name="SUPPLY23" library="supply2" deviceset="GND" device=""/>
<part name="C13" library="resistor" deviceset="C-EU" device="C0402" value="6.2p"/>
<part name="SUPPLY25" library="supply2" deviceset="GND" device=""/>
<part name="C14" library="resistor" deviceset="C-EU" device="C0402" value="10u"/>
<part name="SUPPLY34" library="supply2" deviceset="GND" device=""/>
<part name="C27" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY36" library="supply2" deviceset="GND" device=""/>
<part name="L3" library="Resistors_Caps_Inductors_Misc" deviceset="SRP7030-2R2M__2P2UH" device=""/>
<part name="C53" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C54" library="resistor" deviceset="C-EU" device="C0402" value="10u"/>
<part name="SUPPLY54" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY55" library="supply2" deviceset="GND" device=""/>
<part name="C55" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY58" library="supply2" deviceset="GND" device=""/>
<part name="C56" library="resistor" deviceset="C-EU" device="C0402" value="22u"/>
<part name="C57" library="resistor" deviceset="C-EU" device="C0402" value="22u"/>
<part name="SUPPLY60" library="supply2" deviceset="GND" device=""/>
<part name="L4" library="Resistors_Caps_Inductors_Misc" deviceset="MGV06051R0M-10__1P0UH" device=""/>
<part name="C58" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY62" library="supply2" deviceset="GND" device=""/>
<part name="C59" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY63" library="supply2" deviceset="GND" device=""/>
<part name="C60" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY64" library="supply2" deviceset="GND" device=""/>
<part name="C61" library="resistor" deviceset="C-EU" device="C0402" value="10u"/>
<part name="SUPPLY65" library="supply2" deviceset="GND" device=""/>
<part name="L6" library="Resistors_Caps_Inductors_Misc" deviceset="SRP7030-2R2M__2P2UH" device=""/>
<part name="C65" library="resistor" deviceset="C-EU" device="C0402" value="4.7u"/>
<part name="C66" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C67" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY66" library="supply2" deviceset="GND" device=""/>
<part name="C68" library="resistor" deviceset="C-EU" device="C0402" value="4.7u"/>
<part name="C69" library="resistor" deviceset="C-EU" device="C0402" value="4.7u"/>
<part name="SUPPLY67" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY68" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY69" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY70" library="supply2" deviceset="GND" device=""/>
<part name="L7" library="Resistors_Caps_Inductors_Misc" deviceset="MSS1278T-103__10UH" device=""/>
<part name="L8" library="Resistors_Caps_Inductors_Misc" deviceset="MSS1278T-223__22UH" device=""/>
<part name="U$4" library="Diodes_Transistors" deviceset="SK310A-LTP__SCHOTTKY" device=""/>
<part name="Y1" library="Diodes_Transistors" deviceset="ABS07-32.768KHZ-T__XTAL" device=""/>
<part name="Y2" library="Diodes_Transistors" deviceset="FA-20H__MHZ_XTAL" device=""/>
<part name="SUPPLY12" library="supply2" deviceset="GND" device=""/>
<part name="U10" library="Diodes_Transistors" deviceset="DEA202450BT-1294C1-H__2P4GHZ_BAND_PASS" device=""/>
<part name="SUPPLY13" library="supply2" deviceset="GND" device=""/>
<part name="C15" library="resistor" deviceset="C-EU" device="C0402" value="1p"/>
<part name="SUPPLY14" library="supply2" deviceset="GND" device=""/>
<part name="L1" library="Resistors_Caps_Inductors_Misc" deviceset="VLS6045EX-6R8M__6P8UH" device=""/>
<part name="L9" library="Resistors_Caps_Inductors_Misc" deviceset="MGV06051R0M-10__1P0UH" device=""/>
<part name="E1" library="Diodes_Transistors" deviceset="AH316M245001-T__2P4GHZ_ANTENNA" device=""/>
<part name="U11" library="ICs" deviceset="FT2232D" device=""/>
<part name="SUPPLY15" library="supply2" deviceset="GND" device=""/>
<part name="C16" library="resistor" deviceset="C-EU" device="C0402" value="33n"/>
<part name="SUPPLY16" library="supply2" deviceset="GND" device=""/>
<part name="C17" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="R8" library="resistor" deviceset="R-US_" device="R0402" value="470"/>
<part name="SUPPLY17" library="supply2" deviceset="GND" device=""/>
<part name="R9" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R10" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R11" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R12" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R13" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R21" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R31" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="R32" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="R33" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R34" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R41" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="R42" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="R43" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="SUPPLY18" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY19" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY20" library="supply2" deviceset="GND" device=""/>
<part name="R54" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="U20" library="ICs" deviceset="TPS55332-Q1" device=""/>
<part name="SUPPLY22" library="supply2" deviceset="GND" device=""/>
<part name="R49" library="resistor" deviceset="R-US_" device="R0402" value="200K"/>
<part name="SUPPLY24" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY29" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY30" library="supply2" deviceset="GND" device=""/>
<part name="U$23" library="Resistors_Caps_Inductors_Misc" deviceset="GRM31CR60J476ME19L__47UF" device=""/>
<part name="R55" library="resistor" deviceset="R-US_" device="R0402" value="4.99k"/>
<part name="SUPPLY44" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY45" library="supply2" deviceset="GND" device=""/>
<part name="R61" library="resistor" deviceset="R-US_" device="R0402" value="8.66k"/>
<part name="SUPPLY71" library="supply2" deviceset="GND" device=""/>
<part name="R65" library="resistor" deviceset="R-US_" device="R0402" value="267k"/>
<part name="R66" library="resistor" deviceset="R-US_" device="R0402" value="11.3k"/>
<part name="SUPPLY72" library="supply2" deviceset="GND" device=""/>
<part name="R67" library="resistor" deviceset="R-US_" device="R0402" value="100k"/>
<part name="SUPPLY73" library="supply2" deviceset="GND" device=""/>
<part name="L2" library="Resistors_Caps_Inductors_Misc" deviceset="MSS1278T-223__22UH" device=""/>
<part name="U$13" library="Diodes_Transistors" deviceset="SK310A-LTP__SCHOTTKY" device=""/>
<part name="C20" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C21" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY74" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY75" library="supply2" deviceset="GND" device=""/>
<part name="C22" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C23" library="resistor" deviceset="C-EU" device="C0402" value="100n"/>
<part name="C24" library="resistor" deviceset="C-EU" device="C0402" value="20n"/>
<part name="C33" library="resistor" deviceset="C-EU" device="C0402" value="100n"/>
<part name="C34" library="resistor" deviceset="C-EU" device="C0402" value="100n"/>
<part name="SUPPLY76" library="supply2" deviceset="GND" device=""/>
<part name="C18" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C19" library="resistor" deviceset="C-EU" device="C0402" value="100n"/>
<part name="C70" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="L10" library="Resistors_Caps_Inductors_Misc" deviceset="MSS1278T-103__10UH" device=""/>
<part name="U12" library="ICs" deviceset="SN74LVC125" device=""/>
<part name="U13" library="ICs" deviceset="SN74LVC125" device=""/>
<part name="U14" library="ICs" deviceset="SN74LVC126" device=""/>
<part name="R68" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="SUPPLY77" library="supply2" deviceset="GND" device=""/>
<part name="R69" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R70" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R44" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R48" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="SUPPLY79" library="supply2" deviceset="GND" device=""/>
<part name="R71" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="SUPPLY80" library="supply2" deviceset="GND" device=""/>
<part name="R51" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="SUPPLY81" library="supply2" deviceset="GND" device=""/>
<part name="R72" library="resistor" deviceset="R-US_" device="R0402" value="33"/>
<part name="R73" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="SUPPLY82" library="supply2" deviceset="GND" device=""/>
<part name="R52" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="SUPPLY78" library="supply2" deviceset="GND" device=""/>
<part name="J1" library="Board_Features_MISC" deviceset="TSM_SERIES_2PINS_SAMTEC" device=""/>
<part name="J2" library="Board_Features_MISC" deviceset="TSM_SERIES_2PINS_SAMTEC" device=""/>
<part name="U1" library="Board_Features_MISC" deviceset="MICRO_USB_CAGE" device="&quot;&quot;"/>
<part name="SUPPLY83" library="supply2" deviceset="GND" device=""/>
<part name="R74" library="resistor" deviceset="R-US_" device="R0402" value="28"/>
<part name="R75" library="resistor" deviceset="R-US_" device="R0402" value="28"/>
<part name="R76" library="resistor" deviceset="R-US_" device="R0402" value="1.5k"/>
<part name="C71" library="resistor" deviceset="C-EU" device="C0402" value="27p"/>
<part name="C72" library="resistor" deviceset="C-EU" device="C0402" value="27p"/>
<part name="SUPPLY84" library="supply2" deviceset="GND" device=""/>
<part name="R77" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="R78" library="resistor" deviceset="R-US_" device="R0402" value="7.68K"/>
<part name="C73" library="resistor" deviceset="C-EU" device="C0402" value="2.2u"/>
<part name="SUPPLY85" library="supply2" deviceset="GND" device=""/>
<part name="R79" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="C74" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY86" library="supply2" deviceset="GND" device=""/>
<part name="U$14" library="ICs_Power" deviceset="BQ78350-R1" device=""/>
<part name="U$12" library="ICs" deviceset="XETHRU_X2" device=""/>
<part name="SUPPLY87" library="supply2" deviceset="GND" device=""/>
<part name="U$15" library="Diodes_Transistors" deviceset="SER3409-ND__6MHZ_XTAL" device=""/>
<part name="U$16" library="ICs" deviceset="TPD2EUSB30DRTR" device="TPD2EUSB30DRTR"/>
<part name="SUPPLY88" library="supply2" deviceset="GND" device=""/>
<part name="U$17" library="ICs" deviceset="93LC46B-I_MS-ND__EEPROM" device=""/>
<part name="SUPPLY89" library="supply2" deviceset="GND" device=""/>
<part name="R80" library="resistor" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="R81" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="R82" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="R83" library="resistor" deviceset="R-US_" device="R0402" value="10k"/>
<part name="C75" library="resistor" deviceset="C-EU" device="C0402" value="27p"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-187.96" y="-60.96" size="10.16" layer="91" rot="R90">BATTERY</text>
<text x="-215.9" y="10.16" size="1.778" layer="91">2 milliOhm CS resistor,
HIGH side
INA250, 
CS resistor measurements</text>
<text x="-215.9" y="-76.2" size="1.778" layer="91">10 milliOhm CS resistor,
LOW side
BQ27542-G1, 
Battery Fuel Gauge</text>
<text x="-480.06" y="289.56" size="1.778" layer="91">CL21B106KQQNNNE // 10 uF
Rated @ 6.3V, 0805</text>
<text x="-480.06" y="256.54" size="1.778" layer="91">CL31A226KPHNNNE // 22 uF
Rated @ 10V, 1206</text>
<text x="-480.06" y="241.3" size="1.778" layer="91">GRM31CR60J476ME19L // 47 uF
Rated @ 6.3V, 1206</text>
<text x="-480.06" y="274.32" size="1.778" layer="91">GRM21BR61E106KA73L // 10 uF
Rated @ 25V, 0805, X5R</text>
<text x="-462.28" y="325.12" size="10.16" layer="91" font="fixed">Resistors
Capacitors
Inductors</text>
<text x="-949.96" y="-195.58" size="30.48" layer="91">BQ24616</text>
<text x="-429.26" y="-193.04" size="30.48" layer="91">BQ78350</text>
<text x="-640.08" y="-195.58" size="30.48" layer="91">BQ76920</text>
<text x="-952.5" y="-342.9" size="7.62" layer="91">Minimum pre-charge &amp; termination 
current is 125 mA with 10 mOhm sense
resistor... Can achieve lower charging currents
with different sense resistor values?
section 8.3.5 in datasheet

CE pin can be controled from MCU to 
enable/disable charging.</text>
<text x="-901.7" y="106.68" size="1.778" layer="91">2 FETS: Si4401 (Si4401BDYFDS4141)
CS resistor @ ACP/ACP &amp; VBAT load: WSL2010R0100FEA, 10 mOhms
Zener: BZX84C7V5, 7.5V</text>
<text x="-896.62" y="76.2" size="1.778" layer="91">10 mOhm Current-sense resistor, 0.5 W</text>
<text x="-320.04" y="269.24" size="1.778" layer="91">Schottky diode:
Man.: Micro Commercial Co.
Man. Part No.: SK310A-LTP
100V, 3A</text>
<text x="-467.36" y="208.28" size="1.778" layer="91">^ 22 uF (electrolytic)
Man. Part No.: 
EEE-1HA220WP</text>
<text x="-322.58" y="302.26" size="1.778" layer="91">Schottky diode for reverse protection 
on INPUT back to BQ25570
Man. Part No.: PMEG3030EP,115</text>
<text x="-467.36" y="220.98" size="1.778" layer="91">100 uF here -&gt;
Man. part no.: 
EEE-FK1H101P
</text>
<text x="-281.94" y="297.18" size="1.778" layer="91" rot="R180">&lt;- PMEG3050EP,115 (schottky diode) here</text>
<text x="-772.16" y="2.54" size="1.778" layer="91">10 mOhm Current-sense resistor, 0.5 W</text>
<text x="-784.86" y="-15.24" size="1.778" layer="91">6.8 uH 
inductor
needed</text>
<text x="340.36" y="231.14" size="1.778" layer="91">Man. Part No.: EEE-FK1H101P
Man. Part No.: EEE-1HA220WP
Man. Part No.: EEE-1HA100SP</text>
<text x="149.86" y="327.66" size="50.8" layer="91">Footprints 
needed:</text>
<text x="-985.52" y="38.1" size="1.778" layer="91">^ 22 uF (electrolytic)
Man. Part No.: 
EEE-1HA220WP</text>
<text x="-939.8" y="99.06" size="1.778" layer="91">10 Ohms
1/4 W
R1206 package</text>
<text x="-815.34" y="35.56" size="1.778" layer="91">check 10 uF output capacitors!</text>
<text x="-736.6" y="-25.4" size="1.778" layer="91">check 10 uF output capacitors!</text>
<text x="-647.7" y="-213.36" size="10.16" layer="91">3-5 cell Li-ion battery monitor</text>
<text x="-929.64" y="-236.22" size="10.16" layer="91">Multi Li-ion
cell charger</text>
<text x="-579.12" y="50.8" size="1.778" layer="91">Pin 11
Not 
Connected</text>
<text x="-525.78" y="45.72" size="1.778" layer="91">Pick 2.2uF cap!</text>
<text x="-533.4" y="10.16" size="1.778" layer="91">Pick 4.7uF cap!</text>
<text x="-533.4" y="-7.62" size="1.778" layer="91">Pick 1uF cap!</text>
<text x="187.96" y="50.8" size="1.778" layer="91">Things to check:
1.  Leave 4 AUD pins floating?
Most likely!!
optional interface for 
some applications.  not here, though
2.  Capacitance values</text>
<text x="185.42" y="-25.4" size="1.778" layer="91">make 'nSHUTD' pin 
LOW for Shutdown</text>
<text x="193.04" y="20.32" size="1.778" layer="91">32.768 kHz</text>
<text x="109.22" y="66.04" size="3.302" layer="91" rot="R270">CC2564 HCI Comm.</text>
<text x="111.76" y="-22.86" size="3.302" layer="91" rot="R90">MPU-6050 
I2C Comm.</text>
<text x="165.1" y="167.64" size="1.778" layer="91">Things to check:
1.  I2C lines need to be pulled up with 10K-ohm resistor?  If so, pulled up to what voltage?
2.  FSYNC &amp; INT lines are fine not being pulled up?
3.  Master I2C lines (on right side of symbol) are fine to be left floating if unused?  
(We are not using them because this MPU-6050 part is not an I2C master to other I2C slaves.
4.  Are we using CLKIN pin?</text>
<text x="314.96" y="127" size="20.32" layer="90">Accelerometer, MPU-6050</text>
<text x="317.5" y="50.8" size="20.32" layer="90">Bluetooth I/O, CC2564MODA</text>
<text x="-116.84" y="-213.36" size="20.32" layer="90">CC3200, IoT MCU</text>
<text x="-109.22" y="-144.78" size="1.778" layer="91">32.768 kHz XTAL
ABS07-32.768KHZ-T</text>
<text x="-373.38" y="269.24" size="1.778" layer="91">Inductor: 22 uH
Man.: Coilcraft
Man. Part No.: MSS1278T-223MLB</text>
<text x="-421.64" y="-233.68" size="10.16" layer="91">companion MCU to 
BQ769X0 series</text>
<text x="-612.14" y="-231.14" size="10.16" layer="91">5 Li-ion cells</text>
<text x="114.3" y="-86.36" size="3.302" layer="91" rot="R90">BQ24616
I/O</text>
<text x="-325.12" y="322.58" size="10.16" layer="91" font="fixed">Diodes
FETs</text>
<text x="-403.86" y="-63.5" size="10.16" layer="91">BQ78350-R1 needed!</text>
<text x="337.82" y="-144.78" size="20.32" layer="90">Xethru X2</text>
<text x="-741.68" y="-340.36" size="30.48" layer="91">BQ34Z100-G1
BQ24610</text>
<text x="101.6" y="96.52" size="1.778" layer="91">3.6 nH inductor 
needed!!</text>
<text x="292.1" y="215.9" size="1.778" layer="91">FT2232D
Flash device</text>
<text x="22.86" y="-444.5" size="1.778" layer="91" rot="MR270">BDBUS2.. all unconnected!!</text>
<text x="-66.04" y="-566.42" size="20.32" layer="90">CC3200 Programming Circuit</text>
<text x="-1254.76" y="-116.84" size="30.48" layer="91">TPS55332-Q1 </text>
<text x="-1064.26" y="45.72" size="1.778" layer="91">BQ24616 -- 24 V input to charge 5 cells</text>
<text x="-1109.98" y="73.66" size="1.778" layer="91">Schottky diode:
Man.: Micro Commercial Co.
Man. Part No.: SK310A-LTP
100V, 3A</text>
<text x="-1163.32" y="76.2" size="1.778" layer="91">Inductor: 22 uH
Man.: Coilcraft
Man. Part No.: MSS1278T-223MLB</text>
<text x="-1153.16" y="71.12" size="1.778" layer="91">22 uH</text>
<text x="-1084.58" y="15.24" size="1.778" layer="91">^ 22 uF (electrolytic)
Man. Part No.: 
EEE-1HA220WP</text>
<text x="-1270" y="76.2" size="1.778" layer="91" rot="R90">Schottky diode for reverse protection 
on INPUT back to BQ25570
Man. Part No.: PMEG3030EP,115</text>
<text x="-1231.9" y="-7.62" size="1.778" layer="91">SYNC (input) has interal ~62 kOhm pull-down.</text>
<text x="-1211.58" y="-27.94" size="1.778" layer="91">RST (outut) -- open drain, 
sometimes pulled up to VREG pin.</text>
<text x="-1221.74" y="48.26" size="1.778" layer="91">100 uF here -&gt;
Man. part no.: 
EEE-FK1H101P
</text>
<text x="-1264.92" y="116.84" size="1.778" layer="91" rot="R270">&lt;- PMEG3050EP,115 (schottky diode) here</text>
<text x="-1244.6" y="-139.7" size="10.16" layer="91">Boost Converter</text>
<text x="144.78" y="-327.66" size="1.778" layer="91">U9A-D</text>
<text x="220.98" y="-325.12" size="1.778" layer="91">U4A-D</text>
<text x="220.98" y="-431.8" size="1.778" layer="91">U7A-D</text>
<text x="12.7" y="-172.72" size="10.16" layer="91">JTAG</text>
<text x="312.42" y="-441.96" size="10.16" layer="91">JTAG Header</text>
<text x="-406.4" y="48.26" size="1.778" layer="91" rot="R90">25 ppm/C</text>
<text x="-431.8" y="-30.48" size="1.778" layer="91">Falling edge on DISP pin 
(maybe not exactly 
active low..)
prompts RSOC to be 
displayed on 5 LEDs.</text>
<text x="111.76" y="-58.42" size="3.302" layer="91" rot="R90">XETHRU X2
   SPI</text>
<text x="45.72" y="-147.32" size="3.302" layer="91">ANTSEL1/2 for GPIO???</text>
<text x="289.56" y="-147.32" size="1.778" layer="91">CLK_OUT, probably unconnected/floating.</text>
<text x="175.26" y="-167.64" size="1.778" layer="91">Asynchronous Reset,
Active Low</text>
<text x="165.1" y="-147.32" size="1.778" layer="91">Typical f_SCLK = 15 MHz</text>
<text x="-83.82" y="-424.18" size="1.778" layer="91">SER3409-ND__6MHZ_XTAL</text>
<text x="-101.6" y="-408.94" size="1.778" layer="91">TPD2EUSB30DRTR, 
USB ESD Protection</text>
<text x="-124.46" y="-464.82" size="1.778" layer="91">'ORG' pin unused
on EEPROM.</text>
<text x="-175.26" y="-414.02" size="1.778" layer="91">To Do:
1.  'ID' line on Micro USB 
cage??  Grounded?</text>
<text x="327.66" y="-172.72" size="1.778" layer="91">To Do:
1.  IREF DAC current pin
2.  RF IN antenna
3.  RF OUT antenna
4.  Decoupling capacitors</text>
</plain>
<instances>
<instance part="BAT1" gate="G$1" x="-223.52" y="-38.1"/>
<instance part="U$8" gate="A" x="-383.54" y="271.78" rot="R90"/>
<instance part="U$9" gate="G$1" x="-383.54" y="287.02" rot="R90"/>
<instance part="U$1" gate="G$1" x="-441.96" y="274.32" rot="MR0"/>
<instance part="U$10" gate="A" x="-441.96" y="289.56" rot="MR0"/>
<instance part="U$5" gate="G$1" x="-441.96" y="259.08"/>
<instance part="U$6" gate="A" x="-441.96" y="243.84"/>
<instance part="SUPPLY6" gate="GND" x="-904.24" y="-109.22" rot="MR0"/>
<instance part="SUPPLY11" gate="GND" x="-886.46" y="-109.22" rot="MR0"/>
<instance part="C3" gate="G$1" x="-889" y="55.88" rot="R90"/>
<instance part="C4" gate="G$1" x="-899.16" y="48.26" rot="R180"/>
<instance part="SUPPLY21" gate="GND" x="-899.16" y="40.64" rot="MR0"/>
<instance part="R38" gate="G$1" x="-886.46" y="68.58"/>
<instance part="R14" gate="G$1" x="-960.12" y="-10.16" rot="R270"/>
<instance part="R15" gate="G$1" x="-960.12" y="-45.72" rot="R270"/>
<instance part="R16" gate="G$1" x="-952.5" y="-10.16" rot="R270"/>
<instance part="R18" gate="G$1" x="-952.5" y="-45.72" rot="R270"/>
<instance part="R19" gate="G$1" x="-944.88" y="-10.16" rot="R270"/>
<instance part="R20" gate="G$1" x="-944.88" y="-45.72" rot="R270"/>
<instance part="SUPPLY26" gate="GND" x="-952.5" y="-60.96"/>
<instance part="C25" gate="G$1" x="-840.74" y="-53.34" rot="R270"/>
<instance part="SUPPLY31" gate="GND" x="-848.36" y="-58.42" rot="MR0"/>
<instance part="C26" gate="G$1" x="-848.36" y="-38.1"/>
<instance part="C28" gate="G$1" x="-840.74" y="-20.32" rot="R270"/>
<instance part="SUPPLY32" gate="GND" x="-848.36" y="-25.4" rot="MR0"/>
<instance part="C29" gate="G$1" x="-848.36" y="-81.28"/>
<instance part="SUPPLY33" gate="GND" x="-848.36" y="-91.44" rot="MR0"/>
<instance part="U3" gate="G$1" x="-889" y="-22.86"/>
<instance part="C31" gate="G$1" x="-929.64" y="-99.06" rot="MR0"/>
<instance part="SUPPLY35" gate="GND" x="-929.64" y="-109.22" rot="MR0"/>
<instance part="R17" gate="G$1" x="-840.74" y="22.86"/>
<instance part="R22" gate="G$1" x="-777.24" y="33.02" rot="R90"/>
<instance part="C32" gate="G$1" x="-767.08" y="33.02" rot="R180"/>
<instance part="C30" gate="G$1" x="-848.36" y="43.18" rot="R270"/>
<instance part="SUPPLY37" gate="GND" x="-840.74" y="35.56" rot="MR0"/>
<instance part="C35" gate="G$1" x="-861.06" y="60.96" rot="R270"/>
<instance part="U$27" gate="G$1" x="-739.14" y="-7.62"/>
<instance part="U$29" gate="G$1" x="-723.9" y="-7.62"/>
<instance part="C36" gate="G$1" x="-711.2" y="-10.16" rot="R180"/>
<instance part="SUPPLY38" gate="GND" x="-723.9" y="-20.32" rot="MR0"/>
<instance part="R23" gate="G$1" x="-754.38" y="-20.32" rot="R90"/>
<instance part="C37" gate="G$1" x="-744.22" y="-20.32" rot="R180"/>
<instance part="R24" gate="G$1" x="-754.38" y="-35.56" rot="R90"/>
<instance part="SUPPLY39" gate="GND" x="-754.38" y="-48.26" rot="MR0"/>
<instance part="R45" gate="G$1" x="-762" y="-2.54" rot="MR180"/>
<instance part="Q2" gate="G$1" x="-916.94" y="60.96" rot="R90"/>
<instance part="Q1" gate="G$1" x="-944.88" y="60.96" rot="R90"/>
<instance part="R27" gate="G$1" x="-916.94" y="43.18"/>
<instance part="R28" gate="G$1" x="-937.26" y="53.34" rot="R90"/>
<instance part="C38" gate="G$1" x="-924.56" y="53.34" rot="R180"/>
<instance part="C40" gate="G$1" x="-967.74" y="20.32"/>
<instance part="C41" gate="G$1" x="-957.58" y="20.32"/>
<instance part="SUPPLY41" gate="GND" x="-962.66" y="5.08"/>
<instance part="R30" gate="G$1" x="-932.18" y="71.12" rot="R90"/>
<instance part="Q3" gate="G$1" x="-754.38" y="20.32"/>
<instance part="Q4" gate="G$1" x="-805.18" y="12.7"/>
<instance part="Q5" gate="G$1" x="-805.18" y="-12.7"/>
<instance part="SUPPLY42" gate="GND" x="-805.18" y="-25.4" rot="MR0"/>
<instance part="U$3" gate="G$1" x="-812.8" y="63.5"/>
<instance part="U$7" gate="G$1" x="-797.56" y="55.88" rot="MR0"/>
<instance part="C39" gate="G$1" x="-784.86" y="53.34" rot="R180"/>
<instance part="SUPPLY40" gate="GND" x="-797.56" y="40.64" rot="MR0"/>
<instance part="SUPPLY43" gate="GND" x="-553.72" y="-96.52" rot="MR0"/>
<instance part="SUPPLY46" gate="GND" x="-365.76" y="-40.64"/>
<instance part="L5" gate="G$1" x="-779.78" y="-2.54" rot="R90"/>
<instance part="R25" gate="G$1" x="-581.66" y="-111.76"/>
<instance part="C7" gate="G$1" x="-607.06" y="-58.42" rot="R180"/>
<instance part="R26" gate="G$1" x="-619.76" y="-50.8" rot="R180"/>
<instance part="SUPPLY7" gate="GND" x="-607.06" y="-68.58" rot="MR0"/>
<instance part="C42" gate="G$1" x="-604.52" y="-43.18" rot="R180"/>
<instance part="R37" gate="G$1" x="-619.76" y="-35.56" rot="R180"/>
<instance part="C43" gate="G$1" x="-604.52" y="-22.86" rot="R180"/>
<instance part="R35" gate="G$1" x="-619.76" y="-15.24" rot="R180"/>
<instance part="R29" gate="G$1" x="-497.84" y="35.56" rot="R270"/>
<instance part="U$11" gate="G$1" x="-431.8" y="-10.16" rot="MR0"/>
<instance part="SUPPLY27" gate="GND" x="-436.88" y="-22.86"/>
<instance part="Q8" gate="G$1" x="-307.34" y="254" rot="MR270"/>
<instance part="C44" gate="G$1" x="-604.52" y="-2.54" rot="R180"/>
<instance part="R46" gate="G$1" x="-619.76" y="5.08" rot="R180"/>
<instance part="C45" gate="G$1" x="-604.52" y="17.78" rot="R180"/>
<instance part="R50" gate="G$1" x="-619.76" y="25.4" rot="R180"/>
<instance part="C46" gate="G$1" x="-604.52" y="38.1" rot="R180"/>
<instance part="R53" gate="G$1" x="-619.76" y="45.72" rot="R180"/>
<instance part="U$24" gate="G$1" x="-447.04" y="-2.54" rot="MR0"/>
<instance part="U$25" gate="G$1" x="-462.28" y="5.08" rot="MR0"/>
<instance part="U$26" gate="G$1" x="-477.52" y="12.7" rot="MR0"/>
<instance part="U$28" gate="G$1" x="-492.76" y="20.32" rot="MR0"/>
<instance part="R56" gate="G$1" x="-482.6" y="35.56" rot="R270"/>
<instance part="R57" gate="G$1" x="-467.36" y="35.56" rot="R270"/>
<instance part="R58" gate="G$1" x="-452.12" y="35.56" rot="R270"/>
<instance part="R59" gate="G$1" x="-436.88" y="35.56" rot="R270"/>
<instance part="R60" gate="G$1" x="-528.32" y="-50.8" rot="R180"/>
<instance part="SUPPLY28" gate="GND" x="-535.94" y="-68.58" rot="MR0"/>
<instance part="Q13" gate="G$1" x="-505.46" y="-83.82"/>
<instance part="SUPPLY47" gate="GND" x="-515.62" y="-86.36" rot="MR0"/>
<instance part="R62" gate="G$1" x="-505.46" y="-99.06" rot="R270"/>
<instance part="R63" gate="G$1" x="-505.46" y="-114.3" rot="R270"/>
<instance part="Q14" gate="G$1" x="-508" y="-134.62" rot="R270"/>
<instance part="Q15" gate="G$1" x="-538.48" y="-134.62" rot="R270"/>
<instance part="R64" gate="G$1" x="-543.56" y="-124.46"/>
<instance part="C47" gate="G$1" x="-538.48" y="-5.08"/>
<instance part="SUPPLY48" gate="GND" x="-538.48" y="-15.24" rot="MR0"/>
<instance part="C48" gate="G$1" x="-538.48" y="17.78"/>
<instance part="SUPPLY49" gate="GND" x="-538.48" y="7.62" rot="MR0"/>
<instance part="U$2" gate="G$1" x="-571.5" y="-12.7"/>
<instance part="C49" gate="G$1" x="-528.32" y="30.48"/>
<instance part="SUPPLY50" gate="GND" x="-528.32" y="20.32" rot="MR0"/>
<instance part="C50" gate="G$1" x="-599.44" y="-99.06" rot="MR0"/>
<instance part="SUPPLY51" gate="GND" x="-599.44" y="-109.22" rot="MR0"/>
<instance part="C51" gate="G$1" x="-563.88" y="-99.06"/>
<instance part="SUPPLY52" gate="GND" x="-563.88" y="-109.22" rot="MR0"/>
<instance part="R36" gate="G$1" x="-416.56" y="93.98"/>
<instance part="R39" gate="G$1" x="-571.5" y="-101.6" rot="R270"/>
<instance part="R40" gate="G$1" x="-591.82" y="-101.6" rot="R270"/>
<instance part="R47" gate="G$1" x="-535.94" y="-58.42" rot="MR270"/>
<instance part="C52" gate="G$1" x="-518.16" y="38.1"/>
<instance part="SUPPLY53" gate="GND" x="-518.16" y="27.94" rot="MR0"/>
<instance part="SUPPLY56" gate="GND" x="271.78" y="-22.86"/>
<instance part="SUPPLY57" gate="GND" x="238.76" y="-43.18"/>
<instance part="TP1" gate="G$1" x="279.4" y="35.56"/>
<instance part="C62" gate="G$1" x="170.18" y="27.94"/>
<instance part="SUPPLY59" gate="GND" x="170.18" y="17.78"/>
<instance part="C63" gate="G$1" x="157.48" y="27.94"/>
<instance part="C64" gate="G$1" x="147.32" y="27.94"/>
<instance part="SUPPLY61" gate="GND" x="152.4" y="15.24"/>
<instance part="U7" gate="G$1" x="238.76" y="5.08"/>
<instance part="U8" gate="G$1" x="238.76" y="114.3"/>
<instance part="C1" gate="G$1" x="243.84" y="78.74"/>
<instance part="SUPPLY2" gate="GND" x="243.84" y="66.04"/>
<instance part="C2" gate="G$1" x="233.68" y="78.74"/>
<instance part="C5" gate="G$1" x="228.6" y="152.4"/>
<instance part="SUPPLY1" gate="GND" x="228.6" y="144.78"/>
<instance part="C6" gate="G$1" x="213.36" y="152.4"/>
<instance part="SUPPLY3" gate="GND" x="213.36" y="144.78"/>
<instance part="R1" gate="G$1" x="193.04" y="134.62"/>
<instance part="R2" gate="G$1" x="193.04" y="124.46"/>
<instance part="R3" gate="G$1" x="193.04" y="114.3"/>
<instance part="U9" gate="G$1" x="0" y="-7.62"/>
<instance part="SUPPLY4" gate="GND" x="7.62" y="-101.6"/>
<instance part="C8" gate="G$1" x="-116.84" y="-137.16" rot="R270"/>
<instance part="C9" gate="G$1" x="-116.84" y="-129.54" rot="R270"/>
<instance part="SUPPLY5" gate="GND" x="-132.08" y="-144.78"/>
<instance part="R4" gate="G$1" x="-25.4" y="-167.64" rot="R90"/>
<instance part="R5" gate="G$1" x="-17.78" y="-167.64" rot="R90"/>
<instance part="R6" gate="G$1" x="-10.16" y="-167.64" rot="R90"/>
<instance part="SUPPLY8" gate="GND" x="-17.78" y="-180.34"/>
<instance part="R7" gate="G$1" x="-76.2" y="-154.94" rot="R180"/>
<instance part="C10" gate="G$1" x="-81.28" y="-111.76"/>
<instance part="SUPPLY9" gate="GND" x="-81.28" y="-124.46"/>
<instance part="C11" gate="G$1" x="-96.52" y="-111.76"/>
<instance part="SUPPLY10" gate="GND" x="-96.52" y="-124.46"/>
<instance part="TP2" gate="G$1" x="-2.54" y="-147.32"/>
<instance part="C12" gate="G$1" x="73.66" y="-111.76"/>
<instance part="SUPPLY23" gate="GND" x="73.66" y="-124.46"/>
<instance part="C13" gate="G$1" x="111.76" y="-111.76"/>
<instance part="SUPPLY25" gate="GND" x="111.76" y="-124.46"/>
<instance part="C14" gate="G$1" x="-109.22" y="-91.44"/>
<instance part="SUPPLY34" gate="GND" x="-109.22" y="-104.14"/>
<instance part="C27" gate="G$1" x="-109.22" y="-60.96"/>
<instance part="SUPPLY36" gate="GND" x="-109.22" y="-71.12"/>
<instance part="L3" gate="A" x="-88.9" y="-35.56" rot="R90"/>
<instance part="C53" gate="G$1" x="-111.76" y="-40.64"/>
<instance part="C54" gate="G$1" x="-124.46" y="-40.64"/>
<instance part="SUPPLY54" gate="GND" x="-124.46" y="-50.8"/>
<instance part="SUPPLY55" gate="GND" x="-111.76" y="-50.8"/>
<instance part="C55" gate="G$1" x="-78.74" y="-22.86"/>
<instance part="SUPPLY58" gate="GND" x="-78.74" y="-30.48"/>
<instance part="C56" gate="G$1" x="-93.98" y="-15.24"/>
<instance part="C57" gate="G$1" x="-106.68" y="-15.24"/>
<instance part="SUPPLY60" gate="GND" x="-101.6" y="-27.94"/>
<instance part="L4" gate="G$1" x="-76.2" y="20.32" rot="R180"/>
<instance part="C58" gate="G$1" x="-86.36" y="43.18"/>
<instance part="SUPPLY62" gate="GND" x="-86.36" y="35.56"/>
<instance part="C59" gate="G$1" x="-96.52" y="43.18"/>
<instance part="SUPPLY63" gate="GND" x="-96.52" y="35.56"/>
<instance part="C60" gate="G$1" x="-109.22" y="43.18"/>
<instance part="SUPPLY64" gate="GND" x="-109.22" y="35.56"/>
<instance part="C61" gate="G$1" x="-121.92" y="43.18"/>
<instance part="SUPPLY65" gate="GND" x="-121.92" y="35.56"/>
<instance part="L6" gate="A" x="-86.36" y="104.14" rot="R90"/>
<instance part="C65" gate="G$1" x="-50.8" y="127"/>
<instance part="C66" gate="G$1" x="-7.62" y="127"/>
<instance part="C67" gate="G$1" x="7.62" y="127"/>
<instance part="SUPPLY66" gate="GND" x="-50.8" y="116.84"/>
<instance part="C68" gate="G$1" x="-35.56" y="127"/>
<instance part="C69" gate="G$1" x="-22.86" y="127"/>
<instance part="SUPPLY67" gate="GND" x="-35.56" y="116.84"/>
<instance part="SUPPLY68" gate="GND" x="-22.86" y="116.84"/>
<instance part="SUPPLY69" gate="GND" x="-7.62" y="116.84"/>
<instance part="SUPPLY70" gate="GND" x="7.62" y="116.84"/>
<instance part="L7" gate="G$1" x="-383.54" y="238.76" rot="R90"/>
<instance part="L8" gate="G$1" x="-383.54" y="223.52" rot="R90"/>
<instance part="U$4" gate="G$1" x="-281.94" y="274.32" rot="R270"/>
<instance part="Y1" gate="G$1" x="-106.68" y="-132.08"/>
<instance part="Y2" gate="G$1" x="93.98" y="-114.3" rot="R180"/>
<instance part="SUPPLY12" gate="GND" x="93.98" y="-109.22"/>
<instance part="U10" gate="G$1" x="91.44" y="106.68"/>
<instance part="SUPPLY13" gate="GND" x="91.44" y="93.98"/>
<instance part="C15" gate="G$1" x="121.92" y="99.06"/>
<instance part="SUPPLY14" gate="GND" x="121.92" y="86.36"/>
<instance part="L1" gate="G$1" x="-383.54" y="256.54" rot="R90"/>
<instance part="L9" gate="G$1" x="111.76" y="106.68" rot="R90"/>
<instance part="E1" gate="G$1" x="134.62" y="104.14"/>
<instance part="U11" gate="G$1" x="-10.16" y="-434.34"/>
<instance part="SUPPLY15" gate="GND" x="-10.16" y="-510.54"/>
<instance part="C16" gate="G$1" x="15.24" y="-337.82" rot="R90"/>
<instance part="SUPPLY16" gate="GND" x="22.86" y="-342.9"/>
<instance part="C17" gate="G$1" x="-22.86" y="-330.2" rot="MR0"/>
<instance part="R8" gate="G$1" x="-17.78" y="-317.5" rot="R270"/>
<instance part="SUPPLY17" gate="GND" x="-22.86" y="-340.36"/>
<instance part="R9" gate="G$1" x="27.94" y="-360.68"/>
<instance part="R10" gate="G$1" x="27.94" y="-365.76"/>
<instance part="R11" gate="G$1" x="27.94" y="-370.84"/>
<instance part="R12" gate="G$1" x="27.94" y="-375.92"/>
<instance part="R13" gate="G$1" x="27.94" y="-396.24"/>
<instance part="R21" gate="G$1" x="27.94" y="-391.16"/>
<instance part="R31" gate="G$1" x="-48.26" y="-487.68"/>
<instance part="R32" gate="G$1" x="-48.26" y="-497.84"/>
<instance part="R33" gate="G$1" x="27.94" y="-434.34"/>
<instance part="R34" gate="G$1" x="27.94" y="-439.42"/>
<instance part="R41" gate="G$1" x="-55.88" y="-515.62"/>
<instance part="R42" gate="G$1" x="58.42" y="-391.16"/>
<instance part="R43" gate="G$1" x="58.42" y="-386.08"/>
<instance part="SUPPLY18" gate="GND" x="-43.18" y="-474.98"/>
<instance part="SUPPLY19" gate="GND" x="231.14" y="-424.18"/>
<instance part="SUPPLY20" gate="GND" x="231.14" y="-530.86"/>
<instance part="R54" gate="G$1" x="251.46" y="-452.12"/>
<instance part="U20" gate="A" x="-1150.62" y="-12.7"/>
<instance part="SUPPLY22" gate="GND" x="-1155.7" y="-71.12"/>
<instance part="R49" gate="G$1" x="-1188.72" y="22.86" rot="R270"/>
<instance part="SUPPLY24" gate="GND" x="-1188.72" y="12.7"/>
<instance part="SUPPLY29" gate="GND" x="-1181.1" y="-5.08"/>
<instance part="SUPPLY30" gate="GND" x="-1181.1" y="-45.72"/>
<instance part="U$23" gate="A" x="-1181.1" y="-35.56" rot="MR0"/>
<instance part="R55" gate="G$1" x="-1120.14" y="-58.42" rot="R270"/>
<instance part="SUPPLY44" gate="GND" x="-1120.14" y="-68.58"/>
<instance part="SUPPLY45" gate="GND" x="-1115.06" y="-48.26"/>
<instance part="R61" gate="G$1" x="-1097.28" y="-25.4" rot="R270"/>
<instance part="SUPPLY71" gate="GND" x="-1097.28" y="-35.56"/>
<instance part="R65" gate="G$1" x="-1097.28" y="-7.62" rot="R270"/>
<instance part="R66" gate="G$1" x="-1107.44" y="7.62" rot="R270"/>
<instance part="SUPPLY72" gate="GND" x="-1107.44" y="-2.54"/>
<instance part="R67" gate="G$1" x="-1107.44" y="25.4" rot="R270"/>
<instance part="SUPPLY73" gate="GND" x="-1089.66" y="20.32"/>
<instance part="L2" gate="G$1" x="-1150.62" y="66.04" rot="R90"/>
<instance part="U$13" gate="G$1" x="-1107.44" y="66.04"/>
<instance part="C20" gate="G$1" x="-1049.02" y="33.02"/>
<instance part="C21" gate="G$1" x="-1038.86" y="33.02"/>
<instance part="SUPPLY74" gate="GND" x="-1130.3" y="58.42" rot="R90"/>
<instance part="SUPPLY75" gate="GND" x="-1201.42" y="48.26"/>
<instance part="C22" gate="G$1" x="-1193.8" y="60.96"/>
<instance part="C23" gate="G$1" x="-1186.18" y="60.96"/>
<instance part="C24" gate="G$1" x="-1181.1" y="5.08" rot="MR0"/>
<instance part="C33" gate="G$1" x="-1115.06" y="-38.1" rot="MR0"/>
<instance part="C34" gate="G$1" x="-1117.6" y="10.16" rot="MR0"/>
<instance part="SUPPLY76" gate="GND" x="-1252.22" y="48.26"/>
<instance part="C18" gate="G$1" x="-1244.6" y="60.96"/>
<instance part="C19" gate="G$1" x="-1236.98" y="60.96"/>
<instance part="C70" gate="G$1" x="-1252.22" y="60.96" rot="MR0"/>
<instance part="L10" gate="G$1" x="-1219.2" y="68.58" rot="R90"/>
<instance part="U12" gate="G$1" x="149.86" y="-370.84"/>
<instance part="U13" gate="G$1" x="231.14" y="-365.76"/>
<instance part="U14" gate="G$1" x="231.14" y="-472.44"/>
<instance part="R68" gate="G$1" x="132.08" y="-391.16"/>
<instance part="SUPPLY77" gate="GND" x="124.46" y="-396.24"/>
<instance part="R69" gate="G$1" x="170.18" y="-391.16"/>
<instance part="R70" gate="G$1" x="251.46" y="-406.4"/>
<instance part="R44" gate="G$1" x="251.46" y="-386.08"/>
<instance part="R48" gate="G$1" x="251.46" y="-353.06"/>
<instance part="SUPPLY79" gate="GND" x="259.08" y="-358.14"/>
<instance part="R71" gate="G$1" x="251.46" y="-375.92"/>
<instance part="SUPPLY80" gate="GND" x="149.86" y="-429.26"/>
<instance part="R51" gate="G$1" x="170.18" y="-378.46"/>
<instance part="SUPPLY81" gate="GND" x="177.8" y="-383.54"/>
<instance part="R72" gate="G$1" x="210.82" y="-513.08"/>
<instance part="R73" gate="G$1" x="210.82" y="-472.44"/>
<instance part="SUPPLY82" gate="GND" x="203.2" y="-477.52"/>
<instance part="R52" gate="G$1" x="132.08" y="-350.52"/>
<instance part="SUPPLY78" gate="GND" x="124.46" y="-355.6"/>
<instance part="J1" gate="G$1" x="353.06" y="-414.02" rot="R180"/>
<instance part="J2" gate="G$1" x="353.06" y="-398.78" rot="R180"/>
<instance part="U1" gate="G$1" x="-152.4" y="-383.54" rot="R180"/>
<instance part="SUPPLY83" gate="GND" x="-144.78" y="-393.7"/>
<instance part="R74" gate="G$1" x="-66.04" y="-378.46"/>
<instance part="R75" gate="G$1" x="-66.04" y="-388.62"/>
<instance part="R76" gate="G$1" x="-66.04" y="-398.78"/>
<instance part="C71" gate="G$1" x="-88.9" y="-419.1" rot="MR90"/>
<instance part="C72" gate="G$1" x="-88.9" y="-429.26" rot="MR90"/>
<instance part="SUPPLY84" gate="GND" x="-101.6" y="-429.26"/>
<instance part="R77" gate="G$1" x="-416.56" y="86.36"/>
<instance part="R78" gate="G$1" x="-411.48" y="58.42" rot="R90"/>
<instance part="C73" gate="G$1" x="-424.18" y="58.42"/>
<instance part="SUPPLY85" gate="GND" x="-419.1" y="45.72" rot="MR0"/>
<instance part="R79" gate="G$1" x="-419.1" y="71.12"/>
<instance part="C74" gate="G$1" x="-411.48" y="40.64" rot="R270"/>
<instance part="SUPPLY86" gate="GND" x="-419.1" y="35.56" rot="MR0"/>
<instance part="U$14" gate="G$1" x="-360.68" y="35.56"/>
<instance part="U$12" gate="G$1" x="241.3" y="-129.54"/>
<instance part="SUPPLY87" gate="GND" x="238.76" y="-193.04"/>
<instance part="U$15" gate="G$1" x="-48.26" y="-424.18"/>
<instance part="U$16" gate="G$1" x="-106.68" y="-396.24" rot="R270"/>
<instance part="SUPPLY88" gate="GND" x="-106.68" y="-411.48"/>
<instance part="U$17" gate="G$1" x="-83.82" y="-454.66"/>
<instance part="SUPPLY89" gate="GND" x="-83.82" y="-474.98"/>
<instance part="R80" gate="G$1" x="-60.96" y="-459.74"/>
<instance part="R81" gate="G$1" x="-66.04" y="-431.8"/>
<instance part="R82" gate="G$1" x="-63.5" y="-434.34"/>
<instance part="R83" gate="G$1" x="-60.96" y="-436.88"/>
<instance part="C75" gate="G$1" x="-101.6" y="-449.58" rot="MR0"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="-904.24" y1="-101.6" x2="-904.24" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="-886.46" y1="-101.6" x2="-886.46" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<pinref part="U3" gate="G$1" pin="PAD_GND1"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-899.16" y1="45.72" x2="-899.16" y2="43.18" width="0.1524" layer="91"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="-960.12" y1="-50.8" x2="-960.12" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-960.12" y1="-55.88" x2="-952.5" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="-952.5" y1="-55.88" x2="-952.5" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-952.5" y1="-55.88" x2="-944.88" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="-944.88" y1="-55.88" x2="-944.88" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-952.5" y1="-55.88" x2="-952.5" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
<junction x="-952.5" y="-55.88"/>
<label x="-957.58" y="-55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="-845.82" y1="-53.34" x2="-848.36" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-848.36" y1="-53.34" x2="-848.36" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="-845.82" y1="-20.32" x2="-848.36" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-848.36" y1="-20.32" x2="-848.36" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="-848.36" y1="-88.9" x2="-848.36" y2="-86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
<wire x1="-929.64" y1="-104.14" x2="-929.64" y2="-106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="-845.82" y1="43.18" x2="-840.74" y2="43.18" width="0.1524" layer="91"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<wire x1="-840.74" y1="43.18" x2="-840.74" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$29" gate="G$1" pin="2"/>
<wire x1="-723.9" y1="-12.7" x2="-723.9" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-723.9" y1="-15.24" x2="-739.14" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="U$27" gate="G$1" pin="2"/>
<wire x1="-739.14" y1="-15.24" x2="-739.14" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-723.9" y="-15.24"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="-711.2" y1="-12.7" x2="-711.2" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-711.2" y1="-15.24" x2="-723.9" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
<wire x1="-723.9" y1="-17.78" x2="-723.9" y2="-15.24" width="0.1524" layer="91"/>
<label x="-736.6" y="-15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="-754.38" y1="-40.64" x2="-754.38" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="-957.58" y1="15.24" x2="-957.58" y2="10.16" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="-957.58" y1="10.16" x2="-962.66" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-962.66" y1="10.16" x2="-967.74" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-967.74" y1="10.16" x2="-967.74" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-962.66" y1="10.16" x2="-962.66" y2="7.62" width="0.1524" layer="91"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
<junction x="-962.66" y="10.16"/>
</segment>
<segment>
<pinref part="Q5" gate="G$1" pin="S"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
<wire x1="-805.18" y1="-17.78" x2="-805.18" y2="-22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="2"/>
<pinref part="U$3" gate="G$1" pin="2"/>
<wire x1="-797.56" y1="45.72" x2="-812.8" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-812.8" y1="45.72" x2="-812.8" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-797.56" y1="50.8" x2="-797.56" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="-784.86" y1="50.8" x2="-784.86" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-784.86" y1="45.72" x2="-797.56" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-797.56" y1="45.72" x2="-797.56" y2="43.18" width="0.1524" layer="91"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
<junction x="-797.56" y="45.72"/>
</segment>
<segment>
<wire x1="-373.38" y1="-33.02" x2="-373.38" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="-35.56" x2="-365.76" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="-35.56" x2="-358.14" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-358.14" y1="-35.56" x2="-358.14" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="-33.02" x2="-365.76" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
<wire x1="-365.76" y1="-38.1" x2="-365.76" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-365.76" y="-35.56"/>
<pinref part="U$14" gate="G$1" pin="VSS3"/>
<pinref part="U$14" gate="G$1" pin="VSS1"/>
<pinref part="U$14" gate="G$1" pin="VSS2"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-607.06" y1="-60.96" x2="-607.06" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="-607.06" y1="-63.5" x2="-607.06" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="-604.52" y1="-45.72" x2="-604.52" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="-604.52" y1="-63.5" x2="-607.06" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-607.06" y="-63.5"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="E"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
<wire x1="-436.88" y1="-15.24" x2="-436.88" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U$24" gate="G$1" pin="E"/>
<wire x1="-436.88" y1="-17.78" x2="-436.88" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-436.88" y1="-17.78" x2="-452.12" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-452.12" y1="-17.78" x2="-452.12" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U$25" gate="G$1" pin="E"/>
<wire x1="-452.12" y1="-17.78" x2="-467.36" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-467.36" y1="-17.78" x2="-467.36" y2="0" width="0.1524" layer="91"/>
<pinref part="U$26" gate="G$1" pin="E"/>
<wire x1="-467.36" y1="-17.78" x2="-482.6" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-482.6" y1="-17.78" x2="-482.6" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U$28" gate="G$1" pin="E"/>
<wire x1="-482.6" y1="-17.78" x2="-497.84" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-497.84" y1="-17.78" x2="-497.84" y2="15.24" width="0.1524" layer="91"/>
<junction x="-482.6" y="-17.78"/>
<junction x="-467.36" y="-17.78"/>
<junction x="-452.12" y="-17.78"/>
<junction x="-436.88" y="-17.78"/>
</segment>
<segment>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<wire x1="-535.94" y1="-66.04" x2="-535.94" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="Q13" gate="G$1" pin="G"/>
<wire x1="-510.54" y1="-81.28" x2="-515.62" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-515.62" y1="-81.28" x2="-515.62" y2="-83.82" width="0.1524" layer="91"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="2"/>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
<wire x1="-538.48" y1="-10.16" x2="-538.48" y2="-12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="-538.48" y1="12.7" x2="-538.48" y2="10.16" width="0.1524" layer="91"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="-528.32" y1="25.4" x2="-528.32" y2="22.86" width="0.1524" layer="91"/>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="2"/>
<wire x1="-599.44" y1="-104.14" x2="-599.44" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VSS"/>
<wire x1="-563.88" y1="-88.9" x2="-563.88" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="-563.88" y1="-91.44" x2="-553.72" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<wire x1="-553.72" y1="-91.44" x2="-553.72" y2="-93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C51" gate="G$1" pin="2"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
<wire x1="-563.88" y1="-104.14" x2="-563.88" y2="-106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="2"/>
<wire x1="-518.16" y1="33.02" x2="-518.16" y2="30.48" width="0.1524" layer="91"/>
<pinref part="SUPPLY53" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="269.24" y1="-17.78" x2="271.78" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="SUPPLY56" gate="GND" pin="GND"/>
<wire x1="271.78" y1="-17.78" x2="271.78" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="269.24" y1="-15.24" x2="271.78" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="271.78" y1="-15.24" x2="271.78" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="269.24" y1="-12.7" x2="271.78" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="271.78" y1="-12.7" x2="271.78" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="269.24" y1="-10.16" x2="271.78" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="271.78" y1="-10.16" x2="271.78" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="269.24" y1="-7.62" x2="271.78" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="271.78" y1="-7.62" x2="271.78" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="269.24" y1="-5.08" x2="271.78" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="271.78" y1="-5.08" x2="271.78" y2="-7.62" width="0.1524" layer="91"/>
<junction x="271.78" y="-7.62"/>
<junction x="271.78" y="-10.16"/>
<junction x="271.78" y="-12.7"/>
<junction x="271.78" y="-15.24"/>
<junction x="271.78" y="-17.78"/>
<pinref part="U7" gate="G$1" pin="GND_5"/>
<pinref part="U7" gate="G$1" pin="GND_7"/>
<pinref part="U7" gate="G$1" pin="GND_9"/>
<pinref part="U7" gate="G$1" pin="GND_13"/>
<pinref part="U7" gate="G$1" pin="GND_15"/>
<pinref part="U7" gate="G$1" pin="GND_17"/>
</segment>
<segment>
<pinref part="SUPPLY57" gate="GND" pin="GND"/>
<wire x1="238.76" y1="-35.56" x2="238.76" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-38.1" x2="238.76" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-35.56" x2="233.68" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-38.1" x2="238.76" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-35.56" x2="243.84" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-38.1" x2="238.76" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-35.56" x2="248.92" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-38.1" x2="243.84" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="254" y1="-35.56" x2="254" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="254" y1="-38.1" x2="248.92" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-35.56" x2="259.08" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-38.1" x2="254" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="264.16" y1="-35.56" x2="264.16" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="264.16" y1="-38.1" x2="259.08" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-35.56" x2="228.6" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-38.1" x2="233.68" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-35.56" x2="223.52" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-38.1" x2="228.6" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-35.56" x2="218.44" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-38.1" x2="223.52" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-35.56" x2="213.36" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="213.36" y1="-38.1" x2="218.44" y2="-38.1" width="0.1524" layer="91"/>
<junction x="218.44" y="-38.1"/>
<junction x="223.52" y="-38.1"/>
<junction x="228.6" y="-38.1"/>
<junction x="233.68" y="-38.1"/>
<junction x="238.76" y="-38.1"/>
<junction x="243.84" y="-38.1"/>
<junction x="248.92" y="-38.1"/>
<junction x="254" y="-38.1"/>
<junction x="259.08" y="-38.1"/>
<pinref part="U7" gate="G$1" pin="PGND_25"/>
<pinref part="U7" gate="G$1" pin="PGND_26"/>
<pinref part="U7" gate="G$1" pin="PGND_27"/>
<pinref part="U7" gate="G$1" pin="PGND_28"/>
<pinref part="U7" gate="G$1" pin="PGND_29"/>
<pinref part="U7" gate="G$1" pin="PGND_30"/>
<pinref part="U7" gate="G$1" pin="PGND_31"/>
<pinref part="U7" gate="G$1" pin="PGND_32"/>
<pinref part="U7" gate="G$1" pin="PGND_33"/>
<pinref part="U7" gate="G$1" pin="PGND_34"/>
<pinref part="U7" gate="G$1" pin="PGND_35"/>
</segment>
<segment>
<pinref part="C62" gate="G$1" pin="2"/>
<wire x1="170.18" y1="22.86" x2="170.18" y2="20.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY59" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C63" gate="G$1" pin="2"/>
<wire x1="157.48" y1="22.86" x2="157.48" y2="20.32" width="0.1524" layer="91"/>
<wire x1="157.48" y1="20.32" x2="152.4" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C64" gate="G$1" pin="2"/>
<wire x1="152.4" y1="20.32" x2="147.32" y2="20.32" width="0.1524" layer="91"/>
<wire x1="147.32" y1="20.32" x2="147.32" y2="22.86" width="0.1524" layer="91"/>
<pinref part="SUPPLY61" gate="GND" pin="GND"/>
<wire x1="152.4" y1="17.78" x2="152.4" y2="20.32" width="0.1524" layer="91"/>
<junction x="152.4" y="20.32"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="243.84" y1="73.66" x2="243.84" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="18_GND"/>
<wire x1="243.84" y1="71.12" x2="243.84" y2="68.58" width="0.1524" layer="91"/>
<wire x1="254" y1="86.36" x2="254" y2="71.12" width="0.1524" layer="91"/>
<wire x1="254" y1="71.12" x2="243.84" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="233.68" y1="73.66" x2="233.68" y2="71.12" width="0.1524" layer="91"/>
<wire x1="233.68" y1="71.12" x2="243.84" y2="71.12" width="0.1524" layer="91"/>
<junction x="243.84" y="71.12"/>
<pinref part="U8" gate="G$1" pin="1_CLKIN"/>
<wire x1="223.52" y1="86.36" x2="223.52" y2="71.12" width="0.1524" layer="91"/>
<wire x1="223.52" y1="71.12" x2="233.68" y2="71.12" width="0.1524" layer="91"/>
<junction x="233.68" y="71.12"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="65_PAD_GND"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="7.62" y1="-93.98" x2="7.62" y2="-99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="-121.92" y1="-129.54" x2="-132.08" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="-129.54" x2="-132.08" y2="-137.16" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="-132.08" y1="-137.16" x2="-132.08" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="-137.16" x2="-132.08" y2="-137.16" width="0.1524" layer="91"/>
<junction x="-132.08" y="-137.16"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="-172.72" x2="-25.4" y2="-175.26" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-175.26" x2="-17.78" y2="-175.26" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="-175.26" x2="-10.16" y2="-175.26" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-175.26" x2="-10.16" y2="-172.72" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="-172.72" x2="-17.78" y2="-175.26" width="0.1524" layer="91"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="-17.78" y1="-175.26" x2="-17.78" y2="-177.8" width="0.1524" layer="91"/>
<junction x="-17.78" y="-175.26"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<wire x1="-81.28" y1="-116.84" x2="-81.28" y2="-121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="-96.52" y1="-116.84" x2="-96.52" y2="-121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-116.84" x2="73.66" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="111.76" y1="-116.84" x2="111.76" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="-109.22" y1="-96.52" x2="-109.22" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
<wire x1="-109.22" y1="-66.04" x2="-109.22" y2="-68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C54" gate="G$1" pin="2"/>
<pinref part="SUPPLY54" gate="GND" pin="GND"/>
<wire x1="-124.46" y1="-45.72" x2="-124.46" y2="-48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C53" gate="G$1" pin="2"/>
<pinref part="SUPPLY55" gate="GND" pin="GND"/>
<wire x1="-111.76" y1="-45.72" x2="-111.76" y2="-48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C55" gate="G$1" pin="2"/>
<pinref part="SUPPLY58" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C57" gate="G$1" pin="2"/>
<wire x1="-106.68" y1="-20.32" x2="-106.68" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="-22.86" x2="-101.6" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="C56" gate="G$1" pin="2"/>
<wire x1="-101.6" y1="-22.86" x2="-93.98" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="-22.86" x2="-93.98" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY60" gate="GND" pin="GND"/>
<wire x1="-101.6" y1="-22.86" x2="-101.6" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-101.6" y="-22.86"/>
</segment>
<segment>
<pinref part="C58" gate="G$1" pin="2"/>
<pinref part="SUPPLY62" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C59" gate="G$1" pin="2"/>
<pinref part="SUPPLY63" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C60" gate="G$1" pin="2"/>
<pinref part="SUPPLY64" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C61" gate="G$1" pin="2"/>
<pinref part="SUPPLY65" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C65" gate="G$1" pin="2"/>
<pinref part="SUPPLY66" gate="GND" pin="GND"/>
<wire x1="-50.8" y1="121.92" x2="-50.8" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C66" gate="G$1" pin="2"/>
<pinref part="SUPPLY69" gate="GND" pin="GND"/>
<wire x1="-7.62" y1="121.92" x2="-7.62" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C67" gate="G$1" pin="2"/>
<pinref part="SUPPLY70" gate="GND" pin="GND"/>
<wire x1="7.62" y1="121.92" x2="7.62" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C68" gate="G$1" pin="2"/>
<pinref part="SUPPLY67" gate="GND" pin="GND"/>
<wire x1="-35.56" y1="121.92" x2="-35.56" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C69" gate="G$1" pin="2"/>
<pinref part="SUPPLY68" gate="GND" pin="GND"/>
<wire x1="-22.86" y1="121.92" x2="-22.86" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Y2" gate="G$1" pin="4"/>
<wire x1="91.44" y1="-111.76" x2="91.44" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-104.14" x2="93.98" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="Y2" gate="G$1" pin="2"/>
<wire x1="93.98" y1="-104.14" x2="96.52" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-104.14" x2="96.52" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="93.98" y1="-106.68" x2="93.98" y2="-104.14" width="0.1524" layer="91"/>
<junction x="93.98" y="-104.14"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="2"/>
<wire x1="88.9" y1="101.6" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<wire x1="88.9" y1="99.06" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U10" gate="G$1" pin="4"/>
<wire x1="91.44" y1="99.06" x2="93.98" y2="99.06" width="0.1524" layer="91"/>
<wire x1="93.98" y1="99.06" x2="93.98" y2="101.6" width="0.1524" layer="91"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="91.44" y1="99.06" x2="91.44" y2="96.52" width="0.1524" layer="91"/>
<junction x="91.44" y="99.06"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="121.92" y1="93.98" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<pinref part="E1" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="91.44" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="134.62" y1="99.06" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<wire x1="134.62" y1="91.44" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<junction x="121.92" y="91.44"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="GND_18"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="-10.16" y1="-502.92" x2="-10.16" y2="-505.46" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="AGND_45"/>
<wire x1="-10.16" y1="-505.46" x2="-10.16" y2="-508" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-502.92" x2="-20.32" y2="-505.46" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-505.46" x2="-15.24" y2="-505.46" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="GND_34"/>
<wire x1="-15.24" y1="-505.46" x2="-10.16" y2="-505.46" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-505.46" x2="-5.08" y2="-505.46" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-505.46" x2="0" y2="-505.46" width="0.1524" layer="91"/>
<wire x1="0" y1="-505.46" x2="0" y2="-502.92" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="GND_25"/>
<wire x1="-5.08" y1="-502.92" x2="-5.08" y2="-505.46" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="GND_9"/>
<wire x1="-15.24" y1="-502.92" x2="-15.24" y2="-505.46" width="0.1524" layer="91"/>
<junction x="-15.24" y="-505.46"/>
<junction x="-10.16" y="-505.46"/>
<junction x="-5.08" y="-505.46"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="20.32" y1="-337.82" x2="22.86" y2="-337.82" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-337.82" x2="22.86" y2="-340.36" width="0.1524" layer="91"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="-22.86" y1="-335.28" x2="-22.86" y2="-337.82" width="0.1524" layer="91"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="TEST"/>
<wire x1="-38.1" y1="-469.9" x2="-43.18" y2="-469.9" width="0.1524" layer="91"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="-43.18" y1="-469.9" x2="-43.18" y2="-472.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<pinref part="U13" gate="G$1" pin="GND"/>
<wire x1="231.14" y1="-421.64" x2="231.14" y2="-419.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="231.14" y1="-525.78" x2="231.14" y2="-528.32" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
<pinref part="U20" gate="A" pin="GND3"/>
<wire x1="-1155.7" y1="-68.58" x2="-1155.7" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="GND2"/>
<wire x1="-1155.7" y1="-63.5" x2="-1155.7" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-1163.32" y1="-60.96" x2="-1163.32" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="-1163.32" y1="-63.5" x2="-1155.7" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="GND1"/>
<wire x1="-1170.94" y1="-60.96" x2="-1170.94" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="-1170.94" y1="-63.5" x2="-1163.32" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="PGND"/>
<wire x1="-1145.54" y1="-60.96" x2="-1145.54" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="-1145.54" y1="-63.5" x2="-1155.7" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="PAD"/>
<wire x1="-1137.92" y1="-60.96" x2="-1137.92" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="-1137.92" y1="-63.5" x2="-1145.54" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-1145.54" y="-63.5"/>
<junction x="-1155.7" y="-63.5"/>
<junction x="-1163.32" y="-63.5"/>
</segment>
<segment>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="-1188.72" y1="17.78" x2="-1188.72" y2="15.24" width="0.1524" layer="91"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
<wire x1="-1181.1" y1="0" x2="-1181.1" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
<wire x1="-1181.1" y1="-40.64" x2="-1181.1" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="U$23" gate="A" pin="2"/>
</segment>
<segment>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="-1120.14" y1="-63.5" x2="-1120.14" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
<wire x1="-1115.06" y1="-43.18" x2="-1115.06" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R61" gate="G$1" pin="2"/>
<wire x1="-1097.28" y1="-30.48" x2="-1097.28" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="SUPPLY71" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R66" gate="G$1" pin="2"/>
<wire x1="-1107.44" y1="2.54" x2="-1107.44" y2="0" width="0.1524" layer="91"/>
<pinref part="SUPPLY72" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY73" gate="GND" pin="GND"/>
<wire x1="-1089.66" y1="25.4" x2="-1089.66" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-1049.02" y1="25.4" x2="-1084.58" y2="25.4" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="-1084.58" y1="25.4" x2="-1089.66" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-1049.02" y1="25.4" x2="-1049.02" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="-1049.02" y1="25.4" x2="-1038.86" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-1038.86" y1="25.4" x2="-1038.86" y2="27.94" width="0.1524" layer="91"/>
<junction x="-1049.02" y="25.4"/>
<wire x1="-1084.58" y1="25.4" x2="-1084.58" y2="27.94" width="0.1524" layer="91"/>
<junction x="-1084.58" y="25.4"/>
</segment>
<segment>
<pinref part="SUPPLY74" gate="GND" pin="GND"/>
<wire x1="-1132.84" y1="58.42" x2="-1135.38" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="NC1"/>
<wire x1="-1135.38" y1="58.42" x2="-1135.38" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-1135.38" y1="58.42" x2="-1143" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="NC2"/>
<wire x1="-1143" y1="58.42" x2="-1143" y2="53.34" width="0.1524" layer="91"/>
<junction x="-1135.38" y="58.42"/>
</segment>
<segment>
<pinref part="SUPPLY75" gate="GND" pin="GND"/>
<wire x1="-1201.42" y1="55.88" x2="-1201.42" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="-1201.42" y1="53.34" x2="-1201.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-1193.8" y1="55.88" x2="-1193.8" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-1193.8" y1="53.34" x2="-1201.42" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="-1186.18" y1="55.88" x2="-1186.18" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-1186.18" y1="53.34" x2="-1193.8" y2="53.34" width="0.1524" layer="91"/>
<junction x="-1201.42" y="53.34"/>
<junction x="-1193.8" y="53.34"/>
</segment>
<segment>
<pinref part="SUPPLY76" gate="GND" pin="GND"/>
<wire x1="-1252.22" y1="55.88" x2="-1252.22" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="-1252.22" y1="53.34" x2="-1252.22" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-1244.6" y1="55.88" x2="-1244.6" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-1244.6" y1="53.34" x2="-1252.22" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="-1236.98" y1="55.88" x2="-1236.98" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-1236.98" y1="53.34" x2="-1244.6" y2="53.34" width="0.1524" layer="91"/>
<junction x="-1252.22" y="53.34"/>
<junction x="-1244.6" y="53.34"/>
<pinref part="C70" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R68" gate="G$1" pin="1"/>
<wire x1="127" y1="-391.16" x2="124.46" y2="-391.16" width="0.1524" layer="91"/>
<pinref part="SUPPLY77" gate="GND" pin="GND"/>
<wire x1="124.46" y1="-391.16" x2="124.46" y2="-393.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="256.54" y1="-353.06" x2="259.08" y2="-353.06" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-353.06" x2="259.08" y2="-355.6" width="0.1524" layer="91"/>
<pinref part="SUPPLY79" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="GND"/>
<pinref part="SUPPLY80" gate="GND" pin="GND"/>
<wire x1="149.86" y1="-424.18" x2="149.86" y2="-426.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="175.26" y1="-378.46" x2="177.8" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="177.8" y1="-378.46" x2="177.8" y2="-381" width="0.1524" layer="91"/>
<pinref part="SUPPLY81" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R73" gate="G$1" pin="1"/>
<wire x1="205.74" y1="-472.44" x2="203.2" y2="-472.44" width="0.1524" layer="91"/>
<pinref part="SUPPLY82" gate="GND" pin="GND"/>
<wire x1="203.2" y1="-472.44" x2="203.2" y2="-474.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="127" y1="-350.52" x2="124.46" y2="-350.52" width="0.1524" layer="91"/>
<pinref part="SUPPLY78" gate="GND" pin="GND"/>
<wire x1="124.46" y1="-350.52" x2="124.46" y2="-353.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY83" gate="GND" pin="GND"/>
<wire x1="-144.78" y1="-391.16" x2="-144.78" y2="-388.62" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="-144.78" y1="-388.62" x2="-147.32" y2="-388.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C73" gate="G$1" pin="2"/>
<wire x1="-424.18" y1="53.34" x2="-424.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="50.8" x2="-419.1" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R78" gate="G$1" pin="1"/>
<wire x1="-419.1" y1="50.8" x2="-411.48" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-411.48" y1="50.8" x2="-411.48" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-419.1" y1="50.8" x2="-419.1" y2="48.26" width="0.1524" layer="91"/>
<pinref part="SUPPLY85" gate="GND" pin="GND"/>
<junction x="-419.1" y="50.8"/>
</segment>
<segment>
<pinref part="C74" gate="G$1" pin="2"/>
<wire x1="-416.56" y1="40.64" x2="-419.1" y2="40.64" width="0.1524" layer="91"/>
<pinref part="SUPPLY86" gate="GND" pin="GND"/>
<wire x1="-419.1" y1="40.64" x2="-419.1" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$12" gate="G$1" pin="GNDA_DAC"/>
<wire x1="215.9" y1="-170.18" x2="215.9" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="GNDA_HSC_PIN8"/>
<wire x1="215.9" y1="-185.42" x2="220.98" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="220.98" y1="-185.42" x2="226.06" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-185.42" x2="233.68" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-185.42" x2="236.22" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="236.22" y1="-185.42" x2="238.76" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-185.42" x2="243.84" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-185.42" x2="246.38" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-185.42" x2="254" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="254" y1="-185.42" x2="256.54" y2="-185.42" width="0.1524" layer="91"/>
<wire x1="256.54" y1="-185.42" x2="256.54" y2="-170.18" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="GNDA_HSC_PIN7"/>
<wire x1="254" y1="-170.18" x2="254" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="GNDA_RX_PIN12"/>
<wire x1="246.38" y1="-170.18" x2="246.38" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="GNDA_RX_PIN10"/>
<wire x1="243.84" y1="-170.18" x2="243.84" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="GNDD_TX_PIN31"/>
<wire x1="236.22" y1="-170.18" x2="236.22" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="GNDD_TX_PIN29"/>
<wire x1="233.68" y1="-170.18" x2="233.68" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="GNDD_TCTRL"/>
<wire x1="226.06" y1="-170.18" x2="226.06" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="GNDDIO"/>
<wire x1="220.98" y1="-170.18" x2="220.98" y2="-185.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY87" gate="GND" pin="GND"/>
<wire x1="238.76" y1="-190.5" x2="238.76" y2="-185.42" width="0.1524" layer="91"/>
<junction x="220.98" y="-185.42"/>
<junction x="226.06" y="-185.42"/>
<junction x="233.68" y="-185.42"/>
<junction x="236.22" y="-185.42"/>
<junction x="238.76" y="-185.42"/>
<junction x="243.84" y="-185.42"/>
<junction x="246.38" y="-185.42"/>
<junction x="254" y="-185.42"/>
<junction x="256.54" y="-185.42"/>
</segment>
<segment>
<pinref part="SUPPLY84" gate="GND" pin="GND"/>
<wire x1="-101.6" y1="-426.72" x2="-101.6" y2="-424.18" width="0.1524" layer="91"/>
<pinref part="C72" gate="G$1" pin="2"/>
<wire x1="-93.98" y1="-429.26" x2="-96.52" y2="-429.26" width="0.1524" layer="91"/>
<pinref part="C71" gate="G$1" pin="2"/>
<wire x1="-96.52" y1="-429.26" x2="-96.52" y2="-424.18" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-424.18" x2="-96.52" y2="-419.1" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-419.1" x2="-93.98" y2="-419.1" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="-424.18" x2="-96.52" y2="-424.18" width="0.1524" layer="91"/>
<junction x="-96.52" y="-424.18"/>
</segment>
<segment>
<pinref part="U$16" gate="G$1" pin="GND"/>
<pinref part="SUPPLY88" gate="GND" pin="GND"/>
<wire x1="-106.68" y1="-406.4" x2="-106.68" y2="-408.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY89" gate="GND" pin="GND"/>
<pinref part="U$17" gate="G$1" pin="VSS"/>
<wire x1="-83.82" y1="-467.36" x2="-83.82" y2="-469.9" width="0.1524" layer="91"/>
<pinref part="C75" gate="G$1" pin="2"/>
<wire x1="-83.82" y1="-469.9" x2="-83.82" y2="-472.44" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="-454.66" x2="-101.6" y2="-469.9" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="-469.9" x2="-101.6" y2="-469.9" width="0.1524" layer="91"/>
<junction x="-83.82" y="-469.9"/>
</segment>
</net>
<net name="V_BAT_P" class="0">
<segment>
<pinref part="BAT1" gate="G$1" pin="1"/>
<wire x1="-223.52" y1="0" x2="-223.52" y2="22.86" width="0.1524" layer="91"/>
<label x="-223.52" y="7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="I2C_DATA" class="0">
<segment>
<wire x1="-546.1" y1="-30.48" x2="-530.86" y2="-30.48" width="0.1524" layer="91"/>
<label x="-543.56" y="-30.48" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="SDA"/>
</segment>
</net>
<net name="I2C_CLK" class="0">
<segment>
<wire x1="-546.1" y1="-22.86" x2="-530.86" y2="-22.86" width="0.1524" layer="91"/>
<label x="-543.56" y="-22.86" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="SCL"/>
</segment>
</net>
<net name="BAT_N" class="0">
<segment>
<pinref part="BAT1" gate="G$1" pin="2"/>
<wire x1="-223.52" y1="-81.28" x2="-223.52" y2="-66.04" width="0.1524" layer="91"/>
<label x="-223.52" y="-78.74" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="BQ24616_CE" class="0">
<segment>
<wire x1="-927.1" y1="10.16" x2="-947.42" y2="10.16" width="0.1524" layer="91"/>
<label x="-944.88" y="10.16" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="CE"/>
</segment>
</net>
<net name="VCC_BQ24616" class="0">
<segment>
<wire x1="-927.1" y1="25.4" x2="-957.58" y2="25.4" width="0.1524" layer="91"/>
<label x="-944.88" y="25.4" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="VCC"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="-957.58" y1="25.4" x2="-957.58" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-957.58" y1="25.4" x2="-967.74" y2="25.4" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="-967.74" y1="25.4" x2="-967.74" y2="22.86" width="0.1524" layer="91"/>
<junction x="-957.58" y="25.4"/>
</segment>
<segment>
<wire x1="-932.18" y1="76.2" x2="-932.18" y2="96.52" width="0.1524" layer="91"/>
<label x="-932.18" y="78.74" size="1.778" layer="95" rot="R90"/>
<pinref part="R30" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="-894.08" y1="60.96" x2="-894.08" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-894.08" y1="55.88" x2="-894.08" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-894.08" y1="60.96" x2="-899.16" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="-899.16" y1="60.96" x2="-899.16" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-891.54" y1="68.58" x2="-894.08" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-894.08" y1="68.58" x2="-894.08" y2="60.96" width="0.1524" layer="91"/>
<junction x="-894.08" y="60.96"/>
<pinref part="R38" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="ACP"/>
<pinref part="Q2" gate="G$1" pin="D"/>
<wire x1="-911.86" y1="60.96" x2="-899.16" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-891.54" y1="55.88" x2="-894.08" y2="55.88" width="0.1524" layer="91"/>
<junction x="-894.08" y="55.88"/>
<junction x="-899.16" y="60.96"/>
</segment>
</net>
<net name="VREF_BQ24616" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="-927.1" y1="0" x2="-944.88" y2="0" width="0.1524" layer="91"/>
<wire x1="-944.88" y1="0" x2="-952.5" y2="0" width="0.1524" layer="91"/>
<wire x1="-952.5" y1="0" x2="-960.12" y2="0" width="0.1524" layer="91"/>
<wire x1="-960.12" y1="0" x2="-960.12" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="-952.5" y1="-5.08" x2="-952.5" y2="0" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="-944.88" y1="-5.08" x2="-944.88" y2="0" width="0.1524" layer="91"/>
<label x="-957.58" y="0" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="VREF"/>
<junction x="-944.88" y="0"/>
<junction x="-952.5" y="0"/>
</segment>
</net>
<net name="ISET1" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="-952.5" y1="-15.24" x2="-952.5" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-952.5" y1="-17.78" x2="-952.5" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-952.5" y1="-40.64" x2="-952.5" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<junction x="-952.5" y="-40.64"/>
<wire x1="-927.1" y1="-17.78" x2="-952.5" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-952.5" y="-17.78"/>
<label x="-939.8" y="-17.78" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="ISET1"/>
</segment>
</net>
<net name="ISET2" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="-944.88" y1="-15.24" x2="-944.88" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-944.88" y1="-25.4" x2="-944.88" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-944.88" y="-25.4"/>
<wire x1="-944.88" y1="-25.4" x2="-927.1" y2="-25.4" width="0.1524" layer="91"/>
<label x="-939.8" y="-25.4" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="ISET2"/>
</segment>
</net>
<net name="ACSET" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="-960.12" y1="-15.24" x2="-960.12" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-960.12" y1="-33.02" x2="-960.12" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-927.1" y1="-33.02" x2="-960.12" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-960.12" y="-33.02"/>
<label x="-939.8" y="-33.02" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="ACSET"/>
</segment>
</net>
<net name="LODRV_BQ24616" class="0">
<segment>
<wire x1="-850.9" y1="-15.24" x2="-810.26" y2="-15.24" width="0.1524" layer="91"/>
<label x="-843.28" y="-15.24" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="LODRV"/>
<pinref part="Q5" gate="G$1" pin="G"/>
</segment>
</net>
<net name="TTC_BQ24616" class="0">
<segment>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="-848.36" y1="-78.74" x2="-848.36" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="-848.36" y1="-76.2" x2="-850.9" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="-848.36" y1="-76.2" x2="-815.34" y2="-76.2" width="0.1524" layer="91"/>
<junction x="-848.36" y="-76.2"/>
<label x="-845.82" y="-76.2" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="TTC"/>
</segment>
</net>
<net name="VFB_BQ24616" class="0">
<segment>
<label x="-845.82" y="-68.58" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="VFB"/>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="-754.38" y1="-25.4" x2="-754.38" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="-754.38" y1="-27.94" x2="-754.38" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-744.22" y1="-22.86" x2="-744.22" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-744.22" y1="-27.94" x2="-754.38" y2="-27.94" width="0.1524" layer="91"/>
<junction x="-754.38" y="-27.94"/>
<wire x1="-754.38" y1="-27.94" x2="-759.46" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-759.46" y1="-27.94" x2="-759.46" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="-759.46" y1="-68.58" x2="-850.9" y2="-68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STAT1_BQ24616" class="0">
<segment>
<wire x1="-944.88" y1="-71.12" x2="-977.9" y2="-71.12" width="0.1524" layer="91"/>
<label x="-972.82" y="-71.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="101.6" y1="-68.58" x2="68.58" y2="-68.58" width="0.1524" layer="91"/>
<label x="71.12" y="-68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="STAT2_BQ24616" class="0">
<segment>
<wire x1="-949.96" y1="-76.2" x2="-982.98" y2="-76.2" width="0.1524" layer="91"/>
<label x="-975.36" y="-76.2" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="101.6" y1="-76.2" x2="68.58" y2="-76.2" width="0.1524" layer="91"/>
<label x="71.12" y="-76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="PG_BQ24616" class="0">
<segment>
<wire x1="-944.88" y1="-81.28" x2="-982.98" y2="-81.28" width="0.1524" layer="91"/>
<label x="-975.36" y="-81.28" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="101.6" y1="-83.82" x2="68.58" y2="-83.82" width="0.1524" layer="91"/>
<label x="71.12" y="-83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="TS"/>
<wire x1="-927.1" y1="-93.98" x2="-929.64" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="-929.64" y1="-93.98" x2="-929.64" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HIDRV_BQ24616" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="HIDRV"/>
<wire x1="-850.9" y1="10.16" x2="-810.26" y2="10.16" width="0.1524" layer="91"/>
<label x="-845.82" y="10.16" size="1.778" layer="95"/>
<pinref part="Q4" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="BATDRV"/>
<wire x1="-845.82" y1="22.86" x2="-850.9" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="-777.24" y1="27.94" x2="-777.24" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-777.24" y1="25.4" x2="-767.08" y2="25.4" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="-767.08" y1="25.4" x2="-767.08" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="-835.66" y1="22.86" x2="-777.24" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-777.24" y1="25.4" x2="-777.24" y2="22.86" width="0.1524" layer="91"/>
<junction x="-777.24" y="25.4"/>
<pinref part="Q3" gate="G$1" pin="G"/>
<wire x1="-759.46" y1="22.86" x2="-777.24" y2="22.86" width="0.1524" layer="91"/>
<junction x="-777.24" y="22.86"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="-853.44" y1="43.18" x2="-858.52" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="REGN"/>
<wire x1="-858.52" y1="43.18" x2="-858.52" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BTST_BQ24616" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BTST"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="-866.14" y1="60.96" x2="-868.68" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-868.68" y1="60.96" x2="-868.68" y2="35.56" width="0.1524" layer="91"/>
<label x="-868.68" y="38.1" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="LODRV_FET_DRAIN" class="0">
<segment>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="-858.52" y1="60.96" x2="-830.58" y2="60.96" width="0.1524" layer="91"/>
<label x="-855.98" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-787.4" y1="-2.54" x2="-805.18" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-805.18" y1="-2.54" x2="-805.18" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="PH"/>
<wire x1="-805.18" y1="-2.54" x2="-850.9" y2="-2.54" width="0.1524" layer="91"/>
<label x="-845.82" y="-2.54" size="1.778" layer="95"/>
<junction x="-805.18" y="-2.54"/>
<pinref part="Q4" gate="G$1" pin="S"/>
<wire x1="-805.18" y1="7.62" x2="-805.18" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="1"/>
<pinref part="Q5" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="-767.08" y1="-2.54" x2="-769.62" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="-769.62" y1="-2.54" x2="-772.16" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-769.62" y1="-2.54" x2="-769.62" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="-848.36" y1="-35.56" x2="-848.36" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-848.36" y1="-30.48" x2="-850.9" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-848.36" y1="-30.48" x2="-833.12" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-833.12" y1="-30.48" x2="-833.12" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="-833.12" y1="-20.32" x2="-838.2" y2="-20.32" width="0.1524" layer="91"/>
<junction x="-848.36" y="-30.48"/>
<pinref part="U3" gate="G$1" pin="SRP"/>
<wire x1="-769.62" y1="-30.48" x2="-833.12" y2="-30.48" width="0.1524" layer="91"/>
<junction x="-833.12" y="-30.48"/>
<junction x="-769.62" y="-2.54"/>
<pinref part="L5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="BAT_PACK_P" class="0">
<segment>
<wire x1="-756.92" y1="-2.54" x2="-754.38" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="U$27" gate="G$1" pin="1"/>
<wire x1="-754.38" y1="-2.54" x2="-739.14" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-739.14" y1="-2.54" x2="-739.14" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-739.14" y1="-2.54" x2="-723.9" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="U$29" gate="G$1" pin="1"/>
<wire x1="-723.9" y1="-2.54" x2="-723.9" y2="-5.08" width="0.1524" layer="91"/>
<junction x="-739.14" y="-2.54"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="-711.2" y1="-5.08" x2="-711.2" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-711.2" y1="-2.54" x2="-723.9" y2="-2.54" width="0.1524" layer="91"/>
<label x="-751.84" y="-2.54" size="1.778" layer="95"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="-754.38" y1="-15.24" x2="-754.38" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-754.38" y="-2.54"/>
<pinref part="R45" gate="G$1" pin="2"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="-754.38" y1="-10.16" x2="-754.38" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-744.22" y1="-15.24" x2="-744.22" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-744.22" y1="-10.16" x2="-754.38" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-754.38" y="-10.16"/>
<wire x1="-754.38" y1="-10.16" x2="-764.54" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-764.54" y1="-10.16" x2="-764.54" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="-850.9" y1="-48.26" x2="-848.36" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="-848.36" y1="-48.26" x2="-835.66" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="-835.66" y1="-48.26" x2="-835.66" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="-835.66" y1="-53.34" x2="-838.2" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="-848.36" y1="-43.18" x2="-848.36" y2="-48.26" width="0.1524" layer="91"/>
<junction x="-848.36" y="-48.26"/>
<pinref part="U3" gate="G$1" pin="SRN"/>
<wire x1="-764.54" y1="-48.26" x2="-835.66" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="D"/>
<wire x1="-754.38" y1="15.24" x2="-754.38" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-835.66" y="-48.26"/>
<junction x="-723.9" y="-2.54"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="S"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="-922.02" y1="60.96" x2="-924.56" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="-924.56" y1="60.96" x2="-932.18" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-932.18" y1="60.96" x2="-937.26" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-937.26" y1="60.96" x2="-939.8" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-937.26" y1="58.42" x2="-937.26" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="-924.56" y1="58.42" x2="-924.56" y2="60.96" width="0.1524" layer="91"/>
<junction x="-937.26" y="60.96"/>
<junction x="-924.56" y="60.96"/>
<wire x1="-932.18" y1="66.04" x2="-932.18" y2="60.96" width="0.1524" layer="91"/>
<junction x="-932.18" y="60.96"/>
<pinref part="R30" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ADAPTER_IN_BQ24616" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="-949.96" y1="60.96" x2="-977.9" y2="60.96" width="0.1524" layer="91"/>
<label x="-975.36" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="ACDRV"/>
<wire x1="-909.32" y1="35.56" x2="-909.32" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="-909.32" y1="43.18" x2="-911.86" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="-922.02" y1="43.18" x2="-924.56" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-924.56" y1="43.18" x2="-937.26" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-937.26" y1="43.18" x2="-947.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-947.42" y1="43.18" x2="-947.42" y2="55.88" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="-919.48" y1="55.88" x2="-919.48" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-919.48" y1="48.26" x2="-924.56" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-924.56" y1="48.26" x2="-924.56" y2="43.18" width="0.1524" layer="91"/>
<junction x="-924.56" y="43.18"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="-937.26" y1="48.26" x2="-937.26" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="-924.56" y1="50.8" x2="-924.56" y2="48.26" width="0.1524" layer="91"/>
<junction x="-924.56" y="48.26"/>
<junction x="-937.26" y="43.18"/>
</segment>
</net>
<net name="V_SYSTEM_OUT" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="D"/>
<wire x1="-805.18" y1="17.78" x2="-805.18" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-805.18" y1="20.32" x2="-833.12" y2="20.32" width="0.1524" layer="91"/>
<label x="-828.04" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="1"/>
<pinref part="U$7" gate="G$1" pin="1"/>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-883.92" y1="55.88" x2="-878.84" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-878.84" y1="55.88" x2="-878.84" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-881.38" y1="68.58" x2="-878.84" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-878.84" y1="68.58" x2="-878.84" y2="55.88" width="0.1524" layer="91"/>
<junction x="-878.84" y="55.88"/>
<pinref part="R38" gate="G$1" pin="2"/>
<pinref part="U3" gate="G$1" pin="ACN"/>
<wire x1="-878.84" y1="68.58" x2="-812.8" y2="68.58" width="0.1524" layer="91"/>
<label x="-810.26" y="68.58" size="1.778" layer="95"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="-812.8" y1="68.58" x2="-797.56" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-797.56" y1="68.58" x2="-784.86" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-784.86" y1="68.58" x2="-767.08" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-767.08" y1="38.1" x2="-767.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-767.08" y1="40.64" x2="-777.24" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="-777.24" y1="40.64" x2="-777.24" y2="38.1" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="S"/>
<wire x1="-754.38" y1="25.4" x2="-754.38" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-754.38" y1="40.64" x2="-767.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-767.08" y1="40.64" x2="-767.08" y2="68.58" width="0.1524" layer="91"/>
<junction x="-767.08" y="40.64"/>
<junction x="-878.84" y="68.58"/>
<wire x1="-812.8" y1="66.04" x2="-812.8" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-797.56" y1="58.42" x2="-797.56" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-784.86" y1="58.42" x2="-784.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="-784.86" y="68.58"/>
<junction x="-797.56" y="68.58"/>
<junction x="-812.8" y="68.58"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="-571.5" y1="-88.9" x2="-571.5" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="SRN"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="-563.88" y1="-96.52" x2="-563.88" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="-563.88" y1="-93.98" x2="-571.5" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="-571.5" y1="-93.98" x2="-571.5" y2="-96.52" width="0.1524" layer="91"/>
<junction x="-571.5" y="-93.98"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="SRP"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="-591.82" y1="-93.98" x2="-591.82" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="-599.44" y1="-96.52" x2="-599.44" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="-599.44" y1="-93.98" x2="-591.82" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="-591.82" y1="-93.98" x2="-591.82" y2="-96.52" width="0.1524" layer="91"/>
<junction x="-591.82" y="-93.98"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-607.06" y1="-53.34" x2="-607.06" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="-607.06" y1="-50.8" x2="-614.68" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-607.06" y1="-50.8" x2="-601.98" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-607.06" y="-50.8"/>
<pinref part="U$2" gate="G$1" pin="VC0"/>
</segment>
</net>
<net name="V_CELL0" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<label x="-635" y="-50.8" size="1.778" layer="95"/>
<wire x1="-624.84" y1="-50.8" x2="-637.54" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VC1" class="0">
<segment>
<wire x1="-601.98" y1="-35.56" x2="-604.52" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="-604.52" y1="-35.56" x2="-604.52" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="-604.52" y1="-35.56" x2="-614.68" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-604.52" y="-35.56"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="-604.52" y1="-35.56" x2="-604.52" y2="-25.4" width="0.1524" layer="91"/>
<label x="-604.52" y="-33.02" size="1.778" layer="95" rot="R90"/>
<pinref part="U$2" gate="G$1" pin="VC1"/>
</segment>
<segment>
<pinref part="R60" gate="G$1" pin="1"/>
<wire x1="-523.24" y1="-50.8" x2="-513.08" y2="-50.8" width="0.1524" layer="91"/>
<label x="-520.7" y="-50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="-604.52" y1="-17.78" x2="-604.52" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-604.52" y1="-15.24" x2="-601.98" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="-604.52" y1="-15.24" x2="-614.68" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-604.52" y1="-10.16" x2="-604.52" y2="-15.24" width="0.1524" layer="91"/>
<junction x="-604.52" y="-15.24"/>
<pinref part="U$2" gate="G$1" pin="VC2"/>
</segment>
</net>
<net name="VCC_5" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="-497.84" y1="40.64" x2="-497.84" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BQ78350_LED5" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="B"/>
<wire x1="-426.72" y1="-10.16" x2="-406.4" y2="-10.16" width="0.1524" layer="91"/>
<label x="-424.18" y="-10.16" size="1.778" layer="95"/>
<pinref part="U$14" gate="G$1" pin="LED5"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="-604.52" y1="-10.16" x2="-604.52" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="-604.52" y1="2.54" x2="-604.52" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-604.52" y1="5.08" x2="-601.98" y2="5.08" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="-604.52" y1="5.08" x2="-614.68" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-604.52" y1="10.16" x2="-604.52" y2="5.08" width="0.1524" layer="91"/>
<junction x="-604.52" y="5.08"/>
<pinref part="U$2" gate="G$1" pin="VC3"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="-604.52" y1="10.16" x2="-604.52" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="-604.52" y1="22.86" x2="-604.52" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-604.52" y1="25.4" x2="-601.98" y2="25.4" width="0.1524" layer="91"/>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="-604.52" y1="25.4" x2="-614.68" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-604.52" y1="30.48" x2="-604.52" y2="25.4" width="0.1524" layer="91"/>
<junction x="-604.52" y="25.4"/>
<pinref part="U$2" gate="G$1" pin="VC4"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="-604.52" y1="30.48" x2="-604.52" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="-604.52" y1="43.18" x2="-604.52" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-604.52" y1="45.72" x2="-601.98" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="-604.52" y1="45.72" x2="-614.68" y2="45.72" width="0.1524" layer="91"/>
<junction x="-604.52" y="45.72"/>
<pinref part="U$2" gate="G$1" pin="VC5"/>
</segment>
</net>
<net name="BQ78350_LED3" class="0">
<segment>
<pinref part="U$25" gate="G$1" pin="B"/>
<wire x1="-406.4" y1="5.08" x2="-457.2" y2="5.08" width="0.1524" layer="91"/>
<label x="-424.18" y="5.08" size="1.778" layer="95"/>
<pinref part="U$14" gate="G$1" pin="LED3"/>
</segment>
</net>
<net name="BQ78350_LED4" class="0">
<segment>
<pinref part="U$24" gate="G$1" pin="B"/>
<wire x1="-406.4" y1="-2.54" x2="-441.96" y2="-2.54" width="0.1524" layer="91"/>
<label x="-424.18" y="-2.54" size="1.778" layer="95"/>
<pinref part="U$14" gate="G$1" pin="LED4"/>
</segment>
</net>
<net name="BQ78350_LED2" class="0">
<segment>
<pinref part="U$26" gate="G$1" pin="B"/>
<wire x1="-406.4" y1="12.7" x2="-472.44" y2="12.7" width="0.1524" layer="91"/>
<label x="-424.18" y="12.7" size="1.778" layer="95"/>
<pinref part="U$14" gate="G$1" pin="LED2"/>
</segment>
</net>
<net name="BQ78350_LED1" class="0">
<segment>
<pinref part="U$28" gate="G$1" pin="B"/>
<wire x1="-406.4" y1="20.32" x2="-487.68" y2="20.32" width="0.1524" layer="91"/>
<label x="-424.18" y="20.32" size="1.778" layer="95"/>
<pinref part="U$14" gate="G$1" pin="LED1"/>
</segment>
</net>
<net name="VCC_1" class="0">
<segment>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="-482.6" y1="40.64" x2="-482.6" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_2" class="0">
<segment>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="-467.36" y1="40.64" x2="-467.36" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_3" class="0">
<segment>
<pinref part="R58" gate="G$1" pin="1"/>
<wire x1="-452.12" y1="40.64" x2="-452.12" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_4" class="0">
<segment>
<pinref part="R59" gate="G$1" pin="1"/>
<wire x1="-436.88" y1="40.64" x2="-436.88" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="R60" gate="G$1" pin="2"/>
<wire x1="-546.1" y1="-50.8" x2="-535.94" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-535.94" y1="-50.8" x2="-533.4" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-535.94" y1="-53.34" x2="-535.94" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-535.94" y="-50.8"/>
<pinref part="U$2" gate="G$1" pin="TS1"/>
<pinref part="R47" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="Q13" gate="G$1" pin="S"/>
<wire x1="-546.1" y1="-73.66" x2="-505.46" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-505.46" y1="-73.66" x2="-505.46" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="CHG"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="Q13" gate="G$1" pin="D"/>
<pinref part="R62" gate="G$1" pin="1"/>
<wire x1="-505.46" y1="-88.9" x2="-505.46" y2="-93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="Q15" gate="G$1" pin="G"/>
<wire x1="-535.94" y1="-129.54" x2="-535.94" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="-535.94" y1="-124.46" x2="-535.94" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-535.94" y1="-81.28" x2="-546.1" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="2"/>
<wire x1="-535.94" y1="-124.46" x2="-538.48" y2="-124.46" width="0.1524" layer="91"/>
<junction x="-535.94" y="-124.46"/>
<pinref part="U$2" gate="G$1" pin="DSG"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="Q15" gate="G$1" pin="D"/>
<wire x1="-551.18" y1="-134.62" x2="-543.56" y2="-134.62" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="1"/>
<wire x1="-548.64" y1="-124.46" x2="-551.18" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="-551.18" y1="-124.46" x2="-551.18" y2="-134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="2"/>
<pinref part="R63" gate="G$1" pin="1"/>
<wire x1="-505.46" y1="-104.14" x2="-505.46" y2="-109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R63" gate="G$1" pin="2"/>
<pinref part="Q14" gate="G$1" pin="G"/>
<wire x1="-505.46" y1="-119.38" x2="-505.46" y2="-129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="Q14" gate="G$1" pin="D"/>
<pinref part="Q15" gate="G$1" pin="S"/>
<wire x1="-513.08" y1="-134.62" x2="-533.4" y2="-134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_CELL1" class="0">
<segment>
<wire x1="-624.84" y1="-35.56" x2="-637.54" y2="-35.56" width="0.1524" layer="91"/>
<label x="-635" y="-35.56" size="1.778" layer="95"/>
<pinref part="R37" gate="G$1" pin="2"/>
</segment>
</net>
<net name="V_CELL2" class="0">
<segment>
<wire x1="-624.84" y1="-15.24" x2="-637.54" y2="-15.24" width="0.1524" layer="91"/>
<label x="-635" y="-15.24" size="1.778" layer="95"/>
<pinref part="R35" gate="G$1" pin="2"/>
</segment>
</net>
<net name="V_CELL3" class="0">
<segment>
<wire x1="-624.84" y1="5.08" x2="-637.54" y2="5.08" width="0.1524" layer="91"/>
<label x="-635" y="5.08" size="1.778" layer="95"/>
<pinref part="R46" gate="G$1" pin="2"/>
</segment>
</net>
<net name="V_CELL4" class="0">
<segment>
<wire x1="-624.84" y1="25.4" x2="-637.54" y2="25.4" width="0.1524" layer="91"/>
<label x="-635" y="25.4" size="1.778" layer="95"/>
<pinref part="R50" gate="G$1" pin="2"/>
</segment>
</net>
<net name="V_CELL5" class="0">
<segment>
<wire x1="-624.84" y1="45.72" x2="-637.54" y2="45.72" width="0.1524" layer="91"/>
<label x="-635" y="45.72" size="1.778" layer="95"/>
<pinref part="R53" gate="G$1" pin="2"/>
</segment>
</net>
<net name="BQ76920_CAP1" class="0">
<segment>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="-546.1" y1="0" x2="-543.56" y2="0" width="0.1524" layer="91"/>
<wire x1="-543.56" y1="0" x2="-538.48" y2="0" width="0.1524" layer="91"/>
<wire x1="-538.48" y1="0" x2="-538.48" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-543.56" y1="0" x2="-543.56" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-543.56" y="0"/>
<label x="-543.56" y="-17.78" size="1.778" layer="95" rot="R90"/>
<pinref part="U$2" gate="G$1" pin="CAP1"/>
</segment>
</net>
<net name="BQ76920_REGOUT" class="0">
<segment>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="-546.1" y1="22.86" x2="-543.56" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-543.56" y1="22.86" x2="-538.48" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-538.48" y1="22.86" x2="-538.48" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-543.56" y1="22.86" x2="-543.56" y2="2.54" width="0.1524" layer="91"/>
<junction x="-543.56" y="22.86"/>
<label x="-543.56" y="2.54" size="1.778" layer="95" rot="R90"/>
<pinref part="U$2" gate="G$1" pin="REGOUT"/>
</segment>
<segment>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="-421.64" y1="93.98" x2="-424.18" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="93.98" x2="-424.18" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="101.6" x2="-406.4" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="101.6" x2="-449.58" y2="101.6" width="0.1524" layer="91"/>
<junction x="-424.18" y="101.6"/>
<label x="-447.04" y="101.6" size="1.778" layer="95"/>
<pinref part="R77" gate="G$1" pin="1"/>
<wire x1="-421.64" y1="86.36" x2="-424.18" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="86.36" x2="-424.18" y2="93.98" width="0.1524" layer="91"/>
<junction x="-424.18" y="93.98"/>
<pinref part="U$14" gate="G$1" pin="VCC"/>
</segment>
</net>
<net name="BQ76920_REGSRC" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="REGSRC"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="-546.1" y1="35.56" x2="-528.32" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-528.32" y1="35.56" x2="-528.32" y2="33.02" width="0.1524" layer="91"/>
<label x="-543.56" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="-406.4" y1="93.98" x2="-411.48" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="MRST"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="-586.74" y1="-111.76" x2="-591.82" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="-591.82" y1="-106.68" x2="-591.82" y2="-111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="-571.5" y1="-111.76" x2="-576.58" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="-571.5" y1="-111.76" x2="-571.5" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="BAT"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="-546.1" y1="43.18" x2="-518.16" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-518.16" y1="43.18" x2="-518.16" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MSP430_TX_CC2564_RX" class="0">
<segment>
<wire x1="208.28" y1="-2.54" x2="175.26" y2="-2.54" width="0.1524" layer="91"/>
<label x="177.8" y="-2.54" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="HCI_RX"/>
</segment>
</net>
<net name="MSP430_RX_CC2564_TX" class="0">
<segment>
<wire x1="208.28" y1="5.08" x2="175.26" y2="5.08" width="0.1524" layer="91"/>
<label x="177.8" y="5.08" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="HCI_TX"/>
</segment>
</net>
<net name="CC2564_NSHUTD" class="0">
<segment>
<wire x1="208.28" y1="-17.78" x2="175.26" y2="-17.78" width="0.1524" layer="91"/>
<label x="177.8" y="-17.78" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="NSHUTD"/>
</segment>
<segment>
<wire x1="68.58" y1="17.78" x2="101.6" y2="17.78" width="0.1524" layer="91"/>
<label x="71.12" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="TP1" gate="G$1" pin="1"/>
<wire x1="274.32" y1="35.56" x2="269.24" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="TX_DBG"/>
</segment>
</net>
<net name="VCC_3P0" class="0">
<segment>
<label x="195.58" y="38.1" size="1.778" layer="95"/>
<wire x1="208.28" y1="38.1" x2="157.48" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C63" gate="G$1" pin="1"/>
<wire x1="157.48" y1="38.1" x2="157.48" y2="30.48" width="0.1524" layer="91"/>
<wire x1="157.48" y1="38.1" x2="147.32" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C64" gate="G$1" pin="1"/>
<wire x1="147.32" y1="38.1" x2="147.32" y2="30.48" width="0.1524" layer="91"/>
<junction x="157.48" y="38.1"/>
<pinref part="U7" gate="G$1" pin="VDD_IN"/>
</segment>
</net>
<net name="VCC_1P8" class="0">
<segment>
<label x="195.58" y="33.02" size="1.778" layer="95"/>
<wire x1="208.28" y1="33.02" x2="170.18" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C62" gate="G$1" pin="1"/>
<wire x1="170.18" y1="33.02" x2="170.18" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="VDD_IO"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="187.96" y1="134.62" x2="167.64" y2="134.62" width="0.1524" layer="91"/>
<label x="172.72" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="187.96" y1="124.46" x2="167.64" y2="124.46" width="0.1524" layer="91"/>
<label x="172.72" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="187.96" y1="114.3" x2="167.64" y2="114.3" width="0.1524" layer="91"/>
<label x="172.72" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="CC2564_SLOW_CLK_IN" class="0">
<segment>
<wire x1="208.28" y1="25.4" x2="175.26" y2="25.4" width="0.1524" layer="91"/>
<label x="177.8" y="25.4" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="SLOW_CLK_IN"/>
</segment>
</net>
<net name="HOST_CTS_CC2564_RTS" class="0">
<segment>
<wire x1="101.6" y1="71.12" x2="68.58" y2="71.12" width="0.1524" layer="91"/>
<label x="71.12" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="208.28" y1="-10.16" x2="175.26" y2="-10.16" width="0.1524" layer="91"/>
<label x="177.8" y="-10.16" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="HCI_RTS"/>
</segment>
</net>
<net name="HOST_RTS_CC2564_CTS" class="0">
<segment>
<wire x1="101.6" y1="25.4" x2="68.58" y2="25.4" width="0.1524" layer="91"/>
<label x="71.12" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="208.28" y1="15.24" x2="175.26" y2="15.24" width="0.1524" layer="91"/>
<label x="177.8" y="15.24" size="1.778" layer="95"/>
<pinref part="U7" gate="G$1" pin="HCI_CTS"/>
</segment>
</net>
<net name="CC3200_RX_CC2564_TX" class="0">
<segment>
<wire x1="68.58" y1="55.88" x2="101.6" y2="55.88" width="0.1524" layer="91"/>
<label x="71.12" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="CC3200_TX_CC2564_RX" class="0">
<segment>
<wire x1="68.58" y1="63.5" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<label x="71.12" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="10_REGOUT"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="243.84" y1="86.36" x2="243.84" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="20_CPOUT"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="233.68" y1="86.36" x2="233.68" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VLOGIC_MPU-6050_1P8V" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="8_VLOGIC"/>
<wire x1="236.22" y1="139.7" x2="236.22" y2="160.02" width="0.1524" layer="91"/>
<wire x1="236.22" y1="160.02" x2="228.6" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="228.6" y1="160.02" x2="228.6" y2="154.94" width="0.1524" layer="91"/>
<wire x1="228.6" y1="160.02" x2="195.58" y2="160.02" width="0.1524" layer="91"/>
<junction x="228.6" y="160.02"/>
<label x="198.12" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_MPU-6050_3P0V" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="13_VDD"/>
<wire x1="223.52" y1="139.7" x2="223.52" y2="157.48" width="0.1524" layer="91"/>
<wire x1="223.52" y1="157.48" x2="213.36" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="213.36" y1="157.48" x2="213.36" y2="154.94" width="0.1524" layer="91"/>
<wire x1="213.36" y1="157.48" x2="195.58" y2="157.48" width="0.1524" layer="91"/>
<label x="198.12" y="157.48" size="1.778" layer="95"/>
<junction x="213.36" y="157.48"/>
</segment>
</net>
<net name="I2C_AD0" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="23_SCL"/>
<wire x1="203.2" y1="129.54" x2="200.66" y2="129.54" width="0.1524" layer="91"/>
<label x="172.72" y="129.54" size="1.778" layer="95"/>
<wire x1="200.66" y1="129.54" x2="167.64" y2="129.54" width="0.1524" layer="91"/>
<wire x1="200.66" y1="129.54" x2="200.66" y2="134.62" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="200.66" y1="134.62" x2="198.12" y2="134.62" width="0.1524" layer="91"/>
<junction x="200.66" y="129.54"/>
</segment>
<segment>
<wire x1="203.2" y1="109.22" x2="200.66" y2="109.22" width="0.1524" layer="91"/>
<label x="172.72" y="109.22" size="1.778" layer="95"/>
<wire x1="200.66" y1="109.22" x2="167.64" y2="109.22" width="0.1524" layer="91"/>
<wire x1="200.66" y1="109.22" x2="200.66" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="200.66" y1="114.3" x2="198.12" y2="114.3" width="0.1524" layer="91"/>
<junction x="200.66" y="109.22"/>
<pinref part="U8" gate="G$1" pin="9_AD0"/>
</segment>
<segment>
<wire x1="101.6" y1="-20.32" x2="68.58" y2="-20.32" width="0.1524" layer="91"/>
<label x="71.12" y="-20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_SDA" class="0">
<segment>
<wire x1="203.2" y1="119.38" x2="200.66" y2="119.38" width="0.1524" layer="91"/>
<label x="172.72" y="119.38" size="1.778" layer="95"/>
<wire x1="200.66" y1="119.38" x2="167.64" y2="119.38" width="0.1524" layer="91"/>
<wire x1="200.66" y1="119.38" x2="200.66" y2="124.46" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="200.66" y1="124.46" x2="198.12" y2="124.46" width="0.1524" layer="91"/>
<junction x="200.66" y="119.38"/>
<pinref part="U8" gate="G$1" pin="24_SDA"/>
</segment>
<segment>
<wire x1="101.6" y1="-12.7" x2="68.58" y2="-12.7" width="0.1524" layer="91"/>
<label x="71.12" y="-12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="MPU-6050_FSYNC" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="11_FSYNC"/>
<wire x1="203.2" y1="99.06" x2="167.64" y2="99.06" width="0.1524" layer="91"/>
<label x="172.72" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="101.6" y1="2.54" x2="68.58" y2="2.54" width="0.1524" layer="91"/>
<label x="71.12" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="MPU-6050_INT" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="12_INT"/>
<wire x1="203.2" y1="91.44" x2="167.64" y2="91.44" width="0.1524" layer="91"/>
<label x="172.72" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="101.6" y1="10.16" x2="68.58" y2="10.16" width="0.1524" layer="91"/>
<label x="71.12" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_SCL" class="0">
<segment>
<wire x1="101.6" y1="-5.08" x2="68.58" y2="-5.08" width="0.1524" layer="91"/>
<label x="71.12" y="-5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="JTAG_TDO" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="17_TDO"/>
<wire x1="38.1" y1="-142.24" x2="38.1" y2="-157.48" width="0.1524" layer="91"/>
<label x="38.1" y="-154.94" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R72" gate="G$1" pin="1"/>
<wire x1="205.74" y1="-513.08" x2="177.8" y2="-513.08" width="0.1524" layer="91"/>
<label x="180.34" y="-513.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="332.74" y1="-414.02" x2="347.98" y2="-414.02" width="0.1524" layer="91"/>
<label x="335.28" y="-414.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="JTAG_TDI" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="16_TDI"/>
<wire x1="30.48" y1="-142.24" x2="30.48" y2="-157.48" width="0.1524" layer="91"/>
<label x="30.48" y="-154.94" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="256.54" y1="-386.08" x2="271.78" y2="-386.08" width="0.1524" layer="91"/>
<label x="259.08" y="-386.08" size="1.778" layer="95"/>
</segment>
<segment>
<label x="335.28" y="-408.94" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="360.68" y1="-408.94" x2="332.74" y2="-408.94" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-414.02" x2="360.68" y2="-414.02" width="0.1524" layer="91"/>
<wire x1="360.68" y1="-414.02" x2="360.68" y2="-408.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="JTAG_TMS" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="20_TMS"/>
<wire x1="22.86" y1="-142.24" x2="22.86" y2="-157.48" width="0.1524" layer="91"/>
<label x="22.86" y="-154.94" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="256.54" y1="-452.12" x2="271.78" y2="-452.12" width="0.1524" layer="91"/>
<label x="259.08" y="-452.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="332.74" y1="-398.78" x2="347.98" y2="-398.78" width="0.1524" layer="91"/>
<label x="335.28" y="-398.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="JTAG_TCK" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="19_TCK"/>
<wire x1="15.24" y1="-142.24" x2="15.24" y2="-157.48" width="0.1524" layer="91"/>
<label x="15.24" y="-154.94" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R70" gate="G$1" pin="2"/>
<wire x1="256.54" y1="-406.4" x2="271.78" y2="-406.4" width="0.1524" layer="91"/>
<label x="259.08" y="-406.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="358.14" y1="-398.78" x2="360.68" y2="-398.78" width="0.1524" layer="91"/>
<wire x1="360.68" y1="-398.78" x2="360.68" y2="-393.7" width="0.1524" layer="91"/>
<wire x1="360.68" y1="-393.7" x2="332.74" y2="-393.7" width="0.1524" layer="91"/>
<label x="335.28" y="-393.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="51_RTC_XTAL_P"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="-137.16" x2="-111.76" y2="-137.16" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="-111.76" y1="-137.16" x2="-114.3" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-132.08" x2="-111.76" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="-132.08" x2="-111.76" y2="-137.16" width="0.1524" layer="91"/>
<junction x="-111.76" y="-137.16"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="52_RTC_XTAL_N"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="-129.54" x2="-96.52" y2="-129.54" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="-96.52" y1="-129.54" x2="-114.3" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="-132.08" x2="-96.52" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-132.08" x2="-96.52" y2="-129.54" width="0.1524" layer="91"/>
<junction x="-96.52" y="-129.54"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="21_SOP2"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="-142.24" x2="-25.4" y2="-149.86" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="34_SOP1"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="-149.86" x2="-25.4" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-142.24" x2="-17.78" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="-154.94" x2="-17.78" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-154.94" x2="-68.58" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-154.94" x2="-17.78" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-149.86" x2="-68.58" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-149.86" x2="-68.58" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-154.94" x2="-68.58" y2="-160.02" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="35_SOP0"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="-142.24" x2="-10.16" y2="-147.32" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-147.32" x2="-10.16" y2="-160.02" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-160.02" x2="-10.16" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-160.02" x2="-10.16" y2="-160.02" width="0.1524" layer="91"/>
<junction x="-25.4" y="-149.86"/>
<junction x="-17.78" y="-154.94"/>
<junction x="-10.16" y="-160.02"/>
<junction x="-68.58" y="-154.94"/>
<pinref part="TP2" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="-147.32" x2="-10.16" y2="-147.32" width="0.1524" layer="91"/>
<junction x="-10.16" y="-147.32"/>
</segment>
</net>
<net name="VCC_BRD" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-81.28" y1="-154.94" x2="-109.22" y2="-154.94" width="0.1524" layer="91"/>
<label x="-104.14" y="-154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_PLL" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="24_VDD_PLL"/>
<wire x1="-73.66" y1="-106.68" x2="-81.28" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-81.28" y1="-106.68" x2="-81.28" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="-106.68" x2="-88.9" y2="-106.68" width="0.1524" layer="91"/>
<junction x="-81.28" y="-106.68"/>
<label x="-86.36" y="-106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_RAM" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="49_VDD_RAM"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="-99.06" x2="-96.52" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-99.06" x2="-96.52" y2="-109.22" width="0.1524" layer="91"/>
<label x="-86.36" y="-99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="73.66" y1="-106.68" x2="73.66" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="22_WLAN_XTAL_N"/>
<wire x1="68.58" y1="-106.68" x2="73.66" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-106.68" x2="81.28" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="Y2" gate="G$1" pin="3"/>
<wire x1="86.36" y1="-116.84" x2="81.28" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-106.68" x2="81.28" y2="-116.84" width="0.1524" layer="91"/>
<junction x="73.66" y="-106.68"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="111.76" y1="-109.22" x2="111.76" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="23_WLAN_XTAL_P"/>
<wire x1="111.76" y1="-101.6" x2="109.22" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="Y2" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-101.6" x2="68.58" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-116.84" x2="109.22" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-116.84" x2="109.22" y2="-101.6" width="0.1524" layer="91"/>
<junction x="109.22" y="-101.6"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="47_VDD_ANA2"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="-86.36" x2="-109.22" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-86.36" x2="-109.22" y2="-88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="43_DCDC_DIG_SW"/>
<pinref part="L3" gate="A" pin="2"/>
<wire x1="-73.66" y1="-35.56" x2="-81.28" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDD_DIG" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="9_VDD_DIG1"/>
<wire x1="-73.66" y1="-45.72" x2="-76.2" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="-45.72" x2="-101.6" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="-45.72" x2="-101.6" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="L3" gate="A" pin="1"/>
<wire x1="-101.6" y1="-35.56" x2="-96.52" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="-101.6" y1="-35.56" x2="-111.76" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="-35.56" x2="-124.46" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="-35.56" x2="-124.46" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="C53" gate="G$1" pin="1"/>
<wire x1="-111.76" y1="-35.56" x2="-111.76" y2="-38.1" width="0.1524" layer="91"/>
<junction x="-101.6" y="-35.56"/>
<junction x="-111.76" y="-35.56"/>
<pinref part="U9" gate="G$1" pin="56_VDD_DIG2"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="-55.88" x2="-76.2" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="-55.88" x2="-109.22" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-55.88" x2="-109.22" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="-45.72" x2="-76.2" y2="-55.88" width="0.1524" layer="91"/>
<junction x="-76.2" y="-45.72"/>
<junction x="-76.2" y="-55.88"/>
<label x="-106.68" y="-55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_PA" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="33_VDD_PA_IN"/>
<wire x1="-73.66" y1="-17.78" x2="-78.74" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="-78.74" y1="-17.78" x2="-78.74" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="42_DCDC_PA_OUT"/>
<wire x1="-73.66" y1="-7.62" x2="-78.74" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="-7.62" x2="-78.74" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-78.74" y="-17.78"/>
<pinref part="C56" gate="G$1" pin="1"/>
<wire x1="-93.98" y1="-12.7" x2="-93.98" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="-7.62" x2="-78.74" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="-7.62" x2="-106.68" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="C57" gate="G$1" pin="1"/>
<wire x1="-106.68" y1="-7.62" x2="-106.68" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-93.98" y="-7.62"/>
<junction x="-78.74" y="-7.62"/>
<label x="-104.14" y="-7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="40_DCDC_PA_SW_P"/>
<wire x1="-73.66" y1="30.48" x2="-76.2" y2="30.48" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="-76.2" y1="30.48" x2="-76.2" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="41_DCDC_PA_SW_N"/>
<wire x1="-73.66" y1="7.62" x2="-76.2" y2="7.62" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="-76.2" y1="7.62" x2="-76.2" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDD_ANA" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="25_LDO_IN2"/>
<wire x1="-73.66" y1="48.26" x2="-76.2" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C58" gate="G$1" pin="1"/>
<wire x1="-76.2" y1="48.26" x2="-86.36" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="48.26" x2="-86.36" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C59" gate="G$1" pin="1"/>
<wire x1="-96.52" y1="45.72" x2="-96.52" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="36_LDO_IN1"/>
<wire x1="-96.52" y1="68.58" x2="-76.2" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="68.58" x2="-73.66" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="48.26" x2="-76.2" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C60" gate="G$1" pin="1"/>
<wire x1="-109.22" y1="45.72" x2="-109.22" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="48_VDD_ANA1"/>
<wire x1="-109.22" y1="86.36" x2="-76.2" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="86.36" x2="-73.66" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="68.58" x2="-76.2" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="38_DCDC_ANA_SW"/>
<pinref part="L6" gate="A" pin="2"/>
<wire x1="-73.66" y1="104.14" x2="-76.2" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="104.14" x2="-78.74" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="86.36" x2="-76.2" y2="104.14" width="0.1524" layer="91"/>
<junction x="-76.2" y="104.14"/>
<junction x="-76.2" y="86.36"/>
<junction x="-76.2" y="68.58"/>
<junction x="-76.2" y="48.26"/>
<label x="-101.6" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="L6" gate="A" pin="1"/>
<pinref part="C61" gate="G$1" pin="1"/>
<wire x1="-93.98" y1="104.14" x2="-121.92" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="104.14" x2="-121.92" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBAT_VCC" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="44_VIN_DCDC_DIG"/>
<wire x1="-15.24" y1="111.76" x2="-15.24" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C65" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="137.16" x2="-22.86" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="137.16" x2="-27.94" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="137.16" x2="-35.56" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="137.16" x2="-40.64" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="137.16" x2="-50.8" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="137.16" x2="-50.8" y2="129.54" width="0.1524" layer="91"/>
<pinref part="C66" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="137.16" x2="-7.62" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="137.16" x2="-7.62" y2="129.54" width="0.1524" layer="91"/>
<pinref part="C67" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="137.16" x2="2.54" y2="137.16" width="0.1524" layer="91"/>
<wire x1="2.54" y1="137.16" x2="7.62" y2="137.16" width="0.1524" layer="91"/>
<wire x1="7.62" y1="137.16" x2="7.62" y2="129.54" width="0.1524" layer="91"/>
<pinref part="C68" gate="G$1" pin="1"/>
<wire x1="-35.56" y1="129.54" x2="-35.56" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C69" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="137.16" x2="-22.86" y2="129.54" width="0.1524" layer="91"/>
<label x="-43.18" y="137.16" size="1.778" layer="95"/>
<pinref part="U9" gate="G$1" pin="37_VIN_DCDC_ANA"/>
<wire x1="-40.64" y1="111.76" x2="-40.64" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="39_VIN_DCDC_PA"/>
<wire x1="-27.94" y1="111.76" x2="-27.94" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="54_VIN_IO2"/>
<wire x1="2.54" y1="111.76" x2="2.54" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="10_VIN_IO1"/>
<wire x1="15.24" y1="111.76" x2="15.24" y2="137.16" width="0.1524" layer="91"/>
<wire x1="15.24" y1="137.16" x2="7.62" y2="137.16" width="0.1524" layer="91"/>
<junction x="-40.64" y="137.16"/>
<junction x="-35.56" y="137.16"/>
<junction x="-27.94" y="137.16"/>
<junction x="-22.86" y="137.16"/>
<junction x="-15.24" y="137.16"/>
<junction x="-7.62" y="137.16"/>
<junction x="2.54" y="137.16"/>
<junction x="7.62" y="137.16"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="1"/>
<pinref part="U9" gate="G$1" pin="31_RF_BG"/>
<wire x1="83.82" y1="106.68" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="121.92" y1="101.6" x2="121.92" y2="104.14" width="0.1524" layer="91"/>
<pinref part="L9" gate="G$1" pin="2"/>
<wire x1="121.92" y1="104.14" x2="121.92" y2="106.68" width="0.1524" layer="91"/>
<wire x1="121.92" y1="106.68" x2="119.38" y2="106.68" width="0.1524" layer="91"/>
<pinref part="E1" gate="G$1" pin="IN"/>
<wire x1="129.54" y1="104.14" x2="121.92" y2="104.14" width="0.1524" layer="91"/>
<junction x="121.92" y="104.14"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="L9" gate="G$1" pin="1"/>
<pinref part="U10" gate="G$1" pin="3"/>
<wire x1="104.14" y1="106.68" x2="99.06" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="12.7" y1="-337.82" x2="10.16" y2="-337.82" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="3V3OUT"/>
<wire x1="10.16" y1="-337.82" x2="10.16" y2="-342.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBUS_USB" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="-312.42" x2="-17.78" y2="-309.88" width="0.1524" layer="91"/>
<label x="-17.78" y="-309.88" size="1.778" layer="95" rot="R90"/>
<pinref part="U11" gate="G$1" pin="VCC_3"/>
<wire x1="-17.78" y1="-309.88" x2="-17.78" y2="-297.18" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-342.9" x2="-12.7" y2="-340.36" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-340.36" x2="-12.7" y2="-309.88" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-309.88" x2="-17.78" y2="-309.88" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="VCC_42"/>
<wire x1="-7.62" y1="-342.9" x2="-7.62" y2="-340.36" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-340.36" x2="-12.7" y2="-340.36" width="0.1524" layer="91"/>
<junction x="-17.78" y="-309.88"/>
<junction x="-12.7" y="-340.36"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="RESET#"/>
<wire x1="-38.1" y1="-408.94" x2="-73.66" y2="-408.94" width="0.1524" layer="91"/>
<label x="-68.58" y="-408.94" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-76.2" y1="-378.46" x2="-147.32" y2="-378.46" width="0.1524" layer="91"/>
<label x="-93.98" y="-378.46" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="VBUS"/>
</segment>
</net>
<net name="VCC_3P3" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="VCCIOB"/>
<wire x1="2.54" y1="-342.9" x2="2.54" y2="-340.36" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-340.36" x2="0" y2="-340.36" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="VCCIOA"/>
<wire x1="0" y1="-340.36" x2="-2.54" y2="-340.36" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-340.36" x2="-2.54" y2="-342.9" width="0.1524" layer="91"/>
<wire x1="0" y1="-340.36" x2="0" y2="-327.66" width="0.1524" layer="91"/>
<junction x="0" y="-340.36"/>
<label x="0" y="-337.82" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="-53.34" y1="-487.68" x2="-76.2" y2="-487.68" width="0.1524" layer="91"/>
<label x="-71.12" y="-487.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="-53.34" y1="-497.84" x2="-63.5" y2="-497.84" width="0.1524" layer="91"/>
<label x="-71.12" y="-497.84" size="1.778" layer="95"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="-497.84" x2="-76.2" y2="-497.84" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-515.62" x2="-63.5" y2="-515.62" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-515.62" x2="-63.5" y2="-497.84" width="0.1524" layer="91"/>
<junction x="-63.5" y="-497.84"/>
</segment>
<segment>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="63.5" y1="-391.16" x2="83.82" y2="-391.16" width="0.1524" layer="91"/>
<label x="66.04" y="-391.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="63.5" y1="-386.08" x2="83.82" y2="-386.08" width="0.1524" layer="91"/>
<label x="66.04" y="-386.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="246.38" y1="-327.66" x2="231.14" y2="-327.66" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-327.66" x2="231.14" y2="-330.2" width="0.1524" layer="91"/>
<label x="233.68" y="-327.66" size="1.778" layer="95"/>
<pinref part="U13" gate="G$1" pin="VCC"/>
</segment>
<segment>
<wire x1="231.14" y1="-434.34" x2="251.46" y2="-434.34" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-434.34" x2="231.14" y2="-436.88" width="0.1524" layer="91"/>
<label x="233.68" y="-434.34" size="1.778" layer="95"/>
<pinref part="U14" gate="G$1" pin="VCC"/>
</segment>
<segment>
<wire x1="256.54" y1="-375.92" x2="271.78" y2="-375.92" width="0.1524" layer="91"/>
<label x="259.08" y="-375.92" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="165.1" y1="-332.74" x2="149.86" y2="-332.74" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-332.74" x2="149.86" y2="-335.28" width="0.1524" layer="91"/>
<label x="152.4" y="-332.74" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="205.74" y1="-464.82" x2="220.98" y2="-464.82" width="0.1524" layer="91"/>
<label x="208.28" y="-464.82" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="139.7" y1="-342.9" x2="124.46" y2="-342.9" width="0.1524" layer="91"/>
<label x="127" y="-342.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R83" gate="G$1" pin="2"/>
<wire x1="-55.88" y1="-436.88" x2="-53.34" y2="-436.88" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-436.88" x2="-53.34" y2="-434.34" width="0.1524" layer="91"/>
<pinref part="R82" gate="G$1" pin="2"/>
<wire x1="-53.34" y1="-434.34" x2="-58.42" y2="-434.34" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-434.34" x2="-53.34" y2="-431.8" width="0.1524" layer="91"/>
<pinref part="R81" gate="G$1" pin="2"/>
<wire x1="-53.34" y1="-431.8" x2="-60.96" y2="-431.8" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-434.34" x2="-38.1" y2="-434.34" width="0.1524" layer="91"/>
<junction x="-53.34" y="-434.34"/>
<label x="-50.8" y="-434.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="-17.78" y1="-322.58" x2="-17.78" y2="-325.12" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-325.12" x2="-22.86" y2="-325.12" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="-325.12" x2="-22.86" y2="-327.66" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="AVCC"/>
<wire x1="-17.78" y1="-325.12" x2="-17.78" y2="-342.9" width="0.1524" layer="91"/>
<junction x="-17.78" y="-325.12"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="U11" gate="G$1" pin="ADBUS0"/>
<wire x1="22.86" y1="-360.68" x2="17.78" y2="-360.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="ADBUS1"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-365.76" x2="22.86" y2="-365.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="U11" gate="G$1" pin="ADBUS2"/>
<wire x1="22.86" y1="-370.84" x2="17.78" y2="-370.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="ADBUS3"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-375.92" x2="22.86" y2="-375.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="U11" gate="G$1" pin="ADBUS6"/>
<wire x1="22.86" y1="-391.16" x2="17.78" y2="-391.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="ADBUS7"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-396.24" x2="22.86" y2="-396.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DBG_RST_N" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="ADBUS5"/>
<wire x1="17.78" y1="-386.08" x2="53.34" y2="-386.08" width="0.1524" layer="91"/>
<label x="35.56" y="-386.08" size="1.778" layer="95"/>
<pinref part="R43" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="124.46" y1="-383.54" x2="139.7" y2="-383.54" width="0.1524" layer="91"/>
<label x="124.46" y="-383.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="FT_DO" class="0">
<segment>
<wire x1="33.02" y1="-365.76" x2="48.26" y2="-365.76" width="0.1524" layer="91"/>
<label x="35.56" y="-365.76" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="205.74" y1="-386.08" x2="220.98" y2="-386.08" width="0.1524" layer="91"/>
<label x="208.28" y="-386.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="205.74" y1="-452.12" x2="220.98" y2="-452.12" width="0.1524" layer="91"/>
<label x="208.28" y="-452.12" size="1.778" layer="95"/>
<pinref part="U14" gate="G$1" pin="1A_IN"/>
</segment>
</net>
<net name="FT_SK" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="33.02" y1="-360.68" x2="48.26" y2="-360.68" width="0.1524" layer="91"/>
<label x="35.56" y="-360.68" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="205.74" y1="-406.4" x2="220.98" y2="-406.4" width="0.1524" layer="91"/>
<label x="208.28" y="-406.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="FT_DI" class="0">
<segment>
<wire x1="33.02" y1="-370.84" x2="48.26" y2="-370.84" width="0.1524" layer="91"/>
<label x="35.56" y="-370.84" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="241.3" y1="-513.08" x2="256.54" y2="-513.08" width="0.1524" layer="91"/>
<label x="243.84" y="-513.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="160.02" y1="-411.48" x2="195.58" y2="-411.48" width="0.1524" layer="91"/>
<label x="177.8" y="-411.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="FT_CS" class="0">
<segment>
<wire x1="33.02" y1="-375.92" x2="48.26" y2="-375.92" width="0.1524" layer="91"/>
<label x="35.56" y="-375.92" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="124.46" y1="-370.84" x2="139.7" y2="-370.84" width="0.1524" layer="91"/>
<label x="127" y="-370.84" size="1.778" layer="95"/>
<pinref part="U12" gate="G$1" pin="2A_IN"/>
</segment>
<segment>
<wire x1="205.74" y1="-492.76" x2="220.98" y2="-492.76" width="0.1524" layer="91"/>
<label x="208.28" y="-492.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="SI/WUA"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="-38.1" y1="-487.68" x2="-43.18" y2="-487.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="-38.1" y1="-497.84" x2="-43.18" y2="-497.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="U11" gate="G$1" pin="BDBUS0"/>
<wire x1="22.86" y1="-434.34" x2="17.78" y2="-434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="BDBUS1"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-439.42" x2="22.86" y2="-439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FTDI_TX" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="33.02" y1="-434.34" x2="50.8" y2="-434.34" width="0.1524" layer="91"/>
<label x="35.56" y="-434.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="FTDI_RX" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="33.02" y1="-439.42" x2="50.8" y2="-439.42" width="0.1524" layer="91"/>
<label x="35.56" y="-439.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="DBGENN" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="-50.8" y1="-515.62" x2="-30.48" y2="-515.62" width="0.1524" layer="91"/>
<label x="-48.26" y="-515.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="33.02" y1="-391.16" x2="53.34" y2="-391.16" width="0.1524" layer="91"/>
<label x="35.56" y="-391.16" size="1.778" layer="95"/>
<pinref part="R42" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="205.74" y1="-337.82" x2="220.98" y2="-337.82" width="0.1524" layer="91"/>
<label x="208.28" y="-337.82" size="1.778" layer="95"/>
<pinref part="U13" gate="G$1" pin="1OE"/>
</segment>
<segment>
<wire x1="205.74" y1="-398.78" x2="220.98" y2="-398.78" width="0.1524" layer="91"/>
<label x="208.28" y="-398.78" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="205.74" y1="-378.46" x2="220.98" y2="-378.46" width="0.1524" layer="91"/>
<label x="208.28" y="-378.46" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="205.74" y1="-358.14" x2="220.98" y2="-358.14" width="0.1524" layer="91"/>
<label x="208.28" y="-358.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="DBGMOD" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="33.02" y1="-396.24" x2="48.26" y2="-396.24" width="0.1524" layer="91"/>
<label x="35.56" y="-396.24" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="205.74" y1="-345.44" x2="220.98" y2="-345.44" width="0.1524" layer="91"/>
<label x="208.28" y="-345.44" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="205.74" y1="-365.76" x2="220.98" y2="-365.76" width="0.1524" layer="91"/>
<label x="208.28" y="-365.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="JTAG_TMS_PRE" class="0">
<segment>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-452.12" x2="243.84" y2="-452.12" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-452.12" x2="241.3" y2="-452.12" width="0.1524" layer="91"/>
<junction x="243.84" y="-452.12"/>
<pinref part="U14" gate="G$1" pin="1Y_OUT"/>
<wire x1="243.84" y1="-452.12" x2="243.84" y2="-492.76" width="0.1524" layer="91"/>
<pinref part="U14" gate="G$1" pin="3Y_OUT"/>
<wire x1="243.84" y1="-492.76" x2="241.3" y2="-492.76" width="0.1524" layer="91"/>
<label x="243.84" y="-490.22" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="119.38" y1="-411.48" x2="139.7" y2="-411.48" width="0.1524" layer="91"/>
<label x="121.92" y="-411.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="TPS55332_Q1_SYNC" class="0">
<segment>
<label x="-1201.42" y="-10.16" size="1.778" layer="95"/>
<wire x1="-1178.56" y1="-10.16" x2="-1203.96" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="SYNC"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="U20" gate="A" pin="RT"/>
<wire x1="-1178.56" y1="30.48" x2="-1188.72" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="-1188.72" y1="30.48" x2="-1188.72" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<wire x1="-1178.56" y1="10.16" x2="-1181.1" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-1181.1" y1="10.16" x2="-1181.1" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="CDLY"/>
<pinref part="C24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="TPS55332_Q1_RST" class="0">
<segment>
<label x="-1201.42" y="-20.32" size="1.778" layer="95"/>
<wire x1="-1178.56" y1="-20.32" x2="-1203.96" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="RST"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<wire x1="-1178.56" y1="-30.48" x2="-1181.1" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-1181.1" y1="-30.48" x2="-1181.1" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="U$23" gate="A" pin="1"/>
<pinref part="U20" gate="A" pin="SS"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="-1120.14" y1="-50.8" x2="-1120.14" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="RSLEW"/>
<wire x1="-1120.14" y1="-50.8" x2="-1122.68" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<wire x1="-1115.06" y1="-33.02" x2="-1115.06" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="BOOT"/>
<wire x1="-1115.06" y1="-33.02" x2="-1122.68" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<pinref part="R65" gate="G$1" pin="2"/>
<wire x1="-1097.28" y1="-12.7" x2="-1097.28" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="RST_TH"/>
<wire x1="-1097.28" y1="-17.78" x2="-1097.28" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-1122.68" y1="-17.78" x2="-1097.28" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-1097.28" y="-17.78"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<wire x1="-1117.6" y1="12.7" x2="-1117.6" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="VSENSE"/>
<wire x1="-1117.6" y1="17.78" x2="-1122.68" y2="17.78" width="0.1524" layer="91"/>
<pinref part="R66" gate="G$1" pin="1"/>
<pinref part="R67" gate="G$1" pin="2"/>
<wire x1="-1107.44" y1="20.32" x2="-1107.44" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-1107.44" y1="17.78" x2="-1107.44" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-1117.6" y1="17.78" x2="-1107.44" y2="17.78" width="0.1524" layer="91"/>
<junction x="-1117.6" y="17.78"/>
<junction x="-1107.44" y="17.78"/>
<pinref part="C34" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<wire x1="-1117.6" y1="5.08" x2="-1117.6" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="COMP"/>
<wire x1="-1117.6" y1="-2.54" x2="-1122.68" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<wire x1="-1143" y1="66.04" x2="-1120.14" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-1120.14" y1="66.04" x2="-1120.14" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="SW"/>
<wire x1="-1120.14" y1="48.26" x2="-1122.68" y2="48.26" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
<pinref part="U$13" gate="G$1" pin="ANODE"/>
<wire x1="-1109.98" y1="66.04" x2="-1120.14" y2="66.04" width="0.1524" layer="91"/>
<junction x="-1120.14" y="66.04"/>
</segment>
</net>
<net name="TPS55332_Q1_EN" class="0">
<segment>
<wire x1="-1178.56" y1="40.64" x2="-1206.5" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="EN"/>
<label x="-1203.96" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="BAT_P" class="0">
<segment>
<wire x1="-1275.08" y1="68.58" x2="-1287.78" y2="68.58" width="0.1524" layer="91"/>
<label x="-1285.24" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="VOUT_TPS55332-Q1" class="0">
<segment>
<pinref part="R67" gate="G$1" pin="1"/>
<wire x1="-1107.44" y1="30.48" x2="-1107.44" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="VREG"/>
<wire x1="-1107.44" y1="38.1" x2="-1122.68" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R65" gate="G$1" pin="1"/>
<wire x1="-1097.28" y1="-2.54" x2="-1097.28" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-1097.28" y1="38.1" x2="-1107.44" y2="38.1" width="0.1524" layer="91"/>
<junction x="-1107.44" y="38.1"/>
<junction x="-1097.28" y="38.1"/>
<wire x1="-1097.28" y1="38.1" x2="-1084.58" y2="38.1" width="0.1524" layer="91"/>
<label x="-1076.96" y="38.1" size="1.778" layer="95"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="-1084.58" y1="38.1" x2="-1049.02" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-1049.02" y1="38.1" x2="-1049.02" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="-1049.02" y1="38.1" x2="-1038.86" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-1038.86" y1="38.1" x2="-1038.86" y2="35.56" width="0.1524" layer="91"/>
<junction x="-1049.02" y="38.1"/>
<pinref part="U$13" gate="G$1" pin="CATHODE"/>
<wire x1="-1104.9" y1="66.04" x2="-1097.28" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-1097.28" y1="66.04" x2="-1097.28" y2="38.1" width="0.1524" layer="91"/>
<label x="-1097.28" y="40.64" size="1.778" layer="95" rot="R90"/>
<wire x1="-1084.58" y1="38.1" x2="-1084.58" y2="35.56" width="0.1524" layer="91"/>
<junction x="-1084.58" y="38.1"/>
</segment>
</net>
<net name="VIN_TPS55332-Q2" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="-1226.82" y1="68.58" x2="-1236.98" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-1236.98" y1="68.58" x2="-1244.6" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-1244.6" y1="68.58" x2="-1252.22" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-1252.22" y1="63.5" x2="-1252.22" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-1244.6" y1="63.5" x2="-1244.6" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-1236.98" y1="63.5" x2="-1236.98" y2="68.58" width="0.1524" layer="91"/>
<junction x="-1244.6" y="68.58"/>
<junction x="-1236.98" y="68.58"/>
<pinref part="C70" gate="G$1" pin="1"/>
<pinref part="L10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VIN_TPS55332-Q1" class="0">
<segment>
<wire x1="-1158.24" y1="66.04" x2="-1181.1" y2="66.04" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="U20" gate="A" pin="VIN"/>
<wire x1="-1181.1" y1="48.26" x2="-1178.56" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-1181.1" y1="48.26" x2="-1181.1" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-1201.42" y1="63.5" x2="-1201.42" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-1201.42" y1="66.04" x2="-1193.8" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="-1193.8" y1="66.04" x2="-1186.18" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-1186.18" y1="66.04" x2="-1181.1" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-1193.8" y1="63.5" x2="-1193.8" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="-1186.18" y1="63.5" x2="-1186.18" y2="66.04" width="0.1524" layer="91"/>
<junction x="-1193.8" y="66.04"/>
<junction x="-1186.18" y="66.04"/>
<label x="-1196.34" y="66.04" size="1.778" layer="95"/>
<wire x1="-1201.42" y1="66.04" x2="-1201.42" y2="68.58" width="0.1524" layer="91"/>
<junction x="-1201.42" y="66.04"/>
<wire x1="-1201.42" y1="68.58" x2="-1211.58" y2="68.58" width="0.1524" layer="91"/>
<pinref part="L10" gate="G$1" pin="2"/>
<junction x="-1181.1" y="66.04"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="3A_IN"/>
<pinref part="R68" gate="G$1" pin="2"/>
<wire x1="139.7" y1="-391.16" x2="137.16" y2="-391.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="R69" gate="G$1" pin="1"/>
<pinref part="U12" gate="G$1" pin="3Y_OUT"/>
<wire x1="165.1" y1="-391.16" x2="160.02" y2="-391.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SRSTN" class="0">
<segment>
<pinref part="R69" gate="G$1" pin="2"/>
<wire x1="175.26" y1="-391.16" x2="185.42" y2="-391.16" width="0.1524" layer="91"/>
<label x="177.8" y="-391.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="DBGMODN_OE" class="0">
<segment>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-353.06" x2="243.84" y2="-353.06" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-353.06" x2="243.84" y2="-345.44" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-345.44" x2="241.3" y2="-345.44" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-345.44" x2="271.78" y2="-345.44" width="0.1524" layer="91"/>
<junction x="243.84" y="-345.44"/>
<label x="243.84" y="-345.44" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="200.66" y1="-485.14" x2="220.98" y2="-485.14" width="0.1524" layer="91"/>
<label x="203.2" y="-485.14" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="200.66" y1="-505.46" x2="220.98" y2="-505.46" width="0.1524" layer="91"/>
<label x="203.2" y="-505.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="R70" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-406.4" x2="241.3" y2="-406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-386.08" x2="241.3" y2="-386.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DBGMODN_OE2" class="0">
<segment>
<pinref part="R71" gate="G$1" pin="1"/>
<wire x1="246.38" y1="-375.92" x2="243.84" y2="-375.92" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-375.92" x2="243.84" y2="-365.76" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="2Y_OUT"/>
<wire x1="243.84" y1="-365.76" x2="241.3" y2="-365.76" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-365.76" x2="271.78" y2="-365.76" width="0.1524" layer="91"/>
<junction x="243.84" y="-365.76"/>
<label x="243.84" y="-365.76" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="121.92" y1="-363.22" x2="139.7" y2="-363.22" width="0.1524" layer="91"/>
<label x="121.92" y="-363.22" size="1.778" layer="95"/>
<pinref part="U12" gate="G$1" pin="2OE"/>
</segment>
<segment>
<wire x1="119.38" y1="-403.86" x2="139.7" y2="-403.86" width="0.1524" layer="91"/>
<label x="121.92" y="-403.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="FT_CS_N" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="165.1" y1="-378.46" x2="162.56" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-378.46" x2="162.56" y2="-370.84" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-370.84" x2="160.02" y2="-370.84" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-370.84" x2="185.42" y2="-370.84" width="0.1524" layer="91"/>
<junction x="162.56" y="-370.84"/>
<label x="175.26" y="-370.84" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="205.74" y1="-444.5" x2="220.98" y2="-444.5" width="0.1524" layer="91"/>
<label x="208.28" y="-444.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="4A_IN"/>
<pinref part="R72" gate="G$1" pin="2"/>
<wire x1="220.98" y1="-513.08" x2="215.9" y2="-513.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="R73" gate="G$1" pin="2"/>
<pinref part="U14" gate="G$1" pin="2A_IN"/>
<wire x1="215.9" y1="-472.44" x2="220.98" y2="-472.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="139.7" y1="-350.52" x2="137.16" y2="-350.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MICRO_USB_ID" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="USBID"/>
<wire x1="-147.32" y1="-386.08" x2="-124.46" y2="-386.08" width="0.1524" layer="91"/>
<label x="-142.24" y="-386.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB_DM_MUSB" class="0">
<segment>
<pinref part="R74" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="-378.46" x2="-73.66" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-378.46" x2="-73.66" y2="-381" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="D-"/>
<wire x1="-73.66" y1="-381" x2="-109.22" y2="-381" width="0.1524" layer="91"/>
<label x="-93.98" y="-381" size="1.778" layer="95"/>
<pinref part="U$16" gate="G$1" pin="D-"/>
<wire x1="-109.22" y1="-381" x2="-147.32" y2="-381" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="-386.08" x2="-109.22" y2="-381" width="0.1524" layer="91"/>
<junction x="-109.22" y="-381"/>
</segment>
</net>
<net name="USB_DM_FTDI" class="0">
<segment>
<pinref part="R74" gate="G$1" pin="2"/>
<pinref part="U11" gate="G$1" pin="USBDM"/>
<wire x1="-60.96" y1="-378.46" x2="-38.1" y2="-378.46" width="0.1524" layer="91"/>
<label x="-58.42" y="-378.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB_DP_FTDI" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="USBDP"/>
<pinref part="R75" gate="G$1" pin="2"/>
<wire x1="-38.1" y1="-388.62" x2="-60.96" y2="-388.62" width="0.1524" layer="91"/>
<label x="-58.42" y="-388.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB_DP_MUSB" class="0">
<segment>
<pinref part="R75" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="-388.62" x2="-73.66" y2="-388.62" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-388.62" x2="-73.66" y2="-383.54" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="D+"/>
<wire x1="-73.66" y1="-383.54" x2="-104.14" y2="-383.54" width="0.1524" layer="91"/>
<label x="-93.98" y="-383.54" size="1.778" layer="95"/>
<pinref part="R76" gate="G$1" pin="1"/>
<wire x1="-104.14" y1="-383.54" x2="-147.32" y2="-383.54" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-398.78" x2="-73.66" y2="-398.78" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-398.78" x2="-73.66" y2="-388.62" width="0.1524" layer="91"/>
<junction x="-73.66" y="-388.62"/>
<pinref part="U$16" gate="G$1" pin="D+"/>
<wire x1="-104.14" y1="-386.08" x2="-104.14" y2="-383.54" width="0.1524" layer="91"/>
<junction x="-104.14" y="-383.54"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="R76" gate="G$1" pin="2"/>
<pinref part="U11" gate="G$1" pin="RSTOUT#"/>
<wire x1="-60.96" y1="-398.78" x2="-38.1" y2="-398.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="R77" gate="G$1" pin="2"/>
<wire x1="-406.4" y1="86.36" x2="-411.48" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="KEYIN"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<wire x1="-406.4" y1="66.04" x2="-411.48" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R78" gate="G$1" pin="2"/>
<wire x1="-411.48" y1="66.04" x2="-411.48" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C73" gate="G$1" pin="1"/>
<wire x1="-411.48" y1="66.04" x2="-424.18" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="66.04" x2="-424.18" y2="60.96" width="0.1524" layer="91"/>
<junction x="-411.48" y="66.04"/>
<pinref part="R79" gate="G$1" pin="2"/>
<wire x1="-414.02" y1="71.12" x2="-411.48" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-411.48" y1="71.12" x2="-411.48" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="BAT"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="C74" gate="G$1" pin="1"/>
<wire x1="-408.94" y1="40.64" x2="-406.4" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="R79" gate="G$1" pin="1"/>
<wire x1="-424.18" y1="71.12" x2="-436.88" y2="71.12" width="0.1524" layer="91"/>
<label x="-434.34" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="RSOC_LED_DISP" class="0">
<segment>
<pinref part="U$14" gate="G$1" pin="DISP"/>
<wire x1="-406.4" y1="-15.24" x2="-431.8" y2="-15.24" width="0.1524" layer="91"/>
<label x="-426.72" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC_1P2" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="VDDD_PIN28"/>
<wire x1="269.24" y1="-96.52" x2="269.24" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="VDDD_PIN15"/>
<wire x1="269.24" y1="-81.28" x2="266.7" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-81.28" x2="264.16" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="264.16" y1="-81.28" x2="264.16" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="VDDD_PIN16"/>
<wire x1="266.7" y1="-96.52" x2="266.7" y2="-81.28" width="0.1524" layer="91"/>
<junction x="266.7" y="-81.28"/>
<wire x1="266.7" y1="-81.28" x2="266.7" y2="-68.58" width="0.1524" layer="91"/>
<label x="266.7" y="-78.74" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U$12" gate="G$1" pin="VDDA_RX_PIN13"/>
<wire x1="246.38" y1="-96.52" x2="246.38" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="VDDA_RX_PIN9"/>
<wire x1="246.38" y1="-81.28" x2="243.84" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-81.28" x2="243.84" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-81.28" x2="246.38" y2="-68.58" width="0.1524" layer="91"/>
<junction x="246.38" y="-81.28"/>
<label x="246.38" y="-78.74" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="256.54" y1="-96.52" x2="256.54" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="256.54" y1="-81.28" x2="254" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="254" y1="-81.28" x2="254" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="256.54" y1="-81.28" x2="256.54" y2="-68.58" width="0.1524" layer="91"/>
<junction x="256.54" y="-81.28"/>
<label x="256.54" y="-78.74" size="1.778" layer="95" rot="R90"/>
<pinref part="U$12" gate="G$1" pin="VDDA_HSC_PIN4"/>
<pinref part="U$12" gate="G$1" pin="VDDA_HSC_PIN5"/>
</segment>
<segment>
<wire x1="236.22" y1="-96.52" x2="236.22" y2="-68.58" width="0.1524" layer="91"/>
<label x="236.22" y="-78.74" size="1.778" layer="95" rot="R90"/>
<pinref part="U$12" gate="G$1" pin="VDDD_TX"/>
</segment>
<segment>
<wire x1="226.06" y1="-96.52" x2="226.06" y2="-68.58" width="0.1524" layer="91"/>
<label x="226.06" y="-78.74" size="1.778" layer="95" rot="R90"/>
<pinref part="U$12" gate="G$1" pin="VDDD_TCTRL"/>
</segment>
</net>
<net name="VCC_2P5" class="0">
<segment>
<wire x1="220.98" y1="-96.52" x2="220.98" y2="-68.58" width="0.1524" layer="91"/>
<label x="220.98" y="-78.74" size="1.778" layer="95" rot="R90"/>
<pinref part="U$12" gate="G$1" pin="VDDD25_IO"/>
</segment>
<segment>
<wire x1="215.9" y1="-96.52" x2="215.9" y2="-68.58" width="0.1524" layer="91"/>
<label x="215.9" y="-78.74" size="1.778" layer="95" rot="R90"/>
<pinref part="U$12" gate="G$1" pin="VDDA25_DAC"/>
</segment>
</net>
<net name="XETHRU_CS" class="0">
<segment>
<wire x1="101.6" y1="-60.96" x2="68.58" y2="-60.96" width="0.1524" layer="91"/>
<label x="71.12" y="-60.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="198.12" y1="-152.4" x2="165.1" y2="-152.4" width="0.1524" layer="91"/>
<label x="167.64" y="-152.4" size="1.778" layer="95"/>
<pinref part="U$12" gate="G$1" pin="NSS"/>
</segment>
</net>
<net name="XETHRU_SPI_MOSI" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="7_GPIO16"/>
<wire x1="68.58" y1="-53.34" x2="101.6" y2="-53.34" width="0.1524" layer="91"/>
<label x="71.12" y="-53.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="165.1" y1="-137.16" x2="198.12" y2="-137.16" width="0.1524" layer="91"/>
<label x="167.64" y="-137.16" size="1.778" layer="95"/>
<pinref part="U$12" gate="G$1" pin="MOSI"/>
</segment>
</net>
<net name="XETHRU_SPI_MISO" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="6_GPIO15"/>
<wire x1="68.58" y1="-43.18" x2="101.6" y2="-43.18" width="0.1524" layer="91"/>
<label x="71.12" y="-43.18" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="165.1" y1="-129.54" x2="198.12" y2="-129.54" width="0.1524" layer="91"/>
<label x="167.64" y="-129.54" size="1.778" layer="95"/>
<pinref part="U$12" gate="G$1" pin="MISO"/>
</segment>
</net>
<net name="XETHRU_SPI_CLK" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="5_GPIO14"/>
<wire x1="68.58" y1="-35.56" x2="101.6" y2="-35.56" width="0.1524" layer="91"/>
<label x="71.12" y="-35.56" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="165.1" y1="-144.78" x2="198.12" y2="-144.78" width="0.1524" layer="91"/>
<label x="167.64" y="-144.78" size="1.778" layer="95"/>
<pinref part="U$12" gate="G$1" pin="SCLK"/>
</segment>
</net>
<net name="ADC_IN_THERMISTOR" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="58_GPIO3"/>
<wire x1="68.58" y1="48.26" x2="101.6" y2="48.26" width="0.1524" layer="91"/>
<label x="71.12" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="XETHRU_EXTCLK" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="EXTCLK"/>
<wire x1="284.48" y1="-154.94" x2="325.12" y2="-154.94" width="0.1524" layer="91"/>
<label x="292.1" y="-154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="XETHRU_CLK_OUT" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="CLK_OUT"/>
<wire x1="284.48" y1="-144.78" x2="325.12" y2="-144.78" width="0.1524" layer="91"/>
<label x="292.1" y="-144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="XETHRU_RFOUT" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="RFOUT"/>
<wire x1="284.48" y1="-137.16" x2="325.12" y2="-137.16" width="0.1524" layer="91"/>
<label x="292.1" y="-137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="XETHRU_NARST" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="NARST"/>
<wire x1="198.12" y1="-160.02" x2="165.1" y2="-160.02" width="0.1524" layer="91"/>
<label x="167.64" y="-160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="4_GPIO13"/>
<wire x1="68.58" y1="-27.94" x2="101.6" y2="-27.94" width="0.1524" layer="91"/>
<label x="71.12" y="-27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="XETHRU_RFIN" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="RFIN"/>
<wire x1="198.12" y1="-124.46" x2="165.1" y2="-124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="XTIN"/>
<pinref part="C71" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="-419.1" x2="-40.64" y2="-419.1" width="0.1524" layer="91"/>
<pinref part="U$15" gate="G$1" pin="P$2"/>
<wire x1="-40.64" y1="-419.1" x2="-86.36" y2="-419.1" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="-421.64" x2="-40.64" y2="-421.64" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-421.64" x2="-40.64" y2="-419.1" width="0.1524" layer="91"/>
<junction x="-40.64" y="-419.1"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="C72" gate="G$1" pin="1"/>
<pinref part="U11" gate="G$1" pin="XTOUT"/>
<wire x1="-86.36" y1="-429.26" x2="-40.64" y2="-429.26" width="0.1524" layer="91"/>
<pinref part="U$15" gate="G$1" pin="P$1"/>
<wire x1="-40.64" y1="-429.26" x2="-38.1" y2="-429.26" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="-426.72" x2="-40.64" y2="-426.72" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-426.72" x2="-40.64" y2="-429.26" width="0.1524" layer="91"/>
<junction x="-40.64" y="-429.26"/>
</segment>
</net>
<net name="FT2232D_PWREN" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="PWREN#"/>
<wire x1="-38.1" y1="-480.06" x2="-76.2" y2="-480.06" width="0.1524" layer="91"/>
<label x="-71.12" y="-480.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="FTDI_EEPROM_DO" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="DO"/>
<pinref part="R80" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="-459.74" x2="-68.58" y2="-459.74" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-459.74" x2="-66.04" y2="-459.74" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-459.74" x2="-68.58" y2="-464.82" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-464.82" x2="-40.64" y2="-464.82" width="0.1524" layer="91"/>
<junction x="-68.58" y="-459.74"/>
<label x="-66.04" y="-464.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="FTDI_EEPROM_DI" class="0">
<segment>
<pinref part="R80" gate="G$1" pin="2"/>
<wire x1="-55.88" y1="-459.74" x2="-40.64" y2="-459.74" width="0.1524" layer="91"/>
<pinref part="U$17" gate="G$1" pin="DI"/>
<wire x1="-40.64" y1="-459.74" x2="-38.1" y2="-459.74" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-454.66" x2="-66.04" y2="-454.66" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="-454.66" x2="-40.64" y2="-454.66" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-454.66" x2="-40.64" y2="-459.74" width="0.1524" layer="91"/>
<junction x="-40.64" y="-459.74"/>
<label x="-66.04" y="-454.66" size="1.778" layer="95"/>
<pinref part="R83" gate="G$1" pin="1"/>
<wire x1="-66.04" y1="-436.88" x2="-66.04" y2="-454.66" width="0.1524" layer="91"/>
<junction x="-66.04" y="-454.66"/>
</segment>
</net>
<net name="FTDI_EEPROM_CLK" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="CLK"/>
<wire x1="-71.12" y1="-444.5" x2="-40.64" y2="-444.5" width="0.1524" layer="91"/>
<label x="-66.04" y="-444.5" size="1.778" layer="95"/>
<pinref part="U11" gate="G$1" pin="EESK"/>
<wire x1="-38.1" y1="-449.58" x2="-40.64" y2="-449.58" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-449.58" x2="-40.64" y2="-444.5" width="0.1524" layer="91"/>
<pinref part="R81" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="-431.8" x2="-71.12" y2="-444.5" width="0.1524" layer="91"/>
<junction x="-71.12" y="-444.5"/>
</segment>
</net>
<net name="FTDI_EEPROM_CS" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="CS"/>
<wire x1="-71.12" y1="-449.58" x2="-68.58" y2="-449.58" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-449.58" x2="-43.18" y2="-449.58" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="-449.58" x2="-43.18" y2="-439.42" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="EECS"/>
<wire x1="-43.18" y1="-439.42" x2="-38.1" y2="-439.42" width="0.1524" layer="91"/>
<label x="-66.04" y="-449.58" size="1.778" layer="95"/>
<pinref part="R82" gate="G$1" pin="1"/>
<wire x1="-68.58" y1="-434.34" x2="-68.58" y2="-449.58" width="0.1524" layer="91"/>
<junction x="-68.58" y="-449.58"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="VCC"/>
<wire x1="-93.98" y1="-444.5" x2="-101.6" y2="-444.5" width="0.1524" layer="91"/>
<pinref part="C75" gate="G$1" pin="1"/>
<wire x1="-101.6" y1="-444.5" x2="-101.6" y2="-447.04" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,-119.38,121.92,U1,DVCC,VCC_3P3,,,"/>
<approved hash="104,1,124.46,121.92,U1,DVSS,GND,,,"/>
<approved hash="104,1,-607.06,-383.54,U2,VSS_2,GND,,,"/>
<approved hash="104,1,-675.64,-353.06,U2,VIN_DC,SOLAR_CELL_ANODE,,,"/>
<approved hash="104,1,-607.06,-325.12,U2,VOUT,V_LOAD,,,"/>
<approved hash="104,1,-607.06,-388.62,U2,VSS,GND,,,"/>
<approved hash="104,1,-635,-309.88,U2,VSTOR,VOC_SAMP__VSTOR,,,"/>
<approved hash="104,1,-607.06,-393.7,U2,PAD,GND,,,"/>
<approved hash="104,1,411.48,-408.94,U4,VSS3,GND,,,"/>
<approved hash="104,1,396.24,-408.94,U4,VSS1,GND,,,"/>
<approved hash="104,1,403.86,-408.94,U4,VSS2,GND,,,"/>
<approved hash="104,1,-416.56,-439.42,U$20,GND1,GND,,,"/>
<approved hash="104,1,-401.32,-439.42,U$20,GND3,GND,,,"/>
<approved hash="104,1,-408.94,-439.42,U$20,GND2,GND,,,"/>
<approved hash="104,1,-391.16,-439.42,U$20,PGND,GND,,,"/>
<approved hash="104,1,-383.54,-439.42,U$20,PAD,GND,,,"/>
<approved hash="104,1,-594.36,-81.28,U6,VCC,VCC_2P2,,,"/>
<approved hash="104,1,-167.64,-312.42,U3,VCC,VCC_BQ24616,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
