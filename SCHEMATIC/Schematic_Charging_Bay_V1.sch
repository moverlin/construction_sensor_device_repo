<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="10" fill="10" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="11" fill="10" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="12" fill="10" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="13" fill="10" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="14" fill="10" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="15" fill="10" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="16" fill="10" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="LC3">
<packages>
<package name="1_CL21B106KQQNNNE">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="1_BATTERY">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-7.773" y1="2.383" x2="7.873" y2="2.383" width="0.0508" layer="39"/>
<wire x1="7.873" y1="-2.483" x2="-7.773" y2="-2.483" width="0.0508" layer="39"/>
<wire x1="-7.773" y1="-2.483" x2="-7.773" y2="2.383" width="0.0508" layer="39"/>
<wire x1="7.873" y1="2.383" x2="7.873" y2="-2.483" width="0.0508" layer="39"/>
<wire x1="6.5" y1="2.213" x2="-6.5" y2="2.213" width="0.1016" layer="51"/>
<wire x1="-6.5" y1="-2.213" x2="6.5" y2="-2.213" width="0.1016" layer="51"/>
<smd name="1" x="-4" y="0" dx="4" dy="4" layer="1"/>
<smd name="2" x="4" y="0" dx="4" dy="4" layer="1"/>
<text x="-2.79" y="3.11" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.39" y="-4.4" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-6" y1="-2" x2="-2" y2="2" layer="51"/>
<rectangle x1="2" y1="-2" x2="6" y2="2" layer="51"/>
<text x="-7.62" y="-2.54" size="1.27" layer="21" rot="R90">BAT_P</text>
</package>
</packages>
<symbols>
<symbol name="1_CL21B106KQQNNNE">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="1_BATTERY">
<text x="-16.51" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="19.05" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-27.94" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="38.1" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="-15.24" y1="35.56" x2="15.24" y2="35.56" width="0.254" layer="94"/>
<wire x1="15.24" y1="35.56" x2="15.24" y2="-25.4" width="0.254" layer="94"/>
<wire x1="15.24" y1="-25.4" x2="-15.24" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-25.4" x2="-15.24" y2="35.56" width="0.254" layer="94"/>
<text x="-2.54" y="30.48" size="1.27" layer="94">POS</text>
<text x="-2.54" y="-22.86" size="1.27" layer="94">NEG</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CL21B106KQQNNNE">
<gates>
<gate name="G$1" symbol="1_CL21B106KQQNNNE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1_CL21B106KQQNNNE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BATTERY">
<gates>
<gate name="G$1" symbol="1_BATTERY" x="0" y="-7.62"/>
</gates>
<devices>
<device name="" package="1_BATTERY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;map name="nav_main"&gt;
&lt;area shape="rect" coords="0,1,140,23" href="../military_specs.asp" title=""&gt;
&lt;area shape="rect" coords="0,24,140,51" href="../about.asp" title=""&gt;
&lt;area shape="rect" coords="1,52,140,77" href="../rfq.asp" title=""&gt;
&lt;area shape="rect" coords="0,78,139,103" href="../products.asp" title=""&gt;
&lt;area shape="rect" coords="1,102,138,128" href="../excess_inventory.asp" title=""&gt;
&lt;area shape="rect" coords="1,129,138,150" href="../edge.asp" title=""&gt;
&lt;area shape="rect" coords="1,151,139,178" href="../industry_links.asp" title=""&gt;
&lt;area shape="rect" coords="0,179,139,201" href="../comments.asp" title=""&gt;
&lt;area shape="rect" coords="1,203,138,231" href="../directory.asp" title=""&gt;
&lt;area shape="default" nohref&gt;
&lt;/map&gt;

&lt;html&gt;

&lt;title&gt;&lt;/title&gt;

 &lt;LINK REL="StyleSheet" TYPE="text/css" HREF="style-sheet.css"&gt;

&lt;body bgcolor="#ffffff" text="#000000" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0"&gt;
&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0 height="55%"&gt;
&lt;tr valign="top"&gt;

&lt;/td&gt;
&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/BODY&gt;&lt;/HTML&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="R01005">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="C0201">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
<package name="C01005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="HPC0201">
<description>&lt;b&gt; &lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<smd name="1" x="-0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<smd name="2" x="0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<text x="-0.75" y="0.74" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.785" y="-1.865" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.305" y1="-0.15" x2="0.305" y2="0.15" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-EU-1">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-US_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU-1" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HPC0201" package="HPC0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="C01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ICs">
<packages>
<package name="BQ25570_RGR0020A">
<smd name="1" x="-1.650009375" y="0.999996875" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R180"/>
<smd name="2" x="-1.650009375" y="0.5" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R180"/>
<smd name="3" x="-1.650009375" y="0" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R180"/>
<smd name="4" x="-1.650009375" y="-0.5" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R180"/>
<smd name="5" x="-1.650009375" y="-0.999996875" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R180"/>
<smd name="6" x="-0.999996875" y="-1.650009375" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="-0.5" y="-1.650009375" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="0" y="-1.650009375" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="0.5" y="-1.650009375" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="0.999996875" y="-1.650009375" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="1.650009375" y="-0.999996875" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R180"/>
<smd name="12" x="1.650009375" y="-0.5" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R180"/>
<smd name="13" x="1.650009375" y="0" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R180"/>
<smd name="14" x="1.650009375" y="0.5" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R180"/>
<smd name="15" x="1.650009375" y="0.999996875" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R180"/>
<smd name="16" x="0.999996875" y="1.650009375" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="0.5" y="1.650009375" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R90"/>
<smd name="18" x="0" y="1.650009375" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R90"/>
<smd name="19" x="-0.5" y="1.650009375" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R90"/>
<smd name="20" x="-0.999996875" y="1.650009375" dx="0.6096" dy="0.2286" layer="1" roundness="100" rot="R90"/>
<smd name="21" x="0" y="0" dx="2.0574" dy="2.0574" layer="1"/>
<smd name="V" x="0" y="-0.77500625" dx="0.508" dy="0.508" layer="1" roundness="100"/>
<wire x1="-1.9812" y1="-1.9812" x2="-1.778" y2="-1.9812" width="0.2032" layer="51"/>
<wire x1="-1.9812" y1="-1.9812" x2="-1.9812" y2="-1.778" width="0.2032" layer="51"/>
<wire x1="1.778" y1="-1.9812" x2="1.9812" y2="-1.9812" width="0.2032" layer="51"/>
<wire x1="1.9812" y1="-1.9812" x2="1.9812" y2="-1.778" width="0.2032" layer="51"/>
<wire x1="1.9812" y1="1.778" x2="1.9812" y2="1.9812" width="0.2032" layer="51"/>
<wire x1="1.778" y1="1.9812" x2="1.9812" y2="1.9812" width="0.2032" layer="51"/>
<wire x1="-1.9812" y1="1.9812" x2="-1.397" y2="1.9812" width="0.2032" layer="51"/>
<wire x1="-1.9812" y1="1.4224" x2="-1.9812" y2="1.9812" width="0.2032" layer="51"/>
<text x="-1.8288" y="2.7178" size="1.27" layer="51" ratio="6" rot="SR0">Designator250</text>
<wire x1="-1.8288" y1="-1.8288" x2="-1.8288" y2="1.8288" width="0.1524" layer="21"/>
<wire x1="1.8288" y1="-1.8288" x2="1.8288" y2="1.8288" width="0.1524" layer="21"/>
<wire x1="-1.8288" y1="1.8288" x2="1.8288" y2="1.8288" width="0.1524" layer="21"/>
<wire x1="-1.8288" y1="-1.8288" x2="1.8288" y2="-1.8288" width="0.1524" layer="21"/>
<wire x1="-0.8636" y1="0.9144" x2="-1.4732" y2="0.9144" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.4732" y1="0.9144" x2="-0.8636" y2="0.9144" width="0.1524" layer="21" curve="-180"/>
<text x="-1.524" y="-1.397" size="1.27" layer="21" ratio="6" rot="SR0">.Designator</text>
<polygon width="0" layer="31">
<vertex x="0.1016" y="0.1016"/>
<vertex x="1.016" y="0.1016"/>
<vertex x="1.016" y="1.016"/>
<vertex x="0.1016" y="1.016"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-0.1016" y="1.016"/>
<vertex x="-1.016" y="1.016"/>
<vertex x="-1.016" y="0.1016"/>
<vertex x="-0.1016" y="0.1016"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-0.1016" y="-0.1016"/>
<vertex x="-1.016" y="-0.1016"/>
<vertex x="-1.016" y="-1.016"/>
<vertex x="-0.1016" y="-1.016"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="0.1016" y="-1.016"/>
<vertex x="1.016" y="-1.016"/>
<vertex x="1.016" y="-0.1016"/>
<vertex x="0.1016" y="-0.1016"/>
</polygon>
<text x="-2.8702" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
</package>
<package name="TSSOP_DBT_30">
<smd name="1" x="-3.6703" y="4.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="2" x="-3.6703" y="4" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="3" x="-3.6703" y="3.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="4" x="-3.6703" y="3" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="5" x="-3.6703" y="2.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="6" x="-3.6703" y="2" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="7" x="-3.6703" y="1.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="8" x="-3.6703" y="1" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="9" x="-3.6703" y="0.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="10" x="-3.6703" y="0" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="11" x="-3.6703" y="-0.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="12" x="-3.6703" y="-1" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="13" x="-3.6703" y="-1.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="14" x="-3.6703" y="-2" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="15" x="-3.6703" y="-2.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="16" x="3.6703" y="-2.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="17" x="3.6703" y="-2" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="18" x="3.6703" y="-1.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="19" x="3.6703" y="-1" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="20" x="3.6703" y="-0.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="21" x="3.6703" y="0" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="22" x="3.6703" y="0.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="23" x="3.6703" y="1" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="24" x="3.6703" y="1.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="25" x="3.6703" y="2" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="26" x="3.6703" y="2.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="27" x="3.6703" y="3" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="28" x="3.6703" y="3.5" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="29" x="3.6703" y="4" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="30" x="3.6703" y="4.5" dx="1.6764" dy="0.3556" layer="1"/>
<wire x1="-3.0988" y1="-3.1492" x2="3.0988" y2="-3.1492" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="-3.1492" x2="3.0988" y2="4.9492" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="4.9492" x2="0.3148" y2="4.9492" width="0.1524" layer="21"/>
<wire x1="0.3148" y1="4.9492" x2="-0.2948" y2="4.9492" width="0.1524" layer="21"/>
<wire x1="-0.2948" y1="4.9492" x2="-3.0788" y2="4.9492" width="0.1524" layer="21"/>
<wire x1="-3.0788" y1="4.9492" x2="-3.0988" y2="-3.1492" width="0.1524" layer="21"/>
<wire x1="0.3148" y1="4.9492" x2="-0.2948" y2="4.9492" width="0" layer="21" curve="-180"/>
<text x="-4.6458" y="4.8406" size="0.4" layer="22" ratio="6" rot="SMR0">*</text>
<text x="-2.5302" y="5.565" size="0.4" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-2.5344" y="5.115" size="0.4" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
</package>
<package name="PWP20_2P45X3P2">
<smd name="1" x="-2.9718" y="2.9250625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="2" x="-2.9718" y="2.275028125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="3" x="-2.9718" y="1.625040625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="4" x="-2.9718" y="0.975003125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="5" x="-2.9718" y="0.32501875" dx="1.27" dy="0.3048" layer="1"/>
<smd name="6" x="-2.9718" y="-0.32501875" dx="1.27" dy="0.3048" layer="1"/>
<smd name="7" x="-2.9718" y="-0.975003125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="8" x="-2.9718" y="-1.625040625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="9" x="-2.9718" y="-2.275028125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="10" x="-2.9718" y="-2.9250625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="11" x="2.9718" y="-2.9250625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="12" x="2.9718" y="-2.275028125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="13" x="2.9718" y="-1.625040625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="14" x="2.9718" y="-0.975003125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="15" x="2.9718" y="-0.32501875" dx="1.27" dy="0.3048" layer="1"/>
<smd name="16" x="2.9718" y="0.32501875" dx="1.27" dy="0.3048" layer="1"/>
<smd name="17" x="2.9718" y="0.975003125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="18" x="2.9718" y="1.625040625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="19" x="2.9718" y="2.275028125" dx="1.27" dy="0.3048" layer="1"/>
<smd name="20" x="2.9718" y="2.9250625" dx="1.27" dy="0.3048" layer="1"/>
<smd name="PAD" x="0" y="0" dx="2.4384" dy="3.2004" layer="1"/>
<wire x1="-2.0828" y1="-3.302" x2="2.0828" y2="-3.302" width="0.1524" layer="51"/>
<wire x1="2.0828" y1="3.302" x2="0.3048" y2="3.302" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="3.302" x2="-0.3048" y2="3.302" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="3.302" x2="-2.0828" y2="3.302" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="3.302" x2="-0.3048" y2="3.302" width="0" layer="51" curve="-180"/>
<polygon width="0" layer="51">
<vertex x="-4.1148" y="-2.7432"/>
<vertex x="-4.1148" y="-3.1242"/>
<vertex x="-3.8608" y="-3.1242"/>
<vertex x="-3.8608" y="-2.7432"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="4.1148" y="3.1242"/>
<vertex x="4.1148" y="2.7432"/>
<vertex x="3.8608" y="2.7432"/>
<vertex x="3.8608" y="3.1242"/>
</polygon>
<text x="-3.81" y="3.0988" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<wire x1="-2.2352" y1="2.7686" x2="-2.2352" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="3.0734" x2="-3.302" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="3.0734" x2="-3.302" y2="2.7686" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="2.7686" x2="-2.2352" y2="2.7686" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="2.1336" x2="-2.2352" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="2.4384" x2="-3.302" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="2.4384" x2="-3.302" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="2.1336" x2="-2.2352" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="1.4732" x2="-2.2352" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="1.778" x2="-3.302" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.778" x2="-3.302" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.4732" x2="-2.2352" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="0.8128" x2="-2.2352" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="1.1176" x2="-3.302" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.1176" x2="-3.302" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.8128" x2="-2.2352" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="0.1778" x2="-2.2352" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="0.4826" x2="-3.302" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.4826" x2="-3.302" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.1778" x2="-2.2352" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-0.4826" x2="-2.2352" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-0.1778" x2="-3.302" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-0.1778" x2="-3.302" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-0.4826" x2="-2.2352" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-1.1176" x2="-2.2352" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-0.8128" x2="-3.302" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-0.8128" x2="-3.302" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.1176" x2="-2.2352" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-1.778" x2="-2.2352" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-1.4732" x2="-3.302" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.4732" x2="-3.302" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.778" x2="-2.2352" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-2.4384" x2="-2.2352" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-2.1336" x2="-3.302" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-2.1336" x2="-3.302" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-2.4384" x2="-2.2352" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-3.0734" x2="-2.2352" y2="-2.7686" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-2.7686" x2="-3.302" y2="-2.7686" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-2.7686" x2="-3.302" y2="-3.0734" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-3.0734" x2="-2.2352" y2="-3.0734" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.7686" x2="2.2352" y2="-3.0734" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-3.0734" x2="3.302" y2="-3.0734" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-3.0734" x2="3.302" y2="-2.7686" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-2.7686" x2="2.2352" y2="-2.7686" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.1336" x2="2.2352" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.4384" x2="3.302" y2="-2.4384" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-2.4384" x2="3.302" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-2.1336" x2="2.2352" y2="-2.1336" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-1.4732" x2="2.2352" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-1.778" x2="3.302" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.778" x2="3.302" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.4732" x2="2.2352" y2="-1.4732" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-0.8128" x2="2.2352" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-1.1176" x2="3.302" y2="-1.1176" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.1176" x2="3.302" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.8128" x2="2.2352" y2="-0.8128" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-0.1778" x2="2.2352" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-0.4826" x2="3.302" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.4826" x2="3.302" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.1778" x2="2.2352" y2="-0.1778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="0.4826" x2="2.2352" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="0.1778" x2="3.302" y2="0.1778" width="0.1524" layer="21"/>
<wire x1="3.302" y1="0.1778" x2="3.302" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="3.302" y1="0.4826" x2="2.2352" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="1.1176" x2="2.2352" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="0.8128" x2="3.302" y2="0.8128" width="0.1524" layer="21"/>
<wire x1="3.302" y1="0.8128" x2="3.302" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.1176" x2="2.2352" y2="1.1176" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="1.778" x2="2.2352" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="1.4732" x2="3.302" y2="1.4732" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.4732" x2="3.302" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.778" x2="2.2352" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="2.4384" x2="2.2352" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="2.1336" x2="3.302" y2="2.1336" width="0.1524" layer="21"/>
<wire x1="3.302" y1="2.1336" x2="3.302" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="3.302" y1="2.4384" x2="2.2352" y2="2.4384" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="3.0734" x2="2.2352" y2="2.7686" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="2.7686" x2="3.302" y2="2.7686" width="0.1524" layer="21"/>
<wire x1="3.302" y1="2.7686" x2="3.302" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="3.302" y1="3.0734" x2="2.2352" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-3.302" x2="2.2352" y2="-3.302" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-3.302" x2="2.2352" y2="3.302" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="3.302" x2="0.3048" y2="3.302" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="3.302" x2="-0.3048" y2="3.302" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="3.302" x2="-2.2352" y2="3.302" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="3.302" x2="-2.2352" y2="-3.302" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="3.302" x2="-0.3048" y2="3.302" width="0" layer="21" curve="-180"/>
<text x="-3.81" y="3.0988" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<polygon width="0.1524" layer="1">
<vertex x="-0.635" y="1.6002"/>
<vertex x="-0.635" y="1.905"/>
<vertex x="0.635" y="1.905"/>
<vertex x="0.635" y="1.6002"/>
</polygon>
<polygon width="0.1524" layer="1">
<vertex x="-0.635" y="-1.6002"/>
<vertex x="-0.635" y="-1.905"/>
<vertex x="0.635" y="-1.905"/>
<vertex x="0.635" y="-1.6002"/>
</polygon>
<polygon width="0.1524" layer="29">
<vertex x="-0.635" y="1.6002"/>
<vertex x="-0.635" y="1.905"/>
<vertex x="0.635" y="1.905"/>
<vertex x="0.635" y="1.6002"/>
</polygon>
<polygon width="0.1524" layer="29">
<vertex x="-0.635" y="-1.6002"/>
<vertex x="-0.635" y="-1.905"/>
<vertex x="0.635" y="-1.905"/>
<vertex x="0.635" y="-1.6002"/>
</polygon>
<text x="-2.8702" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
</package>
<package name="VQFN_RGE_24">
<smd name="PAD_GND" x="0" y="0" dx="2.6924" dy="2.6924" layer="1"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="3.25" width="0.1524" layer="25"/>
<wire x1="3.25" y1="-3.25" x2="3.25" y2="3.25" width="0.1524" layer="25"/>
<wire x1="-3.25" y1="3.25" x2="3.25" y2="3.25" width="0.1524" layer="25"/>
<wire x1="-3.25" y1="-3.25" x2="3.25" y2="-3.25" width="0.1524" layer="25"/>
<wire x1="-2.5954" y1="1.9192" x2="-3.0018" y2="1.9192" width="0.1524" layer="25" curve="-180"/>
<wire x1="-3.0018" y1="1.9192" x2="-2.5954" y2="1.9192" width="0.1524" layer="25" curve="-180"/>
<text x="-2.9542" y="-6.143" size="1.27" layer="25" ratio="6" rot="SR0">.Designator</text>
<text x="-2.8702" y="-4.735" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<wire x1="-3.2352" y1="-3.2352" x2="-2.9066" y2="-3.2352" width="0.2032" layer="51"/>
<wire x1="-3.2352" y1="-3.2352" x2="-3.2352" y2="-2.9066" width="0.2032" layer="51"/>
<wire x1="3.2352" y1="-3.2352" x2="3.2352" y2="-2.9066" width="0.2032" layer="51"/>
<wire x1="2.9066" y1="-3.2352" x2="3.2352" y2="-3.2352" width="0.2032" layer="51"/>
<wire x1="2.9066" y1="3.2352" x2="3.2352" y2="3.2352" width="0.2032" layer="51"/>
<wire x1="3.2352" y1="2.9066" x2="3.2352" y2="3.2352" width="0.2032" layer="51"/>
<text x="-3.1544" y="3.665" size="1.27" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
<smd name="6" x="-1.25" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="5" x="-0.75" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<wire x1="-3.2352" y1="3.2352" x2="-3.2352" y2="2.9066" width="0.2032" layer="51"/>
<wire x1="-2.8066" y1="3.2352" x2="-3.2352" y2="3.2352" width="0.2032" layer="51"/>
<smd name="1" x="1.25" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="2" x="0.75" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="3" x="0.25" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="4" x="-0.25" y="2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="18" x="1.25" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="15" x="-0.25" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="17" x="0.75" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="16" x="0.25" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="14" x="-0.75" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="13" x="-1.25" y="-2.65" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R90"/>
<smd name="19" x="2.65" y="-1.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="20" x="2.65" y="-0.75" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="21" x="2.65" y="-0.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="22" x="2.65" y="0.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="23" x="2.65" y="0.75" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="24" x="2.65" y="1.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="10" x="-2.65" y="-0.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="9" x="-2.65" y="0.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="8" x="-2.65" y="0.75" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="11" x="-2.65" y="-0.75" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="7" x="-2.65" y="1.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="12" x="-2.65" y="-1.25" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<pad name="PAD_VIA3" x="-0.6731" y="-0.6731" drill="0.254"/>
<pad name="PAD_VIA1" x="-0.6731" y="0.6731" drill="0.254"/>
<pad name="PAD_VIA2" x="0.6731" y="0.6731" drill="0.254"/>
<pad name="PAD_VIA4" x="0.6731" y="-0.6731" drill="0.254"/>
</package>
<package name="PW20">
<smd name="1" x="-2.8194" y="2.9250125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="2" x="-2.8194" y="2.275003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="3" x="-2.8194" y="1.624990625" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="4" x="-2.8194" y="0.975003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="5" x="-2.8194" y="0.32499375" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="6" x="-2.8194" y="-0.32499375" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="7" x="-2.8194" y="-0.975003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="8" x="-2.8194" y="-1.624990625" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="9" x="-2.8194" y="-2.275003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="10" x="-2.8194" y="-2.9249875" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="11" x="2.8194" y="-2.9250125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="12" x="2.8194" y="-2.275003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="13" x="2.8194" y="-1.624990625" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="14" x="2.8194" y="-0.975003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="15" x="2.8194" y="-0.32499375" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="16" x="2.8194" y="0.32499375" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="17" x="2.8194" y="0.975003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="18" x="2.8194" y="1.624990625" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="19" x="2.8194" y="2.275003125" dx="1.6764" dy="0.3556" layer="1"/>
<smd name="20" x="2.8194" y="2.9249875" dx="1.6764" dy="0.3556" layer="1"/>
<wire x1="-2.2352" y1="-3.302" x2="2.2352" y2="-3.302" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="3.302" x2="0.3048" y2="3.302" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="3.302" x2="-2.2352" y2="3.302" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="3.302" x2="-0.3048" y2="3.302" width="0.127" layer="21" curve="-180"/>
<text x="-3.6576" y="3.1496" size="0.508" layer="21" ratio="6" rot="SR90">*</text>
<text x="-1.778" y="4.1402" size="0.508" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-1.8542" y="3.5052" size="0.508" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="BQ25570RGR">
<pin name="VSS_2" x="33.02" y="-25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="VIN_DC" x="-35.56" y="5.08" length="middle" direction="pwr"/>
<pin name="VOC_SAMP" x="-7.62" y="48.26" length="middle" direction="in" rot="R270"/>
<pin name="VREF_SAMP" x="-35.56" y="27.94" length="middle" direction="in"/>
<pin name="EN_N" x="-35.56" y="-20.32" length="middle" direction="in"/>
<pin name="VOUT_EN" x="-35.56" y="-27.94" length="middle" direction="in"/>
<pin name="VBAT_OV" x="-7.62" y="-43.18" length="middle" direction="in" rot="R90"/>
<pin name="VRDIV" x="-15.24" y="-43.18" length="middle" direction="out" rot="R90"/>
<pin name="NC_2" x="33.02" y="-17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="OK_HYST" x="7.62" y="-43.18" length="middle" direction="in" rot="R90"/>
<pin name="OK_PROG" x="0" y="-43.18" length="middle" direction="in" rot="R90"/>
<pin name="VOUT_SET" x="15.24" y="-43.18" length="middle" direction="in" rot="R90"/>
<pin name="VBAT_OK" x="-35.56" y="-35.56" length="middle" direction="out"/>
<pin name="VOUT" x="33.02" y="33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS" x="33.02" y="-30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="LBUCK" x="33.02" y="38.1" length="middle" direction="pas" rot="R180"/>
<pin name="NC" x="33.02" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="VBAT" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="VSTOR" x="5.08" y="48.26" length="middle" direction="pwr" rot="R270"/>
<pin name="LBOOST" x="-35.56" y="38.1" length="middle" direction="in"/>
<pin name="PAD" x="33.02" y="-35.56" length="middle" direction="pwr" rot="R180"/>
<wire x1="-30.48" y1="-38.1" x2="27.94" y2="-38.1" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-38.1" x2="27.94" y2="43.18" width="0.1524" layer="94"/>
<wire x1="27.94" y1="43.18" x2="-30.48" y2="43.18" width="0.1524" layer="94"/>
<wire x1="-30.48" y1="43.18" x2="-30.48" y2="-38.1" width="0.1524" layer="94"/>
<text x="-4.7244" y="4.0386" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-5.6642" y="1.4986" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="BQ78350">
<wire x1="-43.18" y1="68.58" x2="-43.18" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="-43.18" y1="-66.04" x2="45.72" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="45.72" y1="-66.04" x2="45.72" y2="68.58" width="0.1524" layer="94"/>
<wire x1="45.72" y1="68.58" x2="-43.18" y2="68.58" width="0.1524" layer="94"/>
<text x="-4.7244" y="14.1986" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-5.6642" y="9.1186" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="SMBD" x="48.26" y="-50.8" length="short" rot="R180"/>
<pin name="SMBC" x="48.26" y="-40.64" length="short" rot="R180"/>
<pin name="BAT" x="-45.72" y="38.1" length="short" direction="pwr"/>
<pin name="VSS3" x="2.54" y="-68.58" length="short" direction="pwr" rot="R90"/>
<text x="-10.16" y="-55.88" size="3.81" layer="94">GND</text>
<text x="35.56" y="-53.34" size="3.81" layer="94" rot="R90">SMBus</text>
<pin name="KEYIN" x="-45.72" y="20.32" length="short" direction="in"/>
<pin name="MRST" x="-45.72" y="58.42" length="short" direction="in"/>
<pin name="VCC" x="-45.72" y="66.04" length="short" direction="pwr"/>
<pin name="COM" x="48.26" y="20.32" length="short" direction="out" rot="R180"/>
<pin name="PRES" x="-45.72" y="15.24" length="short" direction="in"/>
<pin name="RBI" x="-45.72" y="0" length="short" direction="pwr"/>
<pin name="VAUX" x="48.26" y="50.8" length="short" direction="pwr" rot="R180"/>
<pin name="SAFE" x="-45.72" y="-7.62" length="short" direction="out"/>
<pin name="SMBA" x="48.26" y="-63.5" length="short" direction="in" rot="R180"/>
<pin name="VSS1" x="-12.7" y="-68.58" length="short" direction="pwr" rot="R90"/>
<pin name="VSS2" x="-5.08" y="-68.58" length="short" direction="pwr" rot="R90"/>
<pin name="ALERT" x="12.7" y="-68.58" length="short" rot="R90"/>
<pin name="BQ769X0_DATA" x="20.32" y="-68.58" length="short" rot="R90"/>
<pin name="BQ769X0_CLK" x="27.94" y="-68.58" length="short" rot="R90"/>
<text x="10.16" y="-50.8" size="2.54" layer="94">BQ769x0 
CLK/DATA
AFE</text>
<pin name="PRECHG" x="48.26" y="-12.7" length="short" direction="out" rot="R180"/>
<pin name="GPIO_B" x="48.26" y="-30.48" length="short" rot="R180"/>
<pin name="GPIO_A" x="48.26" y="-22.86" length="short" rot="R180"/>
<pin name="ADREN" x="-30.48" y="-68.58" length="short" direction="out" rot="R90"/>
<pin name="VEN" x="48.26" y="27.94" length="short" direction="out" rot="R180"/>
<pin name="DISP" x="-45.72" y="-50.8" length="short" direction="in"/>
<pin name="PWRM" x="48.26" y="-2.54" length="short" direction="out" rot="R180"/>
<pin name="LED1" x="-45.72" y="-15.24" length="short" direction="out"/>
<pin name="LED2" x="-45.72" y="-22.86" length="short" direction="out"/>
<pin name="LED3" x="-45.72" y="-30.48" length="short" direction="out"/>
<pin name="LED4" x="-45.72" y="-38.1" length="short" direction="out"/>
<pin name="LED5" x="-45.72" y="-45.72" length="short" direction="out"/>
<text x="-27.94" y="-43.18" size="4.826" layer="94" rot="R90">LEDs</text>
<text x="27.94" y="-27.94" size="1.27" layer="94" ratio="6">Optional 
Gen I/O</text>
</symbol>
<symbol name="TPS55332_Q1">
<pin name="RST" x="-27.94" y="-7.62" length="short" direction="out"/>
<wire x1="-25.4" y1="63.5" x2="-25.4" y2="-45.72" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-45.72" x2="25.4" y2="-45.72" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-45.72" x2="25.4" y2="63.5" width="0.1524" layer="94"/>
<wire x1="25.4" y1="63.5" x2="-25.4" y2="63.5" width="0.1524" layer="94"/>
<text x="-22.5044" y="64.9986" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-3.1242" y="16.7386" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="RSLEW" x="27.94" y="-38.1" length="short" direction="out" rot="R180"/>
<pin name="BOOT" x="27.94" y="-20.32" length="short" direction="out" rot="R180"/>
<pin name="VREG" x="27.94" y="50.8" length="short" direction="in" rot="R180"/>
<pin name="GND1" x="-20.32" y="-48.26" length="short" direction="pwr" rot="R90"/>
<pin name="GND3" x="-5.08" y="-48.26" length="short" direction="pwr" rot="R90"/>
<text x="-7.62" y="-35.56" size="3.81" layer="94">GND</text>
<pin name="RST_TH" x="27.94" y="-5.08" length="short" direction="in" rot="R180"/>
<pin name="SW" x="27.94" y="60.96" length="short" rot="R180"/>
<pin name="SYNC" x="-27.94" y="2.54" length="short" direction="in"/>
<pin name="CDLY" x="-27.94" y="22.86" length="short" direction="out"/>
<pin name="RT" x="-27.94" y="43.18" length="short" direction="out"/>
<pin name="EN" x="-27.94" y="53.34" length="short" direction="in"/>
<pin name="VIN" x="-27.94" y="60.96" length="short" direction="pwr"/>
<pin name="GND2" x="-12.7" y="-48.26" length="short" direction="pwr" rot="R90"/>
<pin name="SS" x="-27.94" y="-17.78" length="short" direction="out"/>
<pin name="VSENSE" x="27.94" y="30.48" length="short" direction="in" rot="R180"/>
<pin name="COMP" x="27.94" y="10.16" length="short" direction="out" rot="R180"/>
<pin name="PGND" x="5.08" y="-48.26" length="short" direction="pwr" rot="R90"/>
<text x="5.08" y="-22.86" size="8.382" layer="94" ratio="6" rot="R90">TPS55332-Q1</text>
<pin name="NC1" x="15.24" y="66.04" length="short" direction="nc" rot="R270"/>
<pin name="NC2" x="7.62" y="66.04" length="short" direction="nc" rot="R270"/>
<pin name="PAD" x="12.7" y="-48.26" length="short" direction="pwr" rot="R90"/>
</symbol>
<symbol name="BQ24616">
<pin name="ACDRV" x="-20.32" y="58.42" length="short" direction="out" rot="R270"/>
<wire x1="-35.56" y1="55.88" x2="-35.56" y2="-76.2" width="0.1524" layer="94"/>
<wire x1="-35.56" y1="-76.2" x2="35.56" y2="-76.2" width="0.1524" layer="94"/>
<wire x1="35.56" y1="-76.2" x2="35.56" y2="55.88" width="0.1524" layer="94"/>
<wire x1="35.56" y1="55.88" x2="-35.56" y2="55.88" width="0.1524" layer="94"/>
<text x="0.3556" y="16.7386" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-0.5842" y="11.6586" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="CE" x="-38.1" y="33.02" length="short" direction="in"/>
<pin name="STAT1" x="-38.1" y="-48.26" length="short" direction="out"/>
<pin name="PG" x="-38.1" y="-58.42" length="short" direction="out"/>
<pin name="GND" x="-15.24" y="-78.74" length="short" direction="pwr" rot="R90"/>
<pin name="PAD_GND1" x="2.54" y="-78.74" length="short" direction="pwr" rot="R90"/>
<text x="-12.7" y="-43.18" size="3.81" layer="94">GNDs</text>
<pin name="TS" x="-38.1" y="-71.12" length="short" direction="in"/>
<pin name="ACP" x="-5.08" y="58.42" length="short" direction="in" rot="R270"/>
<pin name="ACN" x="10.16" y="58.42" length="short" direction="in" rot="R270"/>
<pin name="STAT2" x="-38.1" y="-53.34" length="short" direction="out"/>
<pin name="VREF" x="-38.1" y="22.86" length="short" direction="out"/>
<pin name="ACSET" x="-38.1" y="-10.16" length="short" direction="in"/>
<pin name="ISET1" x="-38.1" y="5.08" length="short" direction="in"/>
<pin name="ISET2" x="-38.1" y="-2.54" length="short" direction="in"/>
<pin name="TTC" x="38.1" y="-53.34" length="short" direction="in" rot="R180"/>
<pin name="VFB" x="38.1" y="-45.72" length="short" direction="in" rot="R180"/>
<pin name="SRP" x="38.1" y="-7.62" length="short" rot="R180"/>
<pin name="SRN" x="38.1" y="-25.4" length="short" rot="R180"/>
<pin name="LODRV" x="38.1" y="7.62" length="short" direction="out" rot="R180"/>
<pin name="PH" x="38.1" y="20.32" length="short" direction="in" rot="R180"/>
<pin name="HIDRV" x="38.1" y="33.02" length="short" direction="out" rot="R180"/>
<pin name="BATDRV" x="38.1" y="45.72" length="short" direction="out" rot="R180"/>
<text x="20.32" y="-17.78" size="1.778" layer="94">CHRG
 CS</text>
<text x="-2.54" y="40.64" size="1.778" layer="94">ADAPTER
    CS</text>
<text x="22.86" y="12.7" size="1.778" layer="94" rot="R90">DRIVE BATTERY CHRGING</text>
<pin name="VCC" x="-38.1" y="48.26" length="short" direction="pwr"/>
<pin name="REGN" x="30.48" y="58.42" length="short" direction="out" rot="R270"/>
<pin name="BTST" x="20.32" y="58.42" length="short" direction="in" rot="R270"/>
</symbol>
<symbol name="BQ76920">
<pin name="VC0" x="-30.48" y="-38.1" length="short" direction="in"/>
<wire x1="-27.94" y1="60.96" x2="-27.94" y2="-73.66" width="0.1524" layer="94"/>
<wire x1="-27.94" y1="-73.66" x2="22.86" y2="-73.66" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-73.66" x2="22.86" y2="60.96" width="0.1524" layer="94"/>
<wire x1="22.86" y1="60.96" x2="-27.94" y2="60.96" width="0.1524" layer="94"/>
<text x="-25.0444" y="62.4586" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-5.6642" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="SDA" x="25.4" y="-17.78" length="short" rot="R180"/>
<pin name="SCL" x="25.4" y="-10.16" length="short" direction="in" rot="R180"/>
<pin name="BAT" x="25.4" y="55.88" length="short" direction="pwr" rot="R180"/>
<pin name="CAP1" x="25.4" y="12.7" length="short" direction="out" rot="R180"/>
<pin name="REGOUT" x="25.4" y="35.56" length="short" direction="pwr" rot="R180"/>
<pin name="ALERT" x="-30.48" y="-63.5" length="short"/>
<pin name="VSS" x="7.62" y="-76.2" length="short" direction="pwr" rot="R90"/>
<text x="7.62" y="-66.04" size="3.81" layer="94" rot="R90">GND</text>
<text x="12.7" y="-17.78" size="3.81" layer="94" rot="R90">I2C</text>
<pin name="TS1" x="25.4" y="-38.1" length="short" direction="in" rot="R180"/>
<pin name="REGSRC" x="25.4" y="48.26" length="short" direction="in" rot="R180"/>
<pin name="NC_PIN11" x="7.62" y="63.5" length="short" direction="nc" rot="R270"/>
<pin name="VC1" x="-30.48" y="-22.86" length="short" direction="in"/>
<pin name="VC2" x="-30.48" y="-2.54" length="short" direction="in"/>
<pin name="VC3" x="-30.48" y="17.78" length="short" direction="in"/>
<pin name="VC4" x="-30.48" y="38.1" length="short" direction="in"/>
<pin name="VC5" x="-30.48" y="58.42" length="short" direction="in"/>
<pin name="SRN" x="0" y="-76.2" length="short" direction="in" rot="R90"/>
<pin name="SRP" x="-20.32" y="-76.2" length="short" direction="in" rot="R90"/>
<text x="-12.7" y="-30.48" size="4.826" layer="94" rot="R90">Battery Terminal Inputs</text>
<pin name="DSG" x="25.4" y="-68.58" length="short" direction="out" rot="R180"/>
<pin name="CHG" x="25.4" y="-60.96" length="short" direction="out" rot="R180"/>
<text x="-17.78" y="-66.04" size="3.302" layer="94">Current
Sense</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BQ25570RGRR" prefix="U">
<gates>
<gate name="A" symbol="BQ25570RGR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BQ25570_RGR0020A">
<connects>
<connect gate="A" pin="EN_N" pad="5"/>
<connect gate="A" pin="LBOOST" pad="20"/>
<connect gate="A" pin="LBUCK" pad="16"/>
<connect gate="A" pin="NC" pad="17"/>
<connect gate="A" pin="NC_2" pad="9"/>
<connect gate="A" pin="OK_HYST" pad="10"/>
<connect gate="A" pin="OK_PROG" pad="11"/>
<connect gate="A" pin="PAD" pad="21"/>
<connect gate="A" pin="VBAT" pad="18"/>
<connect gate="A" pin="VBAT_OK" pad="13"/>
<connect gate="A" pin="VBAT_OV" pad="7"/>
<connect gate="A" pin="VIN_DC" pad="2"/>
<connect gate="A" pin="VOC_SAMP" pad="3"/>
<connect gate="A" pin="VOUT" pad="14"/>
<connect gate="A" pin="VOUT_EN" pad="6"/>
<connect gate="A" pin="VOUT_SET" pad="12"/>
<connect gate="A" pin="VRDIV" pad="8"/>
<connect gate="A" pin="VREF_SAMP" pad="4"/>
<connect gate="A" pin="VSS" pad="15"/>
<connect gate="A" pin="VSS_2" pad="1"/>
<connect gate="A" pin="VSTOR" pad="19"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALTERNATEPARTNUMBER" value="BQ25570RGRT" constant="no"/>
<attribute name="APPLICATION_BUILDNUMBER" value="*" constant="no"/>
<attribute name="COMPTYPE" value="IC" constant="no"/>
<attribute name="MANUFACTURER" value="Texas Instruments" constant="no"/>
<attribute name="MOUNTTYPE" value="SMT" constant="no"/>
<attribute name="PACKAGEREFERENCE" value="RGR0020A" constant="no"/>
<attribute name="PARTNUMBER" value="BQ25570RGRR" constant="no"/>
<attribute name="PIN_COUNT" value="21" constant="no"/>
<attribute name="REFDES" value="RefDes" constant="no"/>
<attribute name="ROHS" value="Y" constant="no"/>
<attribute name="TYPE" value="TYPE" constant="no"/>
<attribute name="VALUE" value="Value" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BQ78350">
<gates>
<gate name="G$1" symbol="BQ78350" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="TSSOP_DBT_30">
<connects>
<connect gate="G$1" pin="ADREN" pad="29"/>
<connect gate="G$1" pin="ALERT" pad="2"/>
<connect gate="G$1" pin="BAT" pad="7"/>
<connect gate="G$1" pin="BQ769X0_CLK" pad="4"/>
<connect gate="G$1" pin="BQ769X0_DATA" pad="3"/>
<connect gate="G$1" pin="COM" pad="1"/>
<connect gate="G$1" pin="DISP" pad="14"/>
<connect gate="G$1" pin="GPIO_A" pad="21"/>
<connect gate="G$1" pin="GPIO_B" pad="28"/>
<connect gate="G$1" pin="KEYIN" pad="9"/>
<connect gate="G$1" pin="LED1" pad="16"/>
<connect gate="G$1" pin="LED2" pad="17"/>
<connect gate="G$1" pin="LED3" pad="18"/>
<connect gate="G$1" pin="LED4" pad="19"/>
<connect gate="G$1" pin="LED5" pad="20"/>
<connect gate="G$1" pin="MRST" pad="24"/>
<connect gate="G$1" pin="PRECHG" pad="5"/>
<connect gate="G$1" pin="PRES" pad="8"/>
<connect gate="G$1" pin="PWRM" pad="15"/>
<connect gate="G$1" pin="RBI" pad="27"/>
<connect gate="G$1" pin="SAFE" pad="10"/>
<connect gate="G$1" pin="SMBA" pad="30"/>
<connect gate="G$1" pin="SMBC" pad="13"/>
<connect gate="G$1" pin="SMBD" pad="11"/>
<connect gate="G$1" pin="VAUX" pad="6"/>
<connect gate="G$1" pin="VCC" pad="26"/>
<connect gate="G$1" pin="VEN" pad="12"/>
<connect gate="G$1" pin="VSS1" pad="22"/>
<connect gate="G$1" pin="VSS2" pad="23"/>
<connect gate="G$1" pin="VSS3" pad="25"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPS55332-Q1">
<gates>
<gate name="A" symbol="TPS55332_Q1" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="PWP20_2P45X3P2">
<connects>
<connect gate="A" pin="BOOT" pad="20"/>
<connect gate="A" pin="CDLY" pad="9"/>
<connect gate="A" pin="COMP" pad="15"/>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="GND1" pad="4"/>
<connect gate="A" pin="GND2" pad="10"/>
<connect gate="A" pin="GND3" pad="12"/>
<connect gate="A" pin="NC1" pad="1"/>
<connect gate="A" pin="NC2" pad="2"/>
<connect gate="A" pin="PAD" pad="PAD"/>
<connect gate="A" pin="PGND" pad="17"/>
<connect gate="A" pin="RSLEW" pad="7"/>
<connect gate="A" pin="RST" pad="8"/>
<connect gate="A" pin="RST_TH" pad="13"/>
<connect gate="A" pin="RT" pad="6"/>
<connect gate="A" pin="SS" pad="11"/>
<connect gate="A" pin="SW" pad="18"/>
<connect gate="A" pin="SYNC" pad="3"/>
<connect gate="A" pin="VIN" pad="19"/>
<connect gate="A" pin="VREG" pad="16"/>
<connect gate="A" pin="VSENSE" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BQ24616">
<gates>
<gate name="G$1" symbol="BQ24616" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="VQFN_RGE_24">
<connects>
<connect gate="G$1" pin="ACDRV" pad="3"/>
<connect gate="G$1" pin="ACN" pad="1"/>
<connect gate="G$1" pin="ACP" pad="2"/>
<connect gate="G$1" pin="ACSET" pad="16"/>
<connect gate="G$1" pin="BATDRV" pad="23"/>
<connect gate="G$1" pin="BTST" pad="22"/>
<connect gate="G$1" pin="CE" pad="4"/>
<connect gate="G$1" pin="GND" pad="17"/>
<connect gate="G$1" pin="HIDRV" pad="21"/>
<connect gate="G$1" pin="ISET1" pad="11"/>
<connect gate="G$1" pin="ISET2" pad="15"/>
<connect gate="G$1" pin="LODRV" pad="19"/>
<connect gate="G$1" pin="PAD_GND1" pad="PAD_GND PAD_VIA1 PAD_VIA2 PAD_VIA3 PAD_VIA4"/>
<connect gate="G$1" pin="PG" pad="8"/>
<connect gate="G$1" pin="PH" pad="20"/>
<connect gate="G$1" pin="REGN" pad="18"/>
<connect gate="G$1" pin="SRN" pad="13"/>
<connect gate="G$1" pin="SRP" pad="14"/>
<connect gate="G$1" pin="STAT1" pad="5"/>
<connect gate="G$1" pin="STAT2" pad="9"/>
<connect gate="G$1" pin="TS" pad="6"/>
<connect gate="G$1" pin="TTC" pad="7"/>
<connect gate="G$1" pin="VCC" pad="24"/>
<connect gate="G$1" pin="VFB" pad="12"/>
<connect gate="G$1" pin="VREF" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BQ76920">
<gates>
<gate name="G$1" symbol="BQ76920" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="PW20">
<connects>
<connect gate="G$1" pin="ALERT" pad="20"/>
<connect gate="G$1" pin="BAT" pad="10"/>
<connect gate="G$1" pin="CAP1" pad="7"/>
<connect gate="G$1" pin="CHG" pad="2"/>
<connect gate="G$1" pin="DSG" pad="1"/>
<connect gate="G$1" pin="NC_PIN11" pad="11"/>
<connect gate="G$1" pin="REGOUT" pad="8"/>
<connect gate="G$1" pin="REGSRC" pad="9"/>
<connect gate="G$1" pin="SCL" pad="5"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="SRN" pad="19"/>
<connect gate="G$1" pin="SRP" pad="18"/>
<connect gate="G$1" pin="TS1" pad="6"/>
<connect gate="G$1" pin="VC0" pad="17"/>
<connect gate="G$1" pin="VC1" pad="16"/>
<connect gate="G$1" pin="VC2" pad="15"/>
<connect gate="G$1" pin="VC3" pad="14"/>
<connect gate="G$1" pin="VC4" pad="13"/>
<connect gate="G$1" pin="VC5" pad="12"/>
<connect gate="G$1" pin="VSS" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Resistors_Caps_Inductors_Misc">
<packages>
<package name="SRP7030-2R2M__2P2UH">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-4.613" y1="2.02" x2="4.719" y2="2.018" width="0.0508" layer="39"/>
<wire x1="4.72" y1="-2.025" x2="-4.614" y2="-2.024" width="0.0508" layer="39"/>
<wire x1="-4.614" y1="-2.024" x2="-4.613" y2="2.02" width="0.0508" layer="39"/>
<wire x1="4.719" y1="2.018" x2="4.72" y2="-2.025" width="0.0508" layer="39"/>
<wire x1="-4.875" y1="2.17" x2="4.875" y2="2.17" width="0.1524" layer="21"/>
<wire x1="4.875" y1="2.17" x2="4.875" y2="-2.17" width="0.1524" layer="51"/>
<wire x1="4.875" y1="-2.17" x2="-4.875" y2="-2.17" width="0.1524" layer="21"/>
<wire x1="-4.875" y1="-2.17" x2="-4.875" y2="2.17" width="0.1524" layer="51"/>
<smd name="1" x="-3.1" y="0" dx="2.5" dy="3.5" layer="1"/>
<smd name="2" x="3.1" y="0" dx="2.5" dy="3.5" layer="1"/>
<text x="-0.721" y="0.604" size="0.254" layer="25" distance="5">&gt;NAME</text>
<text x="-0.733" y="-0.778" size="0.254" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-1.5" x2="1" y2="1.5" layer="35"/>
</package>
<package name="MGV06051R0M-10__1UH">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-4.5732" y1="2.1002" x2="4.6732" y2="2.1002" width="0.1016" layer="21"/>
<wire x1="-4.6478" y1="-2.1002" x2="4.5732" y2="-2.1002" width="0.1016" layer="21"/>
<smd name="1" x="-3.1" y="0" dx="2.5" dy="3.5" layer="1"/>
<smd name="2" x="3.1" y="0" dx="2.5" dy="3.5" layer="1"/>
<text x="-3.305" y="2.305" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.405" y="-3.575" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<wire x1="-4.5998" y1="-2.1478" x2="-4.5998" y2="2.1732" width="0.1016" layer="51"/>
<wire x1="4.6002" y1="-2.1478" x2="4.6002" y2="2.1732" width="0.1016" layer="51"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="744031220__22UH">
<description>&lt;b&gt;Package MSS7341&lt;/b&gt; 6.6 × 6.6 mm x 4.1 mm high, shielded inductors&lt;p&gt;
Source: http://www.farnell.com/datasheets/1681958.pdf</description>
<polygon width="0.2" layer="1">
<vertex x="-2.15" y="0.55"/>
<vertex x="-2.15" y="2.15"/>
<vertex x="2.15" y="2.15"/>
<vertex x="2.15" y="0.55"/>
<vertex x="1.3" y="0.55" curve="133.798882"/>
<vertex x="-1.3" y="0.55"/>
</polygon>
<polygon width="0.3" layer="29">
<vertex x="-2.2" y="0.5"/>
<vertex x="-2.2" y="2.2"/>
<vertex x="2.2" y="2.2"/>
<vertex x="2.2" y="0.5"/>
<vertex x="1.25" y="0.5" curve="133.798882"/>
<vertex x="-1.25" y="0.5"/>
</polygon>
<polygon width="0.1" layer="31">
<vertex x="-2.15" y="0.55"/>
<vertex x="-2.15" y="2.15"/>
<vertex x="2.15" y="2.15"/>
<vertex x="2.15" y="0.55"/>
<vertex x="1.3" y="0.55" curve="133.798882"/>
<vertex x="-1.3" y="0.55"/>
</polygon>
<smd name="1" x="0" y="1.75" dx="0.5" dy="0.5" layer="1" stop="no" cream="no"/>
<polygon width="0.2" layer="1">
<vertex x="2.15" y="-0.55"/>
<vertex x="2.15" y="-2.15"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="-0.55"/>
<vertex x="-1.3" y="-0.55" curve="133.798882"/>
<vertex x="1.3" y="-0.55"/>
</polygon>
<polygon width="0.3" layer="29">
<vertex x="2.2" y="-0.5"/>
<vertex x="2.2" y="-2.2"/>
<vertex x="-2.2" y="-2.2"/>
<vertex x="-2.2" y="-0.55"/>
<vertex x="-1.25" y="-0.55" curve="133.798882"/>
<vertex x="1.25" y="-0.55"/>
</polygon>
<polygon width="0.1" layer="31">
<vertex x="2.15" y="-0.55"/>
<vertex x="2.15" y="-2.15"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="-0.55"/>
<vertex x="-1.3" y="-0.55" curve="133.798882"/>
<vertex x="1.3" y="-0.55"/>
</polygon>
<smd name="2" x="0" y="-1.75" dx="0.5" dy="0.5" layer="1" rot="R180" stop="no" cream="no"/>
<text x="-2.336" y="2.734" size="0.4" layer="25">&gt;NAME</text>
<text x="-0.29" y="2.7455" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.2032" layer="21"/>
</package>
<package name="L1008">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.2" y1="1.2" x2="1.2" y2="1.2" width="0.0508" layer="39"/>
<wire x1="1.2" y1="1.2" x2="1.2" y2="-1.2" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-1.2" x2="-1.2" y2="-1.2" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-1.2" x2="-1.2" y2="1.2" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.8" dy="2.2" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.8" dy="2.2" layer="1"/>
<text x="-1.716" y="-1.007" size="0.33" layer="25" rot="R90">&gt;NAME</text>
<text x="-1.287" y="-1.037" size="0.33" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.165" y1="-1.05" x2="0.165" y2="1.05" layer="35"/>
<wire x1="-1.2" y1="1.2" x2="1.2" y2="1.2" width="0.04" layer="21"/>
<wire x1="1.2" y1="1.2" x2="1.2" y2="-1.2" width="0.04" layer="21"/>
<wire x1="1.2" y1="-1.2" x2="-1.2" y2="-1.2" width="0.04" layer="21"/>
<wire x1="-1.2" y1="-1.2" x2="-1.2" y2="1.2" width="0.04" layer="21"/>
</package>
<package name="MSS1278T-223__22UH">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="1" x="-4.25" y="0" dx="4" dy="5.5" layer="1"/>
<smd name="2" x="4.25" y="0" dx="4" dy="5.5" layer="1"/>
<text x="-5.87" y="3.137" size="0.4" layer="25">&gt;NAME</text>
<text x="-3.917" y="3.182" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-6.67" y1="3.075" x2="6.67" y2="3.075" width="0.05" layer="21"/>
<wire x1="6.67" y1="3.075" x2="6.67" y2="-3.075" width="0.05" layer="21"/>
<wire x1="6.67" y1="-3.075" x2="-6.67" y2="-3.075" width="0.05" layer="21"/>
<wire x1="-6.67" y1="-3.075" x2="-6.67" y2="3.075" width="0.05" layer="21"/>
<wire x1="-6.67" y1="3.075" x2="6.67" y2="3.075" width="0.05" layer="39"/>
<wire x1="6.67" y1="3.075" x2="6.67" y2="-3.075" width="0.05" layer="39"/>
<wire x1="6.67" y1="-3.075" x2="-6.67" y2="-3.075" width="0.05" layer="39"/>
<wire x1="-6.67" y1="-3.075" x2="-6.67" y2="3.075" width="0.05" layer="39"/>
<rectangle x1="-1.9" y1="-2.7" x2="1.9" y2="2.7" layer="35"/>
</package>
<package name="MSS1278T-103__10UH">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="1" x="-4.25" y="0" dx="4" dy="5.5" layer="1"/>
<smd name="2" x="4.25" y="0" dx="4" dy="5.5" layer="1"/>
<text x="-5.87" y="3.137" size="0.4" layer="25">&gt;NAME</text>
<text x="-3.917" y="3.182" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-6.67" y1="3.075" x2="6.67" y2="3.075" width="0.05" layer="21"/>
<wire x1="6.67" y1="3.075" x2="6.67" y2="-3.075" width="0.05" layer="21"/>
<wire x1="6.67" y1="-3.075" x2="-6.67" y2="-3.075" width="0.05" layer="21"/>
<wire x1="-6.67" y1="-3.075" x2="-6.67" y2="3.075" width="0.05" layer="21"/>
<wire x1="-6.67" y1="3.075" x2="6.67" y2="3.075" width="0.05" layer="39"/>
<wire x1="6.67" y1="3.075" x2="6.67" y2="-3.075" width="0.05" layer="39"/>
<wire x1="6.67" y1="-3.075" x2="-6.67" y2="-3.075" width="0.05" layer="39"/>
<wire x1="-6.67" y1="-3.075" x2="-6.67" y2="3.075" width="0.05" layer="39"/>
<rectangle x1="-1.9" y1="-2.7" x2="1.9" y2="2.7" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="VLS6045EX-6R8M__6P8UH">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="1" x="-3.05" y="0" dx="1.9" dy="5.1" layer="1"/>
<smd name="2" x="3.05" y="0" dx="1.9" dy="5.1" layer="1"/>
<text x="-4.05" y="2.987" size="0.4" layer="25">&gt;NAME</text>
<text x="-1.837" y="2.992" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-4.35" y1="2.8" x2="4.35" y2="2.8" width="0.05" layer="21"/>
<wire x1="4.35" y1="2.8" x2="4.35" y2="-2.8" width="0.05" layer="21"/>
<wire x1="4.35" y1="-2.8" x2="-4.35" y2="-2.8" width="0.05" layer="21"/>
<wire x1="-4.35" y1="-2.8" x2="-4.35" y2="2.8" width="0.05" layer="21"/>
<wire x1="-4.35" y1="2.8" x2="4.35" y2="2.8" width="0.05" layer="39"/>
<wire x1="4.35" y1="2.8" x2="4.35" y2="-2.8" width="0.05" layer="39"/>
<wire x1="4.35" y1="-2.8" x2="-4.35" y2="-2.8" width="0.05" layer="39"/>
<wire x1="-4.35" y1="-2.8" x2="-4.35" y2="2.8" width="0.05" layer="39"/>
<rectangle x1="-1.6" y1="-2.4" x2="1.6" y2="2.4" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="L-US">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
</symbol>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R-US_CURRENT_SENSE">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="0" y="5.08" size="1.27" layer="94" ratio="6">CS</text>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SRP7030-2R2M__2P2UH">
<gates>
<gate name="A" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SRP7030-2R2M__2P2UH">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MGV06051R0M-10__1P0UH">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MGV06051R0M-10__1UH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GRM21BR61E106KA73L__10UF">
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CL21B106KQQNNNE__10UF">
<gates>
<gate name="A" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0805">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CL31A226KPHNNNE__22UF">
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GRM31CR60J476ME19L__47UF">
<gates>
<gate name="A" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1206">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WSL2010R0100FEA__10MOHM_CS">
<gates>
<gate name="G$1" symbol="R-US_CURRENT_SENSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="744031220__22UH">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="744031220__22UH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="74479887310A__10UH">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="L1008">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MSS1278T-223__22UH">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MSS1278T-223__22UH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MSS1278T-103__10UH">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MSS1278T-103__10UH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESR03EZPF10R0__10OHM">
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VLS6045EX-6R8M__6P8UH">
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VLS6045EX-6R8M__6P8UH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LC_LIB5">
<packages>
<package name="DRZ_S-PDSO-N12_BQ27542-G1">
<description>&lt;b&gt;DRZ (S-PDSO-N12) PLASTIC SMALL OUTLINE&lt;/b&gt;&lt;p&gt;
Source: bq27500-v100.pdf</description>
<wire x1="2.025" y1="-1.25" x2="2.025" y2="1.25" width="0.1016" layer="51"/>
<wire x1="2.025" y1="1.25" x2="-2.025" y2="1.25" width="0.1016" layer="51"/>
<wire x1="-2.025" y1="1.25" x2="-2.025" y2="-1.25" width="0.1016" layer="51"/>
<wire x1="-2.025" y1="-1.25" x2="2.025" y2="-1.25" width="0.1016" layer="51"/>
<wire x1="-1.375" y1="1.25" x2="-2.025" y2="1.25" width="0.1016" layer="21"/>
<wire x1="2.025" y1="1.25" x2="1.375" y2="1.25" width="0.1016" layer="21"/>
<wire x1="1.375" y1="-1.25" x2="2.025" y2="-1.25" width="0.1016" layer="21"/>
<wire x1="-2.025" y1="-1.25" x2="-1.375" y2="-1.25" width="0.1016" layer="21"/>
<wire x1="0.825" y1="1.25" x2="-0.825" y2="1.25" width="0.1016" layer="21"/>
<wire x1="-0.825" y1="-1.25" x2="0.825" y2="-1.25" width="0.1016" layer="21"/>
<circle x="-1.65" y="1" radius="0.075" width="0" layer="31"/>
<circle x="-1.65" y="1" radius="0.125" width="0" layer="29"/>
<circle x="-1.65" y="0.6" radius="0.075" width="0" layer="31"/>
<circle x="-1.65" y="0.6" radius="0.125" width="0" layer="29"/>
<circle x="-1.65" y="0.2" radius="0.075" width="0" layer="31"/>
<circle x="-1.65" y="0.2" radius="0.125" width="0" layer="29"/>
<circle x="-1.65" y="-0.2" radius="0.075" width="0" layer="31"/>
<circle x="-1.65" y="-0.2" radius="0.125" width="0" layer="29"/>
<circle x="-1.65" y="-0.6" radius="0.075" width="0" layer="31"/>
<circle x="-1.65" y="-0.6" radius="0.125" width="0" layer="29"/>
<circle x="-1.65" y="-1" radius="0.075" width="0" layer="31"/>
<circle x="-1.65" y="-1" radius="0.125" width="0" layer="29"/>
<circle x="1.65" y="-1" radius="0.075" width="0" layer="31"/>
<circle x="1.65" y="-1" radius="0.125" width="0" layer="29"/>
<circle x="1.65" y="-0.6" radius="0.075" width="0" layer="31"/>
<circle x="1.65" y="-0.6" radius="0.125" width="0" layer="29"/>
<circle x="1.65" y="-0.2" radius="0.075" width="0" layer="31"/>
<circle x="1.65" y="-0.2" radius="0.125" width="0" layer="29"/>
<circle x="1.65" y="0.2" radius="0.075" width="0" layer="31"/>
<circle x="1.65" y="0.2" radius="0.125" width="0" layer="29"/>
<circle x="1.65" y="0.6" radius="0.075" width="0" layer="31"/>
<circle x="1.65" y="0.6" radius="0.125" width="0" layer="29"/>
<circle x="1.65" y="1" radius="0.075" width="0" layer="31"/>
<circle x="1.65" y="1" radius="0.125" width="0" layer="29"/>
<pad name="EXP@4" x="-0.9" y="0.5" drill="0.3" diameter="0.4064" stop="no"/>
<pad name="EXP@5" x="0" y="0.5" drill="0.3" diameter="0.4064" stop="no"/>
<pad name="EXP@6" x="0.9" y="0.5" drill="0.3" diameter="0.4064" stop="no"/>
<pad name="EXP@7" x="-0.9" y="-0.5" drill="0.3" diameter="0.4064" stop="no"/>
<pad name="EXP@8" x="0" y="-0.5" drill="0.3" diameter="0.4064" stop="no"/>
<pad name="EXP@9" x="0.9" y="-0.5" drill="0.3" diameter="0.4064" stop="no"/>
<smd name="1" x="-1.975" y="1" dx="0.85" dy="0.2" layer="1" roundness="100" stop="no" cream="no"/>
<smd name="2" x="-1.975" y="0.6" dx="0.85" dy="0.2" layer="1" roundness="100" stop="no" cream="no"/>
<smd name="3" x="-1.975" y="0.2" dx="0.85" dy="0.2" layer="1" roundness="100" stop="no" cream="no"/>
<smd name="4" x="-1.975" y="-0.2" dx="0.85" dy="0.2" layer="1" roundness="100" stop="no" cream="no"/>
<smd name="5" x="-1.975" y="-0.6" dx="0.85" dy="0.2" layer="1" roundness="100" stop="no" cream="no"/>
<smd name="6" x="-1.975" y="-1" dx="0.85" dy="0.2" layer="1" roundness="100" stop="no" cream="no"/>
<smd name="7" x="1.975" y="-1" dx="0.85" dy="0.2" layer="1" roundness="100" rot="R180" stop="no" cream="no"/>
<smd name="8" x="1.975" y="-0.6" dx="0.85" dy="0.2" layer="1" roundness="100" rot="R180" stop="no" cream="no"/>
<smd name="9" x="1.975" y="-0.2" dx="0.85" dy="0.2" layer="1" roundness="100" rot="R180" stop="no" cream="no"/>
<smd name="10" x="1.975" y="0.2" dx="0.85" dy="0.2" layer="1" roundness="100" rot="R180" stop="no" cream="no"/>
<smd name="11" x="1.975" y="0.6" dx="0.85" dy="0.2" layer="1" roundness="100" rot="R180" stop="no" cream="no"/>
<smd name="12" x="1.975" y="1" dx="0.85" dy="0.2" layer="1" roundness="100" rot="R180" stop="no" cream="no"/>
<smd name="PAD" x="0" y="0" dx="2.45" dy="1.95" layer="1" stop="no" cream="no"/>
<smd name="EXP@2" x="-1.1" y="0" dx="0.25" dy="3.3" layer="1" stop="no" cream="no"/>
<smd name="EXP@3" x="1.1" y="0" dx="0.25" dy="3.3" layer="1" stop="no" cream="no"/>
<text x="-2" y="1.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-3.2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.35" y1="0.875" x2="-1.65" y2="1.125" layer="29"/>
<rectangle x1="-2.325" y1="0.925" x2="-1.65" y2="1.075" layer="31"/>
<rectangle x1="-2.35" y1="0.475" x2="-1.65" y2="0.725" layer="29"/>
<rectangle x1="-2.325" y1="0.525" x2="-1.65" y2="0.675" layer="31"/>
<rectangle x1="-2.35" y1="0.075" x2="-1.65" y2="0.325" layer="29"/>
<rectangle x1="-2.325" y1="0.125" x2="-1.65" y2="0.275" layer="31"/>
<rectangle x1="-2.35" y1="-0.325" x2="-1.65" y2="-0.075" layer="29"/>
<rectangle x1="-2.325" y1="-0.275" x2="-1.65" y2="-0.125" layer="31"/>
<rectangle x1="-2.35" y1="-0.725" x2="-1.65" y2="-0.475" layer="29"/>
<rectangle x1="-2.325" y1="-0.675" x2="-1.65" y2="-0.525" layer="31"/>
<rectangle x1="-2.35" y1="-1.125" x2="-1.65" y2="-0.875" layer="29"/>
<rectangle x1="-2.325" y1="-1.075" x2="-1.65" y2="-0.925" layer="31"/>
<rectangle x1="1.65" y1="-1.125" x2="2.35" y2="-0.875" layer="29" rot="R180"/>
<rectangle x1="1.65" y1="-1.075" x2="2.325" y2="-0.925" layer="31" rot="R180"/>
<rectangle x1="1.65" y1="-0.725" x2="2.35" y2="-0.475" layer="29" rot="R180"/>
<rectangle x1="1.65" y1="-0.675" x2="2.325" y2="-0.525" layer="31" rot="R180"/>
<rectangle x1="1.65" y1="-0.325" x2="2.35" y2="-0.075" layer="29" rot="R180"/>
<rectangle x1="1.65" y1="-0.275" x2="2.325" y2="-0.125" layer="31" rot="R180"/>
<rectangle x1="1.65" y1="0.075" x2="2.35" y2="0.325" layer="29" rot="R180"/>
<rectangle x1="1.65" y1="0.125" x2="2.325" y2="0.275" layer="31" rot="R180"/>
<rectangle x1="1.65" y1="0.475" x2="2.35" y2="0.725" layer="29" rot="R180"/>
<rectangle x1="1.65" y1="0.525" x2="2.325" y2="0.675" layer="31" rot="R180"/>
<rectangle x1="1.65" y1="0.875" x2="2.35" y2="1.125" layer="29" rot="R180"/>
<rectangle x1="1.65" y1="0.925" x2="2.325" y2="1.075" layer="31" rot="R180"/>
<rectangle x1="-1.1" y1="0.1" x2="-0.1" y2="0.9" layer="31"/>
<rectangle x1="0.1" y1="0.1" x2="1.1" y2="0.9" layer="31"/>
<rectangle x1="-1.1" y1="-0.9" x2="-0.1" y2="-0.1" layer="31"/>
<rectangle x1="0.1" y1="-0.9" x2="1.1" y2="-0.1" layer="31"/>
<rectangle x1="-1.225" y1="1.1" x2="-0.975" y2="1.625" layer="31"/>
<rectangle x1="0.975" y1="1.1" x2="1.225" y2="1.625" layer="31"/>
<rectangle x1="-1.225" y1="-1.625" x2="-0.975" y2="-1.1" layer="31"/>
<rectangle x1="0.975" y1="-1.625" x2="1.225" y2="-1.1" layer="31"/>
<rectangle x1="-1.25" y1="-1.675" x2="-0.95" y2="1.675" layer="29"/>
<rectangle x1="0.95" y1="-1.675" x2="1.25" y2="1.675" layer="29"/>
<rectangle x1="-0.975" y1="-1" x2="0.975" y2="1" layer="29"/>
<rectangle x1="-2" y1="-1.2" x2="0" y2="0" layer="51"/>
<rectangle x1="-1.5" y1="-1.175" x2="-1.3" y2="-0.975" layer="21"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="1_175-103LAE-301_NTC_THERM">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="BQ27542-G1">
<pin name="VCC" x="-40.64" y="53.34" direction="pwr"/>
<pin name="SRN" x="15.24" y="2.54" rot="R180"/>
<pin name="TS" x="15.24" y="55.88" rot="R180"/>
<wire x1="-33.02" y1="58.42" x2="-33.02" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-33.02" y1="-10.16" x2="7.62" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="58.42" width="0.1524" layer="94"/>
<wire x1="7.62" y1="58.42" x2="-33.02" y2="58.42" width="0.1524" layer="94"/>
<text x="-30.1244" y="59.9186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-18.3642" y="21.8186" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="SE" x="-40.64" y="27.94"/>
<pin name="SDA" x="-40.64" y="5.08"/>
<pin name="SCL" x="-40.64" y="15.24"/>
<pin name="BAT" x="15.24" y="33.02" rot="R180"/>
<pin name="REGIN" x="15.24" y="38.1" rot="R180"/>
<pin name="HDQ" x="-40.64" y="-5.08"/>
<pin name="VSS" x="-20.32" y="-17.78" rot="R90"/>
<pin name="PAD_GND1" x="-5.08" y="-17.78" rot="R90"/>
<text x="-20.32" y="0" size="3.81" layer="94">GNDs</text>
<text x="-22.86" y="12.7" size="3.81" layer="94">I2C</text>
<pin name="SRP" x="15.24" y="10.16" rot="R180"/>
<pin name="REG25" x="-40.64" y="48.26"/>
<text x="-25.4" y="27.94" size="1.778" layer="94">Shutdown enable output</text>
</symbol>
<symbol name="1_R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="NTC_THERMISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-6.8" y1="-3.6" x2="-5" y2="-3.6" width="0.254" layer="94"/>
<wire x1="-5" y1="-3.6" x2="3.2" y2="2.1" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BQ27542-G1">
<gates>
<gate name="G$1" symbol="BQ27542-G1" x="12.7" y="-25.4"/>
</gates>
<devices>
<device name="" package="DRZ_S-PDSO-N12_BQ27542-G1">
<connects>
<connect gate="G$1" pin="BAT" pad="4"/>
<connect gate="G$1" pin="HDQ" pad="12"/>
<connect gate="G$1" pin="PAD_GND1" pad="EXP@2 EXP@3 EXP@4 EXP@5 EXP@6 EXP@7 EXP@8 EXP@9 PAD"/>
<connect gate="G$1" pin="REG25" pad="2"/>
<connect gate="G$1" pin="REGIN" pad="3"/>
<connect gate="G$1" pin="SCL" pad="11"/>
<connect gate="G$1" pin="SDA" pad="10"/>
<connect gate="G$1" pin="SE" pad="1"/>
<connect gate="G$1" pin="SRN" pad="8"/>
<connect gate="G$1" pin="SRP" pad="7"/>
<connect gate="G$1" pin="TS" pad="9"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="VSS" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRF2512-FX-R010ELF__10MOHM_CS">
<gates>
<gate name="G$1" symbol="1_R-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="175-103LAE-301_NTC_THERM_10K">
<gates>
<gate name="G$1" symbol="NTC_THERMISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1_175-103LAE-301_NTC_THERM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diodes_Transistors">
<packages>
<package name="SK310A-LTP__SCHOTTKY">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<smd name="ANODE" x="-2.032" y="0" dx="1.778" dy="2.159" layer="1"/>
<smd name="CATHODE" x="2.032" y="0" dx="1.778" dy="2.159" layer="1"/>
<text x="-3.056" y="1.369" size="0.4" layer="25">&gt;NAME</text>
<text x="-1.031" y="1.365" size="0.4" layer="27">&gt;VALUE</text>
<wire x1="-3.175" y1="1.27" x2="3.175" y2="1.27" width="0.05" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.175" y2="-1.27" width="0.05" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="-3.175" y2="-1.27" width="0.05" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.175" y2="1.27" width="0.05" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="3.175" y2="1.27" width="0.05" layer="39"/>
<wire x1="3.175" y1="1.27" x2="3.175" y2="-1.27" width="0.05" layer="39"/>
<wire x1="3.175" y1="-1.27" x2="-3.175" y2="-1.27" width="0.05" layer="39"/>
<wire x1="-3.175" y1="-1.27" x2="-3.175" y2="1.27" width="0.05" layer="39"/>
<rectangle x1="-0.889" y1="-1.016" x2="0.889" y2="1.016" layer="35"/>
<text x="-3.297" y="-1.192" size="0.5334" layer="21" rot="R90">ANODE</text>
</package>
<package name="POWERPAK1212-8">
<description>Vishay PowerPAK 1212-8 single device package&lt;br&gt;
High-power, low thermal resistance package.</description>
<smd name="S3" x="-1.4224" y="-0.3302" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="S2" x="-1.4224" y="0.3302" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="S1" x="-1.4224" y="0.9906" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="G" x="-1.4224" y="-0.9906" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="D2" x="1.5494" y="0.3302" dx="0.762" dy="0.4064" layer="1"/>
<smd name="D1" x="1.5494" y="0.9906" dx="0.762" dy="0.4064" layer="1"/>
<smd name="D3" x="1.5494" y="-0.3302" dx="0.762" dy="0.4064" layer="1"/>
<smd name="D4" x="1.5494" y="-0.9906" dx="0.762" dy="0.4064" layer="1"/>
<smd name="DPAD" x="0.5842" y="0" dx="1.7272" dy="2.2352" layer="1"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.5" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.35" width="0.127" layer="21"/>
<text x="-1.143" y="0.508" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-1.143" y="-0.381" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="CSD17483F4_N-CHAN_FET">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<smd name="GATE" x="-0.175" y="-0.37" dx="0.15" dy="0.35" layer="1"/>
<smd name="SOURCE" x="0.175" y="-0.37" dx="0.15" dy="0.35" layer="1"/>
<text x="-0.399" y="0.978" size="0.2" layer="25">&gt;NAME</text>
<text x="-0.408" y="0.748" size="0.2" layer="27">&gt;VALUE</text>
<smd name="DRAIN" x="0" y="0.325" dx="0.5" dy="0.25" layer="1"/>
<wire x1="-0.401" y1="0.646" x2="0.393" y2="0.644" width="0.127" layer="21"/>
<wire x1="0.393" y1="-0.722" x2="-0.338" y2="-0.722" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="ANODE" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="CATHODE" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
</symbol>
<symbol name="MOSFET_P">
<wire x1="-3.556" y1="-2.54" x2="-3.556" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.556" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0.508" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="-2.0066" y2="2.159" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="2.159" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.159" x2="1.27" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.159" width="0.1524" layer="94"/>
<circle x="0" y="2.159" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="-2.159" radius="0.127" width="0.4064" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.143" y="-4.318" size="0.8128" layer="93">D</text>
<text x="-1.143" y="3.556" size="0.8128" layer="93">S</text>
<text x="-4.826" y="1.143" size="0.8128" layer="93">G</text>
<rectangle x1="-2.794" y1="-2.794" x2="-2.032" y2="-1.27" layer="94"/>
<rectangle x1="-2.794" y1="1.27" x2="-2.032" y2="2.794" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="point" direction="pas"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="0" y="0"/>
<vertex x="-1.27" y="0.508"/>
<vertex x="-1.27" y="-0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.27" y="0.508"/>
<vertex x="0.762" y="-0.254"/>
<vertex x="1.778" y="-0.254"/>
</polygon>
</symbol>
<symbol name="MOSFET_N">
<wire x1="-3.556" y1="2.54" x2="-3.556" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.556" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0.508" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="-2.0066" y2="2.159" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="2.159" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.159" x2="1.27" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<circle x="0" y="2.159" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="-2.159" radius="0.127" width="0.4064" layer="94"/>
<text x="3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.143" y="3.556" size="0.8128" layer="93">D</text>
<text x="-1.143" y="-4.318" size="0.8128" layer="93">S</text>
<text x="-4.826" y="-1.778" size="0.8128" layer="93">G</text>
<rectangle x1="-2.794" y1="-2.794" x2="-2.032" y2="-1.27" layer="94"/>
<rectangle x1="-2.794" y1="1.27" x2="-2.032" y2="2.794" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="point" direction="pas"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="1.27" y="0.508"/>
<vertex x="0.762" y="-0.254"/>
<vertex x="1.778" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-1.905" y="0"/>
<vertex x="-0.635" y="-0.508"/>
<vertex x="-0.635" y="0.508"/>
</polygon>
</symbol>
<symbol name="NPN-DRIVER">
<wire x1="5.08" y1="2.54" x2="3.048" y2="1.524" width="0.1524" layer="94"/>
<wire x1="4.318" y1="-1.524" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="4.318" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="4.08" y1="-2.04" x2="2.848" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="4.064" y1="-2.413" x2="4.826" y2="-2.413" width="0.254" layer="94"/>
<wire x1="4.826" y1="-2.413" x2="4.318" y2="-1.778" width="0.254" layer="94"/>
<wire x1="4.318" y1="-1.778" x2="4.064" y2="-2.286" width="0.254" layer="94"/>
<wire x1="4.064" y1="-2.286" x2="4.445" y2="-2.286" width="0.254" layer="94"/>
<wire x1="4.445" y1="-2.286" x2="4.318" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.286" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="0.762" x2="-2.032" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.508" x2="-1.778" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="0.762" x2="-1.524" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-0.508" x2="-1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.762" x2="-1.016" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-0.508" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="0.254" y1="0" x2="2.286" y2="0" width="0.1524" layer="94"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0.254" y1="-0.762" x2="0.762" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.016" x2="-0.254" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.27" x2="0.762" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.524" x2="-0.254" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.778" x2="0.762" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-2.032" x2="-0.254" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-2.286" x2="0.254" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.254" y1="-2.54" x2="0.254" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="0.254" y1="-3.556" x2="5.08" y2="-3.556" width="0.1524" layer="94"/>
<circle x="0.254" y="0" radius="0.254" width="0.3048" layer="94"/>
<circle x="5.08" y="-3.556" radius="0.254" width="0.3048" layer="94"/>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="2.286" y1="-2.54" x2="3.048" y2="2.54" layer="94"/>
<pin name="B" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="5.08" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="5.08" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SK310A-LTP__SCHOTTKY">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SK310A-LTP__SCHOTTKY">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SI7617DN-T1-GE3__TRENCH-FET">
<gates>
<gate name="G$1" symbol="MOSFET_P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="POWERPAK1212-8">
<connects>
<connect gate="G$1" pin="D" pad="D1 D2 D3 D4 DPAD"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S1 S2 S3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SIS412DN-T1-GE3__TRENCH-FET">
<gates>
<gate name="G$1" symbol="MOSFET_N" x="0" y="0"/>
</gates>
<devices>
<device name="" package="POWERPAK1212-8">
<connects>
<connect gate="G$1" pin="D" pad="D1 D2 D3 D4 DPAD"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S1 S2 S3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CSD17483F4__N-CHAN_FET">
<gates>
<gate name="G$1" symbol="NPN-DRIVER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CSD17483F4_N-CHAN_FET">
<connects>
<connect gate="G$1" pin="B" pad="GATE"/>
<connect gate="G$1" pin="C" pad="DRAIN"/>
<connect gate="G$1" pin="E" pad="SOURCE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Board_Features_MISC">
<packages>
<package name="KXOB22-04X3L_SOLAR_CELL">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-4.613" y1="3.3" x2="4.719" y2="3.3" width="0.0508" layer="39"/>
<wire x1="4.72" y1="-3.3" x2="-4.614" y2="-3.3" width="0.0508" layer="39"/>
<wire x1="-4.614" y1="-3.3" x2="-4.613" y2="3.3" width="0.0508" layer="39"/>
<wire x1="4.719" y1="3.3" x2="4.72" y2="-3.3" width="0.0508" layer="39"/>
<wire x1="-4.875" y1="3.3" x2="4.875" y2="3.3" width="0.075" layer="21"/>
<wire x1="4.875" y1="3.3" x2="4.875" y2="-3.3" width="0.075" layer="21"/>
<wire x1="4.875" y1="-3.3" x2="-4.875" y2="-3.3" width="0.075" layer="21"/>
<wire x1="-4.875" y1="-3.3" x2="-4.875" y2="3.3" width="0.075" layer="21"/>
<smd name="ANODE" x="-3.1" y="0" dx="2.25" dy="6.2" layer="1"/>
<smd name="CATHODE" x="3.1" y="0" dx="2.25" dy="6.2" layer="1"/>
<text x="-4.086" y="3.42" size="0.254" layer="25" distance="5">&gt;NAME</text>
<text x="-2.779" y="3.442" size="0.254" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-2.95" x2="1.6" y2="2.95" layer="35"/>
<text x="-5.08" y="-2.54" size="1.27" layer="21" rot="R90">ANODE</text>
</package>
</packages>
<symbols>
<symbol name="SOLAR_CELL">
<wire x1="-1.905" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="-1.397" x2="-1.905" y2="-1.397" width="0.254" layer="94"/>
<wire x1="3.556" y1="1.016" x2="4.572" y2="2.032" width="0.1524" layer="94"/>
<wire x1="4.318" y1="0.254" x2="5.334" y2="1.27" width="0.1524" layer="94"/>
<text x="-3.81" y="-1.27" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="-3.81" y="1.27" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="3.556" y="1.524"/>
<vertex x="4.064" y="1.016"/>
<vertex x="3.302" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="4.318" y="0.762"/>
<vertex x="4.826" y="0.254"/>
<vertex x="4.064" y="0"/>
</polygon>
<text x="5.08" y="-2.54" size="1.27" layer="94">Solar
cell</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="KXOB22-04X3L_SOLAR_CELL">
<gates>
<gate name="G$1" symbol="SOLAR_CELL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="KXOB22-04X3L_SOLAR_CELL">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ICs_Power">
<packages>
<package name="VQFN_RHL_20">
<smd name="PAD_GND" x="0" y="0" dx="3.05" dy="2.05" layer="1"/>
<wire x1="-2.973140625" y1="-2.4499" x2="-2.973140625" y2="2.4499" width="0.1524" layer="25"/>
<wire x1="2.9706" y1="-2.4499" x2="2.9706" y2="2.4499" width="0.1524" layer="25"/>
<wire x1="-2.973140625" y1="2.4499" x2="2.9706" y2="2.4499" width="0.1524" layer="25"/>
<wire x1="-2.973140625" y1="-2.4499" x2="2.9706" y2="-2.4499" width="0.1524" layer="25"/>
<wire x1="-2.206778125" y1="-1.7155375" x2="-2.613178125" y2="-1.7155375" width="0.1524" layer="25" curve="-180"/>
<wire x1="-2.613178125" y1="-1.7155375" x2="-2.206778125" y2="-1.7155375" width="0.1524" layer="25" curve="-180"/>
<text x="-2.7535375" y="4.657078125" size="0.8" layer="25" ratio="6" rot="SR0">.Designator</text>
<text x="-2.7559" y="3.613978125" size="0.8" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<wire x1="-2.9685" y1="-2.445259375" x2="-2.6399" y2="-2.445259375" width="0.2032" layer="51"/>
<wire x1="-2.9685" y1="-2.445259375" x2="-2.9685" y2="-2.116659375" width="0.2032" layer="51"/>
<wire x1="2.963421875" y1="-2.44271875" x2="2.963421875" y2="-2.11411875" width="0.2032" layer="51"/>
<wire x1="2.634821875" y1="-2.44271875" x2="2.963421875" y2="-2.44271875" width="0.2032" layer="51"/>
<wire x1="2.637359375" y1="2.45288125" x2="2.965959375" y2="2.45288125" width="0.2032" layer="51"/>
<wire x1="2.965959375" y1="2.12428125" x2="2.965959375" y2="2.45288125" width="0.2032" layer="51"/>
<text x="-2.750540625" y="2.702340625" size="0.8" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
<smd name="17" x="-0.75" y="1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="16" x="-0.25" y="1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<wire x1="-2.9685" y1="2.455421875" x2="-2.9685" y2="2.126821875" width="0.2032" layer="51"/>
<wire x1="-2.5399" y1="2.455421875" x2="-2.9685" y2="2.455421875" width="0.2032" layer="51"/>
<smd name="12" x="1.75" y="1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="13" x="1.25" y="1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="14" x="0.75" y="1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="15" x="0.25" y="1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="7" x="0.75" y="-1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="4" x="-0.75" y="-1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="6" x="0.25" y="-1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="5" x="-0.25" y="-1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="3" x="-1.25" y="-1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="2" x="-1.75" y="-1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="10" x="2.25" y="-0.75" dx="0.95" dy="0.3" layer="1" roundness="99" rot="R180"/>
<smd name="11" x="2.25" y="0.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R180"/>
<smd name="1" x="-2.25" y="-0.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R180"/>
<smd name="20" x="-2.25" y="0.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R180"/>
<pad name="PAD_VIA3" x="-0.6731" y="-0.6731" drill="0.254"/>
<pad name="PAD_VIA1" x="-0.6731" y="0.6731" drill="0.254"/>
<pad name="PAD_VIA2" x="0.6731" y="0.6731" drill="0.254"/>
<pad name="PAD_VIA4" x="0.6731" y="-0.6731" drill="0.254"/>
<smd name="18" x="-1.25" y="1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="19" x="-1.75" y="1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="8" x="1.25" y="-1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="9" x="1.75" y="-1.75" dx="0.9" dy="0.28" layer="1" roundness="99" rot="R90"/>
<smd name="PAD_FIN_11" x="2.25" y="0.275" dx="0.9" dy="0.28" layer="1" rot="R180"/>
<smd name="PAD_FIN_10" x="2.25" y="-0.275" dx="0.9" dy="0.28" layer="1" rot="R180"/>
<smd name="PAD_FIN_20" x="-2.25" y="0.275" dx="0.9" dy="0.28" layer="1" rot="R180"/>
<smd name="PAD_FIN_1" x="-2.25" y="-0.275" dx="0.9" dy="0.28" layer="1" rot="R180"/>
<smd name="PAD_SMALL2" x="1.665" y="0" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="PAD_SMALL1" x="-1.665" y="0" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<text x="3.07848125" y="-0.98298125" size="0.35" layer="21">10</text>
<text x="-3.591559375" y="0.586740625" size="0.35" layer="21">20</text>
</package>
</packages>
<symbols>
<symbol name="BQ51013B">
<wire x1="-27.94" y1="55.88" x2="-27.94" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="-27.94" y1="-53.34" x2="27.94" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-53.34" x2="27.94" y2="55.88" width="0.1524" layer="94"/>
<wire x1="27.94" y1="55.88" x2="-27.94" y2="55.88" width="0.1524" layer="94"/>
<text x="-25.0444" y="57.3786" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-5.6642" y="-6.1214" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<text x="0" y="-35.56" size="3.81" layer="94">PGNDs</text>
<pin name="ILIM" x="-30.48" y="-38.1" length="short"/>
<pin name="OUT" x="30.48" y="50.8" length="short" direction="out" rot="R180"/>
<text x="10.16" y="48.26" size="3.81" layer="94">OUT</text>
<pin name="CLAMP1" x="-30.48" y="-27.94" length="short" direction="out"/>
<text x="2.54" y="5.08" size="5.08" layer="94" rot="R90">BQ51013B</text>
<pin name="RECT" x="30.48" y="30.48" length="short" direction="out" rot="R180"/>
<pin name="TS/CTRL" x="30.48" y="10.16" length="short" direction="in" rot="R180"/>
<pin name="CHG" x="30.48" y="-17.78" length="short" direction="out" rot="R180"/>
<pin name="EN2" x="30.48" y="-33.02" length="short" direction="in" rot="R180"/>
<pin name="EN1" x="30.48" y="-25.4" length="short" direction="in" rot="R180"/>
<text x="0" y="-17.78" size="1.27" layer="94">CHG, open-drain output</text>
<pin name="AD" x="-25.4" y="-55.88" length="short" direction="in" rot="R90"/>
<pin name="AD-EN" x="-17.78" y="-55.88" length="short" direction="out" rot="R90"/>
<text x="-25.4" y="-45.72" size="3.81" layer="94">AD</text>
<pin name="FOD" x="-10.16" y="-55.88" length="short" direction="in" rot="R90"/>
<pin name="CLAMP2" x="-30.48" y="-20.32" length="short" direction="out"/>
<pin name="COMM2" x="-30.48" y="-10.16" length="short" direction="out"/>
<pin name="BOOT2" x="-30.48" y="-2.54" length="short" direction="out"/>
<pin name="AC2" x="-30.48" y="7.62" length="short" direction="in"/>
<pin name="AC1" x="-30.48" y="27.94" length="short" direction="in"/>
<pin name="BOOT1" x="-30.48" y="38.1" length="short" direction="out"/>
<pin name="COMM1" x="-30.48" y="45.72" length="short" direction="out"/>
<pin name="PGND_PIN1" x="-2.54" y="-55.88" length="short" direction="pwr" rot="R90"/>
<pin name="PGND_PIN20" x="2.54" y="-55.88" length="short" direction="pwr" rot="R90"/>
<pin name="PAD_FIN_1" x="7.62" y="-55.88" length="short" direction="pwr" rot="R90"/>
<pin name="PAD_FIN_20" x="12.7" y="-55.88" length="short" direction="pwr" rot="R90"/>
<pin name="PAD_FIN_10" x="17.78" y="-55.88" length="short" direction="pwr" rot="R90"/>
<pin name="PAD_FIN_11" x="22.86" y="-55.88" length="short" direction="pwr" rot="R90"/>
<text x="-15.24" y="7.62" size="5.08" layer="94" rot="R90">AC IN</text>
<text x="-20.32" y="-38.1" size="1.27" layer="94">ILIM</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BQ51013B">
<description>Wireless Power Receiver, VQFN</description>
<gates>
<gate name="G$1" symbol="BQ51013B" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VQFN_RHL_20">
<connects>
<connect gate="G$1" pin="AC1" pad="2"/>
<connect gate="G$1" pin="AC2" pad="19"/>
<connect gate="G$1" pin="AD" pad="9"/>
<connect gate="G$1" pin="AD-EN" pad="8"/>
<connect gate="G$1" pin="BOOT1" pad="3"/>
<connect gate="G$1" pin="BOOT2" pad="17"/>
<connect gate="G$1" pin="CHG" pad="7"/>
<connect gate="G$1" pin="CLAMP1" pad="5"/>
<connect gate="G$1" pin="CLAMP2" pad="16"/>
<connect gate="G$1" pin="COMM1" pad="6"/>
<connect gate="G$1" pin="COMM2" pad="15"/>
<connect gate="G$1" pin="EN1" pad="10"/>
<connect gate="G$1" pin="EN2" pad="11"/>
<connect gate="G$1" pin="FOD" pad="14"/>
<connect gate="G$1" pin="ILIM" pad="12"/>
<connect gate="G$1" pin="OUT" pad="4"/>
<connect gate="G$1" pin="PAD_FIN_1" pad="PAD_FIN_1 PAD_SMALL1 PAD_VIA3"/>
<connect gate="G$1" pin="PAD_FIN_10" pad="PAD_FIN_10 PAD_VIA4"/>
<connect gate="G$1" pin="PAD_FIN_11" pad="PAD_FIN_11 PAD_SMALL2 PAD_VIA2"/>
<connect gate="G$1" pin="PAD_FIN_20" pad="PAD_FIN_20 PAD_GND PAD_VIA1"/>
<connect gate="G$1" pin="PGND_PIN1" pad="1"/>
<connect gate="G$1" pin="PGND_PIN20" pad="20"/>
<connect gate="G$1" pin="RECT" pad="18"/>
<connect gate="G$1" pin="TS/CTRL" pad="13"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="BAT1" library="LC3" deviceset="BATTERY" device=""/>
<part name="U2" library="ICs" deviceset="BQ25570RGRR" device="" value="Value"/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY3" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY4" library="supply2" deviceset="GND" device=""/>
<part name="U$8" library="Resistors_Caps_Inductors_Misc" deviceset="SRP7030-2R2M__2P2UH" device=""/>
<part name="U$9" library="Resistors_Caps_Inductors_Misc" deviceset="MGV06051R0M-10__1P0UH" device=""/>
<part name="U$1" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="U$10" library="Resistors_Caps_Inductors_Misc" deviceset="CL21B106KQQNNNE__10UF" device=""/>
<part name="U$5" library="Resistors_Caps_Inductors_Misc" deviceset="CL31A226KPHNNNE__22UF" device=""/>
<part name="U$6" library="Resistors_Caps_Inductors_Misc" deviceset="GRM31CR60J476ME19L__47UF" device=""/>
<part name="C13" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="C1" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C2" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY8" library="supply2" deviceset="GND" device=""/>
<part name="R1" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R2" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R3" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R4" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R5" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R6" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R7" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="SUPPLY10" library="supply2" deviceset="GND" device=""/>
<part name="U4" library="ICs" deviceset="BQ78350" device=""/>
<part name="SUPPLY6" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY11" library="supply2" deviceset="GND" device=""/>
<part name="U20" library="ICs" deviceset="TPS55332-Q1" device=""/>
<part name="SUPPLY12" library="supply2" deviceset="GND" device=""/>
<part name="R8" library="resistor" deviceset="R-US_" device="R0402" value="200K"/>
<part name="SUPPLY13" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY14" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY15" library="supply2" deviceset="GND" device=""/>
<part name="U$23" library="Resistors_Caps_Inductors_Misc" deviceset="GRM31CR60J476ME19L__47UF" device=""/>
<part name="R9" library="resistor" deviceset="R-US_" device="R0402" value="4.99k"/>
<part name="SUPPLY16" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY17" library="supply2" deviceset="GND" device=""/>
<part name="R10" library="resistor" deviceset="R-US_" device="R0402" value="8.66k"/>
<part name="SUPPLY18" library="supply2" deviceset="GND" device=""/>
<part name="R11" library="resistor" deviceset="R-US_" device="R0402" value="267k"/>
<part name="R12" library="resistor" deviceset="R-US_" device="R0402" value="11.3k"/>
<part name="SUPPLY19" library="supply2" deviceset="GND" device=""/>
<part name="R13" library="resistor" deviceset="R-US_" device="R0402" value="100k"/>
<part name="SUPPLY20" library="supply2" deviceset="GND" device=""/>
<part name="C3" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C4" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY21" library="supply2" deviceset="GND" device=""/>
<part name="R38" library="Resistors_Caps_Inductors_Misc" deviceset="WSL2010R0100FEA__10MOHM_CS" device=""/>
<part name="C14" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="C5" library="resistor" deviceset="C-EU" device="C0402" value="10n"/>
<part name="C6" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C8" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C11" library="Resistors_Caps_Inductors_Misc" deviceset="GRM31CR60J476ME19L__47UF" device=""/>
<part name="C12" library="Resistors_Caps_Inductors_Misc" deviceset="CL31A226KPHNNNE__22UF" device=""/>
<part name="C9" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C10" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY5" library="supply2" deviceset="GND" device=""/>
<part name="U6" library="LC_LIB5" deviceset="BQ27542-G1" device=""/>
<part name="R43" library="LC_LIB5" deviceset="CRF2512-FX-R010ELF__10MOHM_CS" device=""/>
<part name="SUPPLY22" library="supply2" deviceset="GND" device=""/>
<part name="R41" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R44" library="resistor" deviceset="R-US_" device="R0402" value="20K"/>
<part name="C17" library="LC3" deviceset="CL21B106KQQNNNE" device=""/>
<part name="SUPPLY44" library="supply2" deviceset="GND" device=""/>
<part name="R21" library="LC_LIB5" deviceset="175-103LAE-301_NTC_THERM_10K" device=""/>
<part name="C15" library="resistor" deviceset="C-EU" device="C0402" value="0.47u"/>
<part name="SUPPLY45" library="supply2" deviceset="GND" device=""/>
<part name="L4" library="Resistors_Caps_Inductors_Misc" deviceset="744031220__22UH" device=""/>
<part name="L3" library="Resistors_Caps_Inductors_Misc" deviceset="74479887310A__10UH" device=""/>
<part name="L2" library="Resistors_Caps_Inductors_Misc" deviceset="MSS1278T-223__22UH" device=""/>
<part name="U$12" library="Diodes_Transistors" deviceset="SK310A-LTP__SCHOTTKY" device=""/>
<part name="U$13" library="Board_Features_MISC" deviceset="KXOB22-04X3L_SOLAR_CELL" device=""/>
<part name="SUPPLY23" library="supply2" deviceset="GND" device=""/>
<part name="C20" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C21" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="R14" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R15" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R16" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R18" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R19" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R20" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="SUPPLY26" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY29" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY30" library="supply2" deviceset="GND" device=""/>
<part name="C22" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C23" library="resistor" deviceset="C-EU" device="C0402" value="100n"/>
<part name="C24" library="resistor" deviceset="C-EU" device="C0402" value="20n"/>
<part name="C25" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY31" library="supply2" deviceset="GND" device=""/>
<part name="C26" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C28" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY32" library="supply2" deviceset="GND" device=""/>
<part name="C29" library="resistor" deviceset="C-EU" device="C0402" value="56n"/>
<part name="SUPPLY33" library="supply2" deviceset="GND" device=""/>
<part name="U3" library="ICs" deviceset="BQ24616" device=""/>
<part name="C31" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY35" library="supply2" deviceset="GND" device=""/>
<part name="R17" library="resistor" deviceset="R-US_" device="R0402" value="1K"/>
<part name="R22" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="C32" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C33" library="resistor" deviceset="C-EU" device="C0402" value="100n"/>
<part name="C34" library="resistor" deviceset="C-EU" device="C0402" value="100n"/>
<part name="SUPPLY24" library="supply2" deviceset="GND" device=""/>
<part name="C18" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C19" library="resistor" deviceset="C-EU" device="C0402" value="100n"/>
<part name="C16" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="L1" library="Resistors_Caps_Inductors_Misc" deviceset="MSS1278T-103__10UH" device=""/>
<part name="C30" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY37" library="supply2" deviceset="GND" device=""/>
<part name="C35" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="U$27" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="U$29" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="C36" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY38" library="supply2" deviceset="GND" device=""/>
<part name="R23" library="resistor" deviceset="R-US_" device="R0402" value="500K"/>
<part name="C37" library="resistor" deviceset="C-EU" device="C0402" value="25p"/>
<part name="R24" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="SUPPLY39" library="supply2" deviceset="GND" device=""/>
<part name="R45" library="Resistors_Caps_Inductors_Misc" deviceset="WSL2010R0100FEA__10MOHM_CS" device=""/>
<part name="Q2" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="Q1" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="R27" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="R28" library="resistor" deviceset="R-US_" device="R0402" value="100K"/>
<part name="C38" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="C40" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C41" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY41" library="supply2" deviceset="GND" device=""/>
<part name="R30" library="Resistors_Caps_Inductors_Misc" deviceset="ESR03EZPF10R0__10OHM" device=""/>
<part name="Q3" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="Q4" library="Diodes_Transistors" deviceset="SIS412DN-T1-GE3__TRENCH-FET" device=""/>
<part name="Q5" library="Diodes_Transistors" deviceset="SIS412DN-T1-GE3__TRENCH-FET" device=""/>
<part name="SUPPLY42" library="supply2" deviceset="GND" device=""/>
<part name="U$3" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="U$7" library="Resistors_Caps_Inductors_Misc" deviceset="GRM21BR61E106KA73L__10UF" device=""/>
<part name="C39" library="resistor" deviceset="C-EU" device="C0402" value="0.1u"/>
<part name="SUPPLY40" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY43" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY46" library="supply2" deviceset="GND" device=""/>
<part name="L5" library="Resistors_Caps_Inductors_Misc" deviceset="VLS6045EX-6R8M__6P8UH" device=""/>
<part name="R25" library="Resistors_Caps_Inductors_Misc" deviceset="WSL2010R0100FEA__10MOHM_CS" device=""/>
<part name="C7" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R26" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="SUPPLY7" library="supply2" deviceset="GND" device=""/>
<part name="C42" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R37" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="C43" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R35" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="R29" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="U$11" library="Diodes_Transistors" deviceset="CSD17483F4__N-CHAN_FET" device=""/>
<part name="SUPPLY27" library="supply2" deviceset="GND" device=""/>
<part name="Q8" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="Q9" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="U$15" library="Diodes_Transistors" deviceset="SK310A-LTP__SCHOTTKY" device=""/>
<part name="U$17" library="Diodes_Transistors" deviceset="SK310A-LTP__SCHOTTKY" device=""/>
<part name="C44" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R46" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="C45" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R50" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="C46" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="R53" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="U$24" library="Diodes_Transistors" deviceset="CSD17483F4__N-CHAN_FET" device=""/>
<part name="U$25" library="Diodes_Transistors" deviceset="CSD17483F4__N-CHAN_FET" device=""/>
<part name="U$26" library="Diodes_Transistors" deviceset="CSD17483F4__N-CHAN_FET" device=""/>
<part name="U$28" library="Diodes_Transistors" deviceset="CSD17483F4__N-CHAN_FET" device=""/>
<part name="R56" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="R57" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="R58" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="R59" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="R60" library="resistor" deviceset="R-US_" device="R0402" value="10K"/>
<part name="SUPPLY28" library="supply2" deviceset="GND" device=""/>
<part name="Q13" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="SUPPLY47" library="supply2" deviceset="GND" device=""/>
<part name="R62" library="resistor" deviceset="R-US_" device="R0402" value="1K"/>
<part name="R63" library="resistor" deviceset="R-US_" device="R0402" value="1M"/>
<part name="Q14" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="Q15" library="Diodes_Transistors" deviceset="SI7617DN-T1-GE3__TRENCH-FET" device=""/>
<part name="R64" library="resistor" deviceset="R-US_" device="R0402" value="1M"/>
<part name="C47" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY48" library="supply2" deviceset="GND" device=""/>
<part name="C48" library="resistor" deviceset="C-EU" device="C0402" value="4.7u"/>
<part name="SUPPLY49" library="supply2" deviceset="GND" device=""/>
<part name="U$2" library="ICs" deviceset="BQ76920" device=""/>
<part name="C49" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY50" library="supply2" deviceset="GND" device=""/>
<part name="C50" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY51" library="supply2" deviceset="GND" device=""/>
<part name="C51" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY52" library="supply2" deviceset="GND" device=""/>
<part name="R36" library="resistor" deviceset="R-US_" device="R0402" value="1.1K"/>
<part name="R39" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="R40" library="resistor" deviceset="R-US_" device="R0402" value="100"/>
<part name="R47" library="LC_LIB5" deviceset="175-103LAE-301_NTC_THERM_10K" device=""/>
<part name="C52" library="resistor" deviceset="C-EU" device="C0402" value="2.2u"/>
<part name="SUPPLY53" library="supply2" deviceset="GND" device=""/>
<part name="U$16" library="ICs_Power" deviceset="BQ51013B" device=""/>
<part name="SUPPLY9" library="supply2" deviceset="GND" device=""/>
<part name="R31" library="resistor" deviceset="R-US_" device="R0402" value="200K"/>
<part name="C27" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C53" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C54" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C55" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C56" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C57" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="C58" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="U$18" library="Resistors_Caps_Inductors_Misc" deviceset="SRP7030-2R2M__2P2UH" device=""/>
<part name="C59" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY34" library="supply2" deviceset="GND" device=""/>
<part name="R32" library="resistor" deviceset="R-US_" device="R0402" value="200K"/>
<part name="SUPPLY36" library="supply2" deviceset="GND" device=""/>
<part name="R33" library="LC_LIB5" deviceset="175-103LAE-301_NTC_THERM_10K" device=""/>
<part name="SUPPLY25" library="supply2" deviceset="GND" device=""/>
<part name="R34" library="resistor" deviceset="R-US_" device="R0402" value="200K"/>
<part name="C60" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY54" library="supply2" deviceset="GND" device=""/>
<part name="C61" library="resistor" deviceset="C-EU" device="C0402" value="1u"/>
<part name="SUPPLY55" library="supply2" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-327.66" y="-68.58" size="10.16" layer="91" rot="R90">BATTERY</text>
<text x="-355.6" y="2.54" size="1.778" layer="91">2 milliOhm CS resistor,
HIGH side
INA250, 
CS resistor measurements</text>
<text x="-355.6" y="-83.82" size="1.778" layer="91">10 milliOhm CS resistor,
LOW side
BQ27542-G1, 
Battery Fuel Gauge</text>
<text x="-477.52" y="236.22" size="1.778" layer="91">CL21B106KQQNNNE // 10 uF
Rated @ 6.3V, 0805</text>
<text x="-477.52" y="269.24" size="1.778" layer="91">CL31A226KPHNNNE // 22 uF
Rated @ 10V, 1206</text>
<text x="-477.52" y="254" size="1.778" layer="91">GRM31CR60J476ME19L // 47 uF
Rated @ 6.3V, 1206</text>
<text x="-1089.66" y="378.46" size="1.778" layer="91">22 uH</text>
<text x="-1056.64" y="325.12" size="5.08" layer="91" font="fixed">BQ25570</text>
<text x="-477.52" y="220.98" size="1.778" layer="91">GRM21BR61E106KA73L // 10 uF
Rated @ 25V, 0805, X5R</text>
<text x="-1120.14" y="401.32" size="1.778" layer="91">GRM21BR61E106KA73L // 10 uF
Rated @ 25V, 0805, X5R</text>
<text x="-1018.54" y="269.24" size="1.778" layer="91" font="fixed">CHECK
all resistor values here!

The sum of each "branch" 
should be 13 MOhms.</text>
<text x="-1094.74" y="416.56" size="5.08" layer="91" font="fixed">Check all R's, C's, L's!!</text>
<text x="-459.74" y="289.56" size="10.16" layer="91" font="fixed">Resistors
Capacitors
Inductors</text>
<text x="-1122.68" y="177.8" size="30.48" layer="91">BQ25570</text>
<text x="236.22" y="-535.94" size="30.48" layer="91">BQ24616</text>
<text x="-1584.96" y="228.6" size="30.48" layer="91">BQ78350</text>
<text x="-1798.32" y="149.86" size="30.48" layer="91">BQ76920</text>
<text x="233.68" y="-688.34" size="7.62" layer="91">Minimum pre-charge &amp; termination 
current is 125 mA with 10 mOhm sense
resistor... Can achieve lower charging currents
with different sense resistor values?
section 8.3.5 in datasheet

CE pin can be controled from MCU to 
enable/disable charging.</text>
<text x="-965.2" y="342.9" size="1.778" layer="91">Capacity of single-cell battery?  2600 mAh?</text>
<text x="-96.52" y="-505.46" size="30.48" layer="91">TPS55332-Q1 </text>
<text x="116.84" y="-340.36" size="1.778" layer="91">BQ24616 -- 24 V input to charge 5 cells</text>
<text x="-1356.36" y="149.86" size="7.62" layer="91">BAT_OK: 3.2 V (when battery discharges)
BAT_OK_HYST: 3.9V (when battery charges)
V_BAT_OV: 4.2V
V_BAT_UV: 2.6V
V_OUT_SET: 3.3V</text>
<text x="-1353.82" y="236.22" size="7.62" layer="91">VBAT voltage is input to TPS55332-G1
VOUT voltage is input to microcontroller 
and other ICs.</text>
<text x="284.48" y="-238.76" size="1.778" layer="91">2 FETS: Si4401 (Si4401BDYFDS4141)
CS resistor @ ACP/ACP &amp; VBAT load: WSL2010R0100FEA, 10 mOhms
Zener: BZX84C7V5, 7.5V</text>
<text x="289.56" y="-269.24" size="1.778" layer="91">10 mOhm Current-sense resistor, 0.5 W</text>
<text x="-1135.38" y="360.68" size="1.778" layer="91">GRM21BR61E106KA73L // 10 uF
Rated @ 25V, 0805, X5R</text>
<text x="-980.44" y="375.92" size="1.778" layer="91">Recommended LBUCK inductor:
Manufacturer: Wuerth
Man. Part No.: 74479889310
L: 10 uH</text>
<text x="-1150.62" y="381" size="1.778" layer="91">Recommended LBOOST inductor:
Manufacturer: Wuerth
Man. Part No.: 744031220
L: 22 uH</text>
<text x="-998.22" y="378.46" size="1.778" layer="91">10 uH</text>
<text x="-1219.2" y="360.68" size="1.778" layer="91">Surface mount solar celll:
Manufacturer: IXYS
Man. Part No.: KXOB22-04X3L</text>
<text x="-734.06" y="269.24" size="1.778" layer="91">Using I2C communication, not HDQ.
Leave HDQ pin floating (UNUSED).</text>
<text x="-741.68" y="314.96" size="1.778" layer="91">SE unused.  Leave floating.</text>
<text x="-777.24" y="327.66" size="1.778" layer="91">CL21B106KQQNNNE // 10 uF
Rated @ 6.3V, 0805</text>
<text x="-784.86" y="205.74" size="30.48" layer="91">BQ27542-G1</text>
<text x="-777.24" y="345.44" size="1.778" layer="91">Vcc voltage can't be higher than ~2.7V (from datasheet).  
CHECK.</text>
<text x="-650.24" y="266.7" size="1.778" layer="91">CHECK.
Not sure where the other end
of this CS resistor should be wired.</text>
<text x="-647.7" y="256.54" size="1.778" layer="91">Datasheet says cell needs to be protected.
But is "protection" already built into the cell?
CHECK.</text>
<text x="71.12" y="-312.42" size="1.778" layer="91">Schottky diode:
Man.: Micro Commercial Co.
Man. Part No.: SK310A-LTP
100V, 3A</text>
<text x="17.78" y="-309.88" size="1.778" layer="91">Inductor: 22 uH
Man.: Coilcraft
Man. Part No.: MSS1278T-223MLB</text>
<text x="27.94" y="-314.96" size="1.778" layer="91">22 uH</text>
<text x="96.52" y="-370.84" size="1.778" layer="91">^ 22 uF (electrolytic)
Man. Part No.: 
EEE-1HA220WP</text>
<text x="-88.9" y="-309.88" size="1.778" layer="91" rot="R90">Schottky diode for reverse protection 
on INPUT back to BQ25570
Man. Part No.: PMEG3030EP,115</text>
<text x="-50.8" y="-393.7" size="1.778" layer="91">SYNC (input) has interal ~62 kOhm pull-down.</text>
<text x="-30.48" y="-414.02" size="1.778" layer="91">RST (outut) -- open drain, 
sometimes pulled up to VREG pin.</text>
<text x="-40.64" y="-337.82" size="1.778" layer="91">100 uF here -&gt;
Man. part no.: 
EEE-FK1H101P
</text>
<text x="-83.82" y="-269.24" size="1.778" layer="91" rot="R270">&lt;- PMEG3050EP,115 (schottky diode) here</text>
<text x="414.02" y="-342.9" size="1.778" layer="91">10 mOhm Current-sense resistor, 0.5 W</text>
<text x="401.32" y="-360.68" size="1.778" layer="91">6.8 uH 
inductor
needed</text>
<text x="340.36" y="231.14" size="1.778" layer="91">Man. Part No.: EEE-FK1H101P
Man. Part No.: EEE-1HA220WP
Man. Part No.: EEE-1HA100SP</text>
<text x="149.86" y="327.66" size="50.8" layer="91">Footprints 
needed:</text>
<text x="200.66" y="-307.34" size="1.778" layer="91">^ 22 uF (electrolytic)
Man. Part No.: 
EEE-1HA220WP</text>
<text x="246.38" y="-246.38" size="1.778" layer="91">10 Ohms
1/4 W
R1206 package</text>
<text x="370.84" y="-309.88" size="1.778" layer="91">check 10 uF output capacitors!</text>
<text x="449.58" y="-370.84" size="1.778" layer="91">check 10 uF output capacitors!</text>
<text x="-1767.84" y="129.54" size="10.16" layer="91">5 Li-ion cells</text>
<text x="-1115.06" y="109.22" size="10.16" layer="91">Energy Harvesting Chip
1.  Solar cell
2.  Thermoeletric
3.  Piezoelectric</text>
<text x="-35.56" y="-525.78" size="10.16" layer="91">Boost Converter</text>
<text x="256.54" y="-581.66" size="10.16" layer="91">Multi Li-ion
cell charger</text>
<text x="-1737.36" y="396.24" size="1.778" layer="91">Pin 11
Not 
Connected</text>
<text x="-1684.02" y="391.16" size="1.778" layer="91">Pick 2.2uF cap!</text>
<text x="-1691.64" y="355.6" size="1.778" layer="91">Pick 4.7uF cap!</text>
<text x="-1691.64" y="337.82" size="1.778" layer="91">Pick 1uF cap!</text>
<text x="-314.96" y="-368.3" size="7.62" layer="91" rot="R90">Replace inductor with receiving coil!!!</text>
<text x="-330.2" y="-502.92" size="30.48" layer="91">BQ51013B</text>
<text x="-327.66" y="-528.32" size="10.16" layer="91">Wireless Power Receiver</text>
</plain>
<instances>
<instance part="BAT1" gate="G$1" x="-363.22" y="-45.72"/>
<instance part="U2" gate="A" x="-1038.86" y="335.28"/>
<instance part="SUPPLY1" gate="GND" x="-1137.92" y="355.6"/>
<instance part="SUPPLY2" gate="GND" x="-1076.96" y="347.98"/>
<instance part="SUPPLY3" gate="GND" x="-1003.3" y="294.64"/>
<instance part="SUPPLY4" gate="GND" x="-998.22" y="330.2"/>
<instance part="U$8" gate="A" x="-391.16" y="223.52" rot="R90"/>
<instance part="U$9" gate="G$1" x="-391.16" y="243.84" rot="R90"/>
<instance part="U$1" gate="G$1" x="-439.42" y="220.98" rot="MR0"/>
<instance part="U$10" gate="A" x="-439.42" y="236.22" rot="MR0"/>
<instance part="U$5" gate="G$1" x="-439.42" y="271.78"/>
<instance part="U$6" gate="A" x="-439.42" y="256.54"/>
<instance part="C13" gate="G$1" x="-1076.96" y="398.78" rot="MR0"/>
<instance part="C1" gate="G$1" x="-1069.34" y="398.78"/>
<instance part="C2" gate="G$1" x="-1059.18" y="398.78"/>
<instance part="SUPPLY8" gate="GND" x="-1069.34" y="383.54"/>
<instance part="R1" gate="G$1" x="-1054.1" y="276.86" rot="R270"/>
<instance part="R2" gate="G$1" x="-1054.1" y="261.62" rot="R270"/>
<instance part="R3" gate="G$1" x="-1033.78" y="276.86" rot="R270"/>
<instance part="R4" gate="G$1" x="-1033.78" y="261.62" rot="R270"/>
<instance part="R5" gate="G$1" x="-1033.78" y="246.38" rot="R270"/>
<instance part="R6" gate="G$1" x="-1026.16" y="276.86" rot="R270"/>
<instance part="R7" gate="G$1" x="-1026.16" y="261.62" rot="R270"/>
<instance part="SUPPLY10" gate="GND" x="-1033.78" y="233.68"/>
<instance part="U4" gate="G$1" x="-1518.92" y="381"/>
<instance part="SUPPLY6" gate="GND" x="281.94" y="-454.66" rot="MR0"/>
<instance part="SUPPLY11" gate="GND" x="299.72" y="-454.66" rot="MR0"/>
<instance part="U20" gate="A" x="30.48" y="-398.78"/>
<instance part="SUPPLY12" gate="GND" x="25.4" y="-457.2"/>
<instance part="R8" gate="G$1" x="-7.62" y="-363.22" rot="R270"/>
<instance part="SUPPLY13" gate="GND" x="-7.62" y="-373.38"/>
<instance part="SUPPLY14" gate="GND" x="0" y="-391.16"/>
<instance part="SUPPLY15" gate="GND" x="0" y="-431.8"/>
<instance part="U$23" gate="A" x="0" y="-421.64" rot="MR0"/>
<instance part="R9" gate="G$1" x="60.96" y="-444.5" rot="R270"/>
<instance part="SUPPLY16" gate="GND" x="60.96" y="-454.66"/>
<instance part="SUPPLY17" gate="GND" x="66.04" y="-434.34"/>
<instance part="R10" gate="G$1" x="83.82" y="-411.48" rot="R270"/>
<instance part="SUPPLY18" gate="GND" x="83.82" y="-421.64"/>
<instance part="R11" gate="G$1" x="83.82" y="-393.7" rot="R270"/>
<instance part="R12" gate="G$1" x="73.66" y="-378.46" rot="R270"/>
<instance part="SUPPLY19" gate="GND" x="73.66" y="-388.62"/>
<instance part="R13" gate="G$1" x="73.66" y="-360.68" rot="R270"/>
<instance part="SUPPLY20" gate="GND" x="91.44" y="-365.76"/>
<instance part="C3" gate="G$1" x="297.18" y="-289.56" rot="R90"/>
<instance part="C4" gate="G$1" x="287.02" y="-297.18" rot="R180"/>
<instance part="SUPPLY21" gate="GND" x="287.02" y="-304.8" rot="MR0"/>
<instance part="R38" gate="G$1" x="299.72" y="-276.86"/>
<instance part="C14" gate="G$1" x="-1137.92" y="368.3" rot="MR0"/>
<instance part="C5" gate="G$1" x="-1076.96" y="358.14"/>
<instance part="C6" gate="G$1" x="-988.06" y="342.9"/>
<instance part="C8" gate="G$1" x="-977.9" y="342.9"/>
<instance part="C11" gate="A" x="-998.22" y="342.9" rot="MR0"/>
<instance part="C12" gate="G$1" x="-982.98" y="363.22" rot="MR0"/>
<instance part="C9" gate="G$1" x="-972.82" y="363.22"/>
<instance part="C10" gate="G$1" x="-962.66" y="363.22"/>
<instance part="SUPPLY5" gate="GND" x="-982.98" y="350.52"/>
<instance part="U6" gate="G$1" x="-668.02" y="287.02"/>
<instance part="R43" gate="G$1" x="-642.62" y="279.4"/>
<instance part="SUPPLY22" gate="GND" x="-680.72" y="259.08"/>
<instance part="R41" gate="G$1" x="-731.52" y="340.36"/>
<instance part="R44" gate="G$1" x="-716.28" y="332.74" rot="R90"/>
<instance part="C17" gate="G$1" x="-723.9" y="332.74" rot="MR0"/>
<instance part="SUPPLY44" gate="GND" x="-721.36" y="320.04"/>
<instance part="R21" gate="G$1" x="-678.18" y="355.6"/>
<instance part="C15" gate="G$1" x="-678.18" y="365.76" rot="R90"/>
<instance part="SUPPLY45" gate="GND" x="-652.78" y="256.54"/>
<instance part="L4" gate="G$1" x="-1087.12" y="373.38" rot="R90"/>
<instance part="L3" gate="G$1" x="-993.14" y="373.38" rot="R90"/>
<instance part="L2" gate="G$1" x="30.48" y="-320.04" rot="R90"/>
<instance part="U$12" gate="G$1" x="73.66" y="-320.04"/>
<instance part="U$13" gate="G$1" x="-1176.02" y="353.06"/>
<instance part="SUPPLY23" gate="GND" x="-1176.02" y="342.9"/>
<instance part="C20" gate="G$1" x="132.08" y="-353.06"/>
<instance part="C21" gate="G$1" x="142.24" y="-353.06"/>
<instance part="R14" gate="G$1" x="226.06" y="-355.6" rot="R270"/>
<instance part="R15" gate="G$1" x="226.06" y="-391.16" rot="R270"/>
<instance part="R16" gate="G$1" x="233.68" y="-355.6" rot="R270"/>
<instance part="R18" gate="G$1" x="233.68" y="-391.16" rot="R270"/>
<instance part="R19" gate="G$1" x="241.3" y="-355.6" rot="R270"/>
<instance part="R20" gate="G$1" x="241.3" y="-391.16" rot="R270"/>
<instance part="SUPPLY26" gate="GND" x="233.68" y="-406.4"/>
<instance part="SUPPLY29" gate="GND" x="50.8" y="-327.66" rot="R90"/>
<instance part="SUPPLY30" gate="GND" x="-20.32" y="-337.82"/>
<instance part="C22" gate="G$1" x="-12.7" y="-325.12"/>
<instance part="C23" gate="G$1" x="-5.08" y="-325.12"/>
<instance part="C24" gate="G$1" x="0" y="-381" rot="MR0"/>
<instance part="C25" gate="G$1" x="345.44" y="-398.78" rot="R270"/>
<instance part="SUPPLY31" gate="GND" x="337.82" y="-403.86" rot="MR0"/>
<instance part="C26" gate="G$1" x="337.82" y="-383.54"/>
<instance part="C28" gate="G$1" x="345.44" y="-365.76" rot="R270"/>
<instance part="SUPPLY32" gate="GND" x="337.82" y="-370.84" rot="MR0"/>
<instance part="C29" gate="G$1" x="337.82" y="-426.72"/>
<instance part="SUPPLY33" gate="GND" x="337.82" y="-436.88" rot="MR0"/>
<instance part="U3" gate="G$1" x="297.18" y="-368.3"/>
<instance part="C31" gate="G$1" x="256.54" y="-444.5" rot="MR0"/>
<instance part="SUPPLY35" gate="GND" x="256.54" y="-454.66" rot="MR0"/>
<instance part="R17" gate="G$1" x="345.44" y="-322.58"/>
<instance part="R22" gate="G$1" x="408.94" y="-312.42" rot="R90"/>
<instance part="C32" gate="G$1" x="419.1" y="-312.42" rot="R180"/>
<instance part="C33" gate="G$1" x="66.04" y="-424.18" rot="MR0"/>
<instance part="C34" gate="G$1" x="63.5" y="-375.92" rot="MR0"/>
<instance part="SUPPLY24" gate="GND" x="-71.12" y="-337.82"/>
<instance part="C18" gate="G$1" x="-63.5" y="-325.12"/>
<instance part="C19" gate="G$1" x="-55.88" y="-325.12"/>
<instance part="C16" gate="G$1" x="-71.12" y="-325.12" rot="MR0"/>
<instance part="L1" gate="G$1" x="-38.1" y="-317.5" rot="R90"/>
<instance part="C30" gate="G$1" x="337.82" y="-302.26" rot="R270"/>
<instance part="SUPPLY37" gate="GND" x="345.44" y="-309.88" rot="MR0"/>
<instance part="C35" gate="G$1" x="325.12" y="-284.48" rot="R270"/>
<instance part="U$27" gate="G$1" x="447.04" y="-353.06"/>
<instance part="U$29" gate="G$1" x="462.28" y="-353.06"/>
<instance part="C36" gate="G$1" x="474.98" y="-355.6" rot="R180"/>
<instance part="SUPPLY38" gate="GND" x="462.28" y="-365.76" rot="MR0"/>
<instance part="R23" gate="G$1" x="431.8" y="-365.76" rot="R90"/>
<instance part="C37" gate="G$1" x="441.96" y="-365.76" rot="R180"/>
<instance part="R24" gate="G$1" x="431.8" y="-381" rot="R90"/>
<instance part="SUPPLY39" gate="GND" x="431.8" y="-393.7" rot="MR0"/>
<instance part="R45" gate="G$1" x="424.18" y="-347.98" rot="MR180"/>
<instance part="Q2" gate="G$1" x="269.24" y="-284.48" rot="R90"/>
<instance part="Q1" gate="G$1" x="241.3" y="-284.48" rot="R90"/>
<instance part="R27" gate="G$1" x="269.24" y="-302.26"/>
<instance part="R28" gate="G$1" x="248.92" y="-292.1" rot="R90"/>
<instance part="C38" gate="G$1" x="261.62" y="-292.1" rot="R180"/>
<instance part="C40" gate="G$1" x="218.44" y="-325.12"/>
<instance part="C41" gate="G$1" x="228.6" y="-325.12"/>
<instance part="SUPPLY41" gate="GND" x="223.52" y="-340.36"/>
<instance part="R30" gate="G$1" x="254" y="-274.32" rot="R90"/>
<instance part="Q3" gate="G$1" x="431.8" y="-325.12"/>
<instance part="Q4" gate="G$1" x="381" y="-332.74"/>
<instance part="Q5" gate="G$1" x="381" y="-358.14"/>
<instance part="SUPPLY42" gate="GND" x="381" y="-370.84" rot="MR0"/>
<instance part="U$3" gate="G$1" x="373.38" y="-281.94"/>
<instance part="U$7" gate="G$1" x="388.62" y="-289.56" rot="MR0"/>
<instance part="C39" gate="G$1" x="401.32" y="-292.1" rot="R180"/>
<instance part="SUPPLY40" gate="GND" x="388.62" y="-304.8" rot="MR0"/>
<instance part="SUPPLY43" gate="GND" x="-1711.96" y="248.92" rot="MR0"/>
<instance part="SUPPLY46" gate="GND" x="-1524" y="304.8"/>
<instance part="L5" gate="G$1" x="406.4" y="-347.98" rot="R90"/>
<instance part="R25" gate="G$1" x="-1739.9" y="233.68"/>
<instance part="C7" gate="G$1" x="-1765.3" y="287.02" rot="R180"/>
<instance part="R26" gate="G$1" x="-1778" y="294.64" rot="R180"/>
<instance part="SUPPLY7" gate="GND" x="-1765.3" y="276.86" rot="MR0"/>
<instance part="C42" gate="G$1" x="-1762.76" y="302.26" rot="R180"/>
<instance part="R37" gate="G$1" x="-1778" y="309.88" rot="R180"/>
<instance part="C43" gate="G$1" x="-1762.76" y="322.58" rot="R180"/>
<instance part="R35" gate="G$1" x="-1778" y="330.2" rot="R180"/>
<instance part="R29" gate="G$1" x="-1656.08" y="388.62" rot="R270"/>
<instance part="U$11" gate="G$1" x="-1590.04" y="335.28" rot="MR0"/>
<instance part="SUPPLY27" gate="GND" x="-1595.12" y="322.58"/>
<instance part="Q8" gate="G$1" x="452.12" y="-421.64" rot="MR270"/>
<instance part="Q9" gate="G$1" x="441.96" y="-449.58" rot="MR270"/>
<instance part="U$15" gate="G$1" x="477.52" y="-426.72" rot="R180"/>
<instance part="U$17" gate="G$1" x="474.98" y="-439.42" rot="R180"/>
<instance part="C44" gate="G$1" x="-1762.76" y="342.9" rot="R180"/>
<instance part="R46" gate="G$1" x="-1778" y="350.52" rot="R180"/>
<instance part="C45" gate="G$1" x="-1762.76" y="363.22" rot="R180"/>
<instance part="R50" gate="G$1" x="-1778" y="370.84" rot="R180"/>
<instance part="C46" gate="G$1" x="-1762.76" y="383.54" rot="R180"/>
<instance part="R53" gate="G$1" x="-1778" y="391.16" rot="R180"/>
<instance part="U$24" gate="G$1" x="-1605.28" y="342.9" rot="MR0"/>
<instance part="U$25" gate="G$1" x="-1620.52" y="350.52" rot="MR0"/>
<instance part="U$26" gate="G$1" x="-1635.76" y="358.14" rot="MR0"/>
<instance part="U$28" gate="G$1" x="-1651" y="365.76" rot="MR0"/>
<instance part="R56" gate="G$1" x="-1640.84" y="388.62" rot="R270"/>
<instance part="R57" gate="G$1" x="-1625.6" y="388.62" rot="R270"/>
<instance part="R58" gate="G$1" x="-1610.36" y="388.62" rot="R270"/>
<instance part="R59" gate="G$1" x="-1595.12" y="388.62" rot="R270"/>
<instance part="R60" gate="G$1" x="-1686.56" y="294.64" rot="R180"/>
<instance part="SUPPLY28" gate="GND" x="-1694.18" y="276.86" rot="MR0"/>
<instance part="Q13" gate="G$1" x="-1663.7" y="261.62"/>
<instance part="SUPPLY47" gate="GND" x="-1673.86" y="259.08" rot="MR0"/>
<instance part="R62" gate="G$1" x="-1663.7" y="246.38" rot="R270"/>
<instance part="R63" gate="G$1" x="-1663.7" y="231.14" rot="R270"/>
<instance part="Q14" gate="G$1" x="-1666.24" y="210.82" rot="R270"/>
<instance part="Q15" gate="G$1" x="-1696.72" y="210.82" rot="R270"/>
<instance part="R64" gate="G$1" x="-1701.8" y="220.98"/>
<instance part="C47" gate="G$1" x="-1696.72" y="340.36"/>
<instance part="SUPPLY48" gate="GND" x="-1696.72" y="330.2" rot="MR0"/>
<instance part="C48" gate="G$1" x="-1696.72" y="363.22"/>
<instance part="SUPPLY49" gate="GND" x="-1696.72" y="353.06" rot="MR0"/>
<instance part="U$2" gate="G$1" x="-1729.74" y="332.74"/>
<instance part="C49" gate="G$1" x="-1686.56" y="375.92"/>
<instance part="SUPPLY50" gate="GND" x="-1686.56" y="365.76" rot="MR0"/>
<instance part="C50" gate="G$1" x="-1757.68" y="246.38" rot="MR0"/>
<instance part="SUPPLY51" gate="GND" x="-1757.68" y="236.22" rot="MR0"/>
<instance part="C51" gate="G$1" x="-1722.12" y="246.38"/>
<instance part="SUPPLY52" gate="GND" x="-1722.12" y="236.22" rot="MR0"/>
<instance part="R36" gate="G$1" x="-1574.8" y="444.5"/>
<instance part="R39" gate="G$1" x="-1729.74" y="243.84" rot="R270"/>
<instance part="R40" gate="G$1" x="-1750.06" y="243.84" rot="R270"/>
<instance part="R47" gate="G$1" x="-1694.18" y="287.02" rot="MR270"/>
<instance part="C52" gate="G$1" x="-1676.4" y="383.54"/>
<instance part="SUPPLY53" gate="GND" x="-1676.4" y="373.38" rot="MR0"/>
<instance part="U$16" gate="G$1" x="-223.52" y="-368.3"/>
<instance part="SUPPLY9" gate="GND" x="-226.06" y="-431.8"/>
<instance part="R31" gate="G$1" x="-264.16" y="-406.4"/>
<instance part="C27" gate="G$1" x="-264.16" y="-322.58" rot="R90"/>
<instance part="C53" gate="G$1" x="-264.16" y="-330.2" rot="R90"/>
<instance part="C54" gate="G$1" x="-271.78" y="-347.98"/>
<instance part="C55" gate="G$1" x="-264.16" y="-370.84" rot="R90"/>
<instance part="C56" gate="G$1" x="-264.16" y="-378.46" rot="R90"/>
<instance part="C57" gate="G$1" x="-264.16" y="-388.62" rot="R90"/>
<instance part="C58" gate="G$1" x="-287.02" y="-340.36" rot="R90"/>
<instance part="U$18" gate="A" x="-297.18" y="-350.52" rot="R180"/>
<instance part="C59" gate="G$1" x="-264.16" y="-396.24" rot="R90"/>
<instance part="SUPPLY34" gate="GND" x="-248.92" y="-431.8"/>
<instance part="R32" gate="G$1" x="-233.68" y="-447.04" rot="R90"/>
<instance part="SUPPLY36" gate="GND" x="-233.68" y="-459.74"/>
<instance part="R33" gate="G$1" x="-190.5" y="-370.84" rot="R90"/>
<instance part="SUPPLY25" gate="GND" x="-190.5" y="-381"/>
<instance part="R34" gate="G$1" x="-223.52" y="-439.42" rot="R180"/>
<instance part="C60" gate="G$1" x="-213.36" y="-447.04"/>
<instance part="SUPPLY54" gate="GND" x="-213.36" y="-459.74"/>
<instance part="C61" gate="G$1" x="-187.96" y="-322.58"/>
<instance part="SUPPLY55" gate="GND" x="-187.96" y="-332.74"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="-1137.92" y1="363.22" x2="-1137.92" y2="358.14" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="-1076.96" y1="353.06" x2="-1076.96" y2="350.52" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="PAD"/>
<wire x1="-1005.84" y1="299.72" x2="-1003.3" y2="299.72" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="-1003.3" y1="299.72" x2="-1003.3" y2="297.18" width="0.1524" layer="91"/>
<wire x1="-1003.3" y1="299.72" x2="-1003.3" y2="304.8" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="VSS"/>
<wire x1="-1003.3" y1="304.8" x2="-1005.84" y2="304.8" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="VSS_2"/>
<wire x1="-1005.84" y1="309.88" x2="-1003.3" y2="309.88" width="0.1524" layer="91"/>
<wire x1="-1003.3" y1="309.88" x2="-1003.3" y2="304.8" width="0.1524" layer="91"/>
<junction x="-1003.3" y="304.8"/>
<junction x="-1003.3" y="299.72"/>
</segment>
<segment>
<wire x1="-998.22" y1="337.82" x2="-998.22" y2="335.28" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="-998.22" y1="335.28" x2="-998.22" y2="332.74" width="0.1524" layer="91"/>
<wire x1="-977.9" y1="337.82" x2="-977.9" y2="335.28" width="0.1524" layer="91"/>
<wire x1="-977.9" y1="335.28" x2="-988.06" y2="335.28" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="-988.06" y1="335.28" x2="-998.22" y2="335.28" width="0.1524" layer="91"/>
<wire x1="-988.06" y1="337.82" x2="-988.06" y2="335.28" width="0.1524" layer="91"/>
<junction x="-988.06" y="335.28"/>
<junction x="-998.22" y="335.28"/>
<pinref part="C11" gate="A" pin="2"/>
<label x="-995.68" y="335.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="-1076.96" y1="393.7" x2="-1076.96" y2="388.62" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-1076.96" y1="388.62" x2="-1069.34" y2="388.62" width="0.1524" layer="91"/>
<wire x1="-1069.34" y1="388.62" x2="-1059.18" y2="388.62" width="0.1524" layer="91"/>
<wire x1="-1059.18" y1="388.62" x2="-1059.18" y2="393.7" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="-1069.34" y1="393.7" x2="-1069.34" y2="388.62" width="0.1524" layer="91"/>
<junction x="-1069.34" y="388.62"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="-1069.34" y1="388.62" x2="-1069.34" y2="386.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-1054.1" y1="256.54" x2="-1054.1" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-1054.1" y1="238.76" x2="-1033.78" y2="238.76" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-1033.78" y1="238.76" x2="-1033.78" y2="241.3" width="0.1524" layer="91"/>
<wire x1="-1033.78" y1="238.76" x2="-1026.16" y2="238.76" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-1026.16" y1="238.76" x2="-1026.16" y2="256.54" width="0.1524" layer="91"/>
<wire x1="-1033.78" y1="238.76" x2="-1033.78" y2="236.22" width="0.1524" layer="91"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<junction x="-1033.78" y="238.76"/>
</segment>
<segment>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="281.94" y1="-447.04" x2="281.94" y2="-452.12" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="299.72" y1="-447.04" x2="299.72" y2="-452.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<pinref part="U3" gate="G$1" pin="PAD_GND1"/>
</segment>
<segment>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<pinref part="U20" gate="A" pin="GND3"/>
<wire x1="25.4" y1="-454.66" x2="25.4" y2="-449.58" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="GND2"/>
<wire x1="25.4" y1="-449.58" x2="25.4" y2="-447.04" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-447.04" x2="17.78" y2="-449.58" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-449.58" x2="25.4" y2="-449.58" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="GND1"/>
<wire x1="10.16" y1="-447.04" x2="10.16" y2="-449.58" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-449.58" x2="17.78" y2="-449.58" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="PGND"/>
<wire x1="35.56" y1="-447.04" x2="35.56" y2="-449.58" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-449.58" x2="25.4" y2="-449.58" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="PAD"/>
<wire x1="43.18" y1="-447.04" x2="43.18" y2="-449.58" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-449.58" x2="35.56" y2="-449.58" width="0.1524" layer="91"/>
<junction x="35.56" y="-449.58"/>
<junction x="25.4" y="-449.58"/>
<junction x="17.78" y="-449.58"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="-368.3" x2="-7.62" y2="-370.84" width="0.1524" layer="91"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="0" y1="-386.08" x2="0" y2="-388.62" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="0" y1="-426.72" x2="0" y2="-429.26" width="0.1524" layer="91"/>
<pinref part="U$23" gate="A" pin="2"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="60.96" y1="-449.58" x2="60.96" y2="-452.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="66.04" y1="-429.26" x2="66.04" y2="-431.8" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="83.82" y1="-416.56" x2="83.82" y2="-419.1" width="0.1524" layer="91"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-383.54" x2="73.66" y2="-386.08" width="0.1524" layer="91"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="91.44" y1="-360.68" x2="91.44" y2="-363.22" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-360.68" x2="96.52" y2="-360.68" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-360.68" x2="91.44" y2="-360.68" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-360.68" x2="132.08" y2="-358.14" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="132.08" y1="-360.68" x2="142.24" y2="-360.68" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-360.68" x2="142.24" y2="-358.14" width="0.1524" layer="91"/>
<junction x="132.08" y="-360.68"/>
<wire x1="96.52" y1="-360.68" x2="96.52" y2="-358.14" width="0.1524" layer="91"/>
<junction x="96.52" y="-360.68"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="287.02" y1="-299.72" x2="287.02" y2="-302.26" width="0.1524" layer="91"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="-962.66" y1="358.14" x2="-962.66" y2="355.6" width="0.1524" layer="91"/>
<wire x1="-962.66" y1="355.6" x2="-972.82" y2="355.6" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="-972.82" y1="355.6" x2="-982.98" y2="355.6" width="0.1524" layer="91"/>
<wire x1="-972.82" y1="358.14" x2="-972.82" y2="355.6" width="0.1524" layer="91"/>
<junction x="-972.82" y="355.6"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="-982.98" y1="358.14" x2="-982.98" y2="355.6" width="0.1524" layer="91"/>
<wire x1="-982.98" y1="355.6" x2="-982.98" y2="353.06" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<label x="-980.44" y="355.6" size="1.778" layer="95"/>
<junction x="-982.98" y="355.6"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PAD_GND1"/>
<wire x1="-673.1" y1="269.24" x2="-673.1" y2="264.16" width="0.1524" layer="91"/>
<wire x1="-673.1" y1="264.16" x2="-680.72" y2="264.16" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VSS"/>
<wire x1="-680.72" y1="264.16" x2="-688.34" y2="264.16" width="0.1524" layer="91"/>
<wire x1="-688.34" y1="264.16" x2="-688.34" y2="269.24" width="0.1524" layer="91"/>
<wire x1="-680.72" y1="264.16" x2="-680.72" y2="261.62" width="0.1524" layer="91"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
<junction x="-680.72" y="264.16"/>
</segment>
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="-716.28" y1="327.66" x2="-716.28" y2="325.12" width="0.1524" layer="91"/>
<wire x1="-716.28" y1="325.12" x2="-721.36" y2="325.12" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="-721.36" y1="325.12" x2="-723.9" y2="325.12" width="0.1524" layer="91"/>
<wire x1="-723.9" y1="325.12" x2="-723.9" y2="327.66" width="0.1524" layer="91"/>
<wire x1="-721.36" y1="325.12" x2="-721.36" y2="322.58" width="0.1524" layer="91"/>
<junction x="-721.36" y="325.12"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="C"/>
<wire x1="-1176.02" y1="350.52" x2="-1176.02" y2="345.44" width="0.1524" layer="91"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="226.06" y1="-396.24" x2="226.06" y2="-401.32" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-401.32" x2="233.68" y2="-401.32" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="233.68" y1="-401.32" x2="233.68" y2="-396.24" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-401.32" x2="241.3" y2="-401.32" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="241.3" y1="-401.32" x2="241.3" y2="-396.24" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-401.32" x2="233.68" y2="-403.86" width="0.1524" layer="91"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
<junction x="233.68" y="-401.32"/>
<label x="228.6" y="-401.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
<wire x1="48.26" y1="-327.66" x2="45.72" y2="-327.66" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="NC1"/>
<wire x1="45.72" y1="-327.66" x2="45.72" y2="-332.74" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-327.66" x2="38.1" y2="-327.66" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="NC2"/>
<wire x1="38.1" y1="-327.66" x2="38.1" y2="-332.74" width="0.1524" layer="91"/>
<junction x="45.72" y="-327.66"/>
</segment>
<segment>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
<wire x1="-20.32" y1="-330.2" x2="-20.32" y2="-332.74" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="-20.32" y1="-332.74" x2="-20.32" y2="-335.28" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-330.2" x2="-12.7" y2="-332.74" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-332.74" x2="-20.32" y2="-332.74" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="-330.2" x2="-5.08" y2="-332.74" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-332.74" x2="-12.7" y2="-332.74" width="0.1524" layer="91"/>
<junction x="-20.32" y="-332.74"/>
<junction x="-12.7" y="-332.74"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="340.36" y1="-398.78" x2="337.82" y2="-398.78" width="0.1524" layer="91"/>
<wire x1="337.82" y1="-398.78" x2="337.82" y2="-401.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="340.36" y1="-365.76" x2="337.82" y2="-365.76" width="0.1524" layer="91"/>
<wire x1="337.82" y1="-365.76" x2="337.82" y2="-368.3" width="0.1524" layer="91"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="337.82" y1="-434.34" x2="337.82" y2="-431.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
<wire x1="256.54" y1="-449.58" x2="256.54" y2="-452.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
<wire x1="-71.12" y1="-330.2" x2="-71.12" y2="-332.74" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="-71.12" y1="-332.74" x2="-71.12" y2="-335.28" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-330.2" x2="-63.5" y2="-332.74" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-332.74" x2="-71.12" y2="-332.74" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="-55.88" y1="-330.2" x2="-55.88" y2="-332.74" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-332.74" x2="-63.5" y2="-332.74" width="0.1524" layer="91"/>
<junction x="-71.12" y="-332.74"/>
<junction x="-63.5" y="-332.74"/>
<pinref part="C16" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="340.36" y1="-302.26" x2="345.44" y2="-302.26" width="0.1524" layer="91"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<wire x1="345.44" y1="-302.26" x2="345.44" y2="-307.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$29" gate="G$1" pin="2"/>
<wire x1="462.28" y1="-358.14" x2="462.28" y2="-360.68" width="0.1524" layer="91"/>
<wire x1="462.28" y1="-360.68" x2="447.04" y2="-360.68" width="0.1524" layer="91"/>
<pinref part="U$27" gate="G$1" pin="2"/>
<wire x1="447.04" y1="-360.68" x2="447.04" y2="-358.14" width="0.1524" layer="91"/>
<junction x="462.28" y="-360.68"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="474.98" y1="-358.14" x2="474.98" y2="-360.68" width="0.1524" layer="91"/>
<wire x1="474.98" y1="-360.68" x2="462.28" y2="-360.68" width="0.1524" layer="91"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
<wire x1="462.28" y1="-363.22" x2="462.28" y2="-360.68" width="0.1524" layer="91"/>
<label x="449.58" y="-360.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="431.8" y1="-386.08" x2="431.8" y2="-391.16" width="0.1524" layer="91"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="228.6" y1="-330.2" x2="228.6" y2="-335.28" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="228.6" y1="-335.28" x2="223.52" y2="-335.28" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-335.28" x2="218.44" y2="-335.28" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-335.28" x2="218.44" y2="-330.2" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-335.28" x2="223.52" y2="-337.82" width="0.1524" layer="91"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
<junction x="223.52" y="-335.28"/>
</segment>
<segment>
<pinref part="Q5" gate="G$1" pin="S"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
<wire x1="381" y1="-363.22" x2="381" y2="-368.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="2"/>
<pinref part="U$3" gate="G$1" pin="2"/>
<wire x1="388.62" y1="-299.72" x2="373.38" y2="-299.72" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-299.72" x2="373.38" y2="-287.02" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-294.64" x2="388.62" y2="-299.72" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="401.32" y1="-294.64" x2="401.32" y2="-299.72" width="0.1524" layer="91"/>
<wire x1="401.32" y1="-299.72" x2="388.62" y2="-299.72" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-299.72" x2="388.62" y2="-302.26" width="0.1524" layer="91"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
<junction x="388.62" y="-299.72"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="VSS1"/>
<wire x1="-1531.62" y1="312.42" x2="-1531.62" y2="309.88" width="0.1524" layer="91"/>
<wire x1="-1531.62" y1="309.88" x2="-1524" y2="309.88" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VSS3"/>
<wire x1="-1524" y1="309.88" x2="-1516.38" y2="309.88" width="0.1524" layer="91"/>
<wire x1="-1516.38" y1="309.88" x2="-1516.38" y2="312.42" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VSS2"/>
<wire x1="-1524" y1="312.42" x2="-1524" y2="309.88" width="0.1524" layer="91"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
<wire x1="-1524" y1="307.34" x2="-1524" y2="309.88" width="0.1524" layer="91"/>
<junction x="-1524" y="309.88"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-1765.3" y1="284.48" x2="-1765.3" y2="281.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="-1765.3" y1="281.94" x2="-1765.3" y2="279.4" width="0.1524" layer="91"/>
<wire x1="-1762.76" y1="299.72" x2="-1762.76" y2="281.94" width="0.1524" layer="91"/>
<wire x1="-1762.76" y1="281.94" x2="-1765.3" y2="281.94" width="0.1524" layer="91"/>
<junction x="-1765.3" y="281.94"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="E"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
<wire x1="-1595.12" y1="330.2" x2="-1595.12" y2="327.66" width="0.1524" layer="91"/>
<pinref part="U$24" gate="G$1" pin="E"/>
<wire x1="-1595.12" y1="327.66" x2="-1595.12" y2="325.12" width="0.1524" layer="91"/>
<wire x1="-1595.12" y1="327.66" x2="-1610.36" y2="327.66" width="0.1524" layer="91"/>
<wire x1="-1610.36" y1="327.66" x2="-1610.36" y2="337.82" width="0.1524" layer="91"/>
<pinref part="U$25" gate="G$1" pin="E"/>
<wire x1="-1610.36" y1="327.66" x2="-1625.6" y2="327.66" width="0.1524" layer="91"/>
<wire x1="-1625.6" y1="327.66" x2="-1625.6" y2="345.44" width="0.1524" layer="91"/>
<pinref part="U$26" gate="G$1" pin="E"/>
<wire x1="-1625.6" y1="327.66" x2="-1640.84" y2="327.66" width="0.1524" layer="91"/>
<wire x1="-1640.84" y1="327.66" x2="-1640.84" y2="353.06" width="0.1524" layer="91"/>
<pinref part="U$28" gate="G$1" pin="E"/>
<wire x1="-1640.84" y1="327.66" x2="-1656.08" y2="327.66" width="0.1524" layer="91"/>
<wire x1="-1656.08" y1="327.66" x2="-1656.08" y2="360.68" width="0.1524" layer="91"/>
<junction x="-1640.84" y="327.66"/>
<junction x="-1625.6" y="327.66"/>
<junction x="-1610.36" y="327.66"/>
<junction x="-1595.12" y="327.66"/>
</segment>
<segment>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<wire x1="-1694.18" y1="279.4" x2="-1694.18" y2="281.94" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="Q13" gate="G$1" pin="G"/>
<wire x1="-1668.78" y1="264.16" x2="-1673.86" y2="264.16" width="0.1524" layer="91"/>
<wire x1="-1673.86" y1="264.16" x2="-1673.86" y2="261.62" width="0.1524" layer="91"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="2"/>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
<wire x1="-1696.72" y1="335.28" x2="-1696.72" y2="332.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="-1696.72" y1="358.14" x2="-1696.72" y2="355.6" width="0.1524" layer="91"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="-1686.56" y1="370.84" x2="-1686.56" y2="368.3" width="0.1524" layer="91"/>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="2"/>
<wire x1="-1757.68" y1="241.3" x2="-1757.68" y2="238.76" width="0.1524" layer="91"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VSS"/>
<wire x1="-1722.12" y1="256.54" x2="-1722.12" y2="254" width="0.1524" layer="91"/>
<wire x1="-1722.12" y1="254" x2="-1711.96" y2="254" width="0.1524" layer="91"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<wire x1="-1711.96" y1="254" x2="-1711.96" y2="251.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C51" gate="G$1" pin="2"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
<wire x1="-1722.12" y1="241.3" x2="-1722.12" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="2"/>
<wire x1="-1676.4" y1="378.46" x2="-1676.4" y2="375.92" width="0.1524" layer="91"/>
<pinref part="SUPPLY53" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$16" gate="G$1" pin="PGND_PIN1"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<wire x1="-226.06" y1="-424.18" x2="-226.06" y2="-426.72" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="PGND_PIN20"/>
<wire x1="-226.06" y1="-426.72" x2="-226.06" y2="-429.26" width="0.1524" layer="91"/>
<wire x1="-226.06" y1="-426.72" x2="-220.98" y2="-426.72" width="0.1524" layer="91"/>
<wire x1="-220.98" y1="-426.72" x2="-220.98" y2="-424.18" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="PAD_FIN_1"/>
<wire x1="-220.98" y1="-426.72" x2="-215.9" y2="-426.72" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="-426.72" x2="-215.9" y2="-424.18" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="PAD_FIN_20"/>
<wire x1="-215.9" y1="-426.72" x2="-210.82" y2="-426.72" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-426.72" x2="-210.82" y2="-424.18" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="PAD_FIN_10"/>
<wire x1="-210.82" y1="-426.72" x2="-205.74" y2="-426.72" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="-426.72" x2="-205.74" y2="-424.18" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="PAD_FIN_11"/>
<wire x1="-205.74" y1="-426.72" x2="-200.66" y2="-426.72" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="-426.72" x2="-200.66" y2="-424.18" width="0.1524" layer="91"/>
<junction x="-226.06" y="-426.72"/>
<junction x="-220.98" y="-426.72"/>
<junction x="-215.9" y="-426.72"/>
<junction x="-210.82" y="-426.72"/>
<junction x="-205.74" y="-426.72"/>
</segment>
<segment>
<pinref part="U$16" gate="G$1" pin="AD"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
<wire x1="-248.92" y1="-424.18" x2="-248.92" y2="-429.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
<wire x1="-233.68" y1="-452.12" x2="-233.68" y2="-457.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="-190.5" y1="-378.46" x2="-190.5" y2="-375.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C60" gate="G$1" pin="2"/>
<pinref part="SUPPLY54" gate="GND" pin="GND"/>
<wire x1="-213.36" y1="-452.12" x2="-213.36" y2="-457.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C61" gate="G$1" pin="2"/>
<pinref part="SUPPLY55" gate="GND" pin="GND"/>
<wire x1="-187.96" y1="-327.66" x2="-187.96" y2="-330.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_BAT_P" class="0">
<segment>
<pinref part="BAT1" gate="G$1" pin="1"/>
<wire x1="-363.22" y1="-7.62" x2="-363.22" y2="15.24" width="0.1524" layer="91"/>
<label x="-363.22" y="0" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VCC_3P3" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="-736.6" y1="340.36" x2="-759.46" y2="340.36" width="0.1524" layer="91"/>
<label x="-754.38" y="340.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_DATA" class="0">
<segment>
<wire x1="-708.66" y1="292.1" x2="-744.22" y2="292.1" width="0.1524" layer="91"/>
<label x="-741.68" y="292.1" size="1.778" layer="95"/>
<pinref part="U6" gate="G$1" pin="SDA"/>
</segment>
<segment>
<wire x1="-1704.34" y1="314.96" x2="-1689.1" y2="314.96" width="0.1524" layer="91"/>
<label x="-1701.8" y="314.96" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="SDA"/>
</segment>
</net>
<net name="I2C_CLK" class="0">
<segment>
<wire x1="-708.66" y1="302.26" x2="-744.22" y2="302.26" width="0.1524" layer="91"/>
<label x="-741.68" y="302.26" size="1.778" layer="95"/>
<pinref part="U6" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="-1704.34" y1="322.58" x2="-1689.1" y2="322.58" width="0.1524" layer="91"/>
<label x="-1701.8" y="322.58" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="SCL"/>
</segment>
</net>
<net name="BAT_N" class="0">
<segment>
<pinref part="BAT1" gate="G$1" pin="2"/>
<wire x1="-363.22" y1="-88.9" x2="-363.22" y2="-73.66" width="0.1524" layer="91"/>
<label x="-363.22" y="-86.36" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="SRP"/>
<wire x1="-652.78" y1="297.18" x2="-635" y2="297.18" width="0.1524" layer="91"/>
<wire x1="-635" y1="297.18" x2="-635" y2="279.4" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="-635" y1="279.4" x2="-637.54" y2="279.4" width="0.1524" layer="91"/>
<wire x1="-635" y1="279.4" x2="-614.68" y2="279.4" width="0.1524" layer="91"/>
<junction x="-635" y="279.4"/>
<label x="-629.92" y="279.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="LBOOST_BQ25570" class="0">
<segment>
<pinref part="U2" gate="A" pin="LBOOST"/>
<wire x1="-1074.42" y1="373.38" x2="-1076.96" y2="373.38" width="0.1524" layer="91"/>
<wire x1="-1076.96" y1="373.38" x2="-1079.5" y2="373.38" width="0.1524" layer="91"/>
<wire x1="-1076.96" y1="373.38" x2="-1076.96" y2="383.54" width="0.1524" layer="91"/>
<wire x1="-1076.96" y1="383.54" x2="-1109.98" y2="383.54" width="0.1524" layer="91"/>
<label x="-1102.36" y="383.54" size="1.778" layer="95" font="fixed"/>
<junction x="-1076.96" y="373.38"/>
<pinref part="L4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="SOLAR_CELL_ANODE" class="0">
<segment>
<wire x1="-1094.74" y1="373.38" x2="-1137.92" y2="373.38" width="0.1524" layer="91"/>
<wire x1="-1137.92" y1="373.38" x2="-1176.02" y2="373.38" width="0.1524" layer="91"/>
<wire x1="-1176.02" y1="373.38" x2="-1176.02" y2="355.6" width="0.1524" layer="91"/>
<label x="-1163.32" y="373.38" size="1.778" layer="95" font="fixed"/>
<wire x1="-1137.92" y1="370.84" x2="-1137.92" y2="373.38" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<pinref part="L4" gate="G$1" pin="1"/>
<pinref part="U$13" gate="G$1" pin="A"/>
<junction x="-1137.92" y="373.38"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="VIN_DC"/>
<wire x1="-1074.42" y1="340.36" x2="-1102.36" y2="340.36" width="0.1524" layer="91"/>
<label x="-1099.82" y="340.36" size="1.778" layer="95" font="fixed"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U2" gate="A" pin="VREF_SAMP"/>
<wire x1="-1074.42" y1="363.22" x2="-1076.96" y2="363.22" width="0.1524" layer="91"/>
<wire x1="-1076.96" y1="363.22" x2="-1076.96" y2="360.68" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="BQ25570_VBAT_OK" class="0">
<segment>
<pinref part="U2" gate="A" pin="VBAT_OK"/>
<wire x1="-1074.42" y1="299.72" x2="-1102.36" y2="299.72" width="0.1524" layer="91"/>
<label x="-1099.82" y="299.72" size="1.778" layer="95" font="fixed"/>
</segment>
</net>
<net name="BQ25570_VOUT_EN" class="0">
<segment>
<pinref part="U2" gate="A" pin="VOUT_EN"/>
<wire x1="-1074.42" y1="307.34" x2="-1102.36" y2="307.34" width="0.1524" layer="91"/>
<label x="-1099.82" y="307.34" size="1.778" layer="95" font="fixed"/>
</segment>
</net>
<net name="BQ25570_EN_N" class="0">
<segment>
<pinref part="U2" gate="A" pin="EN_N"/>
<wire x1="-1074.42" y1="314.96" x2="-1102.36" y2="314.96" width="0.1524" layer="91"/>
<label x="-1094.74" y="314.96" size="1.778" layer="95" font="fixed"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U2" gate="A" pin="LBUCK"/>
<wire x1="-1005.84" y1="373.38" x2="-1000.76" y2="373.38" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="V_LOAD" class="0">
<segment>
<pinref part="U2" gate="A" pin="VOUT"/>
<wire x1="-1005.84" y1="368.3" x2="-982.98" y2="368.3" width="0.1524" layer="91"/>
<wire x1="-982.98" y1="368.3" x2="-982.98" y2="365.76" width="0.1524" layer="91"/>
<wire x1="-985.52" y1="373.38" x2="-982.98" y2="373.38" width="0.1524" layer="91"/>
<wire x1="-982.98" y1="373.38" x2="-982.98" y2="368.3" width="0.1524" layer="91"/>
<junction x="-982.98" y="368.3"/>
<wire x1="-982.98" y1="368.3" x2="-972.82" y2="368.3" width="0.1524" layer="91"/>
<label x="-967.74" y="368.3" size="1.778" layer="95" font="fixed"/>
<pinref part="C12" gate="G$1" pin="1"/>
<pinref part="L3" gate="G$1" pin="2"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-972.82" y1="368.3" x2="-962.66" y2="368.3" width="0.1524" layer="91"/>
<wire x1="-962.66" y1="368.3" x2="-955.04" y2="368.3" width="0.1524" layer="91"/>
<wire x1="-972.82" y1="365.76" x2="-972.82" y2="368.3" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-962.66" y1="365.76" x2="-962.66" y2="368.3" width="0.1524" layer="91"/>
<junction x="-972.82" y="368.3"/>
<junction x="-962.66" y="368.3"/>
</segment>
</net>
<net name="VOC_SAMP__VSTOR" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-1076.96" y1="401.32" x2="-1076.96" y2="406.4" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-1076.96" y1="406.4" x2="-1069.34" y2="406.4" width="0.1524" layer="91"/>
<wire x1="-1069.34" y1="406.4" x2="-1059.18" y2="406.4" width="0.1524" layer="91"/>
<wire x1="-1059.18" y1="406.4" x2="-1059.18" y2="401.32" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-1069.34" y1="401.32" x2="-1069.34" y2="406.4" width="0.1524" layer="91"/>
<junction x="-1069.34" y="406.4"/>
<pinref part="U2" gate="A" pin="VOC_SAMP"/>
<wire x1="-1046.48" y1="383.54" x2="-1046.48" y2="406.4" width="0.1524" layer="91"/>
<wire x1="-1046.48" y1="406.4" x2="-1059.18" y2="406.4" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="VSTOR"/>
<wire x1="-1033.78" y1="383.54" x2="-1033.78" y2="406.4" width="0.1524" layer="91"/>
<wire x1="-1033.78" y1="406.4" x2="-1046.48" y2="406.4" width="0.1524" layer="91"/>
<junction x="-1046.48" y="406.4"/>
<junction x="-1059.18" y="406.4"/>
<label x="-1066.8" y="406.4" size="1.778" layer="95" font="fixed"/>
</segment>
</net>
<net name="OK_HYST" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-1033.78" y1="271.78" x2="-1033.78" y2="269.24" width="0.1524" layer="91"/>
<wire x1="-1033.78" y1="269.24" x2="-1033.78" y2="266.7" width="0.1524" layer="91"/>
<wire x1="-1033.78" y1="269.24" x2="-1031.24" y2="269.24" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="OK_HYST"/>
<wire x1="-1031.24" y1="269.24" x2="-1031.24" y2="292.1" width="0.1524" layer="91"/>
<junction x="-1033.78" y="269.24"/>
<label x="-1031.24" y="279.4" size="1.778" layer="95" font="fixed" rot="R90"/>
</segment>
</net>
<net name="VOUT_SET" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-1026.16" y1="271.78" x2="-1026.16" y2="269.24" width="0.1524" layer="91"/>
<wire x1="-1026.16" y1="269.24" x2="-1026.16" y2="266.7" width="0.1524" layer="91"/>
<wire x1="-1026.16" y1="269.24" x2="-1023.62" y2="269.24" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="VOUT_SET"/>
<wire x1="-1023.62" y1="269.24" x2="-1023.62" y2="292.1" width="0.1524" layer="91"/>
<junction x="-1026.16" y="269.24"/>
<label x="-1023.62" y="276.86" size="1.778" layer="95" font="fixed" rot="R90"/>
</segment>
</net>
<net name="OK_PROG" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-1033.78" y1="256.54" x2="-1033.78" y2="254" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="OK_PROG"/>
<wire x1="-1033.78" y1="254" x2="-1033.78" y2="251.46" width="0.1524" layer="91"/>
<wire x1="-1038.86" y1="292.1" x2="-1038.86" y2="254" width="0.1524" layer="91"/>
<wire x1="-1038.86" y1="254" x2="-1033.78" y2="254" width="0.1524" layer="91"/>
<junction x="-1033.78" y="254"/>
<label x="-1038.86" y="279.4" size="1.778" layer="95" font="fixed" rot="R90"/>
</segment>
</net>
<net name="VBAT_OV" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-1054.1" y1="271.78" x2="-1054.1" y2="269.24" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="VBAT_OV"/>
<wire x1="-1054.1" y1="269.24" x2="-1054.1" y2="266.7" width="0.1524" layer="91"/>
<wire x1="-1046.48" y1="292.1" x2="-1046.48" y2="269.24" width="0.1524" layer="91"/>
<wire x1="-1046.48" y1="269.24" x2="-1054.1" y2="269.24" width="0.1524" layer="91"/>
<junction x="-1054.1" y="269.24"/>
<label x="-1046.48" y="279.4" size="1.778" layer="95" font="fixed" rot="R90"/>
</segment>
</net>
<net name="VRDIV" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-1033.78" y1="281.94" x2="-1033.78" y2="294.64" width="0.1524" layer="91"/>
<label x="-1033.78" y="284.48" size="1.778" layer="95" font="fixed" rot="R90"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="U2" gate="A" pin="VRDIV"/>
<wire x1="-1054.1" y1="281.94" x2="-1054.1" y2="292.1" width="0.1524" layer="91"/>
<label x="-1054.1" y="284.48" size="1.778" layer="95" font="fixed" rot="R90"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-1026.16" y1="281.94" x2="-1026.16" y2="294.64" width="0.1524" layer="91"/>
<label x="-1026.16" y="284.48" size="1.778" layer="95" font="fixed" rot="R90"/>
</segment>
</net>
<net name="BQ24616_CE" class="0">
<segment>
<wire x1="259.08" y1="-335.28" x2="238.76" y2="-335.28" width="0.1524" layer="91"/>
<label x="241.3" y="-335.28" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="CE"/>
</segment>
</net>
<net name="TPS55332_Q1_SYNC" class="0">
<segment>
<label x="-20.32" y="-396.24" size="1.778" layer="95"/>
<wire x1="2.54" y1="-396.24" x2="-22.86" y2="-396.24" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="SYNC"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U20" gate="A" pin="RT"/>
<wire x1="2.54" y1="-355.6" x2="-7.62" y2="-355.6" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="-355.6" x2="-7.62" y2="-358.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="2.54" y1="-375.92" x2="0" y2="-375.92" width="0.1524" layer="91"/>
<wire x1="0" y1="-375.92" x2="0" y2="-378.46" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="CDLY"/>
<pinref part="C24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="TPS55332_Q1_RST" class="0">
<segment>
<label x="-20.32" y="-406.4" size="1.778" layer="95"/>
<wire x1="2.54" y1="-406.4" x2="-22.86" y2="-406.4" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="RST"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="2.54" y1="-416.56" x2="0" y2="-416.56" width="0.1524" layer="91"/>
<wire x1="0" y1="-416.56" x2="0" y2="-419.1" width="0.1524" layer="91"/>
<pinref part="U$23" gate="A" pin="1"/>
<pinref part="U20" gate="A" pin="SS"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="60.96" y1="-436.88" x2="60.96" y2="-439.42" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="RSLEW"/>
<wire x1="60.96" y1="-436.88" x2="58.42" y2="-436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="66.04" y1="-419.1" x2="66.04" y2="-421.64" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="BOOT"/>
<wire x1="66.04" y1="-419.1" x2="58.42" y2="-419.1" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="83.82" y1="-398.78" x2="83.82" y2="-403.86" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="RST_TH"/>
<wire x1="83.82" y1="-403.86" x2="83.82" y2="-406.4" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-403.86" x2="83.82" y2="-403.86" width="0.1524" layer="91"/>
<junction x="83.82" y="-403.86"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="63.5" y1="-373.38" x2="63.5" y2="-368.3" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="VSENSE"/>
<wire x1="63.5" y1="-368.3" x2="58.42" y2="-368.3" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-365.76" x2="73.66" y2="-368.3" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-368.3" x2="73.66" y2="-373.38" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-368.3" x2="73.66" y2="-368.3" width="0.1524" layer="91"/>
<junction x="63.5" y="-368.3"/>
<junction x="73.66" y="-368.3"/>
<pinref part="C34" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="63.5" y1="-381" x2="63.5" y2="-388.62" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="COMP"/>
<wire x1="63.5" y1="-388.62" x2="58.42" y2="-388.62" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="38.1" y1="-320.04" x2="60.96" y2="-320.04" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-320.04" x2="60.96" y2="-337.82" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="SW"/>
<wire x1="60.96" y1="-337.82" x2="58.42" y2="-337.82" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
<pinref part="U$12" gate="G$1" pin="ANODE"/>
<wire x1="71.12" y1="-320.04" x2="60.96" y2="-320.04" width="0.1524" layer="91"/>
<junction x="60.96" y="-320.04"/>
</segment>
</net>
<net name="VCC_BQ24616" class="0">
<segment>
<wire x1="259.08" y1="-320.04" x2="228.6" y2="-320.04" width="0.1524" layer="91"/>
<label x="241.3" y="-320.04" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="VCC"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-320.04" x2="228.6" y2="-322.58" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-320.04" x2="218.44" y2="-320.04" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="218.44" y1="-320.04" x2="218.44" y2="-322.58" width="0.1524" layer="91"/>
<junction x="228.6" y="-320.04"/>
</segment>
<segment>
<wire x1="254" y1="-269.24" x2="254" y2="-248.92" width="0.1524" layer="91"/>
<label x="254" y="-266.7" size="1.778" layer="95" rot="R90"/>
<pinref part="R30" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="292.1" y1="-284.48" x2="292.1" y2="-289.56" width="0.1524" layer="91"/>
<wire x1="292.1" y1="-289.56" x2="292.1" y2="-309.88" width="0.1524" layer="91"/>
<wire x1="292.1" y1="-284.48" x2="287.02" y2="-284.48" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="287.02" y1="-284.48" x2="287.02" y2="-292.1" width="0.1524" layer="91"/>
<wire x1="294.64" y1="-276.86" x2="292.1" y2="-276.86" width="0.1524" layer="91"/>
<wire x1="292.1" y1="-276.86" x2="292.1" y2="-284.48" width="0.1524" layer="91"/>
<junction x="292.1" y="-284.48"/>
<pinref part="R38" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="ACP"/>
<pinref part="Q2" gate="G$1" pin="D"/>
<wire x1="274.32" y1="-284.48" x2="287.02" y2="-284.48" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="294.64" y1="-289.56" x2="292.1" y2="-289.56" width="0.1524" layer="91"/>
<junction x="292.1" y="-289.56"/>
<junction x="287.02" y="-284.48"/>
</segment>
</net>
<net name="BAT_P_CS" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="REGIN"/>
<wire x1="-652.78" y1="325.12" x2="-650.24" y2="325.12" width="0.1524" layer="91"/>
<wire x1="-650.24" y1="325.12" x2="-650.24" y2="320.04" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="BAT"/>
<wire x1="-650.24" y1="320.04" x2="-652.78" y2="320.04" width="0.1524" layer="91"/>
<wire x1="-650.24" y1="320.04" x2="-629.92" y2="320.04" width="0.1524" layer="91"/>
<junction x="-650.24" y="320.04"/>
<label x="-642.62" y="320.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC_2P2" class="0">
<segment>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="-723.9" y1="335.28" x2="-723.9" y2="340.36" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="-723.9" y1="340.36" x2="-726.44" y2="340.36" width="0.1524" layer="91"/>
<wire x1="-723.9" y1="340.36" x2="-716.28" y2="340.36" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="-716.28" y1="340.36" x2="-716.28" y2="337.82" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VCC"/>
<wire x1="-708.66" y1="340.36" x2="-711.2" y2="340.36" width="0.1524" layer="91"/>
<wire x1="-711.2" y1="340.36" x2="-711.2" y2="335.28" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="REG25"/>
<wire x1="-711.2" y1="335.28" x2="-708.66" y2="335.28" width="0.1524" layer="91"/>
<wire x1="-716.28" y1="340.36" x2="-711.2" y2="340.36" width="0.1524" layer="91"/>
<junction x="-723.9" y="340.36"/>
<junction x="-716.28" y="340.36"/>
<junction x="-711.2" y="340.36"/>
<label x="-721.36" y="340.36" size="1.778" layer="95"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="-683.26" y1="355.6" x2="-685.8" y2="355.6" width="0.1524" layer="91"/>
<wire x1="-685.8" y1="355.6" x2="-711.2" y2="355.6" width="0.1524" layer="91"/>
<wire x1="-711.2" y1="355.6" x2="-711.2" y2="340.36" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="-680.72" y1="365.76" x2="-685.8" y2="365.76" width="0.1524" layer="91"/>
<wire x1="-685.8" y1="365.76" x2="-685.8" y2="355.6" width="0.1524" layer="91"/>
<junction x="-685.8" y="355.6"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="-673.1" y1="355.6" x2="-668.02" y2="355.6" width="0.1524" layer="91"/>
<wire x1="-668.02" y1="355.6" x2="-640.08" y2="355.6" width="0.1524" layer="91"/>
<wire x1="-640.08" y1="355.6" x2="-640.08" y2="342.9" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="TS"/>
<wire x1="-640.08" y1="342.9" x2="-652.78" y2="342.9" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="-673.1" y1="365.76" x2="-668.02" y2="365.76" width="0.1524" layer="91"/>
<wire x1="-668.02" y1="365.76" x2="-668.02" y2="355.6" width="0.1524" layer="91"/>
<junction x="-668.02" y="355.6"/>
</segment>
</net>
<net name="BAT_N_CS" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="SRN"/>
<wire x1="-652.78" y1="289.56" x2="-650.24" y2="289.56" width="0.1524" layer="91"/>
<wire x1="-650.24" y1="289.56" x2="-650.24" y2="279.4" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="-650.24" y1="279.4" x2="-647.7" y2="279.4" width="0.1524" layer="91"/>
<wire x1="-650.24" y1="279.4" x2="-652.78" y2="279.4" width="0.1524" layer="91"/>
<wire x1="-652.78" y1="279.4" x2="-652.78" y2="259.08" width="0.1524" layer="91"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
<junction x="-650.24" y="279.4"/>
<label x="-652.78" y="264.16" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TPS55332_Q1_EN" class="0">
<segment>
<wire x1="2.54" y1="-345.44" x2="-25.4" y2="-345.44" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="EN"/>
<label x="-22.86" y="-345.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="VREF_BQ24616" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="259.08" y1="-345.44" x2="241.3" y2="-345.44" width="0.1524" layer="91"/>
<wire x1="241.3" y1="-345.44" x2="233.68" y2="-345.44" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-345.44" x2="226.06" y2="-345.44" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-345.44" x2="226.06" y2="-350.52" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="233.68" y1="-350.52" x2="233.68" y2="-345.44" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="241.3" y1="-350.52" x2="241.3" y2="-345.44" width="0.1524" layer="91"/>
<label x="228.6" y="-345.44" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="VREF"/>
<junction x="241.3" y="-345.44"/>
<junction x="233.68" y="-345.44"/>
</segment>
</net>
<net name="ISET1" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="233.68" y1="-360.68" x2="233.68" y2="-363.22" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-363.22" x2="233.68" y2="-386.08" width="0.1524" layer="91"/>
<wire x1="233.68" y1="-386.08" x2="233.68" y2="-388.62" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<junction x="233.68" y="-386.08"/>
<wire x1="259.08" y1="-363.22" x2="233.68" y2="-363.22" width="0.1524" layer="91"/>
<junction x="233.68" y="-363.22"/>
<label x="246.38" y="-363.22" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="ISET1"/>
</segment>
</net>
<net name="ISET2" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="241.3" y1="-360.68" x2="241.3" y2="-370.84" width="0.1524" layer="91"/>
<wire x1="241.3" y1="-370.84" x2="241.3" y2="-386.08" width="0.1524" layer="91"/>
<junction x="241.3" y="-370.84"/>
<wire x1="241.3" y1="-370.84" x2="259.08" y2="-370.84" width="0.1524" layer="91"/>
<label x="246.38" y="-370.84" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="ISET2"/>
</segment>
</net>
<net name="ACSET" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="226.06" y1="-360.68" x2="226.06" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-378.46" x2="226.06" y2="-386.08" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-378.46" x2="226.06" y2="-378.46" width="0.1524" layer="91"/>
<junction x="226.06" y="-378.46"/>
<label x="246.38" y="-378.46" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="ACSET"/>
</segment>
</net>
<net name="BAT_P" class="0">
<segment>
<wire x1="-93.98" y1="-317.5" x2="-106.68" y2="-317.5" width="0.1524" layer="91"/>
<label x="-104.14" y="-317.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="VBAT"/>
<wire x1="-1005.84" y1="347.98" x2="-998.22" y2="347.98" width="0.1524" layer="91"/>
<label x="-972.82" y="347.98" size="1.778" layer="95" font="fixed"/>
<wire x1="-998.22" y1="347.98" x2="-988.06" y2="347.98" width="0.1524" layer="91"/>
<wire x1="-988.06" y1="347.98" x2="-977.9" y2="347.98" width="0.1524" layer="91"/>
<wire x1="-977.9" y1="347.98" x2="-955.04" y2="347.98" width="0.1524" layer="91"/>
<wire x1="-998.22" y1="345.44" x2="-998.22" y2="347.98" width="0.1524" layer="91"/>
<junction x="-998.22" y="347.98"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-988.06" y1="345.44" x2="-988.06" y2="347.98" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="-977.9" y1="345.44" x2="-977.9" y2="347.98" width="0.1524" layer="91"/>
<junction x="-988.06" y="347.98"/>
<junction x="-977.9" y="347.98"/>
<pinref part="C11" gate="A" pin="1"/>
</segment>
</net>
<net name="LODRV_BQ24616" class="0">
<segment>
<wire x1="335.28" y1="-360.68" x2="375.92" y2="-360.68" width="0.1524" layer="91"/>
<label x="342.9" y="-360.68" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="LODRV"/>
<pinref part="Q5" gate="G$1" pin="G"/>
</segment>
</net>
<net name="TTC_BQ24616" class="0">
<segment>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="337.82" y1="-424.18" x2="337.82" y2="-421.64" width="0.1524" layer="91"/>
<wire x1="337.82" y1="-421.64" x2="335.28" y2="-421.64" width="0.1524" layer="91"/>
<wire x1="337.82" y1="-421.64" x2="370.84" y2="-421.64" width="0.1524" layer="91"/>
<junction x="337.82" y="-421.64"/>
<label x="340.36" y="-421.64" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="TTC"/>
</segment>
</net>
<net name="VFB_BQ24616" class="0">
<segment>
<label x="340.36" y="-414.02" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="VFB"/>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="431.8" y1="-370.84" x2="431.8" y2="-373.38" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="431.8" y1="-373.38" x2="431.8" y2="-375.92" width="0.1524" layer="91"/>
<wire x1="441.96" y1="-368.3" x2="441.96" y2="-373.38" width="0.1524" layer="91"/>
<wire x1="441.96" y1="-373.38" x2="431.8" y2="-373.38" width="0.1524" layer="91"/>
<junction x="431.8" y="-373.38"/>
<wire x1="431.8" y1="-373.38" x2="426.72" y2="-373.38" width="0.1524" layer="91"/>
<wire x1="426.72" y1="-373.38" x2="426.72" y2="-414.02" width="0.1524" layer="91"/>
<wire x1="426.72" y1="-414.02" x2="335.28" y2="-414.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STAT1_BQ24616" class="0">
<segment>
<wire x1="241.3" y1="-416.56" x2="208.28" y2="-416.56" width="0.1524" layer="91"/>
<label x="213.36" y="-416.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="STAT2_BQ24616" class="0">
<segment>
<wire x1="236.22" y1="-421.64" x2="203.2" y2="-421.64" width="0.1524" layer="91"/>
<label x="210.82" y="-421.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="PG_BQ24616" class="0">
<segment>
<wire x1="241.3" y1="-426.72" x2="203.2" y2="-426.72" width="0.1524" layer="91"/>
<label x="210.82" y="-426.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="TS"/>
<wire x1="259.08" y1="-439.42" x2="256.54" y2="-439.42" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="256.54" y1="-439.42" x2="256.54" y2="-441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HIDRV_BQ24616" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="HIDRV"/>
<wire x1="335.28" y1="-335.28" x2="375.92" y2="-335.28" width="0.1524" layer="91"/>
<label x="340.36" y="-335.28" size="1.778" layer="95"/>
<pinref part="Q4" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="BATDRV"/>
<wire x1="340.36" y1="-322.58" x2="335.28" y2="-322.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="408.94" y1="-317.5" x2="408.94" y2="-320.04" width="0.1524" layer="91"/>
<wire x1="408.94" y1="-320.04" x2="419.1" y2="-320.04" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="419.1" y1="-320.04" x2="419.1" y2="-314.96" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="350.52" y1="-322.58" x2="408.94" y2="-322.58" width="0.1524" layer="91"/>
<wire x1="408.94" y1="-320.04" x2="408.94" y2="-322.58" width="0.1524" layer="91"/>
<junction x="408.94" y="-320.04"/>
<pinref part="Q3" gate="G$1" pin="G"/>
<wire x1="426.72" y1="-322.58" x2="408.94" y2="-322.58" width="0.1524" layer="91"/>
<junction x="408.94" y="-322.58"/>
</segment>
</net>
<net name="VOUT_TPS55332-Q1" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="73.66" y1="-355.6" x2="73.66" y2="-347.98" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="VREG"/>
<wire x1="73.66" y1="-347.98" x2="58.42" y2="-347.98" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="83.82" y1="-388.62" x2="83.82" y2="-347.98" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-347.98" x2="73.66" y2="-347.98" width="0.1524" layer="91"/>
<junction x="73.66" y="-347.98"/>
<junction x="83.82" y="-347.98"/>
<wire x1="83.82" y1="-347.98" x2="96.52" y2="-347.98" width="0.1524" layer="91"/>
<label x="104.14" y="-347.98" size="1.778" layer="95"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="96.52" y1="-347.98" x2="132.08" y2="-347.98" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-347.98" x2="132.08" y2="-350.52" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="132.08" y1="-347.98" x2="142.24" y2="-347.98" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-347.98" x2="142.24" y2="-350.52" width="0.1524" layer="91"/>
<junction x="132.08" y="-347.98"/>
<pinref part="U$12" gate="G$1" pin="CATHODE"/>
<wire x1="76.2" y1="-320.04" x2="83.82" y2="-320.04" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-320.04" x2="83.82" y2="-347.98" width="0.1524" layer="91"/>
<label x="83.82" y="-345.44" size="1.778" layer="95" rot="R90"/>
<wire x1="96.52" y1="-347.98" x2="96.52" y2="-350.52" width="0.1524" layer="91"/>
<junction x="96.52" y="-347.98"/>
</segment>
</net>
<net name="VIN_TPS55332-Q2" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="-317.5" x2="-55.88" y2="-317.5" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-317.5" x2="-63.5" y2="-317.5" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-317.5" x2="-71.12" y2="-317.5" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-322.58" x2="-71.12" y2="-317.5" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-322.58" x2="-63.5" y2="-317.5" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-322.58" x2="-55.88" y2="-317.5" width="0.1524" layer="91"/>
<junction x="-63.5" y="-317.5"/>
<junction x="-55.88" y="-317.5"/>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="L1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="332.74" y1="-302.26" x2="327.66" y2="-302.26" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="REGN"/>
<wire x1="327.66" y1="-302.26" x2="327.66" y2="-309.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BTST_BQ24616" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BTST"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="320.04" y1="-284.48" x2="317.5" y2="-284.48" width="0.1524" layer="91"/>
<wire x1="317.5" y1="-284.48" x2="317.5" y2="-309.88" width="0.1524" layer="91"/>
<label x="317.5" y="-307.34" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="LODRV_FET_DRAIN" class="0">
<segment>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="327.66" y1="-284.48" x2="355.6" y2="-284.48" width="0.1524" layer="91"/>
<label x="330.2" y="-284.48" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="398.78" y1="-347.98" x2="381" y2="-347.98" width="0.1524" layer="91"/>
<wire x1="381" y1="-347.98" x2="381" y2="-353.06" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="PH"/>
<wire x1="381" y1="-347.98" x2="335.28" y2="-347.98" width="0.1524" layer="91"/>
<label x="340.36" y="-347.98" size="1.778" layer="95"/>
<junction x="381" y="-347.98"/>
<pinref part="Q4" gate="G$1" pin="S"/>
<wire x1="381" y1="-337.82" x2="381" y2="-347.98" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="1"/>
<pinref part="Q5" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="419.1" y1="-347.98" x2="416.56" y2="-347.98" width="0.1524" layer="91"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="416.56" y1="-347.98" x2="414.02" y2="-347.98" width="0.1524" layer="91"/>
<wire x1="416.56" y1="-347.98" x2="416.56" y2="-375.92" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="337.82" y1="-381" x2="337.82" y2="-375.92" width="0.1524" layer="91"/>
<wire x1="337.82" y1="-375.92" x2="335.28" y2="-375.92" width="0.1524" layer="91"/>
<wire x1="337.82" y1="-375.92" x2="353.06" y2="-375.92" width="0.1524" layer="91"/>
<wire x1="353.06" y1="-375.92" x2="353.06" y2="-365.76" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="353.06" y1="-365.76" x2="347.98" y2="-365.76" width="0.1524" layer="91"/>
<junction x="337.82" y="-375.92"/>
<pinref part="U3" gate="G$1" pin="SRP"/>
<wire x1="416.56" y1="-375.92" x2="353.06" y2="-375.92" width="0.1524" layer="91"/>
<junction x="353.06" y="-375.92"/>
<junction x="416.56" y="-347.98"/>
<pinref part="L5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="BAT_PACK_P" class="0">
<segment>
<wire x1="429.26" y1="-347.98" x2="431.8" y2="-347.98" width="0.1524" layer="91"/>
<pinref part="U$27" gate="G$1" pin="1"/>
<wire x1="431.8" y1="-347.98" x2="447.04" y2="-347.98" width="0.1524" layer="91"/>
<wire x1="447.04" y1="-347.98" x2="447.04" y2="-350.52" width="0.1524" layer="91"/>
<wire x1="447.04" y1="-347.98" x2="462.28" y2="-347.98" width="0.1524" layer="91"/>
<pinref part="U$29" gate="G$1" pin="1"/>
<wire x1="462.28" y1="-347.98" x2="462.28" y2="-350.52" width="0.1524" layer="91"/>
<junction x="447.04" y="-347.98"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="474.98" y1="-350.52" x2="474.98" y2="-347.98" width="0.1524" layer="91"/>
<wire x1="474.98" y1="-347.98" x2="462.28" y2="-347.98" width="0.1524" layer="91"/>
<label x="434.34" y="-347.98" size="1.778" layer="95"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="431.8" y1="-360.68" x2="431.8" y2="-355.6" width="0.1524" layer="91"/>
<junction x="431.8" y="-347.98"/>
<pinref part="R45" gate="G$1" pin="2"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="431.8" y1="-355.6" x2="431.8" y2="-347.98" width="0.1524" layer="91"/>
<wire x1="441.96" y1="-360.68" x2="441.96" y2="-355.6" width="0.1524" layer="91"/>
<wire x1="441.96" y1="-355.6" x2="431.8" y2="-355.6" width="0.1524" layer="91"/>
<junction x="431.8" y="-355.6"/>
<wire x1="431.8" y1="-355.6" x2="421.64" y2="-355.6" width="0.1524" layer="91"/>
<wire x1="421.64" y1="-355.6" x2="421.64" y2="-393.7" width="0.1524" layer="91"/>
<wire x1="335.28" y1="-393.7" x2="337.82" y2="-393.7" width="0.1524" layer="91"/>
<wire x1="337.82" y1="-393.7" x2="350.52" y2="-393.7" width="0.1524" layer="91"/>
<wire x1="350.52" y1="-393.7" x2="350.52" y2="-398.78" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="350.52" y1="-398.78" x2="347.98" y2="-398.78" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="337.82" y1="-388.62" x2="337.82" y2="-393.7" width="0.1524" layer="91"/>
<junction x="337.82" y="-393.7"/>
<pinref part="U3" gate="G$1" pin="SRN"/>
<wire x1="421.64" y1="-393.7" x2="350.52" y2="-393.7" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="D"/>
<wire x1="431.8" y1="-330.2" x2="431.8" y2="-347.98" width="0.1524" layer="91"/>
<junction x="350.52" y="-393.7"/>
<junction x="462.28" y="-347.98"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="S"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="264.16" y1="-284.48" x2="261.62" y2="-284.48" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="261.62" y1="-284.48" x2="254" y2="-284.48" width="0.1524" layer="91"/>
<wire x1="254" y1="-284.48" x2="248.92" y2="-284.48" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-284.48" x2="246.38" y2="-284.48" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-287.02" x2="248.92" y2="-284.48" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="261.62" y1="-287.02" x2="261.62" y2="-284.48" width="0.1524" layer="91"/>
<junction x="248.92" y="-284.48"/>
<junction x="261.62" y="-284.48"/>
<wire x1="254" y1="-279.4" x2="254" y2="-284.48" width="0.1524" layer="91"/>
<junction x="254" y="-284.48"/>
<pinref part="R30" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ADAPTER_IN_BQ24616" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="236.22" y1="-284.48" x2="215.9" y2="-284.48" width="0.1524" layer="91"/>
<label x="210.82" y="-284.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="ACDRV"/>
<wire x1="276.86" y1="-309.88" x2="276.86" y2="-302.26" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="276.86" y1="-302.26" x2="274.32" y2="-302.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="264.16" y1="-302.26" x2="261.62" y2="-302.26" width="0.1524" layer="91"/>
<wire x1="261.62" y1="-302.26" x2="248.92" y2="-302.26" width="0.1524" layer="91"/>
<wire x1="248.92" y1="-302.26" x2="238.76" y2="-302.26" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-302.26" x2="238.76" y2="-289.56" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="266.7" y1="-289.56" x2="266.7" y2="-297.18" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-297.18" x2="261.62" y2="-297.18" width="0.1524" layer="91"/>
<wire x1="261.62" y1="-297.18" x2="261.62" y2="-302.26" width="0.1524" layer="91"/>
<junction x="261.62" y="-302.26"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="248.92" y1="-297.18" x2="248.92" y2="-302.26" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="261.62" y1="-294.64" x2="261.62" y2="-297.18" width="0.1524" layer="91"/>
<junction x="261.62" y="-297.18"/>
<junction x="248.92" y="-302.26"/>
</segment>
</net>
<net name="V_SYSTEM_OUT" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="D"/>
<wire x1="381" y1="-327.66" x2="381" y2="-325.12" width="0.1524" layer="91"/>
<wire x1="381" y1="-325.12" x2="353.06" y2="-325.12" width="0.1524" layer="91"/>
<label x="358.14" y="-325.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="1"/>
<pinref part="U$7" gate="G$1" pin="1"/>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="302.26" y1="-289.56" x2="307.34" y2="-289.56" width="0.1524" layer="91"/>
<wire x1="307.34" y1="-289.56" x2="307.34" y2="-309.88" width="0.1524" layer="91"/>
<wire x1="304.8" y1="-276.86" x2="307.34" y2="-276.86" width="0.1524" layer="91"/>
<wire x1="307.34" y1="-276.86" x2="307.34" y2="-289.56" width="0.1524" layer="91"/>
<junction x="307.34" y="-289.56"/>
<pinref part="R38" gate="G$1" pin="2"/>
<pinref part="U3" gate="G$1" pin="ACN"/>
<wire x1="307.34" y1="-276.86" x2="373.38" y2="-276.86" width="0.1524" layer="91"/>
<label x="375.92" y="-276.86" size="1.778" layer="95"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="373.38" y1="-276.86" x2="388.62" y2="-276.86" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-276.86" x2="401.32" y2="-276.86" width="0.1524" layer="91"/>
<wire x1="401.32" y1="-276.86" x2="419.1" y2="-276.86" width="0.1524" layer="91"/>
<wire x1="419.1" y1="-307.34" x2="419.1" y2="-304.8" width="0.1524" layer="91"/>
<wire x1="419.1" y1="-304.8" x2="408.94" y2="-304.8" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="408.94" y1="-304.8" x2="408.94" y2="-307.34" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="S"/>
<wire x1="431.8" y1="-320.04" x2="431.8" y2="-304.8" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-304.8" x2="419.1" y2="-304.8" width="0.1524" layer="91"/>
<wire x1="419.1" y1="-304.8" x2="419.1" y2="-276.86" width="0.1524" layer="91"/>
<junction x="419.1" y="-304.8"/>
<junction x="307.34" y="-276.86"/>
<wire x1="373.38" y1="-279.4" x2="373.38" y2="-276.86" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-287.02" x2="388.62" y2="-276.86" width="0.1524" layer="91"/>
<wire x1="401.32" y1="-287.02" x2="401.32" y2="-276.86" width="0.1524" layer="91"/>
<junction x="401.32" y="-276.86"/>
<junction x="388.62" y="-276.86"/>
<junction x="373.38" y="-276.86"/>
</segment>
</net>
<net name="VIN_TPS55332-Q1" class="0">
<segment>
<wire x1="22.86" y1="-320.04" x2="0" y2="-320.04" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="U20" gate="A" pin="VIN"/>
<wire x1="0" y1="-337.82" x2="2.54" y2="-337.82" width="0.1524" layer="91"/>
<wire x1="0" y1="-337.82" x2="0" y2="-320.04" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-322.58" x2="-20.32" y2="-320.04" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-320.04" x2="-12.7" y2="-320.04" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="-320.04" x2="-5.08" y2="-320.04" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-320.04" x2="0" y2="-320.04" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-322.58" x2="-12.7" y2="-320.04" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="-322.58" x2="-5.08" y2="-320.04" width="0.1524" layer="91"/>
<junction x="-12.7" y="-320.04"/>
<junction x="-5.08" y="-320.04"/>
<label x="-15.24" y="-320.04" size="1.778" layer="95"/>
<wire x1="-20.32" y1="-320.04" x2="-20.32" y2="-317.5" width="0.1524" layer="91"/>
<junction x="-20.32" y="-320.04"/>
<wire x1="-20.32" y1="-317.5" x2="-30.48" y2="-317.5" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<junction x="0" y="-320.04"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="-1729.74" y1="256.54" x2="-1729.74" y2="251.46" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="SRN"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="-1722.12" y1="248.92" x2="-1722.12" y2="251.46" width="0.1524" layer="91"/>
<wire x1="-1722.12" y1="251.46" x2="-1729.74" y2="251.46" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="-1729.74" y1="251.46" x2="-1729.74" y2="248.92" width="0.1524" layer="91"/>
<junction x="-1729.74" y="251.46"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="SRP"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="-1750.06" y1="251.46" x2="-1750.06" y2="256.54" width="0.1524" layer="91"/>
<wire x1="-1757.68" y1="248.92" x2="-1757.68" y2="251.46" width="0.1524" layer="91"/>
<wire x1="-1757.68" y1="251.46" x2="-1750.06" y2="251.46" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="-1750.06" y1="251.46" x2="-1750.06" y2="248.92" width="0.1524" layer="91"/>
<junction x="-1750.06" y="251.46"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-1765.3" y1="292.1" x2="-1765.3" y2="294.64" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="-1765.3" y1="294.64" x2="-1772.92" y2="294.64" width="0.1524" layer="91"/>
<wire x1="-1765.3" y1="294.64" x2="-1760.22" y2="294.64" width="0.1524" layer="91"/>
<junction x="-1765.3" y="294.64"/>
<pinref part="U$2" gate="G$1" pin="VC0"/>
</segment>
</net>
<net name="V_CELL0" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<label x="-1793.24" y="294.64" size="1.778" layer="95"/>
<wire x1="-1783.08" y1="294.64" x2="-1795.78" y2="294.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VC1" class="0">
<segment>
<wire x1="-1760.22" y1="309.88" x2="-1762.76" y2="309.88" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="-1762.76" y1="309.88" x2="-1762.76" y2="307.34" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="-1762.76" y1="309.88" x2="-1772.92" y2="309.88" width="0.1524" layer="91"/>
<junction x="-1762.76" y="309.88"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="-1762.76" y1="309.88" x2="-1762.76" y2="320.04" width="0.1524" layer="91"/>
<label x="-1762.76" y="312.42" size="1.778" layer="95" rot="R90"/>
<pinref part="U$2" gate="G$1" pin="VC1"/>
</segment>
<segment>
<pinref part="R60" gate="G$1" pin="1"/>
<wire x1="-1681.48" y1="294.64" x2="-1671.32" y2="294.64" width="0.1524" layer="91"/>
<label x="-1678.94" y="294.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="-1762.76" y1="327.66" x2="-1762.76" y2="330.2" width="0.1524" layer="91"/>
<wire x1="-1762.76" y1="330.2" x2="-1760.22" y2="330.2" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="-1762.76" y1="330.2" x2="-1772.92" y2="330.2" width="0.1524" layer="91"/>
<wire x1="-1762.76" y1="335.28" x2="-1762.76" y2="330.2" width="0.1524" layer="91"/>
<junction x="-1762.76" y="330.2"/>
<pinref part="U$2" gate="G$1" pin="VC2"/>
</segment>
</net>
<net name="VCC_5" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="-1656.08" y1="393.7" x2="-1656.08" y2="411.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BQ78350_LED5" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="B"/>
<wire x1="-1584.96" y1="335.28" x2="-1564.64" y2="335.28" width="0.1524" layer="91"/>
<label x="-1582.42" y="335.28" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="LED5"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="-1762.76" y1="335.28" x2="-1762.76" y2="340.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="-1762.76" y1="347.98" x2="-1762.76" y2="350.52" width="0.1524" layer="91"/>
<wire x1="-1762.76" y1="350.52" x2="-1760.22" y2="350.52" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="-1762.76" y1="350.52" x2="-1772.92" y2="350.52" width="0.1524" layer="91"/>
<wire x1="-1762.76" y1="355.6" x2="-1762.76" y2="350.52" width="0.1524" layer="91"/>
<junction x="-1762.76" y="350.52"/>
<pinref part="U$2" gate="G$1" pin="VC3"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="-1762.76" y1="355.6" x2="-1762.76" y2="360.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="-1762.76" y1="368.3" x2="-1762.76" y2="370.84" width="0.1524" layer="91"/>
<wire x1="-1762.76" y1="370.84" x2="-1760.22" y2="370.84" width="0.1524" layer="91"/>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="-1762.76" y1="370.84" x2="-1772.92" y2="370.84" width="0.1524" layer="91"/>
<wire x1="-1762.76" y1="375.92" x2="-1762.76" y2="370.84" width="0.1524" layer="91"/>
<junction x="-1762.76" y="370.84"/>
<pinref part="U$2" gate="G$1" pin="VC4"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="-1762.76" y1="375.92" x2="-1762.76" y2="381" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="-1762.76" y1="388.62" x2="-1762.76" y2="391.16" width="0.1524" layer="91"/>
<wire x1="-1762.76" y1="391.16" x2="-1760.22" y2="391.16" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="-1762.76" y1="391.16" x2="-1772.92" y2="391.16" width="0.1524" layer="91"/>
<junction x="-1762.76" y="391.16"/>
<pinref part="U$2" gate="G$1" pin="VC5"/>
</segment>
</net>
<net name="BQ78350_LED3" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="LED3"/>
<pinref part="U$25" gate="G$1" pin="B"/>
<wire x1="-1564.64" y1="350.52" x2="-1615.44" y2="350.52" width="0.1524" layer="91"/>
<label x="-1582.42" y="350.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="BQ78350_LED4" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="LED4"/>
<pinref part="U$24" gate="G$1" pin="B"/>
<wire x1="-1564.64" y1="342.9" x2="-1600.2" y2="342.9" width="0.1524" layer="91"/>
<label x="-1582.42" y="342.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="BQ78350_LED2" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="LED2"/>
<pinref part="U$26" gate="G$1" pin="B"/>
<wire x1="-1564.64" y1="358.14" x2="-1630.68" y2="358.14" width="0.1524" layer="91"/>
<label x="-1582.42" y="358.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="BQ78350_LED1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="LED1"/>
<pinref part="U$28" gate="G$1" pin="B"/>
<wire x1="-1564.64" y1="365.76" x2="-1645.92" y2="365.76" width="0.1524" layer="91"/>
<label x="-1582.42" y="365.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC_1" class="0">
<segment>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="-1640.84" y1="393.7" x2="-1640.84" y2="411.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_2" class="0">
<segment>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="-1625.6" y1="393.7" x2="-1625.6" y2="411.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_3" class="0">
<segment>
<pinref part="R58" gate="G$1" pin="1"/>
<wire x1="-1610.36" y1="393.7" x2="-1610.36" y2="411.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_4" class="0">
<segment>
<pinref part="R59" gate="G$1" pin="1"/>
<wire x1="-1595.12" y1="393.7" x2="-1595.12" y2="411.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="R60" gate="G$1" pin="2"/>
<wire x1="-1704.34" y1="294.64" x2="-1694.18" y2="294.64" width="0.1524" layer="91"/>
<wire x1="-1694.18" y1="294.64" x2="-1691.64" y2="294.64" width="0.1524" layer="91"/>
<wire x1="-1694.18" y1="292.1" x2="-1694.18" y2="294.64" width="0.1524" layer="91"/>
<junction x="-1694.18" y="294.64"/>
<pinref part="U$2" gate="G$1" pin="TS1"/>
<pinref part="R47" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="Q13" gate="G$1" pin="S"/>
<wire x1="-1704.34" y1="271.78" x2="-1663.7" y2="271.78" width="0.1524" layer="91"/>
<wire x1="-1663.7" y1="271.78" x2="-1663.7" y2="266.7" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="CHG"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="Q13" gate="G$1" pin="D"/>
<pinref part="R62" gate="G$1" pin="1"/>
<wire x1="-1663.7" y1="256.54" x2="-1663.7" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="Q15" gate="G$1" pin="G"/>
<wire x1="-1694.18" y1="215.9" x2="-1694.18" y2="220.98" width="0.1524" layer="91"/>
<wire x1="-1694.18" y1="220.98" x2="-1694.18" y2="264.16" width="0.1524" layer="91"/>
<wire x1="-1694.18" y1="264.16" x2="-1704.34" y2="264.16" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="2"/>
<wire x1="-1694.18" y1="220.98" x2="-1696.72" y2="220.98" width="0.1524" layer="91"/>
<junction x="-1694.18" y="220.98"/>
<pinref part="U$2" gate="G$1" pin="DSG"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="Q15" gate="G$1" pin="D"/>
<wire x1="-1709.42" y1="210.82" x2="-1701.8" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="1"/>
<wire x1="-1706.88" y1="220.98" x2="-1709.42" y2="220.98" width="0.1524" layer="91"/>
<wire x1="-1709.42" y1="220.98" x2="-1709.42" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="2"/>
<pinref part="R63" gate="G$1" pin="1"/>
<wire x1="-1663.7" y1="241.3" x2="-1663.7" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R63" gate="G$1" pin="2"/>
<pinref part="Q14" gate="G$1" pin="G"/>
<wire x1="-1663.7" y1="226.06" x2="-1663.7" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="Q14" gate="G$1" pin="D"/>
<pinref part="Q15" gate="G$1" pin="S"/>
<wire x1="-1671.32" y1="210.82" x2="-1691.64" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_CELL1" class="0">
<segment>
<wire x1="-1783.08" y1="309.88" x2="-1795.78" y2="309.88" width="0.1524" layer="91"/>
<label x="-1793.24" y="309.88" size="1.778" layer="95"/>
<pinref part="R37" gate="G$1" pin="2"/>
</segment>
</net>
<net name="V_CELL2" class="0">
<segment>
<wire x1="-1783.08" y1="330.2" x2="-1795.78" y2="330.2" width="0.1524" layer="91"/>
<label x="-1793.24" y="330.2" size="1.778" layer="95"/>
<pinref part="R35" gate="G$1" pin="2"/>
</segment>
</net>
<net name="V_CELL3" class="0">
<segment>
<wire x1="-1783.08" y1="350.52" x2="-1795.78" y2="350.52" width="0.1524" layer="91"/>
<label x="-1793.24" y="350.52" size="1.778" layer="95"/>
<pinref part="R46" gate="G$1" pin="2"/>
</segment>
</net>
<net name="V_CELL4" class="0">
<segment>
<wire x1="-1783.08" y1="370.84" x2="-1795.78" y2="370.84" width="0.1524" layer="91"/>
<label x="-1793.24" y="370.84" size="1.778" layer="95"/>
<pinref part="R50" gate="G$1" pin="2"/>
</segment>
</net>
<net name="V_CELL5" class="0">
<segment>
<wire x1="-1783.08" y1="391.16" x2="-1795.78" y2="391.16" width="0.1524" layer="91"/>
<label x="-1793.24" y="391.16" size="1.778" layer="95"/>
<pinref part="R53" gate="G$1" pin="2"/>
</segment>
</net>
<net name="BQ76920_CAP1" class="0">
<segment>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="-1704.34" y1="345.44" x2="-1701.8" y2="345.44" width="0.1524" layer="91"/>
<wire x1="-1701.8" y1="345.44" x2="-1696.72" y2="345.44" width="0.1524" layer="91"/>
<wire x1="-1696.72" y1="345.44" x2="-1696.72" y2="342.9" width="0.1524" layer="91"/>
<wire x1="-1701.8" y1="345.44" x2="-1701.8" y2="327.66" width="0.1524" layer="91"/>
<junction x="-1701.8" y="345.44"/>
<label x="-1701.8" y="327.66" size="1.778" layer="95" rot="R90"/>
<pinref part="U$2" gate="G$1" pin="CAP1"/>
</segment>
</net>
<net name="BQ76920_REGOUT" class="0">
<segment>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="-1704.34" y1="368.3" x2="-1701.8" y2="368.3" width="0.1524" layer="91"/>
<wire x1="-1701.8" y1="368.3" x2="-1696.72" y2="368.3" width="0.1524" layer="91"/>
<wire x1="-1696.72" y1="368.3" x2="-1696.72" y2="365.76" width="0.1524" layer="91"/>
<wire x1="-1701.8" y1="368.3" x2="-1701.8" y2="347.98" width="0.1524" layer="91"/>
<junction x="-1701.8" y="368.3"/>
<label x="-1701.8" y="347.98" size="1.778" layer="95" rot="R90"/>
<pinref part="U$2" gate="G$1" pin="REGOUT"/>
</segment>
<segment>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="-1579.88" y1="444.5" x2="-1582.42" y2="444.5" width="0.1524" layer="91"/>
<wire x1="-1582.42" y1="444.5" x2="-1582.42" y2="447.04" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VCC"/>
<wire x1="-1582.42" y1="447.04" x2="-1564.64" y2="447.04" width="0.1524" layer="91"/>
<wire x1="-1582.42" y1="447.04" x2="-1607.82" y2="447.04" width="0.1524" layer="91"/>
<junction x="-1582.42" y="447.04"/>
<label x="-1605.28" y="447.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="BQ76920_REGSRC" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="REGSRC"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="-1704.34" y1="381" x2="-1686.56" y2="381" width="0.1524" layer="91"/>
<wire x1="-1686.56" y1="381" x2="-1686.56" y2="378.46" width="0.1524" layer="91"/>
<label x="-1701.8" y="381" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="MRST"/>
<wire x1="-1564.64" y1="439.42" x2="-1567.18" y2="439.42" width="0.1524" layer="91"/>
<wire x1="-1567.18" y1="439.42" x2="-1567.18" y2="444.5" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="-1567.18" y1="444.5" x2="-1569.72" y2="444.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="-1744.98" y1="233.68" x2="-1750.06" y2="233.68" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="-1750.06" y1="238.76" x2="-1750.06" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="-1729.74" y1="233.68" x2="-1734.82" y2="233.68" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="-1729.74" y1="233.68" x2="-1729.74" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="BAT"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="-1704.34" y1="388.62" x2="-1676.4" y2="388.62" width="0.1524" layer="91"/>
<wire x1="-1676.4" y1="388.62" x2="-1676.4" y2="386.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="ILIM"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="-254" y1="-406.4" x2="-259.08" y2="-406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="COMM1"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="-254" y1="-322.58" x2="-259.08" y2="-322.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="C53" gate="G$1" pin="2"/>
<pinref part="U$16" gate="G$1" pin="BOOT1"/>
<wire x1="-259.08" y1="-330.2" x2="-254" y2="-330.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="AC1"/>
<wire x1="-254" y1="-340.36" x2="-271.78" y2="-340.36" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="-340.36" x2="-271.78" y2="-330.2" width="0.1524" layer="91"/>
<pinref part="C53" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="-330.2" x2="-266.7" y2="-330.2" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="-266.7" y1="-322.58" x2="-271.78" y2="-322.58" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="-322.58" x2="-271.78" y2="-330.2" width="0.1524" layer="91"/>
<junction x="-271.78" y="-330.2"/>
<junction x="-271.78" y="-340.36"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="-340.36" x2="-271.78" y2="-345.44" width="0.1524" layer="91"/>
<pinref part="C58" gate="G$1" pin="2"/>
<wire x1="-271.78" y1="-340.36" x2="-276.86" y2="-340.36" width="0.1524" layer="91"/>
<pinref part="C59" gate="G$1" pin="1"/>
<wire x1="-276.86" y1="-340.36" x2="-281.94" y2="-340.36" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="-396.24" x2="-276.86" y2="-396.24" width="0.1524" layer="91"/>
<wire x1="-276.86" y1="-396.24" x2="-276.86" y2="-340.36" width="0.1524" layer="91"/>
<junction x="-276.86" y="-340.36"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="-271.78" y1="-353.06" x2="-271.78" y2="-360.68" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="AC2"/>
<wire x1="-271.78" y1="-360.68" x2="-254" y2="-360.68" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="-360.68" x2="-271.78" y2="-370.84" width="0.1524" layer="91"/>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="-370.84" x2="-266.7" y2="-370.84" width="0.1524" layer="91"/>
<pinref part="C56" gate="G$1" pin="1"/>
<wire x1="-266.7" y1="-378.46" x2="-271.78" y2="-378.46" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="-378.46" x2="-271.78" y2="-370.84" width="0.1524" layer="91"/>
<junction x="-271.78" y="-360.68"/>
<junction x="-271.78" y="-370.84"/>
<junction x="-271.78" y="-378.46"/>
<wire x1="-271.78" y1="-378.46" x2="-271.78" y2="-388.62" width="0.1524" layer="91"/>
<pinref part="C57" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="-388.62" x2="-266.7" y2="-388.62" width="0.1524" layer="91"/>
<pinref part="U$18" gate="A" pin="1"/>
<wire x1="-297.18" y1="-358.14" x2="-297.18" y2="-360.68" width="0.1524" layer="91"/>
<wire x1="-297.18" y1="-360.68" x2="-271.78" y2="-360.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="C55" gate="G$1" pin="2"/>
<pinref part="U$16" gate="G$1" pin="BOOT2"/>
<wire x1="-259.08" y1="-370.84" x2="-254" y2="-370.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="COMM2"/>
<pinref part="C56" gate="G$1" pin="2"/>
<wire x1="-254" y1="-378.46" x2="-259.08" y2="-378.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="C57" gate="G$1" pin="2"/>
<pinref part="U$16" gate="G$1" pin="CLAMP2"/>
<wire x1="-259.08" y1="-388.62" x2="-254" y2="-388.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="C58" gate="G$1" pin="1"/>
<wire x1="-289.56" y1="-340.36" x2="-297.18" y2="-340.36" width="0.1524" layer="91"/>
<pinref part="U$18" gate="A" pin="2"/>
<wire x1="-297.18" y1="-340.36" x2="-297.18" y2="-342.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="CLAMP1"/>
<pinref part="C59" gate="G$1" pin="2"/>
<wire x1="-254" y1="-396.24" x2="-259.08" y2="-396.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="-269.24" y1="-406.4" x2="-271.78" y2="-406.4" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="-406.4" x2="-271.78" y2="-439.42" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="2"/>
<pinref part="U$16" gate="G$1" pin="FOD"/>
<wire x1="-233.68" y1="-424.18" x2="-233.68" y2="-439.42" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="-439.42" x2="-233.68" y2="-441.96" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="-439.42" x2="-233.68" y2="-439.42" width="0.1524" layer="91"/>
<junction x="-233.68" y="-439.42"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="-228.6" y1="-439.42" x2="-233.68" y2="-439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="-190.5" y1="-365.76" x2="-190.5" y2="-358.14" width="0.1524" layer="91"/>
<pinref part="U$16" gate="G$1" pin="TS/CTRL"/>
<wire x1="-190.5" y1="-358.14" x2="-193.04" y2="-358.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BQ51013B_CHG" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="CHG"/>
<wire x1="-193.04" y1="-386.08" x2="-165.1" y2="-386.08" width="0.1524" layer="91"/>
<label x="-185.42" y="-386.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="BQ51013B_EN1" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="EN1"/>
<wire x1="-193.04" y1="-393.7" x2="-165.1" y2="-393.7" width="0.1524" layer="91"/>
<label x="-185.42" y="-393.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="BQ51013B_EN2" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="EN2"/>
<wire x1="-193.04" y1="-401.32" x2="-165.1" y2="-401.32" width="0.1524" layer="91"/>
<label x="-185.42" y="-401.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="BQ51013B_RECT" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="RECT"/>
<wire x1="-193.04" y1="-337.82" x2="-165.1" y2="-337.82" width="0.1524" layer="91"/>
<label x="-185.42" y="-337.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C60" gate="G$1" pin="1"/>
<wire x1="-213.36" y1="-444.5" x2="-213.36" y2="-439.42" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="-213.36" y1="-439.42" x2="-218.44" y2="-439.42" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="-439.42" x2="-187.96" y2="-439.42" width="0.1524" layer="91"/>
<junction x="-213.36" y="-439.42"/>
<label x="-208.28" y="-439.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="BQ51013B_OUT" class="0">
<segment>
<pinref part="U$16" gate="G$1" pin="OUT"/>
<wire x1="-193.04" y1="-317.5" x2="-187.96" y2="-317.5" width="0.1524" layer="91"/>
<pinref part="C61" gate="G$1" pin="1"/>
<wire x1="-187.96" y1="-317.5" x2="-187.96" y2="-320.04" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="-317.5" x2="-165.1" y2="-317.5" width="0.1524" layer="91"/>
<junction x="-187.96" y="-317.5"/>
<label x="-185.42" y="-317.5" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,-119.38,121.92,U1,DVCC,VCC_3P3,,,"/>
<approved hash="104,1,124.46,121.92,U1,DVSS,GND,,,"/>
<approved hash="104,1,-607.06,-383.54,U2,VSS_2,GND,,,"/>
<approved hash="104,1,-675.64,-353.06,U2,VIN_DC,SOLAR_CELL_ANODE,,,"/>
<approved hash="104,1,-607.06,-325.12,U2,VOUT,V_LOAD,,,"/>
<approved hash="104,1,-607.06,-388.62,U2,VSS,GND,,,"/>
<approved hash="104,1,-635,-309.88,U2,VSTOR,VOC_SAMP__VSTOR,,,"/>
<approved hash="104,1,-607.06,-393.7,U2,PAD,GND,,,"/>
<approved hash="104,1,411.48,-408.94,U4,VSS3,GND,,,"/>
<approved hash="104,1,396.24,-408.94,U4,VSS1,GND,,,"/>
<approved hash="104,1,403.86,-408.94,U4,VSS2,GND,,,"/>
<approved hash="104,1,-416.56,-439.42,U$20,GND1,GND,,,"/>
<approved hash="104,1,-401.32,-439.42,U$20,GND3,GND,,,"/>
<approved hash="104,1,-408.94,-439.42,U$20,GND2,GND,,,"/>
<approved hash="104,1,-391.16,-439.42,U$20,PGND,GND,,,"/>
<approved hash="104,1,-383.54,-439.42,U$20,PAD,GND,,,"/>
<approved hash="104,1,-594.36,-81.28,U6,VCC,VCC_2P2,,,"/>
<approved hash="104,1,-167.64,-312.42,U3,VCC,VCC_BQ24616,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
