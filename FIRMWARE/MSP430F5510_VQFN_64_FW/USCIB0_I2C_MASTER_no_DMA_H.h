#ifndef USCI_LIB
#define USCI_LIB

/*
==============================================================================================
									DEFINE VARIABLES.
==============================================================================================
*/

#define SDA_PIN 				0x80  // MSP430G2955 UCB0SDA pin, P1
#define SCL_PIN 				0x40  // MSP430G2955 UCB0SCL pin, P1

#define I2C_ADDRESS_MSP430G2955 0xF0  // MSP430G2955 UCB0SCL pin

// DMA defines

// DMACTL0 16-bit register
// DMA2TSELx .. pg. 303 family user guide .. DMACTL0 register
#define DMA2TSEL_0 				0x0000 	  //
#define DMA2TSEL_1 				0x0100 	  //
#define DMA2TSEL_2 				0x0200 	  //
#define DMA2TSEL_3 				0x0300 	  //
#define DMA2TSEL_4 				0x0400 	  //
#define DMA2TSEL_5 				0x0500 	  //
#define DMA2TSEL_6 				0x0600 	  //
#define DMA2TSEL_7 				0x0700 	  //
#define DMA2TSEL_8 				0x0800 	  //
#define DMA2TSEL_9 				0x0900 	  //
#define DMA2TSEL_10 			0x0A00 	  //
#define DMA2TSEL_11 			0x0B00 	  //
#define DMA2TSEL_12 			0x0C00 	  // Serial data received  ...  UCB0RXIFG
#define DMA2TSEL_13 			0x0D00 	  // Serial data transmit ready UCB0TXIFG
#define DMA2TSEL_14 			0x0E00 	  //
#define DMA2TSEL_15 			0x0F00 	  //

// DMADSTINCRx .. pg. 304 family user guide .. DMA0CTL register
#define DMADSTINCR_0			0x0000    //
#define DMADSTINCR_1			0x0400    //
#define DMADSTINCR_2			0x0800    //
#define DMADSTINCR_3			0x0C00    //

#define DMADSTBYTE              0x0080    // DMA destination byte.  Selects the
										  // destination as a byte (1) or word (0).
#define DMASRCBYTE				0x0040	  // DMA source byte. Selects the
										  // source as a byte (1) or word (0).
#define DMAIE					0x0004	  // DMA Interrupt enable (1) or disable(0).
#define DMAEN					0x0010	  // DMA Interrupt enable.

/*
==============================================================================================
// SPI-Mode Bits
#define UCCKPH                 (0x80)         // Sync. Mode: Clock Phase
#define UCCKPL                 (0x40)         // Sync. Mode: Clock Polarity
#define UCMST                  (0x08)         // Sync. Mode: Master Select

// I2C-Mode Bits
#define UCA10                  (0x80)         // 10-bit Address Mode
#define UCSLA10                (0x40)         // 10-bit Slave Address Mode
#define UCMM                   (0x20)         // Multi-Master Environment
//#define res                  (0x10)    	  // reserved
#define UCMODE_0               (0x00)         // Sync. Mode: USCI Mode: 0
#define UCMODE_1               (0x02)         // Sync. Mode: USCI Mode: 1
#define UCMODE_2               (0x04)         // Sync. Mode: USCI Mode: 2
#define UCMODE_3               (0x06)         // Sync. Mode: USCI Mode: 3

#define UCNACKIE               (0x08)         // NACK Condition interrupt enable

// UART-Mode Bits
#define UCSSEL1                (0x80)         // USCI 0 Clock Source Select 1
#define UCSSEL0                (0x40)         // USCI 0 Clock Source Select 0
#define UCRXEIE                (0x20)         // RX Error interrupt enable
#define UCBRKIE                (0x10)         // Break interrupt enable
#define UCDORM                 (0x08)         // Dormant (Sleep) Mode
#define UCTXADDR               (0x04)         // Send next Data as Address
#define UCTXBRK                (0x02)         // Send next Data as Break
#define UCSWRST                (0x01)         // USCI Software Reset
==============================================================================================
*/

/*
==============================================================================================
									DEFINE FUNCTIONS.
==============================================================================================
*/

/*
==============================================================================================
void USCI_I2C_DMA_transmitinit(unsigned char slave_address,
                                   unsigned char prescale)

 This function initializes the USCI module for master-transmit operation or master-receive mode.

 IN:   unsigned char slave_address   =>  Slave Address
       unsigned char prescale        =>  SCL clock adjustment
       boolean receive_INIT          =>  initialize for receiving or transmitting.
==============================================================================================
*/
void USCIB0_I2C__MASTER_INIT(unsigned char slave_address, unsigned char prescale, boolean receive_INIT);

/*
==============================================================================================
void USCI_I2C_DMA_receive(unsigned char byteCount, unsigned char *field)

 This function is used to start an I2C commuincation in master-receiver mode.

 IN:   unsigned char byteCount  =>  number of bytes that should be read
       unsigned char *field     =>  array variable used to store received data
==============================================================================================
*/
void USCIB0_I2C__RECEIVE(unsigned char byteCount, unsigned char *field);

/*
==============================================================================================
void USCI_I2C_DMA_transmit(unsigned char byteCount, unsigned char *field)

This function is used to start an I2C commuincation in master-transmit mode.

IN:   unsigned char byteCount  =>  number of bytes that should be transmitted
      unsigned char *field     =>  array variable. Its content will be sent.
==============================================================================================
*/
void USCIB0_I2C__TRANSMIT(unsigned char byteCount, unsigned char *field);

/*
==============================================================================================
unsigned char USCI_I2C_slave_present(unsigned char slave_address)

This function is used to look for a slave address on the I2C bus.

IN:   unsigned char slave_address  =>  Slave Address
OUT:  unsigned char                =>  0: address was not found,
                                       1: address found
==============================================================================================
*/
unsigned char USCIB0_I2C__SLAVE_PRESENT(unsigned char slave_address);

/*
==============================================================================================
unsigned char USCI_I2C_notready()

This function is used to check if there is communication in progress.

OUT:  unsigned char  =>  0: I2C bus is idle,
                         1: communication is in progress
==============================================================================================
*/
unsigned char USCIB0_I2C__NOT_READY();

#endif
