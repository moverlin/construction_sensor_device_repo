#include "msp430g2955.h"                       // device specific header
#include "USCI_I2C_master_dma_H.h"
#include <stdint.h>
#include <stdio.h>

unsigned char byteCtr;
unsigned char last;
unsigned char *save;

/*
==============================================================================================
INITIALIZATION FUNCTION.  Initialize as MSP430G2955 as I2C master.
==============================================================================================
*/

// Initialize USCI module for I2C master communication.
void USCIB0_I2C__MASTER_INIT(unsigned char slave_address, unsigned char prescale, boolean receive_INIT) {
	// USCI module initialization
	P1SEL    |= SDA_PIN + SCL_PIN;             // Assign I2C pins to USCI_B0, P1
	UCB0CTL1  = UCSWRST;                       // Enable SW reset

	UCB0CTL0  = UCMST + UCMODE_3 + UCSYNC;     // UCMST:    Master
											   // UCMODE_3: I2C Mode
  	  	  	  	  	  	  	  	  	  	  	   // UCSYNC:   Synchronous Mode
	UCB0CTL1  = UCSSEL_2 + UCSWRST;            // Use SMCLK, keep SW reset.

	UCB0BR0   = prescale;                      // set prescaler.  Set Baud rate control register. (i.e. 12)
	UCB0BR1   = 0;							   // CHECK (USCI_BR0/USCI_BR1) !!                    (i.e.  0)

	UCB0I2CSA = slave_address;                 // Set slave address for  MSP430G2955's peripheral device.
	UCB0I2COA = I2C_ADDRESS_MSP430G2955;       // Set master address for MSP430G2955.

	UCB0I2CIE = UCNACKIE + UCALIE;			   // Not-acknowledge interrupt &
											   // Arbitration lost interrupts enabled.
											   // .. UCB0I2CIE has 4 different interrut enables.
											   // See family user guide .. pg. 465, 471
	UCB0CTL1 &= ~UCSWRST;                      // Clear SW reset, resume operation.
	if (receive_INIT) {
		IE2 |= UCB0RXIE;                       // Enable RX interrupt
	} else {
		IE2 |= UCB0TXIE;                       // Enable TX interrupt
	}
}

/*
==============================================================================================
TRANSMIT & RECEIVE FUNCTIONS
==============================================================================================
*/

// RECEIVE
void USCIB0_I2C__RECEIVE(unsigned char byteCount, unsigned char *field) {
	receive_field = field;

    byteCtr = byteCount;
    save    = field;

    if (byteCount == 1) {
    	byteCtr = 0;
    	__disable_interrupt();				   // CHECK!
    	UCB0CTL1 |= UCTXSTT;                   // I2C start condition
    	while (UCB0CTL1 & UCTXSTT);            // UCTXSTT bit will be cleared when slave
    										   // acknowledges its address to master.

    	// ... byte is sent to slave ... in background.
    	UCB0CTL1 |= UCTXSTP;                   // I2C stop condition
    	__enable_interrupt();				   // CHECK!

    } else if (byteCount > 1) {
    	byteCtr = byteCount - 2;
    	UCB0CTL1 |= UCTXSTT;                   // I2C start condition
    	// ... bytes are sent to slave ... in background.
    	// A STOP condition will be sent if the slave sends a NACK.
    	// OR
    	// A STOP condition will be sent if there is an "arbitration lost" issue.
    	// Otherwise, a STOP conditino will not be sent.
    } else {
    	while (1);                             // illegal parameter.  byteCount is some kind of
    }	  	  	  	  	  	  	  	  	  	   // invalid value/argument ..
}

// TRANSMIT
void USCIB0_I2C__TRANSMIT(unsigned char byteCount, unsigned char *field) {
	transmit_field = field;
	byteCtr = byteCount;
	UCB0CTL1 |= UCTR + UCTXSTT;                // I2C Transmitter, I2C start condition
}

/*
==============================================================================================
MISC. FUNCTIONS
==============================================================================================
*/

// SLAVE PRESENT?
unsigned char USCIB0_I2C__SLAVE_PRESENT(unsigned char slave_address) {
	unsigned char IE2_OLD, SLAVE_ADR_OLD, UCB0I2CIE_OLD, returnValue;
	UCB0I2CIE_OLD = UCB0I2CIE;                 // store old UCB0I2CIE
	IE2_OLD       = IE2;                       // store IE2 register
	SLAVE_ADR_OLD = UCB0I2CSA;                 // store old slave address

	UCB0I2CIE &= ~UCNACKIE;                    // no NACK interrupt
	UCB0I2CIE |= ~UCALIE;                      // no AL   interrupt
	UCB0I2CSA  = slave_address;                // set slave address
	IE2       &= ~(UCB0TXIE + UCB0RXIE);       // no RX or TX interrupts
	__disable_interrupt();					   // CHECK!
	UCB0CTL1  |= UCTR + UCTXSTT + UCTXSTP;     // I2C TX, start condition
	while (UCB0CTL1 & UCTXSTP);                // wait for STOP condition
	returnValue = !(UCB0STAT & (UCNACKIFG | UCALIFG));
											   // Check if either NACK or AL flag is set.
											   // Check in the UCB0STAT register..
	__enable_interrupt();					   // CHECK!

	IE2       = IE2_OLD;                       // restore old IE2
	UCB0I2CSA = SLAVE_ADR_OLD;                 // restore old slave address
	UCB0I2CIE = UCB0I2CIE_OLD;                 // restore old UCB0CTL1

	// return 1 only if both AL and NACK flags are NOT set.
	// Otherwise, return 0 if either or both of them are set.
	return returnValue;                        // Return whether or not
                                               // a NACK occured.
}

// COMMUNICATION IN PROGRESS?
unsigned char USCIB0_I2C__NOT_READY() {
	return (UCB0STAT & UCBBUSY);
}


/*********************************************************************************************
==============================================================================================
INTERRUPT SERVICE ROUTINES
==============================================================================================
*********************************************************************************************/

// RECEIVE ISR
#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCIAB0RX_ISR(void) {
	if (UCB0STAT & UCNACKIFG) {            	   // send STOP if slave sends NACK
		UCB0CTL1 |= UCTXSTP;
		UCB0STAT &= ~UCNACKIFG;
	} else if (UCB0STAT & UCALIFG) {		   // send STOP if arbitration is lost.
		UCB0CTL1 |= UCTXSTP;
		UCB0STAT &= ~UCALIFG;
	}
}

// TRANSMIT ISR
#pragma vector = USCIAB0TX_VECTOR
__interrupt void USCIAB0TX_ISR(void) {
	if (IFG2 & UCB0RXIFG) {
		if ( byteCtr == 0 ) {
			UCB0CTL1 |= UCTXSTP;               // I2C stop condition
			*receive_field = UCB0RXBUF;
			receive_field++;
		} else {
			*receive_field = UCB0RXBUF;
			receive_field++;
			byteCtr--;
		}
    } else {
    	if (byteCtr == 0) {
    		UCB0CTL1 |= UCTXSTP;               // I2C stop condition
    		IFG2 &= ~UCB0TXIFG;                // Clear USCI_B0 TX int flag
    	} else {
    		UCB0TXBUF = *transmit_field;
    		transmit_field++;
    		byteCtr--;
    	}
    }
}
