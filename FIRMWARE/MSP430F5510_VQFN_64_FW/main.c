#include <stdint.h>
#include <stdio.h>
#include <msp430.h>
#include "msp430f5510.h"
#include "main_H.h"

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    // CONFIGURATION.
    // Below, we will want to set up some of the fundamental modules in the MSP430G2955 that we are using.
    // That is, we set up:
    // 1.  Basic Clock Module+
    // 2.  I/O Ports 1-4
    // 3.  Timer A0 Module
    configure_Basic_Clock_Module(); // In the "clock.c" source file, included in this project
    configure_IO_ports();           // In "IO_Ports.c" .. configure 32/38 pins .. 4 ports x 8 pins
    config_timer_A(TACCR0_COUNT);   // In the "timer_a.c" source file, included in this project

    // Below, which one should be used?
    //_BIS_SR(GIE); 	            // Enable global interrupt.
    //_EINT();						// Enable global interrupt flag.

    freq_toggle = false;
    while(1) {

    	write_byte_to_LED1(0x56);   // Invoke function from IO_Ports.c to display 8-bit argument.

    	dummy_counter++;     	    // increment a dummy counter for no reason whatsoever..  :)
    }
	//return 0;
}

/*
==============================================================================================
	TIMER A0 INTERRUPT SERVICE ROUTINE
	This interrupt service routine increments a count, time_count.
	Whenever time_count is an integer multiple of a particular constant, the high/low level
	when transition on one of the output pins (indicated below).
	We are essentially producing several different square waves of different frequencies
	on each of the output pins indicated below.
==============================================================================================
*/
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A_ISR_vector (void){
	TACTL &= 0xFFFE; // reset TAIFG interrupt flag to 0, indicating that no interrupt is pending.
	time_count++;    // Keep track of the number of times the code enters this interrupt service routine.

	//write_byte_to_LED1();

	if ((time_count%time_count_value) == 0) { // P2.1 will toggle every 2.5s, ideally
		P2OUT ^= 0x02;		// Toggle the P2.1 output pin.
		// time_count = 0;
	}

}

/*
==============================================================================================
	PORT 1 INTERRUPT SERVICE ROUTINE:
	A input applied to P1.3, will toggle P1.6 high and low.
==============================================================================================
*/
#pragma vector=PORT1_VECTOR
__interrupt void Port_1_ISR_vector(void) {
	P2IFG &= ~0x08;       // P1.3 Interrupt Flag cleared
						  // P1IFG.3 was 1 before this interrupt service routine was entered.
						  // With this line of code,
						  // we make that bit "0" again.. which means there is NO interrupt pending.

	//P1OUT ^= BIT6;

}

/*
==============================================================================================
	PORT 2 INTERRUPT SERVICE ROUTINE:
	This interrupt service routine is serviced when a bit in the PXIFG is set, indicating
	that an interrupt is pending and needs to be serviced.  The bit is set when a level
	transition (a low-to-high transition in this code, not a high-to-low transition)
	occurs at the corresponding I/O pin on the port.  In this code, only this interrupt
	functionality is enabled on the Most significant bit/pin of Port 2.  So, a low-to-high
	transition on the P2.7 pin will set the most significant bit in P2IFG.
	In this interrupt service routine, we first clear (make '0') the IFG bit that was set.
	Then, we change a parameter in the BCSCTL2 register each time we enter this routine,
	and the SMCLK frequency is changed as a result.
==============================================================================================
*/
#pragma vector=PORT2_VECTOR
__interrupt void Port_2_ISR_vector(void) {
	P2IFG &= ~0x80;       // P2.7 Interrupt Flag cleared
						  // P2IFG.7 was 1 before this interrupt service routine was entered.
						  // With this line of code,
						  // we make that bit "0" again.. which means there is NO interrupt pending.

	if (freq_toggle == true){
		P2OUT &= 0xFE;
		//BCSCTL2 |= 0x02;  // change the divider for SMCLK..
						  // It would now be DCOCLK/2 since the selected divider is .. /2
	    freq_toggle = false;
	}

	else {
		P2OUT |= 0x01;
		//BCSCTL2 &= 0x01;  // change the divider for SMCLK..
						  // It would now be DCOCLK/1 since the selected divider is .. /1
	    freq_toggle = true;
	}
}

/*
==============================================================================================
	PORT 3 INTERRUPT SERVICE ROUTINE:
==============================================================================================
*/
#pragma vector=PORT3_VECTOR
__interrupt void Port_3_ISR_vector(void) {
	P3IFG &= ~0x08;       // P1.3 Interrupt Flag cleared
						  // P1IFG.3 was 1 before this interrupt service routine was entered.
						  // With this line of code,
						  // we make that bit "0" again.. which means there is NO interrupt pending.

	//P1OUT ^= BIT6;

}

/*
==============================================================================================
	PORT 4 INTERRUPT SERVICE ROUTINE:
==============================================================================================
*/
#pragma vector=PORT4_VECTOR
__interrupt void Port_4_ISR_vector(void) {
	P4IFG &= ~0x08;       // P1.3 Interrupt Flag cleared
						  // P1IFG.3 was 1 before this interrupt service routine was entered.
						  // With this line of code,
						  // we make that bit "0" again.. which means there is NO interrupt pending.

	//P1OUT ^= BIT6;

}
