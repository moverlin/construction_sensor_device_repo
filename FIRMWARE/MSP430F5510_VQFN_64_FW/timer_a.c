#include <stdint.h>

//#include "msp430_xc.h"
#include "msp430g2553.h"
//#include "main.h"

void config_timer_A(unsigned long TACCR0_COUNT) {
	TACCTL0 = CCIE;             // Enable periodic interrupt
	TACCR0 = TACCR0_COUNT;      // 20,000 samples per second  = BCSCTL1/TACCR0 = (1*10^6)/50
								// Because of how we have programmed TACTL, the count will count up to TACCR0.
	// TACTL = TASSEL_2 + MC_1 + TAIE_1;    // source SMCLK, counts up to TACCR0. interrupt enabled
	TACTL &= 0xFC0C;            // See p. 370/644 in the fqamily user guide for more detailed information.
	TACTL |= 0x0212;
		// XXXX XX10 0001 XX10
}
