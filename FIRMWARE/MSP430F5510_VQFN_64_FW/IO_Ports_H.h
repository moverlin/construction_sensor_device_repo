#ifndef IO_PORTS_H_H_
#define IO_PORTS_H_H_

#include <stdio.h>
#include <stdbool.h>  // NEEDED IN ORDER TO USE BOOLEAN-TYPE VARIABLES (i.e. True/False)

/*
==============================================================================================
									DEFINE VARIABLES.
==============================================================================================
*/
bool freq_toggle;
unsigned long dummy_counter = 0;  				// unsigned long -- at least 32 bits in size
unsigned long time_count = 0;                           // increments forever
const unsigned long TACCR0_COUNT = 50;                  // TIMER_A counts up to this value
const uint8_t ONE = 0x01;

/*
==============================================================================================
									DEFINE FUNCTIONS.
==============================================================================================
*/

/*
==============================================================================================
*************************************NON-PORT PINS MAPPING************************************

THESE PINS ARE NOT ON ANY PORT (P1, P2, P3, P4):
Pin  #		Pin Name				Net Name on pin
--------------------------------------------------------------------------------------------
Pin  1: 	TEST/SBWTCK				EZ-FET_SPY-BI-WIRE__CLK
Pin  2: 	DVCC					VCC_3P3
Pin  4: 	DVSS					VCC_3P3
Pin  7: 	*RST/NMI/SBWTDIO		EZ-FET_SPY-BI-WIRE__DATA
Pin 15: 	AVSS					GND
Pin 16: 	AVCC					GND

==============================================================================================
*/

/*
==============================================================================================
*************************************PORT 1 PIN MAPPING*************************************

P1.0: BQ24292I_INT				-- GEN I/O INPUT  -- BQ24292I FAULT occurrence.
													 Active low pulses (256 us) to MSP430G2955
													 to report device status and fault.
													 (Active-low open-drain output.)

P1.1: TPS2546_STATUS_1			-- GEN I/O INPUT  -- Indicates load detection on USB port #1.
												 	 Logic HIGH indicates no load detected.
												 	 Logic LOW 	indicates a device is
												 	 mated & charging from USB port #1
												 	 (i.e. "Load detected").
												 	 (Active-low open-drain output.)

P1.2: TPS2546_FAULT_1			-- GEN I/O INPUT  -- Indicates FAULT has occurred on USB port #1.
												 	 Logic HIGH indicates no problems/"faults".
												 	 Logic LOW  indicates over-temp OR
												 	 current-limit conditions have occurred.
												 	 (Active-low open-drain output.)

P1.3: BQ24292I_STAT				-- GEN I/O INPUT  --

P1.4: TPS2546_STATUS_2			-- GEN I/O INPUT  -- Indicates load detection on USB port #2.
												 	 Logic HIGH indicates no load detected.
												 	 Logic LOW 	indicates a device is
												 	 mated & charging from USB port #1
												 	 (i.e. "Load detected").
												 	 (Active-low open-drain output.)

P1.5: TPS2546_FAULT_2			-- GEN I/O INPUT  -- Indicates FAULT has occurred on USB port #2.
												 	 Logic HIGH indicates no problems/"faults".
												 	 Logic LOW  indicates over-temp OR
												 	 current-limit conditions have occurred.
												 	 (Active-low open-drain output.)

P1.6: NON_MS_MATED	    		-- GEN I/O INPUT  -- Indicates if a Non-MS host is mated or not.
												 	 Logic HIGH when a non-MS host is mated.
												 	 Logic LOW  when    no    host is mated.
												 	 Logic LOW when Mother Station is mated.

P1.7: HOST_MATED				-- GEN I/O INPUT  -- Indicates whether or not ANY kind of host is mated
												 	 via USB-C connector.
												 	 Logic HIGH when any host is mated.
												 	 Logic LOW  when no  host is mated.

==============================================================================================
*/
void configure_IO_ports__Port1();


/*
==============================================================================================
*************************************PORT 2 PIN MAPPING*************************************

P2.0: ADC_IN_CS					-- ADC10 INPUT    -- ANALOG voltage IN.  1.5V indicates
													 a "0A" reading (i.e. no current
													 into/out of battery).
													 This analog voltage will vary above
													 or below 1.5V, based on the direction
													 of current flow.

P2.1: ADC_IN_VS					-- ADC10 INPUT    -- ANALOG voltage IN.  The analog
													 voltage is the voltage across the
													 battery & CS resistor.  More precisely,
													 it's the voltage across the battery
													 AND 2 milliOhm CS resistor.

P2.2: BQ24292I_PG				-- GEN I/O INPUT --  BQ24292I PGOOD indicator.
													 Logic LOW indicates a good input source.
													 (Conditions for a low PGOOD signal -
													 pg. 4 in BQ24292I datasheet).
													 (Active-low open-drain output.)

P2.3: BQ27542-G1_SHUTDOWN_EN	-- GEN I/O INPUT  -- BQ27542-G1 shutdown enable output.
													 With SE_PU = 0 & SE_POL = 1 !!!!!
													 (pg. 13 in BQ27542-G1 datasheet ..
													 Pack Configuration register)
													 Logic LOW indicates NORMAL operation.
													 Logic HIGH (high-imped.) indicates
													 shutdown operation.

P2.4: VREF_3P0					-- VREF+ INPUT    -- ANALOG voltage IN.  The analog voltage
													 of 3.0V ("VREF+") will serve as a
													 reference for ADC readings.
													 3.0V is an output from REF1930.

P2.5: TPS61232_PGOOD			-- GEN I/O INPUT  -- TPS61232 PGOOD Logic signal.
													 The PGOOD pin goes LOW when the TPS61232's
													 output voltage goes below 90% of its nominal value.
													 (5.0V * 0.9 = 4.5V)
													 (Active-low open-drain output.)

P2.6: TUSB320_ID	    		-- GEN I/O INPUT  -- Asserted low when CC pins detect device
													 attachement (when used in DRP or DFP ..
													 but we're using in UFP .. maybe not relelvant).
												 	 (Active-low open-drain output.)

P2.7: TUSB320_OUT3_INT_N		-- GEN I/O INPUT  -- When used in I2C mode, this pin goes LOW
													 when an I2C register has been changed.
													 So, the signal going low can be used to
													 trigger an interrupt service routine.
												 	 (Active-low open-drain output.)

==============================================================================================
*/
void configure_IO_ports__Port2();


/*
==============================================================================================
*************************************PORT 3 PIN MAPPING*************************************

P3.0: I2C_SLAVE_TRANSMIT_EN		-- GEN I/O OUTPUT -- Logic signal going to Base Station's
													 MSP430 MCU.  MSP430G2955 will go HIGH
													 when it has to communicate something back
													 to the Base Station's MSP430 MCU.
													 MSP430G2955 controls this logig signal's
													 HIGH/LOW level!

P3.1: I2C_DATA					-- GEN I/O IN/OUT -- I2C Bi-directional data line.
													 It is used to communicate to the Base Station's
													 MSP430 MCU when the MSP430G2955 is a slave.
													 It is used to communicate to some peripheral I2C
													 peers on the charger's board.  And in this case,
													 the MSP430G2955 switches roles and acts
													 as an I2C master.

P3.2: I2C_CLK					-- GEN I/O INPUT  -- I2C CLOCK line.
													 When the MSP430G2955 is in a slave role,
													 the Base Station's MSP430 MCU will issue the
													 I2C clock signal, and it will be an input to
													 the MSP430G2955.  However, when the MSP430G2955
													 acts as an I2C master, it issues the I2C
													 clock signal.

P3.3: ILIM_HI_1					-- GEN I/O OUTPUT -- TS3A4741 switch control - USB port #1
													 Logic HIGH closes switch, which allows for the
													 TPS2546's HIGHER ILIM_HI current limit setting.
													 Logic LOW  opens  switch, which means
													 higher resistance from ILIM_HI pin to ground,
													 which allows for the TPS2546's LOWER
													 ILIM_HI current limit setting.

P3.4: ILIM_HI_2					-- GEN I/O OUTPUT -- TS3A4741 switch control - USB port #2
													 Logic HIGH closes switch, which allows for the
													 TPS2546's HIGHER ILIM_HI current limit setting.
													 Logic LOW  opens  switch, which means
													 higher resistance from ILIM_HI pin to ground,
													 which allows for the TPS2546's LOWER
													 ILIM_HI current limit setting.

==============================================================================================
*/
void configure_IO_ports__Port3();


/*
==============================================================================================
*************************************PORT 4 PIN MAPPING*************************************

P4.0: BQ24292I_OTG				-- GEN I/O OUTPUT --



P4.1: BQ24292I_CE				-- GEN I/O OUTPUT  -- BQ24292i Charge Enable pin.
													 To enable charging, make this pin LOW
													 AND set: REG01[5:4]=01
													 CE pin must be pulled HIGH or LOW.

P4.2: TPS2546_EN_1				-- GEN I/O OUTPUT -- Enable TPS2546 #1 switch for USB charging.
												 	 Logic HIGH enables TPS2546.
												 	 Logic LOW disables TPS2546.

P4.3: ADC_IN_THERMISTOR			-- ADC10 INPUT    -- ANALOG voltage IN.  Convert
													 the digitized reading to a temperature.

P4.4: TPS2546_EN_2				-- GEN I/O OUTPUT -- Enable TPS2546 #2 switch for USB charging.
												 	 Logic HIGH enables TPS2546.
												 	 Logic LOW disables TPS2546.

P4.5: SPARE_NET_TO_BASE_STATION -- GEN I/O INPUT  -- Spare net through TS3USB31E switch to
									ADC10INPUT ?	 Base Station's MSP430 MCU.
													 This net/signal may be used as a
													 Logic HIGH/LOW level or as an ADC10 input
													 where a certain analog voltage is set on
													 this signal in the base station,
													 and the MSP430G2955 digitizes the
													 analog voltage for authentication.

P4.6: TPS61232_EN				-- GEN I/O OUTPUT -- TPS61232 enable pin.
													 Logic HIGH enables  TPS61232.
													 Logic LOW  disables TPS61232.

P4.7: TUSB320_EN_N				-- GEN I/O OUTPUT -- TUSB320 enable signal.
													 Logic HIGH disables TUSB320.
													 Logic LOW  enables  TUSB320.
													 EN_N pin is pulled up inside TUSB320.

==============================================================================================
*/
void configure_IO_ports__Port4();


/*
==============================================================================================
*************************************PORT 5 PIN MAPPING*************************************

P5.0: BQ24292I_CE				-- GEN I/O OUTPUT  -- BQ24292i Charge Enable pin.

P5.1: BQ24292I_CE				-- GEN I/O OUTPUT  -- BQ24292i Charge Enable pin.
													 To enable charging, make this pin LOW
													 AND set: REG01[5:4]=01
													 CE pin must be pulled HIGH or LOW.

P5.2: TPS2546_EN_1				-- GEN I/O OUTPUT -- Enable TPS2546 #1 switch for USB charging.
												 	 Logic HIGH enables TPS2546.
												 	 Logic LOW disables TPS2546.

P5.3: ADC_IN_THERMISTOR			-- ADC10 INPUT    -- ANALOG voltage IN.  Convert
													 the digitized reading to a temperature.

P5.4: TPS2546_EN_2				-- GEN I/O OUTPUT -- Enable TPS2546 #2 switch for USB charging.
												 	 Logic HIGH enables TPS2546.
												 	 Logic LOW disables TPS2546.

P5.5: SPARE_NET_TO_BASE_STATION -- GEN I/O INPUT  -- Spare net through TS3USB31E switch to
									ADC10INPUT ?	 Base Station's MSP430 MCU.
													 This net/signal may be used as a
													 Logic HIGH/LOW level or as an ADC10 input
													 where a certain analog voltage is set on
													 this signal in the base station,
													 and the MSP430G2955 digitizes the
													 analog voltage for authentication.

==============================================================================================
*/
void configure_IO_ports__Port5();


/*
==============================================================================================
*************************************PORT 6 PIN MAPPING*************************************

P6.0: BQ24292I_CE				-- GEN I/O OUTPUT  -- BQ24292i Charge Enable pin.

P6.1: BQ24292I_CE				-- GEN I/O OUTPUT  -- BQ24292i Charge Enable pin.
													 To enable charging, make this pin LOW
													 AND set: REG01[5:4]=01
													 CE pin must be pulled HIGH or LOW.

P6.2: TPS2546_EN_1				-- GEN I/O OUTPUT -- Enable TPS2546 #1 switch for USB charging.
												 	 Logic HIGH enables TPS2546.
												 	 Logic LOW disables TPS2546.

P6.3: ADC_IN_THERMISTOR			-- ADC10 INPUT    -- ANALOG voltage IN.  Convert
													 the digitized reading to a temperature.

P6.4: TPS2546_EN_2				-- GEN I/O OUTPUT -- Enable TPS2546 #2 switch for USB charging.
												 	 Logic HIGH enables TPS2546.
												 	 Logic LOW disables TPS2546.

P6.5: SPARE_NET_TO_BASE_STATION -- GEN I/O INPUT  -- Spare net through TS3USB31E switch to
									ADC10INPUT ?	 Base Station's MSP430 MCU.
													 This net/signal may be used as a
													 Logic HIGH/LOW level or as an ADC10 input
													 where a certain analog voltage is set on
													 this signal in the base station,
													 and the MSP430G2955 digitizes the
													 analog voltage for authentication.

P6.6: TPS61232_EN				-- GEN I/O OUTPUT -- TPS61232 enable pin.
													 Logic HIGH enables  TPS61232.
													 Logic LOW  disables TPS61232.

P6.7: TUSB320_EN_N				-- GEN I/O OUTPUT -- TUSB320 enable signal.
													 Logic HIGH disables TUSB320.
													 Logic LOW  enables  TUSB320.
													 EN_N pin is pulled up inside TUSB320.

==============================================================================================
*/
void configure_IO_ports__Port6();

void configure_IO_ports();

#endif /* IO_PORTS_H_H_ */
