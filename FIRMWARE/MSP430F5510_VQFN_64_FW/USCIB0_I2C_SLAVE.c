#include "USCIB0_I2C_slave_H.h"
#include "msp430g2955.h"                       // device specific header
#include <stdint.h>
#include <stdio.h>
// #include <msp430.h>
// #include <legacymsp430.h>

void (*receive_callback)(unsigned char receive);
void (*transmit_callback)(unsigned char volatile *send_next);
void (*start_callback)(void);

void USCIB0_I2C__SLAVE_INIT(void (*SCallback)(),
                        void (*TCallback)(unsigned char volatile *value),
                        void (*RCallback)(unsigned char value),
                        unsigned char slave_address) {
    P3SEL            |= SDA_PIN + SCL_PIN;     // Assign I2C pins to USCIB0
    P3SEL2    	     |= SDA_PIN + SCL_PIN;     // Assign I2C pins to USCIB0
    UCB0CTL1         |= UCSWRST;               // Enable SW reset
    UCB0CTL0          = UCMODE_3 + UCSYNC;     // I2C Slave, synchronous mode
    UCB0I2COA  	      = slave_address;         // set own (slave) address
    UCB0CTL1  		 &= ~UCSWRST;              // Clear SW reset, resume operation
    IE2       		 |= UCB0TXIE + UCB0RXIE;   // Enable TX interrupt
    UCB0I2CIE 		 |= UCSTTIE;               // Enable STT interrupt
    start_callback    = SCallback;
    receive_callback  = RCallback;
    transmit_callback = TCallback;
}

/*********************************************************************************************
==============================================================================================
INTERRUPT SERVICE ROUTINES
==============================================================================================
*********************************************************************************************/

// USCIB0 Data ISR
interrupt(USCIAB0TX_VECTOR) usci_i2c_data_isr(void) {
    if (IFG2 & UCB0TXIFG) {
        transmit_callback(&UCB0TXBUF);
    } else {
        receive_callback(UCB0RXBUF);
    }
}

// USCIB0 State ISR
interrupt(USCIAB0RX_VECTOR) usci_i2c_state_isr(void) {
    UCB0STAT &= ~UCSTTIFG;                     // Clear start condition int flag
    start_callback();
}
