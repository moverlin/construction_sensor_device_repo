#include <stdint.h>

//#include "msp430_xc.h"
#include "msp430g2955.h"

void configure_IO_ports__Port1(void){
	//
	P1SEL = 0x10;  // P1.4 outputs SMCLK.
				   // The rest are GEN I/O and are configured as OUTPUTS.
	P1SEL2 = 0x00; // P1.4 outputs SMCLK.  P1.0 outputs ACLK.
				   // The rest are GEN I/O and are configured as OUTPUTS.
	P1DIR = 0xF7;  // All pins are configured as outputs except P1.3, which receives the interrupt signal.
	P1REN = 0x08;  // The pull up/down resistor is not enabled on any of Port 1's 8 pins except P1.3.
				   // Though, it shouldn't matter what we set P1REN to since all 8 pins are configured as outputs.
	P1OUT &= 0x19; // Make all the GEN I/O outputs low, but don't mess with the SMCLK and ACLK outputs.
				   // Also, select pull-up mode for P1.3.

	P1IE = 0x08;   // The I/O external interrupt is only enabled on P1.3.
	P1IES = 0x08;  // "0" bits indicate that low-to-high transitions trigger the interrupt service routines, but
			       // we aren't using interrupts with Port 1 at all anyway.

	P1IFG = 0x00;

	// P1IN
	// P1IN should NEVER be written to for any reason!
	// If a write operation to P2 is ever attempted, bad things happen.
	// (Some circuit internal to the MCU consumes a lot of current
	// in order to try to do something it can't... ) No reason to do it, ever.

}

void configure_IO_ports__Port2(void) {
	P2SEL = 0x00;  // All pins on Port 2 (2.0-2.7) are configured to be used as I/O pins.
				   // That is, they are not configured to perform some other function for a built-in peripheral module.
	P2SEL2 = 0x00; // All pins on Port 2 (2.0-2.7) are configured to be used as I/O pins.
	   	   	   	   // That is, they are not configured to perform some other function for a buiilt-in peripheral module.
				   // The function of each of the Port 2 pins is selected by the corresponding bits in P2SEL AND P2SEL2.
	P2DIR = 0x7F;  // = 0111 1111 -- Pin 2.7 is configured as an INPUT.  Pins 2.0-2.6 are configured as OUTPUTS.
	P2REN = 0xFF;  // = 1111 1111 -- The pull up/down resistor is enabled on Pin 2.7 (the INPUT) as well as all other pins, 2.0-2.6.
	P2OUT = 0x00;  // = 0000 0000 -- Pin 2.7 is configured as a pull down resistor -- external interrupt signal to change SMCLK freq.
				   //                The other pins (2.0-2.6) are configured to output low levels.
				   //				 From the family user guide: The value of the unused GEN I/O OUTPUT pins is irrelevant, since the pin is unconnected.

	P2IE = 0x80;   // The I/O external interrupt is only enabled on pin 2.7, which is an input pin.
	P2IES = 0x00;  // The interrupt service routine (which is triggered from a changing signal on Pin 2.7) is triggered when
				   // there is a low-to-high transition on the signal at pin 2.7.  So, let's say we start up the MCU with this pin being low (w/ P2OUT).
				   // Then, some time goes by, and the voltage at this pin changes to be high.  Upon the detection of this edge, the corresponding
				   // bit (the most significant bit of the P2IFG byte) is set (i.e. is a 1).
				   // A programmer may write some kind of while(1) loop, for example, which
				   // continuously checks if the flag has been set (i.e. if that particular bit is high).
				   // IF the flag HAS been set (checked with an IF statement, for example), then
				   // the program may jump to another part of the code and run another part of the code.  In this "other" part of code, that bit should
				   // be changed back to be 0, which means there is no interrupt pending.  With the bit now being reset back to 0, the program can go
				   // back to the original while(1) loop and do its normal routine until the flag has been set again with another low-to-high transition...

	P2IFG = 0x00;
	// P2IFG should be read in a continuous while(1) loop or something like that.. until it has been discovered that a (or multiple)
	// P2IFG bit(s) are read to be set (i.e. is a 1).
	// The bits in P2IFG that are being used for interrupts should be RESET to 0 during their respective interrupt service routines!

	// P2IN
	// P2IN should NEVER be written to for any reason!
	// If a write operation to P2 is ever attempted, bad things happen.  (Some circuit internal to the MCU consumes a lot of current
	// in order to try to do something it can't... ) No reason to do it, ever.
}

void configure_IO_ports__Port3(void) {

}

void configure_IO_ports__Port4(void) {

}

void configure_IO_ports__Port5(void) {

}

void configure_IO_ports__Port6(void) {

}

void configure_IO_ports(void){
	configure_IO_ports__Port1;
	configure_IO_ports__Port2;
	configure_IO_ports__Port3;
	configure_IO_ports__Port4;
	configure_IO_ports__Port5;
	configure_IO_ports__Port6;
}

//  This function gives a delay.
//  The integer argument ot this function is the number of ms of the delay.
void delay(uint32_t delay) { // the uint32_t argument is supposed to specify the number of ms of the delay.
    while (delay--)
    {
        __delay_cycles(1000);
        // PUT_CPU_CLOCK_SPEED_IN_HZ_DIVIDED_BY_1000_HERE
        // ^^ that's the argument to the __delay_cycles(...) function^^.
        // So, 1 MHz / 1000 = 1000 = argument to __delay_cycles(...)
    }
}

//  This function takes in an 8-bit value as its argument.
//  Then, each bit is presented to an output pin on the MSP430G2553, one at a time.
//  Data is displayed on P1.0, and "clock" signal is displayed on P1.6.
void write_byte_to_LED1(uint8_t byte_in) {

	P1OUT = 0x00;
	P1OUT = BIT0 | BIT6;
	delay(5000);
	P1OUT = 0x00;
	delay(5000);

	P1OUT = 0x00;
	P1OUT = BIT0 | BIT6;
	delay(3000);
	P1OUT = 0x00;
	delay(3000);

	int i;

	for (i = 0; i<8; i++){
		P1OUT = 0x00;
		P1OUT |= (BIT0 & (byte_in >> i)) | BIT6;
		delay(1000);
		P1OUT &= ~BIT6;
		P1OUT &= ~BIT0;
		delay(1000);
	}
}
