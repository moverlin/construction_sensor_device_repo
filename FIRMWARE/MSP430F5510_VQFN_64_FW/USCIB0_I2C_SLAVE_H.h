#ifndef TI_USCI_I2C_SLAVE
#define TI_USCI_I2C_SLAVE

#define SDA_PIN 	BIT1  // P3.1
#define SCL_PIN 	BIT2  // P3.2

/*
==============================================================================================
void USCI_I2C_slaveinit(void (*SCallback)(),
                        void (*TCallback)(unsigned char volatile *value),
                        void (*RCallback)(unsigned char value),
                        unsigned char slave_address)

This function initializes the USCI module for I2C Slave operation.

IN:   void (*SCallback)() => function is called when a START condition was detected
      void (*TCallback)(unsigned char volatile *value) => function is called for every byte requested by master
      void (*RCallback)(unsigned char value) => function is called for every byte that is received
      unsigned char slave_address  =>  Slave Address
==============================================================================================
*/
void USCIB0_I2C__SLAVE_INIT(void (*SCallback)(unsigned char volatile *value),
                           void (*TCallback)(unsigned char volatile *value),
                           void (*RCallback)(unsigned char value),
                           unsigned char slave_address);

#endif
