/*
 * notes.h
 *
 *  Created on: Sep 12, 2015
 *      Author: moverlin
 */

#ifndef NOTES_H_
#define NOTES_H_


/*
       *.c                                   *.h           // *********************  COMMENTS  ****************************

main.c                                     main_H.h        // main.c is the central file for the whole MSP430G2955 application
  clocks.c                                                 // File to set up clocks inside MSP430G2955 ..
  	  	  	  	  	  	  	  	  	  	  	  	  	  	   // one of the first things done before the application is done.
  IO_Ports.c                            IO_Ports_H.h       // File to set up/specify the function of each pin on the MSP430G2955.
  timer_a.c                                                // File to set up a timer inside MSP430G2955 .. may not even need this file.
I2C_master.c                            I2C_master_H.h     // Charger will communicate back to base station MSP430 micro.
														   // This code is for communicating back to base station.
I2C_master_BQ24292I.c             I2C_master_BQ24292I_H.h  // I2C code for communicating with battery charger chip
I2C_master_BQ27542_G1.c         I2C_master_BQ27542_G1_H.h  // I2C code for communicating with battery health monitor chip
I2C_master_TUSB320.c               I2C_master_TUSB320_H.h  // I2C code for communicating with USB-C front end chip
IO_for_Output_Stages.c           IO_for_Output_Stages_H.h  // Code written to control the Gen I/O needed for the boost converter,
														   // current-limiting switches, and all things associated
                                                           // with the output stages of the charger to charger a mobile device(s)
IO_misc.c                               IO_misc_H.h        // the MSP430G2955 will have various I/O to read in temperatrure, control, LEDs,
														   // detect if its mated to a host, etc.
ADC10.c                                   ADC10.h          // code implemented to make use of the ADC10 module within the MSP430G2955,
														   // to read in battery current, battery voltage, temperature,
														   // & spare line to base station
 */


#endif /* NOTES_H_ */
