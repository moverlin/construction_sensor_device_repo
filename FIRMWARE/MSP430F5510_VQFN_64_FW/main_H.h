#ifndef MAIN_H_
#define MAIN_H_

#include <stdio.h>
#include <stdbool.h>  // NEEDED IN ORDER TO USE BOOLEAN-TYPE VARIABLES (i.e. True/False)

//=========================================================================================
// DECLARE GLOBAL VARIABLES USED IN MAIN.C
//=========================================================================================
bool freq_toggle;
unsigned long dummy_counter = 0;  // unsigned long -- at least 32 bits in size
unsigned long time_count = 0;                           // increments forever
const unsigned long TACCR0_COUNT = 50;                  // TIMER_A counts up to this value
const unsigned long time_count_value = 500;
const unsigned long time_count_LED_blink_test0 = 1500;
const unsigned long time_count_LED_blink_test1 = 2;
const unsigned long time_count_LED_blink_test2 = 8;
const unsigned long time_count_LED_blink_test3 = 40;
const unsigned long time_count_LED_blink_test4 = 200;
const unsigned long time_count_LED_blink_test5 = 1000;
const unsigned long time_count_LED_blink_test6 = 5000;
const uint8_t ONE = 0x01;

//=========================================================================================
// DECLARE GLOBAL FUNCTIONS USED IN MAIN.C
//=========================================================================================
void configure_Basic_Clock_Module();
void config_timer_A();
void write_byte_to_LED1();
void delay(uint32_t delay);

#endif /* MAIN_H_ */
