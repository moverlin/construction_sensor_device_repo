#include <stdint.h>

//#include "msp430_xc.h"
#include "msp430f5510.h"

void configure_Basic_Clock_Module(void) { // pg. 282 of MSP430x2xx family user's guide
	//if (CALCBC1_1MHZ == 0xFF || CALDCO_1MHZ == 0xFF) while(1);
	// If these calibration constants are not present/erased,
	// then we want to trap the CPU and not load the application code.

	DCOCTL = 0x60;  // 011 00000 -- DCOx = 3 & MODx = 0
	// DCOCTL = CALDCO_1MHZ;  // Set MCU to run at approximately 1 MHz
	// DCOCTL = CAL_DCO_1MHZ;  //Not sure here.  Trying to configure the DCO frequency to be 1 MHz
					  // The datasheet says to make: DC0x = 3, MODx = 0
		              // (Refer to pg. 29 in the MSP430G2553 datasheet and 283 in family user guide),
	                  // but the msp430g2553.h file says to use "CAL_DCO_1MHZ" which is 0x0006
	BCSCTL1 &= 0x40;  // There is no XT2 oscillator in the MSP430G2553.  Make the MSB 1 ... which means the XT2 is off.
	BCSCTL1 |= 0x87;  // 1X00 0111 -- BCSCTL1.6 is a LFXT1 mode select- low freq mode = 0, high freq mode = 1
					  // In order to set the lower 4 bits of BCSCTL1 (i.e. RSELx), check pg. 29 of the MSP430G2553 datasheet.
	//BCSCTL1 = CALBC1_1MHZ;

	BCSCTL2 &= 0x00;  // SELM_0 = 0x00.  All of the bits should be zero, including the DCOR bit.
					  // From the TI guy: "The DCOR bit should be clear."
					  // The source for SMCLK and MCLK is DCOCLK.
					  // So, we just needed to make sure that DCOCLK is 1 MHz.
	//BCSCTL3 = ;

	IE1 &= 0xFD;      // 1111 1101
					  // Make bit IE1.1 0 so that the "Oscillator fault interrupt enable" is not enabled.
	//IFG1 = ;        // We're not using the IFG1 register whatsoever.  If we did want to use it, we'd have to
					  // enable the IE1.1 bit in the IE1 register above.


	_BIS_SR(OSCOFF);  // Turn off the LFTX oscillator.
	    			  // Oscillator off. This bit, when set, turns off the
	    			  // LFXT1 crystal oscillator when LFXT1CLK is not used for MCLK or SMCLK.
}
