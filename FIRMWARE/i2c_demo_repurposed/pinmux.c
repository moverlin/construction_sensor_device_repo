#include "pinmux.h"
#include "hw_types.h"
#include "hw_memmap.h"
#include "hw_gpio.h"
#include "pin.h"
#include "rom.h"
#include "rom_map.h"
#include "gpio.h"
#include "prcm.h"

//=============================================================================
void PinMuxConfig(void) {
	                                     // Enable Peripheral Clocks
    MAP_PRCMPeripheralClkEnable(PRCM_UARTA0, PRCM_RUN_MODE_CLK);
    MAP_PRCMPeripheralClkEnable(PRCM_I2CA0,  PRCM_RUN_MODE_CLK);
    MAP_PinTypeUART(PIN_55, PIN_MODE_3); // Configure PIN_55 for UART0 UART0_TX
    MAP_PinTypeUART(PIN_57, PIN_MODE_3); // Configure PIN_57 for UART0 UART0_RX
    MAP_PinTypeI2C(PIN_01, PIN_MODE_1);  // Configure PIN_01 for I2C0 I2C_SCL
    MAP_PinTypeI2C(PIN_02, PIN_MODE_1);  // Configure PIN_02 for I2C0 I2C_SDA

    // .../driverlib/prcm.c
    // .../driverlib/prcm.h
    // .../driverlib/pin.h
    // .../driverlib/pin.c
}
