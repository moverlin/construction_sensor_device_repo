#include <stdio.h> // Standard includes
#include <string.h>
#include <stdlib.h>
#include "hw_types.h" // Driverlib includes
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "rom.h"
#include "rom_map.h"
#include "interrupt.h"
#include "prcm.h"
#include "utils.h"
#include "uart.h"
#include "uart_if.h" // Common interface includes
#include "i2c_if.h"

#include "pinmux.h"

//*****************************************************************************
//                      MACRO DEFINITIONS
//*****************************************************************************
#define APPLICATION_VERSION     "1.1.0"
#define APP_NAME                "I2C Demo"
#define UART_PRINT              Report
#define FOREVER                 1
#define CONSOLE                 UARTA0_BASE
#define FAILURE                 -1
#define SUCCESS                 0
#define RETERR_IF_TRUE(condition) {if(condition) return FAILURE;}
#define RET_IF_ERR(Func)          {int iRetVal = (Func); \
                                   if (SUCCESS != iRetVal) \
                                     return  iRetVal;}

//*****************GLOBAL VARIABLES -- START***********************************
#if defined(ccs)
extern void (* const g_pfnVectors[])(void);
#endif
#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif
//*****************GLOBAL VARIABLES -- END*************************************
//=============================================================================
//***********************LOCAL FUNCTION DEFINITIONS****************************
//=============================================================================
//             Display a prompt for the user to enter a command.
//                   Parameters:  None, Returns: None
//
//=============================================================================
void DisplayPrompt() {
    UART_PRINT("\n\rcmd# ");
}

//=============================================================================
//                  Display the supported I2C commands.
//                   Parameters:  None, Returns: None
//
//=============================================================================
void DisplayUsage() {
    UART_PRINT("Command Usage \n\r");
    UART_PRINT("------------- \n\r");
    UART_PRINT("write <dev_addr> <wrlen> <<byte0> [<byte1> ... ]> <stop>\n\r");
    UART_PRINT("\t - Write data to the specified i2c device\n\r");
    UART_PRINT("read  <dev_addr> <rdlen> \n\r\t - Read data frpm the specified "
                "i2c device\n\r");
    UART_PRINT("writereg <dev_addr> <reg_offset> <wrlen> <<byte0> [<byte1> ... "
                "]> \n\r");
    UART_PRINT("\t - Write data to the specified register of the i2c device\n\r");
    UART_PRINT("readreg <dev_addr> <reg_offset> <rdlen> \n\r");
    UART_PRINT("\t - Read data from the specified register of the i2c device\n\r");
    UART_PRINT("\n\r");
    UART_PRINT("Parameters \n\r");
    UART_PRINT("---------- \n\r");
    UART_PRINT("dev_addr - slave address of the i2c device, a hex value "
                "preceeded by '0x'\n\r");
    UART_PRINT("reg_offset - register address in the i2c device, a hex value "
                "preceeded by '0x'\n\r");
    UART_PRINT("wrlen - number of bytes to be written, a decimal value \n\r");
    UART_PRINT("rdlen - number of bytes to be read, a decimal value \n\r");
    UART_PRINT("bytex - value of the data to be written, a hex value preceeded "
                "by '0x'\n\r");
    UART_PRINT("stop - number of stop bits, 0 or 1\n\r");
    UART_PRINT("--------------------------------------------------------------"
                "--------------- \n\r\n\r");
}

//=============================================================================
//
//! Display the buffer contents over I2C
//!
//! \param  pucDataBuf is the pointer to the data store to be displayed
//! \param  ucLen is the length of the data to be displayed
//!
//! \return none
//! 
//=============================================================================

void DisplayBuffer(unsigned char *pucDataBuf, unsigned char ucLen) {
    unsigned char ucBufIndx = 0;
    UART_PRINT("Read contents");
    UART_PRINT("\n\r");
    while(ucBufIndx < ucLen)
    {
        UART_PRINT(" 0x%x, ", pucDataBuf[ucBufIndx]);
        ucBufIndx++;
        if((ucBufIndx % 8) == 0)
        {
            UART_PRINT("\n\r");
        }
    }
    UART_PRINT("\n\r");
}

//=============================================================================
//                  Application startup display on UART
//                   Parameters:  None, Returns: None
//
//=============================================================================

static void
DisplayBanner(char * AppName)
{

    Report("\n\n\n\r");
    Report("\t\t *************************************************\n\r");
    Report("\t\t      CC3200 %s Application       \n\r", AppName);
    Report("\t\t *************************************************\n\r");
    Report("\n\n\n\r");
}

//=============================================================================
//
//! Parses the read command parameters and invokes the I2C APIs
//!
//! \param pcInpString pointer to the user command parameters
//! 
//! This function  
//!    1. Parses the read command parameters.
//!    2. Invokes the corresponding I2C APIs
//!
//! \return 0: Success, < 0: Failure.
//
//=============================================================================
int ProcessReadCommand(char *pcInpString) {
    unsigned char ucDevAddr, ucLen;
    unsigned char aucDataBuf[256];
    char *pcErrPtr;
    int iRetVal;

    // Get the device address
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucDevAddr = (unsigned char)strtoul(pcInpString+2, &pcErrPtr, 16);

    // Get the length of data to be read
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucLen = (unsigned char)strtoul(pcInpString, &pcErrPtr, 10);
    //RETERR_IF_TRUE(ucLen > sizeof(aucDataBuf));
    
    // Read the specified length of data
    iRetVal = I2C_IF_Read(ucDevAddr, aucDataBuf, ucLen);

    if(iRetVal == SUCCESS)
    {
        UART_PRINT("I2C Read complete\n\r");
        
        // Display the buffer over UART on successful write
        DisplayBuffer(aucDataBuf, ucLen);
    }
    else
    {
        UART_PRINT("I2C Read failed\n\r");
        return FAILURE;
    }

    return SUCCESS;
}

//=============================================================================
//
//! Parses the readreg command parameters and invokes the I2C APIs
//! i2c readreg 0x<dev_addr> 0x<reg_offset> <rdlen>
//!
//! \param pcInpString pointer to the readreg command parameters
//! 
//! This function  
//!    1. Parses the readreg command parameters.
//!    2. Invokes the corresponding I2C APIs
//!
//! \return 0: Success, < 0: Failure.
//
//=============================================================================
int ProcessReadRegCommand(char *pcInpString) {
    unsigned char ucDevAddr, ucRegOffset, ucRdLen;
    unsigned char aucRdDataBuf[256];
    char *pcErrPtr;

    // Get the device address
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucDevAddr = (unsigned char)strtoul(pcInpString+2, &pcErrPtr, 16);

    // Get the register offset address
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucRegOffset = (unsigned char)strtoul(pcInpString+2, &pcErrPtr, 16);

    // Get the length of data to be read
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucRdLen = (unsigned char)strtoul(pcInpString, &pcErrPtr, 10);
    //RETERR_IF_TRUE(ucLen > sizeof(aucDataBuf));

    // Write the register address to be read from.
    // Stop bit implicitly assumed to be 0.
    RET_IF_ERR(I2C_IF_Write(ucDevAddr,&ucRegOffset,1,0));
    
    // Read the specified length of data
    RET_IF_ERR(I2C_IF_Read(ucDevAddr, &aucRdDataBuf[0], ucRdLen));

    UART_PRINT("I2C Read From address complete\n\r");
    
    DisplayBuffer(aucRdDataBuf, ucRdLen); // Display the buffer over UART on successful readreg

    return SUCCESS;
}

//=============================================================================
//
//! Parses the writereg command parameters and invokes the I2C APIs
//! i2c writereg 0x<dev_addr> 0x<reg_offset> <wrlen> <0x<byte0> [0x<byte1> ...]>
//!
//! \param pcInpString pointer to the readreg command parameters
//! 
//! This function  
//!    1. Parses the writereg command parameters.
//!    2. Invokes the corresponding I2C APIs
//!
//! \return 0: Success, < 0: Failure.
//
//=============================================================================
int ProcessWriteRegCommand(char *pcInpString) {
    unsigned char ucDevAddr, ucRegOffset, ucWrLen;
    unsigned char aucDataBuf[256];
    char *pcErrPtr;
    int iLoopCnt = 0;

    // Get the device address
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucDevAddr = (unsigned char)strtoul(pcInpString+2, &pcErrPtr, 16);
    
    // Get the register offset to be written
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucRegOffset = (unsigned char)strtoul(pcInpString+2, &pcErrPtr, 16);
    aucDataBuf[iLoopCnt] = ucRegOffset;
    iLoopCnt++;
    
    // Get the length of data to be written
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucWrLen = (unsigned char)strtoul(pcInpString, &pcErrPtr, 10);
    //RETERR_IF_TRUE(ucWrLen > sizeof(aucDataBuf));
   
    // Get the bytes to be written
    for(; iLoopCnt < ucWrLen + 1; iLoopCnt++) {
        // Store the data to be written
        pcInpString = strtok(NULL, " ");
        RETERR_IF_TRUE(pcInpString == NULL);
        aucDataBuf[iLoopCnt] = 
                (unsigned char)strtoul(pcInpString+2, &pcErrPtr, 16);
    }

    // Write the data values.
    RET_IF_ERR(I2C_IF_Write(ucDevAddr,&aucDataBuf[0],ucWrLen+1,1));

    UART_PRINT("I2C Write To address complete\n\r");

    return SUCCESS;
}

//=============================================================================
//
//! Parses the write command parameters and invokes the I2C APIs
//!
//! \param pcInpString pointer to the write command parameters
//! 
//! This function  
//!    1. Parses the write command parameters.
//!    2. Invokes the corresponding I2C APIs
//!
//! \return 0: Success, < 0: Failure.
//
//=============================================================================

int ProcessWriteCommand(char *pcInpString) {
    unsigned char ucDevAddr, ucStopBit, ucLen;
    unsigned char aucDataBuf[256];
    char *pcErrPtr;
    int iRetVal, iLoopCnt;

    // Get the device address
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucDevAddr = (unsigned char)strtoul(pcInpString+2, &pcErrPtr, 16);
    
    // Get the length of data to be written
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucLen = (unsigned char)strtoul(pcInpString, &pcErrPtr, 10);
    //RETERR_IF_TRUE(ucLen > sizeof(aucDataBuf));

    for(iLoopCnt = 0; iLoopCnt < ucLen; iLoopCnt++) {
        // Store the data to be written
        pcInpString = strtok(NULL, " ");
        RETERR_IF_TRUE(pcInpString == NULL);
        aucDataBuf[iLoopCnt] = 
                (unsigned char)strtoul(pcInpString+2, &pcErrPtr, 16);
    }
    
    // Get the stop bit
    pcInpString = strtok(NULL, " ");
    RETERR_IF_TRUE(pcInpString == NULL);
    ucStopBit = (unsigned char)strtoul(pcInpString, &pcErrPtr, 10);
    
    // Write the data to the specified address
    iRetVal = I2C_IF_Write(ucDevAddr, aucDataBuf, ucLen, ucStopBit);
    if(iRetVal == SUCCESS) {
        UART_PRINT("I2C Write complete\n\r");
    } else {
        UART_PRINT("I2C Write failed\n\r");
        return FAILURE; // 'FAILURE' is defined @ top of this file
    }

    return SUCCESS;    // 'SUCCESS' is defined @ top of this file
}

/*
==============================================================================================
                                PARSE INPUT USER COMMAND

Parameters: pcCmdBuffer pointer to the user command
Return:     0: Success, < 0: Failure. // #define FAILURE -1
									  // 'FAILURE' is defined @ top of this file

This function:
    1. Parses the user command.
    2. Invokes the corresponding I2C APIs
==============================================================================================
*/

int ParseNProcessCmd(char *pcCmdBuffer) {
    char *pcInpString;
    int iRetVal = FAILURE;

    pcInpString = strtok(pcCmdBuffer, " \n\r");
    if(pcInpString != NULL) {
        if(!strcmp(pcInpString, "read")) {
            iRetVal = ProcessReadCommand(pcInpString);     // Invoke read command handler
        } else if(!strcmp(pcInpString, "readreg")) {
            iRetVal = ProcessReadRegCommand(pcInpString);  // Invoke readreg command handler
        } else if(!strcmp(pcInpString, "writereg")) {
            iRetVal = ProcessWriteRegCommand(pcInpString); // Invoke writereg command handler
        } else if(!strcmp(pcInpString, "write")) {
            iRetVal = ProcessWriteCommand(pcInpString);    // Invoke write command handler.
        } else {
            UART_PRINT("Unsupported command\n\r");
            return FAILURE;
        }
    } // end if(pcInpString != NULL)
    return iRetVal;
}

/*
==============================================================================================
                        BOARD INITIALIZATION & CONFIGURATION.
                           Parameters:  None, Returns: None

==============================================================================================
*/
static void BoardInit(void) {
#ifndef USE_TIRTOS // In case of TI-RTOS vector table is initialize by OS itself
				   // a.k.a. "If 'USE_TIRTOS' is NOT defined... then ..
    // Set vector table base ... for 'interrupt vector table'
	/* From pg. 47/521 in CC3200 Technical Reference Manual:
	 * On system reset, the vector table is fixed at address 0x0000.0000. Privileged software can write to the
	 * Vector Table Offset (VTABLE) register to relocate the vector table start address to a different memory
	 * location, in the range 0x0000.0400 to 0x3FFF.FC00. Note that when configuring the VTABLE register, the
	 * offset must be aligned on a 1024-byte boundary.
	 */

#if defined(ccs) // ??
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);
    							  // Sets the NVIC VTable base.
    							  // Specifies base address for VTable.
    							  // This function must be called before using
    							  // IntRegister() for registering any interrupt handler.
    							  //
    							  // 'IntVTableBaseSet' method in .../driverlib/interrupt.c
    							  //
    							  // 'NVIC_VTABLE' defined in ../inc/hw_nvic.h
#endif
#if defined(ewarm)
    MAP_IntVTableBaseSet((unsigned long)&__vector_table);
								  // Sets the NVIC VTable base.
								  // Specifies base address for VTable.
								  // This function must be called before using
								  // IntRegister() for registering any interrupt handler.
	  	  	  	  	  	  	  	  // 'IntVTableBaseSet' method in .../driverlib/interrupt.c
#endif
#endif

    // Enable Processor
    MAP_IntMasterEnable();        // 'IntMasterEnable' method found in .../driverlib/interrupt.c
    							  // This function enables processor to respond to interrupts.
    							  // ***Does not affect interrupts in interrupt controller.***
    							  // 'IntMasterDisable()' (or 'MAP_IntMasterDisable()' ??)
    							  // DISABLE/restricts the processor to respond to interrupts.

    MAP_IntEnable(FAULT_SYSTICK); // Enable the System Tick interrupt.
    							  // 'IntEnable' method found in .../driverlib/interrupt.c
    							  // enables an interrupt in the interrupt controller ..
    							  // peripheral interrupts are unaffected with this function.
    							  // references 'hw_nvic.h' file as well.

    PRCMCC3200MCUInit();          // .../driverlib/prcm.c
}

/*
==============================================================================================
                                          MAIN
                             Parameters:  None, Returns: None

==============================================================================================
*/
void main() {
    int iRetVal;
    char acCmdStore[512];
    
    BoardInit();                      // Method located immediately above ^^.
    								  // Initialize things for interrupts.
    								  // Initialize PRCM module in CC3200.
    PinMuxConfig();                   // Configure the pinmux settings for the peripherals exercised ..
    								  // Method located in pinmux.c file.
    InitTerm();                       // Configuring UART Configuration
    								  // Method located in uart.c file.
    								  // .../example/common/uart_if.h // *.h also relevant.
    I2C_IF_Open(I2C_MASTER_MODE_FST); // I2C Initialization.  Enables and configures I2C.
    								  // Method located in i2c_if.c file
    								  // 'I2C_MASTER_MODE_FST'

    DisplayBanner(APP_NAME);          // Display the banner followed by the usage description
    DisplayUsage();                   // Displays more stuff.. variables, I guess ?? .. addresses??

    while(FOREVER) {
      DisplayPrompt();                // Provide a prompt for the user to enter a command
      	  	  	  	  	  	  	  	  // .. "\n\rcmd# " ... @ top of this file.
      iRetVal = GetCmd(acCmdStore, sizeof(acCmdStore));
      	  	  	  	  	  	  	  	  // Get the user command line
      	  	  	  	  	  	  	  	  // 'GetCmd' method located in uart_if.c file
      if(iRetVal < 0) {
          UART_PRINT("Command length exceeded 512 bytes \n\r"); // Error in parsing the command as length is exceeded.
          DisplayUsage();
      } else if(iRetVal == 0) {
          // No input. Just an enter pressed probably. Display a prompt.
      } else {
          iRetVal = ParseNProcessCmd(acCmdStore); // Parse the user command and try to process it.
          if(iRetVal < 0)
          {
              UART_PRINT("Error in processing command\n\r");
              DisplayUsage();
          } // end IF block
      } // end IF/ELSE IF/ELSE block
    } // end of while loop
} // end of main method.
